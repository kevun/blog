#
# This file installs the following Lektor plugins required for this project
# and their dependencies:
#
# lektor-slugify==0.4
# lektor-index-pages==1.0
# lektor-markdown-highlighter==0.3.1
# lektor-limit-dependencies==1.0.0b1
# lektor-markdown-image-attrs==0.2.1
#
lektor-index-pages==1.0 \
    --hash=sha256:3b8d027d60159aab51fd9f0e4a82797f9ca7c210103be642aa6a2ddfe9691df6 \
    --hash=sha256:4c7a0ee651a747dc5e0f7626f547b70833b5f99672fc57934b45aad7943c5264
lektor-limit-dependencies==1.0.0b1 \
    --hash=sha256:979f747c2f9193935b7a522a75669a742931f8dcd0046436558249cd89d06a02 \
    --hash=sha256:ed559948b284a61f5ce64c2e045a710c3d9832dfc97818b5d94c7dfd280e65b1
lektor-markdown-highlighter==0.3.1 \
    --hash=sha256:ce257b608a2094540702209f99e28b546581777a67882cebc8e8dabb6429422e \
    --hash=sha256:e9fe5f7bd11d7a072e262ca094d25edef7eeeba5d81d8b37518fa391d0a6555f
lektor-markdown-image-attrs==0.2.1 \
    --hash=sha256:141e127d7c210c1305eb7880b9267160c807919f10bc7a73056bc58c791dd3cc \
    --hash=sha256:e76a191aa9da92716a6bc695b1aecde296e9c4656254cf5dc105208b31fc5fd5
lektor-slugify==0.4 \
    --hash=sha256:01f3cd83880755471bc0a4bdda6f107b9652f2ead2490fc4cac62b7ee8a33448 \
    --hash=sha256:df075b644f0744a583d84d9ea25b3b29bb86e3368350011a564150d59380f928
lektorlib==1.0.0 \
    --hash=sha256:8156a0e49f87a9cc5c4ff9959e13979f7d2747be7d36b2427a105cd0780b3aad \
    --hash=sha256:ac107b4e8a29e6ed742a233e04e177e8e58a0e582663c48de18702faf857c245
more-itertools==9.0.0 \
    --hash=sha256:250e83d7e81d0c87ca6bd942e6aeab8cc9daa6096d12c5308f3f92fa5e5c1f41 \
    --hash=sha256:5a6257e40878ef0520b1803990e3e22303a41b5714006c32a3fd8304b26ea1ab
pygments==2.13.0 \
    --hash=sha256:56a8508ae95f98e2b9bdf93a6be5ae3f7d8af858b43e02c5a2ff083726be40c1 \
    --hash=sha256:f643f331ab57ba3c9d89212ee4a2dabc6e94f117cf4eefde99a0574720d14c42
