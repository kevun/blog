title: DEFCON Toronto Meetup
---
author: steph
---
start_date: 2018-09-20
---
body:

Schedule:  

6:00pm - 9:00pm (Technical Talks)

Talks & Presenters:

Talk #1:  

Tor: Internet privacy in the age of big surveillance  

Tor is a free-software anonymizing network that helps people around the  

world use the Internet in safety. Tor's 8000 volunteer relays carry  

traffic for millions of daily users, including ordinary citizens who  

want protection from identity theft and prying corporations, corporations  

who want to look at a competitor's website in private, people around the  

world whose Internet connections are censored, and even governments and  

law enforcement.

In this talk I'll take you on a tour of the Tor landscape, starting  

with a crash course on Tor, how it works, and what security it provides.  

I'll explain why Tor's open design and radical approach to transparency  

are critical to its success, and then compare the censorship circumvention  

arms race to the nation-state surveillance arms race. We'll end with  

a discussion of onion services, which are essentially an even stronger  

version of https, but which you might instead know from confusing phrases  

like "the dark web".

Roger Dingledine is president and co-founder of the Tor Project, a  

nonprofit that develops free and open source software to protect people  

from tracking, censorship, and surveillance online. Wearing one hat,  

Roger works with journalists and activists on many continents to help them  

understand and defend against the threats they face. Wearing another,  

he is a lead researcher in the online anonymity field, coordinating  

and mentoring academic researchers working on Tor-related topics. Since  

2002 he has helped organize the yearly international Privacy Enhancing  

Technologies Symposium (PETS). Among his achievements, Roger was chosen  

by the MIT Technology Review as one of its top 35 innovators under 35,  

he co-authored the Tor design paper that won the Usenix Security "Test  

of Time" award, and he has been recognized by Foreign Policy magazine  

as one of its top 100 global thinkers.

Talk #2:  

Building Machine Learning at Scale to Detect Post-exploitation Attacks  

In this talk ROY Firestein will talk about how his team deployed a machine-learning pipeline, with feedback loops, on AWS to detect post-exploitation attacks using logs from Active Directory and endpoint agents. He will share the architectural decisions and walk us through the implementation, deployment automation and tools used in the project. By the end attendees will learn how to approach similar projects in their own companies, when to use hosted machine-learning tools or run your own, and common pitfalls to avoid.

Roy Firestein, CPO at Cycura, is a seasoned hacker and expert in cyber security, business development and project management. He has a background in security, research management, marketing, sales and is a frequent speaker in security conferences. Roy's passion lies in Big Data and Machine Learning, especially when applied to cyber security.

Want to learn more about DEFCON Toronto?

Visit our Website ([http://dc416.com](http://dc416.com "http://dc416.com"))

Join our Facebook group ([https://www.facebook.com/groups/DC647/](https://www.facebook.com/groups/DC647/ "https://www.facebook.com/groups/DC647/"))

Join the conversation on Twitter! Share and follow along with @defcon\_toronto  

([https://twitter.com/defcon\_toronto](https://twitter.com/defcon_toronto "https://twitter.com/defcon_toronto"))

