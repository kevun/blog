title: PrivChat with Tor - "The Good, the Bad, and the Ugly of Censorship Circumvention" (live stream)
---
author: antonela
---
start_date: 2020-08-28
---
summary:

This edition topic: Censorship Circumvention in today’s context



---
body:

**August 28, Friday, 10:00 AM Pacific Time ∙ 17:00 UTC ∙ 13:00 Eastern Time | [@thetorproject](https://youtu.be/gSyDvG4Z308) YouTube channel.**

*PrivChat: a conversation about tech, human rights, and internet freedom brought to you by the Tor Project. The topic for this edition: the Good, the Bad, and the Ugly of Censorship Circumvention.*

Every year, internet censorship increases globally. From network level blocking to nation-wide internet blackouts, governments and private companies have powerful tools to restrict information and hault connection between people. Many people, groups, and organizations are doing innovative work to study, measure, and fight back against internet censorship--and they are helping millions of people connect more regularly and safely to the internet. Despite these successes, we're faced with well-funded adversaries that have billions of dollars to spend on censorship mechanisms, and the arms race is ongoing. The second edition of PrivChat with Tor will be about the Good, the Bad and the Ugly that is happening in the front lines of censorship circumvention. In a world where censorship technology is increasingly sophisticated and bought and sold between nations, so is our creativity to measure it and build tools to bypass it, as well as the willingness of people to fight back. But is it enough? What are the barriers facing the people and organizations fighting for internet freedom?

#### * Felicia Anthonio

#### * Vrinda Bhandari

#### * Cecylia Bocovich

#### * Arturo Filastò

*** Cory Doctorow**



**About PrivChat** 

PrivChat is a fundraising event series held to raise donations for the Tor Project. Our goal is to bring you important information related to what is happening in tech, human rights, and internet freedom by convening experts for a chat with our community. PrivChat events are free to attend. If you find value in this content and Tor, please consider becoming a monthly donor: <https://donate.torproject.org/monthly-giving>



**About the Tor Project** 

The Tor Project is a US 501(c)(3) non-profit organization advancing human rights and freedoms by creating and deploying free and open source anonymity and privacy technologies, supporting their unrestricted availability and use, and furthering their scientific and popular understanding.

**More information: <https://torproject.org/privchat/>**

Website:
[YouTube channel](https://youtu.be/aOOChyMCZH4)
---
tags:

privacy
video
