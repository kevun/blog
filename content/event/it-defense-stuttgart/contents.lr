title: IT Defense (Stuttgart)
---
author: steph
---
start_date: 2019-02-06
---
end_date: 2019-02-08
---
body:

**Tor: Internet privacy in the age of big surveillance**  

Roger Dingledine

Tor is a free-software anonymizing network that helps people around the world use the Internet in safety. Tor's 8000 volunteer relays carry traffic for millions of daily users, including ordinary citizens who want protection from identity theft and prying corporations, corporations who want to look at a competitor's website in private, people around the world whose Internet connections are censored, and even governments and law enforcement.

In this talk I'll take you on a tour of the Tor landscape, starting with a crash course on Tor, how it works, and what security it provides. I'll explain why Tor's open design and radical approach to transparency are critical to its success, and then compare the censorship circumvention arms race to the nation-state surveillance arms race. We'll end with a discussion of onion services, which are essentially an even stronger version of https, but which you might instead know from confusing phrases like "the dark web".

Roger will also be part of a Roundtable on Friday.

Website:
[Agenda ](https://www.it-defense.de/en/it-defense-2019/program/agenda)
