title: Q&A with The Tor’s Circumvention Lead About Snowflake (IFF - CKS)
---
author: ggus
---
start_date: 2021-03-30
---
body:

Every week, the Team CommUNITY hosts Community Knowledge Share (CKS), online workshops and interactive conversations led by the Internet Freedom community.

**Q&A with The Tor’s Circumvention Lead About Snowflake**

    Who: Cecylia, Anti-Censorship Team Lead & Gus, Community Team Lead  

    Date: Tuesday, March 30th  

    Time: 12:00pm EDT / 4:00pm UTC (other times below)  

    Language: ِ English

Come meet members of the Tor Team, including their Anti-Censorship Lead, Cecylia, as they share the new pluggable transport for the Tor Browser called Snowflake. This development is great news, as it improves user’s ability to circumvent censorship, even in places where regular Tor connections are censored.

Join us and find out how Snowflake works, how it it's different than Tor Bridges, how this strengthens our ability to defeat censorship, and how you can help. The Tor team is also hoping to get feedback from the community, so come prepared with feedback and questions.

Snowflake uses the highly effective domain fronting technique to make a connection to one of the thousands of snowflake proxies run by volunteers. These proxies are lightweight, ephemeral, and easy to run, allowing us to scale Snowflake more easily than previous techniques.

// We will be hosting a 25 minute post-workshop networking exercise to allow folks to meet others who share their interest, and strengthen collaborations across various lines. Make sure to schedule in 25 minutes extra on your calendar, if you are interested in joining

Cecylia: Lead and developer on the anti-censorship team. Works on pluggable transport development and integration, and BridgeDB.

Gus: Works as Community Team Lead at the Tor Project.

-----

What is the time in my city? 12:00pm EDT/NYC is ....

Mexico City: 10:00am UTC-6  

London: 5:00pm UTC+1  

Amsterdam: 6:00pm UTC+2  

Kampala: 7:00pm UTC+3  

Delhi: 9:30pm UTC+5:30  

Jakarta: 11:00pm UTC+7  

Taipei: 12:00am UTC+8 (Wed, March 21)

---
tags:

anti-censorship
censorship circumvention
