title: Tor descriptors à la carte: Tor Metrics Library 2
---
pub_date: 2017-06-29
---
author: karsten
---
tags: metrics
---
categories: metrics
---
_html_body:

<p></p><center> </center>
<p>We're often asked by researchers, users, and journalists for Tor network data. How can you find out how many people use the Tor network daily? How many relays make up the network? How many times has Tor Browser been downloaded in your language? In order to get to these answers from archived data, we have to continuously fetch, parse, and evaluate Tor descriptors. We do this with the <a href="https://metrics.torproject.org/metrics-lib.html">Tor Metrics Library</a>.</p>
<p>Today, the <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/MetricsTeam">Tor Metrics Team</a> is proud to announce major improvements and launch Tor Metrics Library version 2.0.0. These improvements, supported by a <a href="https://blog.mozilla.org/blog/2016/06/22/mozilla-awards-385000-to-open-source-projects-as-part-of-moss-mission-partners-program/ ">Mozilla Open Source Support (MOSS) “Mission Partners” award</a>, enhance our ability to monitor the performance and stability of the Tor network.</p>
<h3>From internal tool to public resource </h3>
<p>Originally, the library was an internal tool. We used it to fetch the latest descriptors archived by <a href="https://collector.torproject.org/ ">CollecTor</a> in all Java-based codebases and to parse descriptors that had been published by Tor relays, bridges, directory authorities, and other parts of the public Tor network.</p>
<p>Over the years, we've added more data sources and made it into a publicly-available resource. Our data has been used in many <a href="https://gitweb.torproject.org/metrics-tasks.git/">ad-hoc analyses</a>, as well as in <a href="https://atlas.torproject.org/">Atlas</a>, <a href="https://exonerator.torproject.org/">ExoneraTor</a>, and <a href="https://metrics.torproject.org/">Tor Metrics</a>.</p>
<h3><b>Better</b><b> memory-efficiency</b><b>, fewer bugs </b></h3>
<p>This launch adds numerous improvements, from interface simplifications over memory-efficiency improvements to added support for newly-added descriptor parts and, last but not least, bugfixes. You can check out the <a href="https://gitweb.torproject.org/metrics-lib.git/tree/CHANGELOG.md?id=metrics-lib-2.0.0">change log</a> for a complete overview.</p>
<p>A few months ago, the library found a home on the recently-reorganized <a href="https://metrics.torproject.org/metrics-lib.html">Tor Metrics website</a>. Here you'll find tutorials for getting started with the library by downloading descriptors from CollecTor and performing two simple analyses to determine the current relay capacity by Tor version and frequency of bridge transports. The project page also contains links to <a href="https://dist.torproject.org/metrics-lib/?C=M;O=D">all releases</a>, the <a href="https://gitweb.torproject.org/metrics-lib.git/plain/CHANGELOG.md ">full change log</a>, and the <a href="https://metrics.torproject.org/metrics-lib/">latest JavaDocs</a>.</p>
<h3>You can be a part of what's next </h3>
<p class="text-align-center"><em>“Tor metrics are the ammunition that lets Tor and other security advocates argue for a more private and secure Internet from a position of data, rather than just dogma or perspective.” <br />
—Bruce Schneier (June 1, 2016)</em></p>
<p>As always, if you're a developer doing something cool with <a href="https://metrics.torproject.org/sources.html">Tor network data</a>, please <a href="https://metrics.torproject.org/about.html#contact">let us know</a> what features you're finding valuable so we continue to support those. And, if we think other people could learn from your project, we could feature it on the Tor Metrics website.</p>
<p>Happy à la carte descriptor collecting, reading, and parsing with Tor Metrics Library 2. Bon appétit!</p>

---
_comments:

<a id="comment-269519"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269519" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 30, 2017</p>
    </div>
    <a href="#comment-269519">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269519" class="permalink" rel="bookmark">How many times has Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>How many times has Tor Browser been downloaded in your country?</p></blockquote>
<p>Can one really fetch nb of TB downloads <b>per country</b>? If true that would be really awesome!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-269521"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269521" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  karsten
  </article>
    <div class="comment-header">
      <p class="comment__submitted">karsten said:</p>
      <p class="date-time">June 30, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-269519" class="permalink" rel="bookmark">How many times has Tor…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-269521">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269521" class="permalink" rel="bookmark">Uhm, no, that&#039;s not possible…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Uhm, no, that's not possible. We do have numbers on Tor Browser downloads by locale, which is related but not the same. Looks like this sentence slipped in in one of the edits and nobody noticed. Oops. Changed to say "How many times has Tor Browser been downloaded in your language?" which is probably easier to understand than locale. Thanks for pointing this out!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-269560"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269560" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 02, 2017</p>
    </div>
    <a href="#comment-269560">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269560" class="permalink" rel="bookmark">&quot;You&#039;re a data person and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><em>"You're a data person and only trust the statistics that you doctored yourself? Here's all the data right from the source, doctor." </em></p>
<p>Hahahaha... What are the differences of the same data in the eyes of "a data person" and "a non-data person"? Here the illustration:<br />
+) Data in the eyes of a non-data person: :[[... B@$$ @$$!... #$^^&amp;&amp;*%$^&amp;...<br />
+) For a data person: ^.^... $$... $$$$... $$$$$$... $$$$$$$$$$$$$$$$$$$... xD</p>
</div>
  </div>
</article>
<!-- Comment END -->
