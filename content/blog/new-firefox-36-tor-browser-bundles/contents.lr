title: New Firefox 3.6 Tor Browser Bundles
---
pub_date: 2011-08-21
---
author: rransom
---
tags:

tor browser bundle
security fixes
tor browser
firefox updates
package updates
---
categories:

applications
releases
---
_html_body:

<p>The stable Tor Browser Bundle for Windows and the beta Firefox 3.6 Tor Browser Bundles for Linux and OS X have been updated to Firefox 3.6.20.</p>

<p>We are currently building new versions of the experimental Tor Browser Bundles, this time containing Firefox 6; we will announce them when they are ready.  Firefox 5 and the experimental Firefox 5 TBBs are no longer safe to use.</p>

<p><a href="https://www.torproject.org/download/download" rel="nofollow">https://www.torproject.org/download/download</a></p>

<p><strong>Windows bundle</strong><br />
<strong>1.3.27: Released 2011-08-19</strong></p>

<ul>
<li>Update Firefox to 3.6.20</li>
<li>Update Libevent to 2.0.13-stable</li>
<li>Update HTTPS-Everywhere to 1.0.0development.5</li>
</ul>

<p><strong>OS X bundle</strong><br />
<strong>1.0.23: Released 2011-08-19</strong></p>

<ul>
<li>Update Tor to 0.2.2.31-rc</li>
<li>Update Firefox to 3.6.20</li>
<li>Update Libevent to 2.0.13-stable</li>
<li>Update NoScript to 2.1.2.6</li>
<li>Update HTTPS-Everywhere to 1.0.0development.5</li>
<li>Remove BetterPrivacy until we can figure out how to make it safe in all bundles (see <a href="https://trac.torproject.org/projects/tor/ticket/3597" rel="nofollow">#3597</a>)</li>
</ul>

<p><strong>Linux bundle</strong><br />
<strong>1.1.13: Released 2011-08-19</strong></p>

<ul>
<li>Update Tor to 0.2.2.31-rc</li>
<li>Update Firefox to 3.6.20</li>
<li>Update Libevent to 2.0.13-stable</li>
<li>Update NoScript to 2.1.2.6</li>
<li>Update HTTPS-Everywhere to 1.0.0development.5</li>
<li>Remove BetterPrivacy until we can figure out how to make it safe in all bundles (see <a href="https://trac.torproject.org/projects/tor/ticket/3597" rel="nofollow">#3597</a>)</li>
</ul>

