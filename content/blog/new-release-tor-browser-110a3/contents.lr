title: New Release: Tor Browser 11.0a3 (Android Only)
---
pub_date: 2021-08-06
---
author: sysrqb
---
tags:

tor browser
tbb-11.0
tbb
---
categories: applications
---
summary: Tor Browser 11.0a3 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 11.0a3 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/11.0a3/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-1053">latest stable release</a> instead.</p>
<p>This version updates Fenix to 91.0.0-beta.5.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser Alpha does <em><b>not</b> support <u>version 2 onion services</u></em>. Tor Browser (Stable) will <em><b>stop</b> supporting <u>version 2 onion services</u></em> later <em><b>this year</b></em>. Please see the <a href="https://support.torproject.org/onionservices/v2-deprecation/">deprecation F.A.Q.</a> entry regarding Tor version 0.4.6. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master"> Tor Browser 11.0a2:</a></p>
<ul>
<li>Android
<ul>
<li>Update NoScript to 11.2.11</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40176">Bug 40176</a>: TBA: sometimes I only see the banner and can't tap on the address bar</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40185">Bug 40185</a>: Use NimbusDisabled</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40181">Bug 40181</a>: Remove V2 Deprecation banner on about:tor for Android</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40184">Bug 40184</a>: Rebase fenix patches to fenix v91.0.0-beta.5</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40063">Bug 40063</a>: Move custom search providers</li>
</ul>
</li>
<li>Build System
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40331">Bug 40331</a>: Update components for mozilla91</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292532"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292532" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 09, 2021</p>
    </div>
    <a href="#comment-292532">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292532" class="permalink" rel="bookmark">What really matters and how…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What really matters and how to measure it properly (true for tor too):<br />
<a href="https://blog.codavel.com/the-tail-shows-the-truth-wireless-latency-sucks-globally" rel="nofollow">https://blog.codavel.com/the-tail-shows-the-truth-wireless-latency-suck…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292543"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292543" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 10, 2021</p>
    </div>
    <a href="#comment-292543">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292543" class="permalink" rel="bookmark">Guardian project f-droid is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Guardian project f-droid is still on 10.0.18 stable and 10.5a17 alpha. Last published update was on June 20. Guardian project is slow. Tor project doesnt run a repo. TB4A not in f-droid.org official repo. No internal updater or out-of-date warning banner in TB4A. I'm not assigning blame to anyone but this situation is putting a serious portion of android users at risk.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292577"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292577" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>LostUser (not verified)</span> said:</p>
      <p class="date-time">August 13, 2021</p>
    </div>
    <a href="#comment-292577">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292577" class="permalink" rel="bookmark">I have issues with downloads…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have issues with downloads, whatever i try i get download failed. I turned off firewall and allowed every permissions. Any ideas what could cause the issue?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292595"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292595" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292577" class="permalink" rel="bookmark">I have issues with downloads…</a> by <span>LostUser (not verified)</span></p>
    <a href="#comment-292595">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292595" class="permalink" rel="bookmark">&gt; issues with downloads
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; issues with downloads</p>
<p><a href="https://blog.torproject.org/comment/292387#comment-292387" rel="nofollow">https://blog.torproject.org/comment/292387#comment-292387</a><br />
"Can you provide an example site where the download is failing? Is it for specific file formats?"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292693"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292693" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 27, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292577" class="permalink" rel="bookmark">I have issues with downloads…</a> by <span>LostUser (not verified)</span></p>
    <a href="#comment-292693">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292693" class="permalink" rel="bookmark">Make sure your device has…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Make sure your device has free space in the user partition (i.e. sdcard partition). I know it sounds way too obvious, but it's gotten me several times.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
