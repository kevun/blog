title: August 2009 Progress Report
---
pub_date: 2009-09-21
---
author: phobos
---
tags:

progress report
anonymity advocacy
bug fixes
releases
---
categories:

advocacy
releases
reports
---
_html_body:

<p><strong>New releases</strong></p>

<p>On August 4, we released Tor Browser Bundle 1.2.7. It is updated primarily due to Firefox 3.0.13 with its ssl fixes.</p>

<p>The full changelist is:<br />
1.2.7: Released 2009-08-04</p>

<ul>
<li>update Firefox to 3.0.13 </li>
<li>add Polish translation </li>
<li>update libevent to 1.4.12 </li>
</ul>

<p>On August 19, we released Tor Browser Bundle 1.2.8.  The big changes are the inclusion of statically linked openssl dlls to resolve a few geoip lookup and functionality issues with Vidalia, and the upgrade to the new Vidalia 0.2.2. </p>

<p>The full list of updates and fixes:</p>

<ul>
<li>update Torbutton to 1.2.2 </li>
<li>update Vidalia to 0.2.2 </li>
<li>compile OpenSSL 0.9.8k with Visual C to make dlls </li>
<li>update Pidgin to 2.6.1 </li>
</ul>

<p>On August 3rd, we release Vidalia 0.2.1.  This is a major change in the way OS X and Windows bundles are installed, as well as many usability enhancements.  This also sets the stage for a plugin-API being developed over the next few months.  </p>

<p>The changes are:
</p>

<ul>
<li>Add a "Find Bridges Now" button that will attempt to automatically<br />
download a set of bridge addresses and add them to the list of bridges<br />
in the Network settings page. </li>
<li>Add support for building with Google's Breakpad crash reporting<br />
library (currently disabled by default). </li>
<li>Show or hide the "Who has used my bridge recently?" link along with<br />
the other bridge-related widgets when the user toggles the relay mode<br />
in the Network settings page. (Ticket #480) </li>
<li>Tolerate bridge addresses that do not specify a port number, since Tor<br />
now defaults to using port 443 in such cases. </li>
<li>Add support for viewing the map as a full screen widget when built<br />
with KDE Marble support. </li>
<li>Compute the salted hash of the control password ourself when starting<br />
Tor, rather than launching Tor once to hash the password, parsing the<br />
output, and then again to actually start Tor. </li>
<li>Add a signal handler that allows Vidalia to clean up and exit normally<br />
when it catches a SIGINT or SIGTERM signal. (Ticket #481)</li>
<li>If the user chooses to ignore further warnings for a particular port,<br />
remove it from the WarnPlaintextPorts and RejectPlaintextPorts<br />
settings immediately. Also remember their preferences and reapply them<br />
later, even if Tor is unable to writes to its torrc.(Ticket #493) </li>
<li>Don't display additional plaintext port warning message boxes until<br />
the first visible message box is dismissed. (Ticket #493) </li>
<li>Renamed the 'make win32-installer' CMake target to 'make dist-win32'<br />
for consistency with our 'make dist-osx' target. </li>
<li>Fix a couple bugs in the WiX-based Windows installer related to building<br />
a Marble-enabled Vidalia installer. </li>
<li>Write the list of source files containing translatable strings to a<br />
.pro file and supply just the .pro file as an argument to lupdate, rather<br />
than supplying all of the source file names themselves.</li>
</ul>

<p>On August 14th, we release Vidalia 0.2.2. It addresses an issue with openssl which causes the geoip lookups to fail on various versions of Windows. It also switches from the Nullsoft Installer to the Microsoft System Installer for better compatibility with Microsoft Windows.<br />
There are now separate Apple OS X builds, one for PowerPC architectures and one for i386 architectures. No more Universal binary bloat to download.<br />
The changes are:
</p>

<ul>
<li>When the user clicks "Browse" in the Advanced settings page to locate<br />
a new torrc, set the initial directory shown in the file dialog to the<br />
current location of the user's torrc. (Ticket #505) </li>
<li>Use 'ditto' to strip the architectures we don't want from the Qt<br />
frameworks installed into the app bundle with the dist-osx,<br />
dist-osx-bundle and dist-osx-split-bundle build targets. </li>
<li>Fix a bug in the CMakeLists.txt files for ts2po and po2ts that caused<br />
build errors on Panther for those two tools. </li>
<li>Include rebuilt OpenSSL libraries in the Windows packages that are<br />
built with the static (/MT) version of the Microsoft Visual C++<br />
Runtime. Otherwise, we would require users to install the MSVC<br />
Redistributable, which doesn't work for portable installations such as<br />
the Tor Browser Bundle. </li>
<li>Remove the NSIS file for the Vidalia installer since we now ship<br />
MSI-based installers on Windows. </li>
</ul>

<p>On August 27th, we released Vidalia 0.2.3.  This fixes some more bugs with "Who has used by bridge" functionality and switches to Qt signals for event handling.<br />
The changes are:
</p>

<ul>
<li>Create the data directory before trying to copy over the default<br />
Vidalia configuration file from inside the application bundle on Mac<br />
OS X. Affects only OS X drag-and-drop installer users without a<br />
previous Vidalia installation. </li>
<li>Change all Tor event handling to use Qt's signals and slots mechanism<br />
instead of custom QEvent subclasses. </li>
<li>Fix another bug that resulted in the "Who has used my bridge?" link<br />
initially being visible when the user clicks "Setup Relaying" from<br />
the control panel if they are running a non-bridge relay.<br />
(Ticket #509, reported by "vrapp") </li>
<li>Always hide the "Who has used my bridge?" link when Tor isn't running,<br />
since clicking it won't return useful information until Tor actually<br />
is running. </li>
</ul>

<p>On August 9th, we released Torbutton 1.2.2.<br />
The changes and enhancements are:
</p>

<ul>
<li>bugfix: Workaround Firefox Bug 440892 to prevent external apps from<br />
    being launched (and thus bypassing proxy settings) without user<br />
    confirmation. Independently reported by Greg Fleischer and optimist.</li>
<li>bugfix: Create a separate "No Proxy For" option and remove the<br />
    string "localhost" from proxy exemptions. Prevents a theoretical<br />
    proxy bypass condition discovered by optimist. Fix based on patch from<br />
    optimist.</li>
<li>bugfix: bug 970: Purge undo tab list on Tor toggle.</li>
<li>bugfix: bug 1040: Scrub URLs from log level 4 and higher log messages.<br />
    Mac OS writes Firefox console messages to disk by default.</li>
<li>bugfix: bug 1033: Fix FoxyProxy conflict that caused some FoxyProxy<br />
    strings to fail to display.</li>
<li>misc: bug 1006: Pop up a more specific failure message for pref<br />
    changing errors during Tor toggle.</li>
<li>misc: Fix a couple of strict javascript warns on FF3.5</li>
<li>misc: Add chrome url protection call to conceal other addons during<br />
    non-Tor usage. Patch by Sebastian Lisken.</li>
<li>misc: Remove torbutton log system init message that may have scared some<br />
    paranoids. </li>
</ul>

<p><strong>Architecture and technical design docs</strong></p>

<p>Update our secure updater, Thandy, to have optional BitTorrent support to distribute load spikes following new releases better.   Currently, it uses the mainline BitTorrent libraries that can be installed along with Thandy, but there is also some groundwork to support other BitTorrent libraries later on.</p>

<p><strong>Advocacy and outreach.</strong></p>

<p>Andrew, Jacob, Karsten, Mike, Nick, and Roger attended the Privacy Enhancing Technologies Symposium in Seattle, WA.  Details can be found at <a href="http://petsymposium.org/2009/" rel="nofollow">http://petsymposium.org/2009/</a>.  Jacob, Karsten, Mike, and Roger each presented their work on Tor.</p>

<p>Jacob, Karsten, Mike, Roger, and Sebastian attended Hacking at Random 2009 in  Vierhouten, Netherlands.  Details of the conference can be found at <a href="https://wiki.har2009.org/page/Main_Page" rel="nofollow">https://wiki.har2009.org/page/Main_Page</a>.  Jacob and Roger presented about Tor.</p>

<p>Jacob attended FooCamp 2009.  More details can be found at <a href="http://foocamp09.wiki.oreilly.com/wiki/index.php/Main_Page" rel="nofollow">http://foocamp09.wiki.oreilly.com/wiki/index.php/Main_Page</a>.  Jacob presented about Tor.</p>

<p>Andrew contacted  Tor relay operators that started running a relay between June 12, 2009 and July 13, 2009; ostensibly for the Iranian protest movement.  Of the 37 new relays, 13 had gone offline.  After contacting the relay operators, 7 of the 13 are back online.</p>

<p><strong>Preconfigured privacy (circumvention) bundles for USB or LiveCD.</strong></p>

<p>On August 4, we released Tor Browser Bundle 1.2.7. It is updated primarily due to Firefox 3.0.13 with its ssl fixes.</p>

<p>The full changelist is:<br />
1.2.7: Released 2009-08-04</p>

<ul>
<li>update Firefox to 3.0.13 </li>
<li>add Polish translation </li>
<li>update libevent to 1.4.12 </li>
</ul>

<p>On August 19, we released Tor Browser Bundle 1.2.8.  The big changes are the inclusion of statically linked openssl dlls to resolve a few geoip lookup and functionality issues with Vidalia, and the upgrade to the new Vidalia 0.2.2. </p>

<p>The full list of updates and fixes:</p>

<ul>
<li>update Torbutton to 1.2.2 </li>
<li>update Vidalia to 0.2.2 </li>
<li>compile OpenSSL 0.9.8k with Visual C to make dlls </li>
<li>update Pidgin to 2.6.1 </li>
</ul>

<p>Jacob and Steve Tyree started work on a portable Tor Browser Bundle for Apple OS X.  Jacob started work on a portable Tor Browser Bundle for generic Linux.  Both bundles are currently in developer testing, gearing up for an alpha release in September 2009.</p>

<p>Updated TorVM with current packages for torbutton, tor, qemu.  Added geoip and pycrypto to TorVM.</p>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong></p>

<p>Continued metrics work with torperf and directory request statistics.  Update bufferstats report, <a href="http://git.torproject.org/checkout/metrics/master/report/buffer/bufferstats-2009-08-25.pdf" rel="nofollow">http://git.torproject.org/checkout/metrics/master/report/buffer/buffers…</a><br />
Updated circuit window report, <a href="http://git.torproject.org/checkout/metrics/master/report/circwindow/circwindow-2009-08-19.pdf" rel="nofollow">http://git.torproject.org/checkout/metrics/master/report/circwindow/cir…</a></p>

<p>updated statistics on directory requests, <a href="http://git.torproject.org/checkout/metrics/master/report/dirarch/" rel="nofollow">http://git.torproject.org/checkout/metrics/master/report/dirarch/</a></p>

<p>And updated measurements on overall tor network performance, <a href="http://git.torproject.org/checkout/metrics/master/report/performance/torperf-2009-08-24.pdf" rel="nofollow">http://git.torproject.org/checkout/metrics/master/report/performance/to…</a></p>

<p>Continued work on our bandwidth node scanner to provide better extra-info for clients to make better routing decisions.</p>

<p>Added a seventh directory authority run by Jacob Appelbaum.  </p>

<p><strong>More reliable (e.g. split) download mechanism.</strong></p>

<p>Christian Fromme started work on our email auto-responder, get-tor, to better handle split downloads via email.</p>

<p>Jon, our mirror volunteer, continued to contact mirrors and update their status accordingly.  The net change is zero, but we added a new mirror and removed a stale mirror.</p>

<p><strong>Translation work</strong></p>

<p>Runa, our Google Summer of Code student, finished the project to allow for website content to be translated via the Tor Translation Portal, <a href="https://translation.torproject.org/" rel="nofollow">https://translation.torproject.org/</a>.   The conversion tools are now live and Danish and Farsi are the first languages enabled in the Tor Translation Portal for testing.</p>

<p>In August, there were:</p>

<p>8 Russian updates for the website<br />
29 Polish updates for the website<br />
15 Chinese updates for the website<br />
Danish updates for Torbutton<br />
Nederlandish updates for Torbutton</p>

---
_comments:

<a id="comment-2584"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2584" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2009</p>
    </div>
    <a href="#comment-2584">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2584" class="permalink" rel="bookmark">hi, government has probably</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi, government has probably blocked tor in iran, do you have any idea how they can block tor, and how can we solve this problem?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2660"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2660" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 27, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2584" class="permalink" rel="bookmark">hi, government has probably</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2660">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2660" class="permalink" rel="bookmark">re: Iran blocks</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>From our research, they are blocking by IP:port combination.  Nothing fancy. Bridges continue to work well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2585"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2585" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2009</p>
    </div>
    <a href="#comment-2585">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2585" class="permalink" rel="bookmark">Hi!&quot;geoip.vidalia-project.net</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi!"geoip.vidalia-project.net:1443"how to become a"geoip.vidalia-project.net:80"? I use TOR 0.2.1.19,now.I can't see "Location".</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2595"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2595" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Darek (not verified)</span> said:</p>
      <p class="date-time">September 23, 2009</p>
    </div>
    <a href="#comment-2595">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2595" class="permalink" rel="bookmark">Use Tor by region</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, I wonder is it possible to use set tor on region - ex. USA or Europe or such a country ? 'cause is needed to some vortals when see other country there is no opiton to use some function. Anyway What should to do or What to do to run this project.I'm ready to help.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2657"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2657" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 27, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2595" class="permalink" rel="bookmark">Use Tor by region</a> by <span>Darek (not verified)</span></p>
    <a href="#comment-2657">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2657" class="permalink" rel="bookmark">re: Tor by region</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can do this by setting a country code in the Exitnodes or Entrynodes configuration option in your torrc.  Our graphical interface can't do this yet, but TorK can, if you're on linux. TorK is at <a href="http://www.anonymityanywhere.com/tork/" rel="nofollow">http://www.anonymityanywhere.com/tork/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2605"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2605" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2009</p>
    </div>
    <a href="#comment-2605">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2605" class="permalink" rel="bookmark">China great fire wall has</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>China great fire wall has block the tor node,sad!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2608"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2608" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>1 in a billion (not verified)</span> said:</p>
      <p class="date-time">September 24, 2009</p>
    </div>
    <a href="#comment-2608">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2608" class="permalink" rel="bookmark">Great Firewall of China</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ahhh... that would explain why Tor stopped working today. Hm... Unfortunately, China has blocked the downloads page - how can MAC users download the latest version of Tor to hopefully bridge connections or bypass this node block?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2609"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2609" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2009</p>
    </div>
    <a href="#comment-2609">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2609" class="permalink" rel="bookmark">Tor is blocked in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is blocked in China,almost totally. Any one can give some solution? Thanks !</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2610"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2610" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2009</p>
    </div>
    <a href="#comment-2610">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2610" class="permalink" rel="bookmark">use an tor bridge.. ;-)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>use an tor bridge.. ;-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2614"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2614" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2610" class="permalink" rel="bookmark">use an tor bridge.. ;-)</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2614">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2614" class="permalink" rel="bookmark">the bridge not work Mr.
tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the bridge not work Mr.<br />
tor has blocked in Iran<br />
please please please please please please help men</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2611"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2611" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2009</p>
    </div>
    <a href="#comment-2611">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2611" class="permalink" rel="bookmark">Seems the fingerprint in Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Seems the fingerprint in Tor TLS handshake has been fully analyzed by the GFW people.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2658"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2658" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 27, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2611" class="permalink" rel="bookmark">Seems the fingerprint in Tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2658">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2658" class="permalink" rel="bookmark">re: fingerprint</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>China would have to block all SSL for this to work.  We've confirmed the GFW is doing simple IP:port blocking at this time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2612"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2612" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2009</p>
    </div>
    <a href="#comment-2612">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2612" class="permalink" rel="bookmark">Hi! Why I can&#039;t see the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi! Why I can't see the "location" in the Vidalia?(can't see the flag)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2644"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2644" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 26, 2009</p>
    </div>
    <a href="#comment-2644">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2644" class="permalink" rel="bookmark">Hi. I want to ask you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi. I want to ask you something: why to leave firefox (in tor browser) update itself? why not to deactivate that option in Firefox Portable? why are there other options activated, like google's safebrowsing?<br />
I mean, I want privacy, ok, then why to let google to track me? (Please, read this: <a href="http://ha.ckers.org/blog/20090824/google-safe-browsing-and-chrome-privacy-leak/" rel="nofollow">http://ha.ckers.org/blog/20090824/google-safe-browsing-and-chrome-priva…</a>). Why do you think I can even trust in google/firefox/whatever? Ok, firefox is FOSS, but safebrowsing is something that, being an informed user, as I am, is not needed.<br />
Please, deactivate all that settings.<br />
Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2659"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2659" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 27, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2644" class="permalink" rel="bookmark">Hi. I want to ask you</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2659">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2659" class="permalink" rel="bookmark">re: firefox updating</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The included firefox in the tor browser bundle is set to disable automatic updates when Torbutton is enabled.  </p>
<p>As of TBB 1.2.9:</p>
<p>user_pref("browser.safebrowsing.enabled", false);<br />
user_pref("browser.safebrowsing.malware.enabled", false);<br />
user_pref("browser.search.suggest.enabled", false);<br />
user_pref("browser.search.update", false);  </p>
<p>These are set automatically.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2653"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2653" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 27, 2009</p>
    </div>
    <a href="#comment-2653">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2653" class="permalink" rel="bookmark">hi, how can I change server</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi, how can I change server that tor connects to ? I want to have IP from USA is that possible ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2741"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2741" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Compression Colin (not verified)</span> said:</p>
      <p class="date-time">October 01, 2009</p>
    </div>
    <a href="#comment-2741">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2741" class="permalink" rel="bookmark">Compressing Data in the Tor network...</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi there,</p>
<p>I think this topic came up before, but I cant see an answer to it.<br />
Is it possible to add compression to Tor for all node 2 node comms ?</p>
<p>If not already, this could compress data (plain HTML) 10 times or more, which would relieve traffic on the network and create the illusion of more bandwidth.</p>
<p>For those of using it to browse the net (not so much those pesky p2p people) we could see an instant increase in page load times ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2755"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2755" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 04, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2741" class="permalink" rel="bookmark">Compressing Data in the Tor network...</a> by <span>Compression Colin (not verified)</span></p>
    <a href="#comment-2755">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2755" class="permalink" rel="bookmark">re: compressing data in the tor network</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is a tunnel, it doesn't know anything about the content stream sent into it, therefore we don't have the ability to compress or not. The issue is on the other end, how would this compression work?</p>
<p>Most websites already support gzip compression between the browser and the web server.  The compression should be done at the application level before it ever gets into Tor.  </p>
<p>Think of Tor like anonymous tcp.  tcp/ip doesn't compress your data because it should just transport the data, not modify it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2853"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2853" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Compression Colin... (not verified)</span> said:</p>
      <p class="date-time">October 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-2853">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2853" class="permalink" rel="bookmark">Thank you...</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you phobos for getting back to me.<br />
I appreciate the response.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-2789"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2789" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Traveller (not verified)</span> said:</p>
      <p class="date-time">October 09, 2009</p>
    </div>
    <a href="#comment-2789">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2789" class="permalink" rel="bookmark">if you have trouble</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>if you have trouble downloading Tor in China, use a proxy site like vtunnel.com or zend2.com.</p>
<p>2 weeks ago it got more difficult to connect to Vidalia in China. I had to add a bridge from here: <a href="https://bridges.torproject.org/" rel="nofollow">https://bridges.torproject.org/</a> to make it work again. If you cannot enter the bridge url, use one of the above proxy sites.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2804"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2804" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Edward (not verified)</span> said:</p>
      <p class="date-time">October 10, 2009</p>
    </div>
    <a href="#comment-2804">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2804" class="permalink" rel="bookmark">Is Vidalia included in torproject.org Ubuntu repository? </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm trying to install Tor with Vidalia on Ubuntu Hardy.  The torproject wiki says, "Most users should simply download Vidalia as part of a Tor software bundle", and links to instructions for adding the torproject.org repository.</p>
<p>I have successfully (I think) added that repository in Synaptic, and installed Tor from that repository. But I can't find any indication that Vidalia has been installed along with Tor.  And even with the torproject.org repository enabled and the sources updated, "sudo apt-get install vidalia" gets the response, "Couldn't find package vidalia".</p>
<p>Is Vidalia included in the Tor package in your Ubuntu repository?</p>
<p>if so, can you suggest what I did wrong, or should do to make it right to get Vidalia?</p>
<p>If Vidalia isn't in your Ubuntu repository, what is your recommended repository or download location for Vidalia for Ubuntu (hardy)?<br />
,</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2976"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2976" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Sleek (not verified)</span> said:</p>
      <p class="date-time">October 29, 2009</p>
    </div>
    <a href="#comment-2976">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2976" class="permalink" rel="bookmark">Please do you know how I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please do you know how I could configure bittorrent to work with tor? thanks in advance</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2985"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2985" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">November 01, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2976" class="permalink" rel="bookmark">Please do you know how I</a> by <span>Sleek (not verified)</span></p>
    <a href="#comment-2985">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2985" class="permalink" rel="bookmark">re:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, we don't know.  We don't support bittorrent over Tor.  Please use another program, perhaps bitblinder, <a href="http://www.bitblinder.com/" rel="nofollow">http://www.bitblinder.com/</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
