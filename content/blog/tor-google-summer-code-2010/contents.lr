title: Tor in Google Summer of Code 2010
---
pub_date: 2010-03-23
---
author: karsten
---
tags:

gsoc
gsoc2010
---
categories: internships
---
_html_body:

<p>Interested in coding on Tor and getting paid for it by Google? If you are a student, we have good news for you: We have been <a href="http://socghop.appspot.com/gsoc/program/accepted_orgs/google/gsoc2010" rel="nofollow">accepted</a> as a mentoring organization for <a href="http://socghop.appspot.com/gsoc/program/home/google/gsoc2010" rel="nofollow">Google Summer of Code 2010</a> together with <a href="https://www.eff.org/" rel="nofollow">The Electronic Frontier Foundation</a>! Woo!</p>

<p>Here are the facts: The summer of code gives you the opportunity to work on your own Tor-related coding project with one of the Tor developers as your mentor. You can apply for a coding project related to Tor itself or to one of its many <a href="https://www.torproject.org/projects/" rel="nofollow">supplemental projects</a> (as well as for an <a href="https://www.eff.org/gsoc2010" rel="nofollow">EFF-related project</a>). Your mentor will help you when you're stuck with your project and guide you in becoming part of the Tor community. Google pays you 4,500 USD for the three months of your project, so that you can focus on coding and don't have to worry about how to pay your bills.</p>

<p>Did we catch your attention? These are your next steps: Go look at the <a href="http://socghop.appspot.com/document/show/gsoc_program/google/gsoc2010/faqs" rel="nofollow">Google Summer of Code FAQ</a> to make sure you are <a href="http://socghop.appspot.com/document/show/gsoc_program/google/gsoc2010/faqs#eligibility" rel="nofollow">eligible</a> to participate. Have a look at our <a href="https://www.torproject.org/gsoc.html.en#Ideas" rel="nofollow">ideas list</a> or <a href="https://www.eff.org/gsoc2010" rel="nofollow">EFF's ideas list</a> to see if one of those projects matches your interests. If there is no project on that list that you'd want to work on, read the <a href="https://www.torproject.org/documentation.html.en" rel="nofollow">documentation on our website</a> and make up your own project idea! Come to <a href="irc://irc.oftc.net/tor/" rel="nofollow">#tor on OFTC</a> and let us know about your project idea. Communication is essential to success in the summer of code, and we're unlikely to accept students we haven't heard from before reading their application. So really, come to the IRC channel and talk to us!</p>

<p>Finally, write down your project idea using our <a href="https://www.torproject.org/gsoc#Template" rel="nofollow">template</a> and <a href="http://socghop.appspot.com/document/show/gsoc_program/google/gsoc2010/faqs#student_apply" rel="nofollow">submit your application</a> to Google until <a href="//socghop.appspot.com/document/show/gsoc_program/google/gsoc2010/faqs#timeline" rel="nofollow">April 9, 2010, 19:00 UTC</a>.</p>

<p>We are looking forward to discussing your project idea with you!</p>

