title: Strength in Numbers: The Internet Freedom Movement Must Be Localized
---
pub_date: 2018-12-18
---
author: emmapeel
---
tags:

localization
translations
Strength in Numbers
---
categories: localization
---
summary: Working on localization is more interesting when you consider what you learned in History class back at school: the current situation of localization on the internet reflects how colonialism and cultural imposition have shaped the language landscape around the world.
---
_html_body:

<p>Original photo by <a href="https://unsplash.com/photos/u9QuzlImpzA?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Arthur Edelman</a> on <a href="https://unsplash.com/search/photos/globe?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash </a></p>
<p><em>This post is one in a series of blogs to complement our 2018 crowdfunding campaign, Strength in Numbers. Anonymity loves company and we are all safer and stronger when we work together. <a href="https://torproject.org/donate/donate-sin-lo-bp">Please contribute today</a>, and your gift will be matched by Mozilla. </em></p>
<p>The Tor Project is an umbrella for many different projects. Some have a user interface and others are running in the background. Our aim is to make Tor and Tor Browser usable all over the world, and to do that we need to localize our tools and resources. In July, I joined as the Localization Coordinator and have been working on localization with the rest of the Tor team.</p>
<p>What is the difference between localization and translation? The aim of localization is much broader than just translating strings of words. To localize an application means to ensure that the application stays relevant in the local context, is understandable, and is usable.  </p>
<p>In this, I am helped by Tor’s UX and Community teams. They work with groups of users around the globe, conducting user tests and identifying problems that have made Tor difficult to use in certain contexts; for example, when a user does not have a reliable internet connection. If we develop applications that only work with good connections, a lot of people will not be able to use them, or their experience will be frustrating. Tor developers have since worked on those issues, and now Tor performs better when the connection is poor.</p>
<p>Working on localization is more interesting when you consider what you learned in History class back at school: the current situation of localization on the internet reflects how colonialism and cultural imposition have shaped the language landscape around the world. <a href="https://www.statista.com/statistics/262946/share-of-the-most-common-languages-on-the-internet/">English is the most common language</a> used on the internet. Sadly, people that have been colonized by the English or Spanish and have lost their original language have it easier on the internet than others. Even as a native Spanish speaker, I felt like an underdog on the internet. Little did I know what being an underdog on the internet meant. At least we can write most of our language in <a href="https://en.wikipedia.org/wiki/ASCII">ASCII code</a>, the character encoding standard for electronic communication which helps display the right letter, so we can communicate even when there is no possibility of writing in correct Spanish, and at least Spanish is written from left to right! But other languages have it harder. Moroccans and other Arabic speakers must often resort to French when online because the local dialects have no written standards, and forms and websites do not support Arabic well. Some languages have characters that are not even part of UTF-8 yet, so their letters cannot be displayed on the computer. In some countries with lower reading levels, icons and drawings play a much bigger role on delivering a message. We don’t want these differences to be a barrier to using tools that protect people from tracking, surveillance, and censorship on the web. Localizing technology can even play a role in <a href="https://www.localizationlab.org/blog/2018/11/19/why-localizing-technbspmatters">ensuring a native language survives</a>. When more and more contributors join the movement to localize the internet, the stronger and more sustainable this work becomes.</p>
<h1>Translating for local contexts takes a world of considerations</h1>
<p>For the actual translations, I am helped by an army of volunteer translators from around the globe who donate time and skills to translate open source software to their local languages. This is also done with the support of <a href="https://www.localizationlab.org/">Localization Lab</a>. When it's sunny outside, volunteers sit behind their computers and translate strings like 'Accept,' 'There was an error,' etc., for free. The reasons why individuals volunteer to do translations vary as much as their personal circumstances, but in the end we all benefit from their work, and I want to use this opportunity to thank them again: thank you!</p>
<p>The work can become more complicated based on what has come before. Some volunteers are working on translation projects in languages that have an existing collection of text on Tor and censorship circumvention; these volunteers must integrate their work into previously translated material. But others are translating many terms for the first time, and so they debate about which local term will make the different metaphors, such as <a href="https://www.torproject.org/docs/bridges.html.en">bridges</a>, make sense in their language and context. This process often involves terms related to other applications or general internet security terms that have not been previously translated. It is a long process, but it brings the apps closer to the local people. Some translators argue against anglicisms, or the direct use of an English word in another language, but then the users may complain about not understanding the translation because they are used to the English terms. Each situation is different and requires careful attention.</p>
<p>Translation and localization require many volunteers, many minds, and lots of time. When we do this work together, we can do more research, hear more voices, and finalize our work faster. In our mission to localize the internet freedom movement, there's strength in numbers.</p>
<h1>What we’ve done so far</h1>
<p>- <a href="https://www.torproject.org/download/download-easy.html.en">Tor Browser</a> now supports 25 languages, and 4 additional languages are supported in alpha<br />
- We started <a href="https://twitter.com/torproject/status/1010156373752107008">tweeting</a> more often in languages besides English<br />
- We published additional subtitles for the <a href="https://www.youtube.com/watch?v=JWII85UlzKw">Tor Animation</a><br />
- Now we have better <a href="https://torpat.ch/">statistics</a> about our language support<br />
- We are working on localizing our user <a href="https://support.torproject.org/">support website</a> and the <a href="https://tb-manual.torproject.org/">Tor Browser User Manual</a>, with more languages added every month</p>
<h1>What’s next</h1>
<p>We have decided on a priority list for localization, and we would like to eventually release all our software (more than just Tor Browser) in at least these 12 languages: English, Farsi, Spanish, Russian, Simplified Chinese, Portuguese, French, German, Korean, Turkish, Italian, and Arabic. To decide on this list, we took several circumstances into account: the countries that are more likely to endure censorship events, the most downloaded locales, and the countries our users come from--especially the <a href="https://metrics.torproject.org/userstats-bridge-country.html">bridge users</a>.</p>
<p>My main goal is to have our content, including the user documentation, <a href="https://torproject.org/relay-guide">relay guide</a>, and more, in these languages. If you want to lend a hand, <a href="https://tb-manual.torproject.org/becoming-tor-translator/">here’s how</a>. We’ve also been working with the developers and sysadmins on ways to make it easier to add even more locales to all related Tor projects, so if you can help with another language, we welcome your help.</p>
<p>We’ve also been working on a <a href="https://trac.torproject.org/projects/tor/wiki/doc/community/glossary">glossary of Tor related terms</a> we want to translate into as many languages as possible and offer it as a resource for other internet security projects to use.</p>
<p>If you spot problems with translations, you can file a bug report at our <a href="https://trac.torproject.org/projects/tor/query?status=!closed&amp;component=%5ECommunity%2FTranslations">bug tracker</a> under the component Community/Translations. You can also ping us in <a href="https://www.torproject.org/about/contact.html.en#irc">#tor-l10n on OFTC IRC</a>.</p>
<p>If you’re not able to offer a hand with translations, you can support our work financially <a href="https://torproject.org/donate/donate-sin-lo-bp">by making a donation</a>. <strong>Language barriers are holding back the internet freedom movement, but there is strength in numbers: your support can play a critical role in making sure that Tor software can be localized for everyone who needs it. </strong>Plus, the more people that use Tor in different contexts and communities, the stronger it becomes for everyone.</p>
<p><a href="https://torproject.org/donate/donate-sin-lo-bp"><img alt="donate button" src="/static/images/blog/inline-images/tor-donate-button_9.png" class="align-center" /></a></p>
<p>Your donation will support a global community of people giving their time and resources to strengthen the internet freedom movement, and with Mozilla matching every donation through 2018, your impact will be doubled. <a href="https://torproject.org/donate/donate-sin-lo-bp">Join us</a>.</p>

---
_comments:

<a id="comment-279142"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279142" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2018</p>
    </div>
    <a href="#comment-279142">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279142" class="permalink" rel="bookmark">Just wanted to thank…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just wanted to thank everyone who works on translations and localizations!  This is a critical part of getting Tor into the hands of people who need it.</p>
<p>It's wonderful to see how Tor Project has been embracing the thesis that keeping Tor and Tor users safe involves more than just probability theory, networking protocols and programming languages, but also natural languages, national cultures, political attitudes, legal jurisdictions, and even, as you said, history.</p>
<p>An intriguing hard challenge would be to make Tor available in some of the most widely spoken indigenous languages in the Americas, such as Navajo (about 170K speakers in the US) and Yanomaman (about 20K speakers, mostly in Brazil).  The relevant political point here is that both in the US and in Brazil, civil liberties enjoyed by indigenous people, and even tribal sovereignty itself, are under assault, and in Latin America, land defenders are all too often simply murdered.  In Brazil, the newly elected President, Jair Bolsonaro, has derisively referred to the Yanomami as "zoo animals", and he has vowed to create a new death squads in order to rid Brazil of "subhumans" [sic].  (Decades ago he apparently participated in the original death squads, which murdered leftists and entire villages of indigenous people.) In the US, the Interior Department is apparently plotting to downgrade the legal status of tribal members, and may be plotting land seizures.  In both countries, the government has been increasingly aggressive in suppressing protests by indigenous peoples of destructive exploitation of natural resources on the reservations.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279143"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279143" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2018</p>
    </div>
    <a href="#comment-279143">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279143" class="permalink" rel="bookmark">Following up on the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Following up on the suggestion of localizations for Navajo and Yanomaman languages/cultures, the linguistic part might be easiest, because plenty of people are fluent in one of these languages and English or Spanish/Portuguese--- I guess that Anthropology Departments in US/BR universities could put you in touch with some of them.  The hard part--- this might be a question for the Googlers--- would be figuring out how to bring networking to remote areas without expensive and environmentally damaging conventional infrastructure projects.  We certainly don't want to bring the horrendously exploitative Comcast model to the Amazon or to the American Southwest!  Tor for sat phones maybe?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279144"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279144" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2018</p>
    </div>
    <a href="#comment-279144">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279144" class="permalink" rel="bookmark">Oops, forgot to say that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oops, forgot to say that Bolsonaro has apparently vowed to eradicate the Yanomaman language, so this speaks directly to your point about preserving threatened languages (and cultures), not to mention discouraging outright genocide.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279172"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279172" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279144" class="permalink" rel="bookmark">Oops, forgot to say that…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279172">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279172" class="permalink" rel="bookmark">Yet another F/U:
Google has…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yet another F/U:</p>
<p>Google has been hit with some very bad publicity lately (Censorbrowser, aka "Dragonfly") and is no doubt looking for a technologically challenging project which it can use to try to reconnect ideologically with its most talent employees.  I don't really trust Google (the company) and don't think TP should really trust the Google executive suite either, but the employees have unique skills upon which TP can draw.  I suggest approaching Google for some Summer of Code brainstorming about how to design (and produce and provide gratis) Tor enabled devices suitable for use in very remote regions with no telecom infrastructure, which can be used by indigenous people, who in some cases have no written script.</p>
<p>(No doubt CIA would love to fund that too, for their own purposes such as manipulating people living in remote regions of Afghanistan or Pakistan, but please stay away from such toxic funding sources.)</p>
<p>Note that Yanomamo villages have plenty of children, who I expect would be just as quick to figure out how to use devices as youngsters in "Western" societies, even devices which their elders may find baffling.</p>
<p>Yanomamo culture is very non-Western so you should expect to experience considerable culture shock.  My view on culture shock is that it should make one think hard about what you think is "the right way to live" and why, rather than falling into neo-colonial knee-jerk type reactions.</p>
<p>I know the suggested project is somewhat crazy, but I just love the image of people from a hunter-gatherer society living in a very remote area rebutting the false claim by "Western" elites that they are "ineducable savages" [sic] by using technologically sophisticated devices in politically sophisticated ways to defend their culture and the environment from powerful entities which seek their destruction.  Imagine if a village could hold a Tor protected video conference with another village, or with an opposition politician in Brazilia.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-279173"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279173" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2018</p>
    </div>
    <a href="#comment-279173">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279173" class="permalink" rel="bookmark">&gt; We’ve also been working on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; We’ve also been working on a glossary of Tor related terms we want to translate into as many languages as possible and offer it as a resource for other internet security projects to use.</p>
<p>It's great!  Some suggestions:</p>
<p>Add an entry for AES/Rijndael</p>
<p>Add en entry for Onion Share</p>
<p>Add an entry for TLS </p>
<p>Add an entry for Tor node (can refer to an entry node, relay node, or exit node)</p>
<p>Add an entry for Postquantum cryptography</p>
<p>Modify entry for "circuit" by incorporating the EFF graphic beloved by the universal adversary (NSA) showing how tor circuits use encryption.</p>
<p>Modify entry for "hop" by adding that in NSA parlance, "hop" refers to a link in the social networking graph (A communicates with B who communicates with C)</p>
<p>Update entry for "Tor Messenger" to say that project was discontinued in (month,year).</p>
<p>Consider adding entries for some named proposals for nextgen Tor protocols.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279202"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279202" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  emmapeel
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">emmapeel</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">emmapeel said:</p>
      <p class="date-time">December 28, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279173" class="permalink" rel="bookmark">&gt; We’ve also been working on…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279202">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279202" class="permalink" rel="bookmark">Thanks for your suggestions!…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for your suggestions!</p>
<p>Do you maybe want to work a definition for them, also?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279214"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279214" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Another anon (not verified)</span> said:</p>
      <p class="date-time">December 30, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279173" class="permalink" rel="bookmark">&gt; We’ve also been working on…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279214">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279214" class="permalink" rel="bookmark">Suggested glossary…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Suggested glossary definition for Tor Node:</p>
<p>A (server) computer that functions as part of the volunteer Tor network, servicing the traffic of Tor (client) users by running Tor server software.  The "entry node" also know as  "guard" is the first Tor network computer the Tor user's computer connects to.  It sends the traffic to the middle server or "relay node" which then sends it to the final Tor network server known as "exit node" or "exit relay".  The exit relay sends the traffic out as regular internet traffic.  If the the user's target web site (off the Tor network) is non-encrypted (i.e. not secure, not httpS) then at this stage the traffic degenerates to non-encrypted to match the destination.  The user's origin IP address is still hidden from the destination regardless.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
