title: Anonymity by Design versus by Policy
---
pub_date: 2009-09-17
---
author: phobos
---
tags:

anonymity advocacy
online anonymity
blogging
commenting
courts
free speech
news stories
---
categories:

advocacy
human rights
---
_html_body:

<p>There have been some recent stories in the news about various <a href="http://www.citmedialaw.org/blog/2009/skanky-blogging-anonymity-and-whats-right" rel="nofollow">"anonymous" bloggers</a> and <a href="http://www.citmedialaw.org/blog/2009/splitting-digital-baby-california-court-creates-new-procedure-uncovering-anonymous-comment" rel="nofollow">commenters</a> being unmasked by court order.   A business promises not to give up your identity unless forced to do so via court order.  This is anonymity by policy.  If a business doesn't have your identity, then there is nothing to divulge.  This is anonymity by design.</p>

<p><a href="http://www.washingtonpost.com/wp-dyn/content/article/2009/04/08/AR2009040803248.html" rel="nofollow">Advocates for anonymous comments</a> understand the value of being able to speak freely.  However, if you're simply connecting from your home or office, parts of your identity are leaked to the site on which you commented.  As shown above, you can be unmasked fairly easily.  Some sites simply respond to a subpoena, others legal threats, and still others simply don't care; giving away your identity with any request.  There are <a href="https://www.torproject.org/torusers" rel="nofollow">plenty of valid reasons</a> to want to protect your identity in a blog, comment, or feedback form.  </p>

<p>I've participated in "anonymous reviews" being conducted in a company where employees get to give their opinion on anything in the company: strategy, management, branding, etc.  Human Resources rolls out the "anonymous survey"  with great fanfare as a chance for the line employees to get their voice heard.  At the same time, upper management asks IT to ascertain which IP address maps to which employee, whether connecting internally or not.  Employees quickly figure out what's going on and feel undervalued and coerced into toeing the company line.  And management lacks the feedback they probably should hear.  In this case, anonymity by policy doesn't help anyone actually improve the company.</p>

<p>Another example is the <a href="https://www.cia.gov/about-cia/iraqi-rewards-program.html" rel="nofollow">Iraqi Rewards Program</a> run by the CIA.  It's designed such that concerned Iraqi's can report illegal activities and get a reward for verified intelligence.  Essentially, an anonymous tip line. The issue is an Iraqi citizen will go into an Internet cafe and have an encrypted conversation with cia.gov.  This is bad for the people who want to report something.  An observer on the network only sees someone talking to cia.gov with encryption.  A truly anonymous tip line should protect the identity of the tipster, and provide the tipster with the ability to divulge as much of their identity as comfortable.</p>

<p>The examples and news stories above show you the difference between anonymity by policy and anonymity by design.  We encourage the courts to keep raising the requirements before forcing a provider to divulge your identity.  We encourage companies to learn how privacy can enhance their relationship with their customers.  We designed Tor such that relying on court tests and company policies isn't your only protection.  Tor users and relay operators don't have the data to divulge.  This is anonymity and privacy by design.</p>

---
_comments:

<a id="comment-2550"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2550" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 18, 2009</p>
    </div>
    <a href="#comment-2550">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2550" class="permalink" rel="bookmark">You should talk to Canada</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The Ontario Privacy Commissioner has ads about Privacy by Design.  Perhaps there is something in common.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2554"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2554" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 18, 2009</p>
    </div>
    <a href="#comment-2554">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2554" class="permalink" rel="bookmark">I recently dropped a comment</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I recently dropped a comment on a blog, and they responded mentioning that tor isn't all its cracked up to be.  The conversation had nothing to do with tor, they just saw I was using it based on their logs, I guess.  They mentioned that it has exit nodes leakage issues or some such.  I don't profess to understand all that, so what I'm wondering is if that means they can see more specific information about me, or if its just showing tha I use tor.  Mind shedding some light?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2578"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2578" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2554" class="permalink" rel="bookmark">I recently dropped a comment</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2578">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2578" class="permalink" rel="bookmark">re: I recently dropped a comment</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We make it easy for sites to detect if you exit from Tor.  Unless you give more information, all someone will see is you are coming from Tor.</p>
<p>There's lots of misinformation out there about Tor.  People either don't understand it or purposely choose to not try.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2557"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2557" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 18, 2009</p>
    </div>
    <a href="#comment-2557">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2557" class="permalink" rel="bookmark">my window explorer could not open with tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi can you pls tell me what to do to my explorer,it could not connect or open page.<br />
regards</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2579"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2579" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2557" class="permalink" rel="bookmark">my window explorer could not open with tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2579">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2579" class="permalink" rel="bookmark">re: my window explorer could not open with tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You shouldn't use Tor with Internet Explorer.  Try the Tor Browser Bundle, it's far safer.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2562"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2562" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 19, 2009</p>
    </div>
    <a href="#comment-2562">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2562" class="permalink" rel="bookmark">tor not work from Iran in 2</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>tor not work from Iran in 2 days ago<br />
please help !!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2580"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2580" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2562" class="permalink" rel="bookmark">tor not work from Iran in 2</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2580">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2580" class="permalink" rel="bookmark">re: tor not work from Iran in 2</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What happens?  We're still seeing connections from Iran.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2586"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2586" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-2586">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2586" class="permalink" rel="bookmark">I am Expert i cant connect</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am Expert i cant connect with tor in 3 days ago<br />
the governmental telecommunication company in Iran close any port in internet traffic excluding 80<br />
please help us !!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2591"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2591" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 22, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2586" class="permalink" rel="bookmark">I am Expert i cant connect</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2591">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2591" class="permalink" rel="bookmark">443 filtered </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi.i noticed my download software,one line has 443 port number.then in the next line it says connection close.and retry.i couldn't run my TOR in the last two days,even with many bridges,with different port number.please,do not let them to close these few holes to the free world.this is exactly they want.and also any kind of address,such as gmail which starts with HTTPS is not reachable still.<br />
thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-2563"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2563" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Newc (not verified)</span> said:</p>
      <p class="date-time">September 19, 2009</p>
    </div>
    <a href="#comment-2563">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2563" class="permalink" rel="bookmark">recently dropped comment</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I was not aware that people could detect that you were using Tor??? Yes any more info here would be much appreciated.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2564"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2564" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 19, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2563" class="permalink" rel="bookmark">recently dropped comment</a> by <span>Newc (not verified)</span></p>
    <a href="#comment-2564">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2564" class="permalink" rel="bookmark">detecting use of tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it is easy to detect that a web comment was made via tor, because the list of tor exit node ip addresses is public.  however, your system may still leak information to blog and comment websites due to various weaknesses in browser design and implementation.  torbutton (and some other software) is designed to address the latter sorts of issues.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2573"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2573" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2564" class="permalink" rel="bookmark">detecting use of tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2573">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2573" class="permalink" rel="bookmark">I suppose that makes it easy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I suppose that makes it easy compared to detection of some other security measures, but it wouldn't seem to be effortless.  Unless I am missing something, there would still need to be a dynamic process to harvest the current exit nodes (I'm assuming that exit nodes change over time, not just relays), and an automated or manual process to compare the IP address source for comments to that list.  I wouldn't think that most blog publishers would have an incentive to make that effort.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-2565"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2565" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 19, 2009</p>
    </div>
    <a href="#comment-2565">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2565" class="permalink" rel="bookmark">443 is filtered.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi.i noticed my download software,one line has 443 port number.then  in the next line it says connection close.and retry.i couldnt run my TOR in the last two days,even with many bridges,with different port number.please,do not let them to close these few holes to the free world.this is exactly they want.and also any kind of address,such as gmail which starts with HTTPS is not reachable still.<br />
thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2588"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2588" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2565" class="permalink" rel="bookmark">443 is filtered.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2588">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2588" class="permalink" rel="bookmark">Please help me in title of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please help me in title of bridge for port<br />
<a href="mailto:karen_5752@yahoo.com" rel="nofollow">karen_5752@yahoo.com</a><br />
Please</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2566"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2566" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Ru-Kun (not verified)</span> said:</p>
      <p class="date-time">September 19, 2009</p>
    </div>
    <a href="#comment-2566">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2566" class="permalink" rel="bookmark">I agree with Tor but disagree with Tor abuse</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree with using Tor to protect your privacy. But as I have seen a rise of a new type of spammer. I disagree with abuse some names users have decided to use Tor for. As is I just started running a website to have a few users of Tor abuse my contact form to spam up my email box. So now I have all IP addresses banned server-side. </p>
<p>You I use Tor myself once in a while, but I my opinion it users like these that make Tor have a bad name. As is I'm think of disallow the use of proxies on my site.<br />
Other then that go Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2574"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2574" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2566" class="permalink" rel="bookmark">I agree with Tor but disagree with Tor abuse</a> by <span>Ru-Kun (not verified)</span></p>
    <a href="#comment-2574">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2574" class="permalink" rel="bookmark">No one that I know likes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No one that I know likes receiving spam.  But an important and elemental point of information privacy is that it be without exception.  As soon as you begin making it conditional (your definition of spam or mine? - your definition of freedom fighter vs terrorist or mine?) you have started your toboggan down a very steep and slippery slope inevitably leading to massive compromises of privacy from all kinds of motivations for all manner of beneficiaries (few of them good or even benign).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2575"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2575" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2566" class="permalink" rel="bookmark">I agree with Tor but disagree with Tor abuse</a> by <span>Ru-Kun (not verified)</span></p>
    <a href="#comment-2575">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2575" class="permalink" rel="bookmark">I am also not too clear on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am also not too clear on how Tor facilitates this.  I could drop a comment on your web form using any number of anonymous email addresses (assuming that there is even a script that checks for valid addresses).  I can tell you from professional experience that ISPs that are eager to go after UCE abusers, even when furnished with a complaint and email headers, are in a very small minority.  Since a web form could be filled in by someone in a public library, at an internet hotspot, or from a computer at their workplace, I would think that ISPs would be even more reluctant to go after this kind of abuser.  In short, I see not reason why Tor would be necessary or even very useful for this (although it may have been used).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2581"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2581" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2575" class="permalink" rel="bookmark">I am also not too clear on</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2581">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2581" class="permalink" rel="bookmark">re: I am also not too clear on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor lets you decide how much information to give to the site/person on the other end of your connection.  In your examples, sure someone could do that, but then the IP address is logged on the remote site, any ISP along the path could see the traffic and connection, and in most locations, there are observers who could place you at the computer (observers could be other patrons or cameras).</p>
<p>ISPs go after all sorts of abusers, they don't care where the connection originates.  It depends upon your ISP.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-2630"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2630" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 25, 2009</p>
    </div>
    <a href="#comment-2630">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2630" class="permalink" rel="bookmark">Tor dosenot Work in Iran</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i can not connect to Tor since yesterday. i tried every Bridges but it dosen't work out.</p>
<p>the port doesn't work. please help us.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2633"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2633" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 25, 2009</p>
    </div>
    <a href="#comment-2633">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2633" class="permalink" rel="bookmark">Tor cann&#039;t work in China</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor cann't work in China since Sep.25. Please help us.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2748"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2748" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 02, 2009</p>
    </div>
    <a href="#comment-2748">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2748" class="permalink" rel="bookmark">Tor proxy type</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is Tor a transparent or disguised proxy?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2762"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2762" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 05, 2009</p>
    </div>
    <a href="#comment-2762">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2762" class="permalink" rel="bookmark">Torcheck expired? at Xenobite</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Torcheck at Xenobite.eu seems to have expired June 2009.  Is there a new current torcheck site available to verify valid Tor page</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2992"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2992" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://www.babapandey.com/">Seo Expert (not verified)</a> said:</p>
      <p class="date-time">November 02, 2009</p>
    </div>
    <a href="#comment-2992">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2992" class="permalink" rel="bookmark">Design doesn&#039;t have any policy.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Policy is not for design. I looked the Iraqi rewards program and it was fabulous.<br />
Best Regards<br />
<a href="http://www.blissmedia.com.au/" rel="nofollow">Web Design Melbourne</a><a href="http://www.comparecontracthire.com/" rel="nofollow">car leasing </a><a href="http://www.983wedding.com/" rel="nofollow">Wedding Invitations</a> <a href="http://www.onlinedetoxstore.com/kits-to-detox-thc-and-marijuana/" rel="nofollow">Wedding Invitations</a><a href="http://www.lfhair.com/" rel="nofollow">wigs</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-8241"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-8241" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2010</p>
    </div>
    <a href="#comment-8241">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-8241" class="permalink" rel="bookmark">anyone know how I can make</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>anyone know how I can make tor shows an IP address specified, an IP address from which I got the number previously, is for business proposal (I want tor shows I am in the office)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9417"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9417" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 10, 2011</p>
    </div>
    <a href="#comment-9417">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9417" class="permalink" rel="bookmark">I like the article so much.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I like the article so much.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9792"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9792" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2011</p>
    </div>
    <a href="#comment-9792">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9792" class="permalink" rel="bookmark">The article is great!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The article is great!</p>
</div>
  </div>
</article>
<!-- Comment END -->
