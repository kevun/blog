title: New Release: Tor Browser 10.0a9 (Android Only)
---
pub_date: 2020-10-26
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.0
---
categories: applications
---
summary: Android Tor Browser 10.0a9 is now available from the Tor Browser Alpha download page and also from our distribution directory.
---
_html_body:

<p>Android Tor Browser 10.0a9 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.0a9/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-954">latest stable release</a> instead.</p>
<p>We are happy to announce the second alpha version for <strong>Android users</strong> based on Fenix 82.</p>
<p>Tor Browser 10.0a9 ships with Fenix 82.1.1 (see Mozilla's <a href="https://blog.mozilla.org/blog/2020/08/25/introducing-a-new-firefox-for-android-experience/">blog post</a> for more information about this new browser). As this is the second alpha version based on Fenix we expect more bugs than usual. <strong>Please report them</strong> (with steps to reproduce), either here or on <a href="https://gitlab.torproject.org">Gitlab</a>, or essentially with any other means that would reach us. We are in particular interested in potential <strong>proxy bypasses</strong> which our proxy audit missed. This version is expected to be the last alpha release before Tor Browser 10.0 is considered stable on Android.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/tree/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.0a9">changelog</a> since Tor Browser 10.0a8 is:</p>
<ul>
<li>Android
<ul>
<li>Update Fenix to 82.1.1</li>
<li>Update NoScript to 11.1.3</li>
<li>Update OpenSSL to 1.1.1h</li>
<li><a href="https://bugs.torproject.org/30605">Bug 30605</a>: Honor privacy.spoof_english</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-android-service/40003">Bug 40003</a>: Block starting Tor when setup is not complete</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-android-service/40004">Bug 40004</a>: "Tor Browser" string is used instead of "Alpha"/"Nightly" for non en-US locales</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40016">Bug 40016</a>: Allow inheriting from AddonCollectionProvider</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40017">Bug 40017</a>: Rebase android-components patches to 60</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40019">Bug 40019</a>: Expose spoofEnglish pref</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40020">Bug 40020</a>: Disable third-party cookies</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40021">Bug 40021</a>: Force telemetry=false in Fennec settings migration</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40022">Bug 40022</a>: Migrate tor security settings</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40023">Bug 40023</a>: Stop Private Notification Service</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40024">Bug 40024</a>: Disable tracking protection by default</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40050">Bug 40050</a>: Rebase Fenix patches to Fenix 82</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40053">Bug 40053</a>: Select your security settings panel on start page is confusing</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40058">Bug 40058</a>: Disabling/Enabling addon still shows option to disallow in private mode</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40062">Bug 40062</a>: HTTPS Everywhere is not shown as installed</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40068">Bug 40068</a>: Tor Service closes when changing theme</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40071">Bug 40071</a>: Show only supported locales</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40073">Bug 40073</a>: Use correct branding on About page</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40076">Bug 40076</a>: "Explore privately" not visible</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40078">Bug 40078</a>: Crash at Android startup from background service</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40082">Bug 40082</a>: Security level is reset when the app is killed</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40083">Bug 40083</a>: Locale ordering in BuildConfig is non-deterministic</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40087">Bug 40087</a>: Implement a switch for english locale spoofing</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40088">Bug 40088</a>: Use Tor Browser logo in migration screen</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40094">Bug 40094</a>: Do not use MasterPasswordTipProvider in HomeFragment</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40095">Bug 40095</a>: Hide "Sign in to sync" in bookmarks</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40097">Bug 40097</a>: Bump allowed_addons.json</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40133">Bug 40133</a>: Rebase tor-browser patches to 82.0b1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40166">Bug 40166</a>: Disable security.certerrors.mitm.auto_enable_enterprise_roots</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40198">Bug 40198</a>: Expose privacy.spoof_english pref</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40199">Bug 40199</a>: Avoid using system locale for intl.accept_languages</li>
<li>Translations update</li>
</ul>
</li>
<li>Build System
<ul>
<li>Android
<ul>
<li>Update Go to 1.14.10</li>
<li><a href="https://bugs.torproject.org/34360">Bug 34360</a>: Bump binutils version to 2.35.1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40097">Bug 40097</a>: Update toolchain for Fenix 82</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40108">Bug 40108</a>: Package tooling-glean-gradle archive, too</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40115">Bug 40115</a>: Update components for switch to mozilla82-based Fenix</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40121">Bug 40121</a>: Use updated glean_parser for application-services as well</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40124">Bug 40124</a>: Remove unused torbrowser-android-all (and related) targets</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40125">Bug 40125</a>: Remove fenix-* projects</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40129">Bug 40129</a>: application-services is missing rustc in PATH</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40130">Bug 40130</a>: More mobile clean-up</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-290142"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290142" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Akash (not verified)</span> said:</p>
      <p class="date-time">October 30, 2020</p>
    </div>
    <a href="#comment-290142">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290142" class="permalink" rel="bookmark">Its good love it</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Its good <strong>love it</strong></p>
</div>
  </div>
</article>
<!-- Comment END -->
