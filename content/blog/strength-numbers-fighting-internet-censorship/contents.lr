title: Strength in Numbers: Fighting Internet Censorship
---
pub_date: 2018-12-05
---
author: agrabeli
---
tags:

ooni
censorship
Strength in Numbers
---
categories: circumvention
---
summary: Internet censorship is becoming ubiquitous, but not all censorship cases are easy to identify and confirm. Transparency of internet censorship is essential.  We need evidence of internet censorship.
---
_html_body:

<p><em>This post is one in a series of blogs to complement our 2018 crowdfunding campaign, Strength in Numbers. Anonymity loves company, and the internet freedom movement is stronger when we fight together. <a href="https://torproject.org/donate/donate-sin-oo-bp">Please contribute today</a>, and your gift will be matched by Mozilla.</em></p>
<p>Imagine having to pay an extra tax to your government to access social media. People in <a href="https://ooni.torproject.org/post/uganda-social-media-tax/">Uganda</a> have to. Imagine not being able to access Instagram or Telegram during a demonstration or protest. People in <a href="https://ooni.torproject.org/post/2018-iran-protests/">Iran</a> experienced this earlier this year. Now imagine not being able to access referendum-related websites during a referendum. People in <a href="https://ooni.torproject.org/post/internet-censorship-catalonia-independence-referendum/">Europe </a>experienced this last year.</p>
<p>Internet censorship is becoming ubiquitous, but not all censorship cases are easy to identify and confirm.</p>
<p>If you live in a country that censors the internet, you may notice the blocking of popular platforms (like WhatsApp or Facebook), but you may not necessarily notice the <a href="https://ooni.torproject.org/post/indonesia-internet-censorship/#lgbt">blocking of LGBTQI sites in Indonesia</a>, the blocking of <a href="https://ooni.torproject.org/post/pakistan-internet-censorship/#minority-groups">ethnic minority sites in Pakistan</a>, or the blocking of other minority group sites elsewhere. Most censorship techniques adopted by Internet Service Providers (ISPs) around the world are quite subtle, which can make it hard to distinguish <em>intentional</em> government censorship from transient network failures, cases of server-side blocking, or other issues.</p>
<p>Transparency of internet censorship is therefore essential.</p>
<p>We need to know which online services are blocked and how. We need to know which ISPs implement censorship. We also need to know where internet censorship takes place to examine it within the country’s legal, political, social, economic, and cultural context. This level of transparency is essential to support public debate and to limit the potential for abuse.</p>
<p>In short, we need <em>evidence</em> of internet censorship.</p>
<p>You can help the Tor Project collect such evidence by running <a href="https://ooni.torproject.org/install/">OONI Probe</a>, our software designed to measure internet censorship and other forms of network interference. Since 2012, Tor’s <a href="https://ooni.torproject.org/">Open Observatory of Network Interference</a> (OONI) team has been designing and developing free software tests aimed at enabling potentially anyone on the planet to measure their network and to uncover internet censorship.</p>
<p>To increase transparency, OONI collects network measurements from around the world and <a href="https://ooni.torproject.org/data/">openly publishes</a> them every day. Thanks to our global community, <a href="https://explorer.ooni.io/world/">millions of network measurements</a> have been collected over the last six years. In 2018, OONI Probe was run by volunteers every month in <a href="https://api.ooni.io/stats">more than 200 countries</a>, measuring thousands of distinct networks around the world.</p>
<p>As a result, OONI’s <a href="https://ooni.torproject.org/data/">dataset</a> is probably the largest publicly-available resource on internet censorship to date. It serves as a growing archive, providing snapshots of what censorship looks like in thousands of networks around the world and how it changes over time. You can contribute to this archive by <a href="https://ooni.torproject.org/install/">running OONI Probe</a>, along with hundreds of thousands of community members globally.</p>
<p>OONI data has shed light on the <a href="https://ooni.torproject.org/post/egypt-internet-censorship/">blocking of hundreds of media websites in Egypt</a>, <a href="https://ooni.torproject.org/post/ethiopia-report/">social media blocking during Ethiopia’s wave of protests</a> (and the <a href="https://ooni.torproject.org/post/ethiopia-unblocking/">unblocking of sites under a new Prime Minister</a>), which <a href="https://ooni.torproject.org/post/uganda-social-media-tax/">Ugandan ISPs implement the social media tax</a>, the <a href="https://ooni.torproject.org/post/venezuela-internet-censorship/#currency-exchange">blocking of currency exchange sites in Venezuela</a>, and numerous other cases.</p>
<p>Since OONI measurements are openly published, you can <em>verify</em> our <a href="https://ooni.torproject.org/post/">censorship findings </a>and use the data as part of your own research, policy and advocacy efforts -- as <a href="https://freedomhouse.org/report/freedom-net/2018/zambia">Freedom House</a>, <a href="https://www.amnesty.org/en/latest/news/2016/12/ethiopia-government-blocking-of-websites-during-protests-widespread-systematic-and-illegal/">Amnesty International</a>, and many of our local partners have. You can also coordinate censorship measurement campaigns, <a href="https://sinarproject.org/digital-rights/updates/open-observatory-of-network-interference-ooni-southeast-asia-regional-workshop">engaging local communities</a> with the use of our open tools, methodologies, and data to better understand and fight internet censorship.</p>
<p>Our work though is far from over.</p>
<p>Censorship is a cat-and-mouse game. As censorship techniques become more sophisticated, we need to develop more sophisticated censorship-detection tests and more sophisticated circumvention techniques for Tor. As internet censorship becomes more pervasive, more of us need to hold the censors to account.</p>
<p><strong>You can support this fight by donating to the Tor Project.</strong></p>
<p><a href="https://torproject.org/donate/donate-sin-oo-bp"><img alt="donate-button" src="/static/images/blog/inline-images/tor-donate-button_6.png" class="align-center" /></a></p>
<p>By supporting Tor’s <a href="https://ooni.torproject.org/">OONI project</a>, you are enabling us to develop more sophisticated methodologies, more usable apps, more accessible data resources, and to publish more insightful research reports.</p>
<p><strong>You are enabling us to better serve human rights communities around the world.</strong></p>
<p><a href="https://torproject.org/donate/donate-sin-oo-bp">Donate today</a>, and Mozilla will match your donation.</p>

---
_comments:

<a id="comment-278729"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278729" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2018</p>
    </div>
    <a href="#comment-278729">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278729" class="permalink" rel="bookmark">You&#039;re not really censorship…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You're not really censorship-resistant if you can deplatform someone. Use hidden services and bitchute. If evil nazi scum can't use it, it's not censorship-resistant.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278829"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278829" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 09, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278729" class="permalink" rel="bookmark">You&#039;re not really censorship…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278829">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278829" class="permalink" rel="bookmark">One of the hardest things to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>One of the hardest points to explain to politicians is that their own d-m spooks use Tor to hide their tracks when doing the things that they do, and yes, evil white supremacists--- some of whose day job invovles wearing a uniform or a badge--- use Tor to hide their (generally bungled but admittedly often alarming) plans to incite a race war, and Tor has no way of stopping them without making it impossible for someone living in a dangerous country run by an oppressive government to get outside information about things they need to know.  Which is exactly why TP provides Tor and why the whole world needs Tor, despite the evil done by spooks, cyberwarriors, and would be race-warriors.</p>
<p>Back in the early days of something called the USA, a then young nation which no one including its founders really thought was likely to exist for survive in the face of internal tensions for more than another few years, a man known as Dr. Franklin forcefully argued for the creation of a uniquely American postal service whose carriers (unlike similar services in the European monarchies) would actually not read the mail they carried.  Franklin argued that the benefits of freedom of communication and freedom of association would far outweigh the danger of potentially allowing conspiracies to develop unchecked by government spies.  The correspondence of Aaron Burr (who conspired by mail to try to set up another new nation in what is now part of the Southern midwest of the USA) later provided a good example of the fact that sometimes real conspiracies really do happen--- and mostly fail, as did Burr's conspiracy.  But the point is of course that Franklin has been proven correct by all the good things which have flowed from freedom of information and freedom of association.</p>
<p>And the point is that someone like Rod Rosenstein is really more of a "King's man" than a Patriot in the original American understanding of what it means to be a Patriot.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278750"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278750" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2018</p>
    </div>
    <a href="#comment-278750">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278750" class="permalink" rel="bookmark">Wanna to donate, but how to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wanna to donate, but how to do it anonymously? even with bitcoin, you have to somehow show up in a counter to 'convert my nickel' and get webcamed.......</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278779"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278779" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278750" class="permalink" rel="bookmark">Wanna to donate, but how to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278779">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278779" class="permalink" rel="bookmark">+ sms verification for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>+ sms verification for amounts under thousends of dollars, euro's or whatever currency it is.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278812"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278812" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278750" class="permalink" rel="bookmark">Wanna to donate, but how to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278812">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278812" class="permalink" rel="bookmark">Few if any cryptocurrencies…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Few if any cryptocurrencies are anonymous even at the blockchain level.  Snowden mentioned (with reservations) Monero and Zcash) as two projects which aim to achieve anonymous payments, and it seems the governments noticed:</p>
<p>zdnet.com<br />
DHS looking into tracking Monero and Zcash transactions<br />
DHS has had great success with tracking and analyzing Bitcoin transactions already. They are now looking for similar solutions for tracking "privacy coins."<br />
Catalin Cimpanu for Zero Day<br />
7 Dec 2018</p>
<p>&gt; The US Department of Homeland Security (DHS) is interested in acquiring technology solutions that can track newer cryptocurrencies, such as Zcash and Monero.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278754"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278754" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>WD (not verified)</span> said:</p>
      <p class="date-time">December 05, 2018</p>
    </div>
    <a href="#comment-278754">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278754" class="permalink" rel="bookmark">What a waste of time. If Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What a waste of time. If Tor would just spend some effort making relay and onion hosting setup easier, perhaps refining it's technology so that Tor connections could never be stopped - this worldwide censorship scourge might ease.</p>
<p>You aren't gonna talk these people well again. They have consumed the censorship punch. And corporate censors are the worst by far.....</p>
<p>Tor, just give us an alternate internet and shut the hell up :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278778"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278778" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>keb (not verified)</span> said:</p>
      <p class="date-time">December 07, 2018</p>
    </div>
    <a href="#comment-278778">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278778" class="permalink" rel="bookmark">Please do not play into the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please do not play into the US neoliberal agenda by singling out Iran.  Yes Iran does block and censor the internet, even outside of protests.  However every western country now routinely blocks mobile devices during protests, or uses IMSI-catchers to track the devices and reroute their traffic.  The 5-eyes countries also record all internet and voice traffic even when their own laws prohibit it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278827"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278827" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278778" class="permalink" rel="bookmark">Please do not play into the…</a> by <span>keb (not verified)</span></p>
    <a href="#comment-278827">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278827" class="permalink" rel="bookmark">The present government of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The present government of Iran is not well regarded by human rights NGOs, and on that point I'd like to think I stand with the people of Iran, not all of whom are happy to be censored by the present government of Iran.</p>
<p>That said, you raise a valid point which I would rephrase thusly:</p>
<p>Many nations are ramping up government censorship and even more sophisticated population control systems such as "social credit" systems.  Many of government censors use censorship-as-a-service systems from "Western democracies" such as Bluecoat.  And many "US based" companies whose business model involves surveillance-as-a-service, cyberwar-as-a-service, and social-credit-as-a-service are quietly contemplating ditching USG contracts for ultimately more lucrative contracts with the Chinese government which in any case is apparently less unstable.</p>
<p>Censorship. Dragnets.  Corruption.  Endemic, drastic and worsening social inequality driven by offshoring.  Legal travesties.  Oppression.  Hate crimes.  Political police.  Mass incarceration of innocents including children.   Family separation.  Dehousing.  Apartheid.  Forced relocations.  Incarceration of dissidents.  Extrajudicial executions.   Invasions.  The bombing and shelling of cities.  The murder of journalists.  Of land defenders. Genocide.</p>
<p>Some actions are so horrid that it matters not a jot which government does them or what their stated reasons are, because no good human being could fail to oppose these actions with every fiber of their strength.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278780"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278780" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2018</p>
    </div>
    <a href="#comment-278780">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278780" class="permalink" rel="bookmark">In the dialog between…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In the dialog between Snowden and his lawyer (Ben Wizner of ACLU) in a special issue of McSweeney's</p>
<p><a href="https://www.eff.org/deeplinks/2018/11/end-trust-sale-bookstores-and-free-download-now" rel="nofollow">https://www.eff.org/deeplinks/2018/11/end-trust-sale-bookstores-and-fre…</a></p>
<p>you will find a very clear explanation of how blockchains work, plus a discussion of why electronic currencies are not (yet) anonymous.  ES expresses doubt that they ever will be, but mentions some intriguing new examples of cryptocurrency which may offer more anonymity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278781"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278781" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2018</p>
    </div>
    <a href="#comment-278781">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278781" class="permalink" rel="bookmark">Years ago EFF offered a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Years ago EFF offered a special version of Raspbian adapted for OONI surveys using a Raspberry Pi, but that version is now too outdated to be safe.   Are there plans for a new edition?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278783"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278783" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2018</p>
    </div>
    <a href="#comment-278783">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278783" class="permalink" rel="bookmark">Has OONI considered…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Has OONI considered broadening its investigations to examine another aspect of the ill effects of living in a Surveillance State?  Beyond censorship, another issue is that people who are aware that they are under surveillance tend to cut back on their social activity (i.e. give up their attempt to exercise by default their right to freedom of association) on the grounds that only a small fraction of the people they know understand the risks they are running by being seen talking even casually to someone under surveillance.</p>
<p>In the US, some politically active people have even given up calling their representatives in the national capital because we dislike NSA's RIMROCK surveillance so much.  This amounts to another kind of political censorship.  So for example my Rep won't get an urgent phone call expressing my support for assistance to Puerto Rico.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278802"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278802" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span># (not verified)</span> said:</p>
      <p class="date-time">December 07, 2018</p>
    </div>
    <a href="#comment-278802">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278802" class="permalink" rel="bookmark">We need software that will…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We need software that will counter anything  Australia tries to do with their new encryption laws</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278820"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278820" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>skrillex (not verified)</span> said:</p>
      <p class="date-time">December 08, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278802" class="permalink" rel="bookmark">We need software that will…</a> by <span># (not verified)</span></p>
    <a href="#comment-278820">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278820" class="permalink" rel="bookmark">That should be pretty easy…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That should be pretty easy. Just use free an open tools like OMEMO, TOR and avoid Australian service providers. The hard part is getting the majority of people to do the same.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278828"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278828" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 09, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278802" class="permalink" rel="bookmark">We need software that will…</a> by <span># (not verified)</span></p>
    <a href="#comment-278828">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278828" class="permalink" rel="bookmark">Or rather, we need safe…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Or rather, we need safe software from Tor and others, which requires legal and political advocacy sufficiently powerful to defeat such laws before they spread any further.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278814"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278814" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>network (not verified)</span> said:</p>
      <p class="date-time">December 08, 2018</p>
    </div>
    <a href="#comment-278814">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278814" class="permalink" rel="bookmark">What about production high…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about production high-anonimus Tor-Browser, without Tor? For use with custom VPN and direct connecting</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278826"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278826" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278814" class="permalink" rel="bookmark">What about production high…</a> by <span>network (not verified)</span></p>
    <a href="#comment-278826">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278826" class="permalink" rel="bookmark">Perhaps I misunderstand what…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Perhaps I misunderstand what you have in mind, but neither direct connection or VPNs are not generally regarded by experts as helping much with anonymity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278834"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278834" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 09, 2018</p>
    </div>
    <a href="#comment-278834">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278834" class="permalink" rel="bookmark">Shout out to whoever is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Shout out to whoever is doing the graphics: love the artwork!   Comparing with the clumsy NSA graphics from the Snowden leaks, I definitely think we are winning the Art Wars!</p>
<p>Graphics can play an important role in "creating the Tor brand" (not a term I adore but we know what we mean I presume) so I am glad to see Tor exhibit visible talent in this area.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278887"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278887" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 11, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278834" class="permalink" rel="bookmark">Shout out to whoever is…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278887">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278887" class="permalink" rel="bookmark">I also like the graphic in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I also like the graphic in which an anonymous arm (an androgynous "Lady" Liberty perhaps?) is raising an onion.  Tor is definitely winning the Art Wars.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
