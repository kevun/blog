title: New Alpha Release: Tor Browser 12.0a4 (Android, Windows, macOS, Linux)
---
pub_date: 2022-10-31
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 12.0a4 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 12.0a4 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/12.0a4/).

Tor Browser 12.0a4 updates Firefox on Android, Windows, macOS, and Linux to 102.4.0esr.

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-45/) to Firefox and GeckoView. There were no Android-specific security updates to backport from the Firefox 106 release.

## What's new?

Tor Browser 12.0a4 includes a number of changes that require testing and feedback:

### Multi-locale bundles (Desktop)

This is the first multi-locale release of Tor Browser Alpha for desktop. All supported languages are now included in a single bundle, and can be changed without requiring additional downloads via the Language menu in General settings.

**What to test:** Tor Browser Alpha should default to your system language on first launch if it matches [a language we support](https://support.torproject.org/tbb/tbb-37/). Alpha testers are also encouraged to test changing language within about:preferences#general, and to report any new bugs with localization in general.

### tor-launcher migration (Desktop)

Parts of the code that power tor-launcher – which starts tor within Tor Browser – have been refactored. Although this work doesn't include any changes to the user experience, those who run non-standard Tor Browser setups are encouraged to test 12.0a4 on their systems.

**What to test:** Alpha testers who run non-standard Tor Browser setups (including, but not limited to, those who use system tor in conjunction with Tor Browser) should test starting and connecting to Tor, and report any unexpected error messages they encounter. All of the previously supported environment variables should still behave the same way as in the stable series.

### Onion Auth fixes (Desktop)

12.0a4 includes two fixes to Onion Service client authorization:

1. A fix to the auth window itself, which was broken in Alpha due to a regression caused by the esr102 transition: [tor-browser#41344](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41344)
2. Another fix to a longstanding issue with Onion Auth failing on subdomains, which has also been backported to 11.5.5: [tor-browser#40465](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40465)

**What to test:** Accessing client authorized Onion Services on both top-level and subdomains.

### Always prioritize .onion sites (Android)

Android users can now enable automatic Onion-Location redirects by switching "Prioritize .onion sites" within Privacy and Security settings.

**What to test:** Enable "Prioritize .onion sites" within settings, visit a website that supports Onion-Location, and verify that you were redirected to the website's .onion address.

### Unified Español locale (Desktop and Android)

Previous versions of Tor Browser Alpha were available in both "es" and "es-AR" (Español Argentina) locales. In Tor Browser 12.0a4 these have been unified into a single Spanish locale instead.

**What to test:** Alpha testers who use the "es-AR" locale should be automatically switched to "es-ES" after updating.

The full changelog since [Tor Browser 12.0a3](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs/ChangeLog.txt) is:

- All Plaforms
  - Update Translations
  - [Bug tor-browser#24686](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/24686): In Tor Browser context, should network.http.tailing.enabled be set to false?
  - [Bug tor-browser#27127](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/27127): Audit and enable HTTP/2 push
  - [Bug tor-browser#40057](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40057): ensure that CSS4 system colors are not a fingerprinting vector
  - [Bug tor-browser#40058](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40058): ensure no locale leaks from new Intl APIs
  - [Bug tor-browser#40251](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40251): Clear obsolete prefs after torbutton!27
  - [Bug tor-browser#40406](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40406): Remove Presentation API related prefs
  - [Bug tor-browser#40465](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40465): Onion Authentication fails when connecting to a subdomain
  - [Bug tor-browser#40491](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40491): Don't auto-pick a v2 address when it's in Onion-Location header
  - [Bug tor-browser#40494](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40494): Update Startpage and Blockchair onion search providers
  - [Bug tor-browser-build#40629](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40629): Bump snowflake version to 9ce1de4eee4e
  - [Bug tor-browser-build#40633](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40633): Remove Team Cymru hard-coded bridges
  - [Bug tor-browser-build#40646](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40646): Don't build Español AR anymore
  - [Bug tor-browser-build#40649](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40649): Update meek default bridge
  - [Bug tor-browser-build#40654](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40654): Enable uTLS and use the full bridge line for snowflake
  - [Bug tor-browser-build#40665](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40665): Snowflake bridge parameters are too long (535 bytes) in 11.5.5
  - [Bug tor-browser#41098](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41098): Compare Tor Browser's and GeckoView's profiles
  - [Bug tor-browser#41154](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41154): Review Mozilla 1765167: Deprecate Cu.import
  - [Bug tor-browser#41164](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41164): Add some #define for the base-browser section
  - [Bug tor-browser#41306](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41306): Typo "will not able" in "Tor unexpectedly exited" dialog
  - [Bug tor-browser#41317](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41317): Tor Browser leaks banned ports in network.security.ports.banned
  - [Bug tor-browser#41326](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41326): Update preference for remoteRecipes
  - [Bug tor-browser#41345](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41345): fonts: windows whitelist contains supplemental fonts
  - [Bug tor-browser#41398](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41398): Disable Web MIDI API
- Windows + macOS + Linux
  - Update Firefox to 102.4.0esr
  - [Bug tor-browser#17400](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/17400): Decide how to use the multi-lingual Tor Browser in the alpha/release series
  - [Bug tor-browser-build#40638](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40638): Visit our website link after build-to-build upgrade in Nightly channel points to old v2 onion
  - [Bug tor-browser-build#40648](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40648): Do not customize update.locale in multi-lingual builds
  - [Bug tor-browser#40853](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40853): use Subprocess.jsm to launch tor
  - [Bug tor-browser#40933](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40933): Migrate remaining tor-launcher functionality to tor-browser
  - [Bug tor-browser#41117](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41117): Review Mozilla 1512851:  Add Share Menu to File Menu on macOS
  - [Bug tor-browser#41323](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41323): Tor-ify notification bar gradient colors (branding)
  - [Bug tor-browser#41337](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41337): Add a title to the new identity confirmation
  - [Bug tor-browser#41342](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41342): Update the New Identity dialog to the proton modal style
  - [Bug tor-browser#41344](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41344): Authenticated Onion Services do not prompt for auth key in 12.0 alpha series
  - [Bug tor-browser#41352](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41352): Update or drop the show manual logic in torbutton
  - [Bug tor-browser#41369](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41369): Consider a different list-order for locales in language menu
  - [Bug tor-browser#41374](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41374): Missing a few torconnect strings in the DTD
  - [Bug tor-browser#41377](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41377): Hide `Search for more languages...` from Language selector in multi-locale build
  - [Bug tor-browser#41385](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41385): Bootstrap failure is logged but not raised up to about:torconnect
  - [Bug tor-browser#41386](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41386): The new tor-launcher has a problem when another Tor is running
  - [Bug tor-browser#41387](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41387): New identity and new circuit ended up inside history
  - [Bug tor-browser#41400](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41400): Missing onionAuthViewKeys causes "Onion Services Keys" dialog to be empty.
  - [Bug tor-browser#41401](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41401): Missing some mozilla icons because we still loading them from "chrome://browser/skin" rather than "chrome://global/skin/icons"
  - [Bug tor-browser#41404](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41404): Fix blockchair-onion search extension
- Windows
  - [Bug tor-browser#41149](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41149): Review Mozilla 1762576:  Firefox is not allowing Symantec DLP to inject DLL into the browser for Data Loss Prevention software
  - [Bug tor-browser#41278](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41278): Create Tor Browser styled pdf logo similar to the vanilla Firefox one
- macOS
  - [Bug tor-browser#41294](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41294): Bookmarks manager broken in 12.0a2 on MacOS
  - [Bug tor-browser#41348](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41348): cherry-pick macOS OSSpinLock  replacements
  - [Bug tor-browser#41370](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41370): Find a way to ship custom default bookmarks without changing language-packs on macOS
  - [Bug tor-browser#41372](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41372): "Japanese" language menu item is localised in multi-locale testbuild (on mac OS)
  - [Bug tor-browser#41379](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41379): The new tor-launcher is broken also on macOS
- Linux
  - [Bug tor-browser#40359](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40359): Tor Browser Launcher has Wrong Icon
- Android
  - Update GeckoView to 102.4.0esr
  - [Bug tor-browser-build#40631](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40631): Stop bundling HTTPS Everywhere on Android
  - [Bug tor-browser#41360](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41360): Backport Android-specific Firefox 106 to ESR 102.4-based Tor Browser
  - [Bug tor-browser#41394](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41394): Implement a setting to always prefer onion sites
- Build
  - All Platforms
    - Update Go to 1.19.2
    - [Bug tor-browser-build#23656](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/23656): Use .mozconfig files in tor-browser repo for rbm builds
    - [Bug tor-browser-build#28754](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/28754): make testbuild-android-armv7 stalls during sha256sum step
    - [Bug tor-browser-build#40397](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40397): Create a new build target to package tor daemon, pluggable transports and dependencies
    - [Bug tor-browser-build#40619](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40619): Make sure translations are taken from gitlab.tpo and not git.tpo
    - [Bug torbrowser-build#40627](https://gitlab.torproject.org/tpo/applications/torbrowser-build/-/issues/40627): Add boklm to torbutton.gpg
    - [Bug tor-browser-build#40634](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40634): Update the project/browser path in tools/changelog-format-blog-post and other files
    - [Bug tor-browser-build#40636](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40636): Remove https-everywhere from projects/browser/config
    - [Bug tor-browser-build#40639](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40639): Remove tor-launcher references
    - [Bug tor-browser-build#40643](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40643): Update Richard's key in torbutton.gpg
    - [Bug tor-browser-build#40655](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40655): Published tor-expert-bundle tar.gz files should not include their tor-browser-build build id
    - [Bug tor-browser-build#40656](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40656): Improve get_last_build_version in tools/signing/nightly/sign-nightly
    - [Bug tor-browser-build#40658](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40658): Create an anticensorship team keyring
    - [Bug tor-browser-build#40660](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40660): Update changelog-format-blog-post script to point gitlab rather than gitolite
    - [Bug tor-browser-build#40662](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40662): Make base-browser nightly build from tag
    - [Bug tor-browser-build#40671](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40671): Update langpacks URL
    - [Bug tor-browser#41308](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41308): Use the same branch for Desktop and GeckoView
    - [Bug tor-browser#41340](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41340): Opt in to some of the NIGHTLY_BUILD features
    - [Bug tor-browser#41343](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41343): Add -without-wam-sandboxed-libraries to mozconfig-linux-x86_64-dev for local builds
    - [Bug tor-browser-build#40585](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40585): Prune the manual more
    - [Bug tor-browser-build#40663](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40663): Do not ship bookmarks in tor-browser-build anymore
    - [Bug tor-browser-build#40669](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40669): Remove HTTPS-Everywhere keyring
    - [Bug tor-browser#41357](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41357): Enable browser toolbox debugging by default for dev builds
  - macOS
    - [Bug tor-browser-build#40158](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40158): Add support for macOS AArch64
    - [Bug tor-browser-build#40464](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40464): go 1.18 fails to build on macOS
  - Linux
    - [Bug tor-browser-build#40659](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40659): Error building goservice for linux in nightly build
  - Android
    - [Bug tor-browser-build#40640](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40640): Extract Gradle in the toolchain setup
