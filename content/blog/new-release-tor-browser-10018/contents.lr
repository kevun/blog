title: New Release: Tor Browser 10.0.18
---
pub_date: 2021-06-21
---
author: sysrqb
---
tags:

tor browser
tbb-10.0
tbb
---
categories: applications
---
summary: Tor Browser 10.0.18 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 10.0.18 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.0.18/">distribution directory</a>.</p>
<p>This version updates <a href="https://blog.torproject.org/node/2041">Tor to 0.4.5.9</a>, including important <a href="https://blog.torproject.org/node/2041">security fixes</a>. In addition, on Android, this version updates Firefox to 89.1.1, and NoScript to 11.2.8 This version includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-23/">security updates</a> to Firefox for Android.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser will <em><b>stop</b> supporting <u>version 2 onion services</u></em> later <em><b>this year</b></em>. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a>. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.0-desktop">Desktop</a> Tor Browser 10.0.17 and <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.0-android">Android</a> Tor Browser 10.0.16:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Tor to 0.4.5.9</li>
</ul>
</li>
<li>Android
<ul>
<li>Update Fenix to 89.1.1</li>
<li>Update NoScript to 11.2.8</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40055">Bug 40055</a>: Rebase android-componets patches on 75.0.22 for Fenix 89</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40165">Bug 40165</a>: Announce v2 onion service deprecation on about:tor</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40166">Bug 40166</a>: Hide "Normal" tab (again) and Sync tab in TabTray</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40167">Bug 40167</a>: Hide "Save to Collection" in menu</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40169">Bug 40169</a>: Rebase fenix patches to fenix v89.1.1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40170">Bug 40170</a>: Error building tor-browser-89.1.1-10.5-1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40432">Bug 40432</a>: Prevent probing installed applications</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40470">Bug 40470</a>: Rebase 10.0 patches onto 89.0</li>
</ul>
</li>
<li>Build System
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40290">Bug 40290</a>: Update components for mozilla89-based Fenix</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292009"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292009" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>sno (not verified)</span> said:</p>
      <p class="date-time">June 21, 2021</p>
    </div>
    <a href="#comment-292009">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292009" class="permalink" rel="bookmark">When will we get Snowflake…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When will we get Snowflake in the stable Tor Browser release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292011"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292011" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">June 21, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292009" class="permalink" rel="bookmark">When will we get Snowflake…</a> by <span>sno (not verified)</span></p>
    <a href="#comment-292011">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292011" class="permalink" rel="bookmark">https://blog.torproject.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please see <a href="https://blog.torproject.org/snowflake-in-tor-browser-stable" rel="nofollow">https://blog.torproject.org/snowflake-in-tor-browser-stable</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292017"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292017" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 21, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292009" class="permalink" rel="bookmark">When will we get Snowflake…</a> by <span>sno (not verified)</span></p>
    <a href="#comment-292017">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292017" class="permalink" rel="bookmark">They said 10.5 later this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>They said 10.5 later this month.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292028"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292028" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 22, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292009" class="permalink" rel="bookmark">When will we get Snowflake…</a> by <span>sno (not verified)</span></p>
    <a href="#comment-292028">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292028" class="permalink" rel="bookmark">Hope SnowFlake can be used…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hope SnowFlake can be used and fast in China</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292013"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292013" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Your name (not verified)</span> said:</p>
      <p class="date-time">June 21, 2021</p>
    </div>
    <a href="#comment-292013">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292013" class="permalink" rel="bookmark">Where is changelog? Stating…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where is changelog? Stating that something is changed is not a changelog.<br />
What was changed in tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292057"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292057" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">June 28, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292013" class="permalink" rel="bookmark">Where is changelog? Stating…</a> by <span>Your name (not verified)</span></p>
    <a href="#comment-292057">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292057" class="permalink" rel="bookmark">You may read more about the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You may read more about the changes on <a href="https://blog.torproject.org/node/2041" rel="nofollow">https://blog.torproject.org/node/2041</a></p>
<p>I'll link to it in the above post, as well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292015"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292015" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>file signature (not verified)</span> said:</p>
      <p class="date-time">June 21, 2021</p>
    </div>
    <a href="#comment-292015">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292015" class="permalink" rel="bookmark">&quot;DigiCert Timestamp 2021&quot;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"DigiCert Timestamp 2021" signature in the TBB 10.0.18-32Bit(File -&gt; rightclick -&gt; signature) is missing and i hope this is not a problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292063"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292063" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">June 28, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292015" class="permalink" rel="bookmark">&quot;DigiCert Timestamp 2021&quot;…</a> by <span>file signature (not verified)</span></p>
    <a href="#comment-292063">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292063" class="permalink" rel="bookmark">Thanks for asking. That is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for asking. That is not a problem, but that issue should be resolved now. Please download the installer again.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292024"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292024" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 22, 2021</p>
    </div>
    <a href="#comment-292024">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292024" class="permalink" rel="bookmark">android: is the exit-button…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>android: is the exit-button gone for a specific reason?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292058"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292058" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">June 28, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292024" class="permalink" rel="bookmark">android: is the exit-button…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292058">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292058" class="permalink" rel="bookmark">That&#039;s https://gitlab…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's <a href="https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40172" rel="nofollow">https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40172</a></p>
<p>The button should be restored in an upcoming version.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292117"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292117" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 03, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-292117">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292117" class="permalink" rel="bookmark">There are many complaints in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are many complaints in <a href="https://play.google.com/store/apps/details?id=org.torproject.torbrowser&amp;showAllReviews=true" rel="nofollow">reviews on the Play store</a> about the missing Quit/Exit button. But why? Is using the button better than swiping?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292127"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292127" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 06, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292117" class="permalink" rel="bookmark">There are many complaints in…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292127">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292127" class="permalink" rel="bookmark">No, that was simply missed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, that was simply missed during QA when we moved onto Fenix/Firefox 89.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-292030"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292030" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 23, 2021</p>
    </div>
    <a href="#comment-292030">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292030" class="permalink" rel="bookmark">Tor Browser is affected too:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Browser is affected too: <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1445211" rel="nofollow">https://bugzilla.mozilla.org/show_bug.cgi?id=1445211</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292032"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292032" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 23, 2021</p>
    </div>
    <a href="#comment-292032">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292032" class="permalink" rel="bookmark">Are there issues in running…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are there issues in running a snowflake proxy while running an obsf4 bridge? What should operators of other types of nodes know?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292328"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292328" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 14, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292032" class="permalink" rel="bookmark">Are there issues in running…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292328">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292328" class="permalink" rel="bookmark">This sort of answers some of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This sort of answers some of your questions:<br />
<a href="https://support.torproject.org/operators/multiple-relays/" rel="nofollow">https://support.torproject.org/operators/multiple-relays/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292044"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292044" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 24, 2021</p>
    </div>
    <a href="#comment-292044">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292044" class="permalink" rel="bookmark">I click on Tor Browser or…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I click on Tor Browser or start-tor-browser and nothing happens :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292059"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292059" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">June 28, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292044" class="permalink" rel="bookmark">I click on Tor Browser or…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292059">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292059" class="permalink" rel="bookmark">What operating system are…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What operating system are you using?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292055"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292055" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 27, 2021</p>
    </div>
    <a href="#comment-292055">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292055" class="permalink" rel="bookmark">A real bug that happens…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A real bug that happens sometimes when you paste&amp;go some URL in a new tab:<br />
08:16:41.540 TypeError: event.target.closest is not a function UrlbarInput.jsm:2172:26<br />
    _on_mousedown resource:///modules/UrlbarInput.jsm:2172<br />
    handleEvent resource:///modules/UrlbarInput.jsm:367</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292060"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292060" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">June 28, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292055" class="permalink" rel="bookmark">A real bug that happens…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292060">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292060" class="permalink" rel="bookmark">Can you reproduce this error…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you reproduce this error on demand, or does it happen randomly?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292067"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292067" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 29, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-292067">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292067" class="permalink" rel="bookmark">It happens rarely, but looks…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It happens rarely, but looks weird, like some site is blank.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-292074"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292074" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>SDan (not verified)</span> said:</p>
      <p class="date-time">June 29, 2021</p>
    </div>
    <a href="#comment-292074">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292074" class="permalink" rel="bookmark">Tor browser for Android,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor browser for Android, long press tabs icon:<br />
<a href="https://ibb.co/r5MT8qb" rel="nofollow">https://ibb.co/r5MT8qb</a><br />
Is there a reason for 2 new tab?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292076"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292076" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">June 29, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292074" class="permalink" rel="bookmark">Tor browser for Android,…</a> by <span>SDan (not verified)</span></p>
    <a href="#comment-292076">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292076" class="permalink" rel="bookmark">No,  &quot;New Tab&quot; should be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No,  "New Tab" should be removed, thanks for asking. We'll remove it in <a href="https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40174" rel="nofollow">https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40174</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292075"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292075" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>John (not verified)</span> said:</p>
      <p class="date-time">June 29, 2021</p>
    </div>
    <a href="#comment-292075">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292075" class="permalink" rel="bookmark">Sha256 values of *.exe.asc…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sha256 values of *.exe.asc files don't match which listed in sha256sums-signed-build.txt.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292077"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292077" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">June 29, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292075" class="permalink" rel="bookmark">Sha256 values of *.exe.asc…</a> by <span>John (not verified)</span></p>
    <a href="#comment-292077">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292077" class="permalink" rel="bookmark">That was caused when fixing…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That was caused when fixing <a href="https://blog.torproject.org/comment/292015#comment-292015" rel="nofollow">https://blog.torproject.org/comment/292015#comment-292015</a>.</p>
<p>This should be fixed now. Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292103"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292103" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Lia  (not verified)</span> said:</p>
      <p class="date-time">July 02, 2021</p>
    </div>
    <a href="#comment-292103">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292103" class="permalink" rel="bookmark">Ahhh ahhh ahhh</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ahhh ahhh ahhh</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292105"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292105" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>rod (not verified)</span> said:</p>
      <p class="date-time">July 02, 2021</p>
    </div>
    <a href="#comment-292105">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292105" class="permalink" rel="bookmark">Thank you for helping to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for helping to keep us free in person, in spirit and in truth!</p>
</div>
  </div>
</article>
<!-- Comment END -->
