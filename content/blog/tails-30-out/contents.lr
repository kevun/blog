title: Tails 3.0 is out
---
pub_date: 2017-06-13
---
author: tails
---
tags: anonymous operating system
---
_html_body:

<p>We are especially proud to present you Tails 3.0, the first version of Tails based on Debian 9 (Stretch). It brings a completely new startup and shutdown experience, a lot of polishing to the desktop, security improvements in depth, and major upgrades to a lot of the included software.</p>

<p>Debian 9 (Stretch) will be <a href="https://lists.debian.org/debian-devel-announce/2017/05/msg00002.html" rel="nofollow">released on June 17</a>. It is the first time that we are releasing a new version of Tails almost at the same time as the version of Debian it is based upon. This was an important objective for us as it is beneficial to both our users and users of Debian in general and strengthens our <a href="https://tails.boum.org/contribute/relationship_with_upstream/" rel="nofollow">relationship with upstream</a>:</p>

<ul>
<li>Our users can benefit from the cool changes in Debian earlier.</li>
<li>We can detect and fix issues in the new version of Debian while it is still in development so that our work also benefits Debian earlier.</li>
</ul>

<p>This release also fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_2.12/" rel="nofollow">many security issues</a> and users should upgrade as soon as possible.</p>

<h1><a rel="nofollow"></a>Changes</h1>

<h2><a rel="nofollow"></a>New features</h2>

<h3><a rel="nofollow"></a>New startup and shutdown experience</h3>

<ul>
<li><em>Tails Greeter</em>, the application to configure Tails at startup, has been completely redesigned for ease of use:
<ul>
<li>All options are available from a single window.</li>
<li>Language and region settings are displayed first to benefit our international audience.</li>
<li>Accessibility features can be enabled from the start.</li>
</ul>
<p>This has been a long process, started three years ago with the UX team of <a href="https://paris.numa.co/" rel="nofollow">NUMA Paris</a> and lead only by volunteers. Join us on <a href="https://tails.boum.org/about/contact/#tails-ux" rel="nofollow">tails-ux@boum.org</a> to participate in future designs!</p>
<p><img src="https://tails.boum.org/news/version_3.0/tails-greeter.png" width="451" /></p></li>
<li>The shutdown experience has also been redesigned in order to be:
<ul>
<li>More reliable. It was crashing on various computers with unpredictable results.</li>
<li>More discrete. The screen is now totally black to look less suspicious.</li>
</ul>
<p>Technically speaking, it is now using the <a href="https://tails.boum.org/contribute/design/memory_erasure/" rel="nofollow">freed memory poisoning feature</a> of the Linux kernel.</p></li>
</ul>

<h3><a rel="nofollow"></a>Polishing the desktop</h3>

<ul>
<li>We switched to the default black theme of GNOME which has a more modern and discrete look:
<p><img src="https://tails.boum.org/news/version_3.0/black-theme.png" width="683" /></p></li>
<li>Tails 3.0 benefits from many other small improvements to the GNOME desktop:
<ul>
<li><em>Files</em> has been redesigned to reduce clutter and make the interface easier to use. Several new features have been added, such as the ability to rename multiple files at the same time and the ability to extract compressed files without needing a separate application.
<p><img src="https://tails.boum.org/news/version_3.0/batch-rename.png" width="552" /></p></li>
<li>The notification area has been improved to allow easy access to previous notifications. Notification popups have also been repositioned to make them more noticeable.
<p><img src="https://tails.boum.org/news/version_3.0/message-list.png" width="552" /></p></li>
<li>Shortcut windows have been added to help you discover keyboard shortcuts in GNOME applications.
<p>For example, press Ctrl+F1 in <em>Files</em> to display its shortcut window.</p></li>
</ul>
</li>
</ul>

<h3><a rel="nofollow"></a>Security improvements in depth</h3>

<ul>
<li>Tails 3.0 works on <a href="https://en.wikipedia.org/wiki/x86%2D64" rel="nofollow">64-bit computers</a> only and not on 32-bit computers anymore. Dropping hardware support, even for a small portion of our user base, is always a hard decision to make but being 64-bit only has important security and reliability benefits. For example, to protect against some types of security exploits, support for the <a href="https://en.wikipedia.org/wiki/NX%5Fbit" rel="nofollow">NX bit</a> is compulsory and most binaries are hardened with <a href="https://en.wikipedia.org/wiki/Position%2Dindependent%5Fcode" rel="nofollow">PIE</a> which allows <a href="https://en.wikipedia.org/wiki/Address%5Fspace%5Flayout%5Frandomization" rel="nofollow">ASLR</a>.
</li>
</ul>

<ul>
<li>Update <em>Tor Browser</em> to 7.0 (based on Firefox 52 ESR) which is <a href="https://developer.mozilla.org/en-US/Firefox/Multiprocess_Firefox" rel="nofollow">multiprocess</a> and paves the way to <a href="https://wiki.mozilla.org/Security/Sandbox" rel="nofollow">content sandboxing</a>. This should make it harder to exploit security vulnerabilities in the browser.</li>
</ul>

<h3><a rel="nofollow"></a>Major upgrades to included software</h3>

<ul>
<li>Most included software has been upgraded in Debian 9, for example:
<ul>
<li><em>KeePassX</em> from 0.4.3 to 2.0.3<br />
Your password database will be<br />
<a href="https://tails.boum.org/doc/encryption_and_privacy/manage_passwords/#migrate" rel="nofollow">migrated automatically</a> to the new format of <em>KeePassX</em> 2.</li>
<li><em>LibreOffice</em> from 4.3.3 to 5.2.6</li>
<li><em>Inkscape</em> from 0.48.5 to 0.92.1</li>
<li><em>Audacity</em> from 2.0.6 to 2.1.2</li>
<li><em>Enigmail</em> from 1.8.2 to 1.9.6</li>
<li><em>MAT</em> from 0.5.2 to 0.6.1</li>
<li><em>Dasher</em> from 4.11 to 5.0</li>
<li><em>git</em> from 2.1.4 to 2.11.0</li>
</ul>
</li>
</ul>

<h2><a rel="nofollow"></a>Upgrades and changes</h2>

<ul>
<li>The <em>Pidgin</em> tray icon was removed from the top navigation bar and replaced by popup notifications.
<p><img src="https://tails.boum.org/news/version_3.0/pidgin.png" width="683" /></p></li>
<li><em>Icedove</em> was renamed as <em>Thunderbird</em>, its original name, inheriting this change from Debian.</li>
<li>The search box and the search feature of the address bar of the <em>Unsafe Browser</em> were removed. (<a href="https://labs.riseup.net/code/issues/12540" rel="nofollow">#12540</a>)</li>
<li>The read-only option of the persistent storage was removed. It was used by very few users, created confusion, and lead to unexpected issues. (<a href="https://labs.riseup.net/code/issues/12093" rel="nofollow">#12093</a>)</li>
</ul>

<h2><a rel="nofollow"></a>Fixed problems</h2>

<ul>
<li>The new <em>X.Org</em> display server in Tails 3.0 should work on more newer<br />
graphical hardware.</li>
<li>UEFI boot has been fixed on some machines (ThinkPad X220).</li>
<li>MAC spoofing has been fixed on some network interfaces (TP-Link WN725N). (<a href="https://labs.riseup.net/code/issues/12362" rel="nofollow">#12362</a>)</li>
</ul>

<p>For more details, read our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">changelog</a>.</p>

<h1><a rel="nofollow"></a>Known issues</h1>

<ul>
<li>Tails fails to start on some computers with <a href="https://tails.boum.org/support/known_issues/#xorg-driver" rel="nofollow">Intel graphical hardware</a>.</li>
<li>Some users have reported problems during the migration from <em>Icedove</em> to <em>Thunderbird</em>, in particular that <em>Thunderbird</em> doesn't start.
<p>If this happens to you, please <a href="https://tails.boum.org/doc/first_steps/bug_reporting/#whisperback" rel="nofollow">send us a <em>WhisperBack</em> report</a> without restarting Tails.</p></li>
</ul>

<p>See the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h1><a rel="nofollow"></a>Get Tails 3.0</h1>

<ul>
<li>To install, follow our <a href="https://tails.boum.org/install/" rel="nofollow">installation instructions</a>.</li>
<li>To upgrade, all users have to do a <a href="https://tails.boum.org/upgrade/" rel="nofollow">manual upgrade</a>.</li>
<li><a href="https://tails.boum.org/install/download/" rel="nofollow">Download Tails 3.0.</a></li>
</ul>

<h1><a rel="nofollow"></a>What's coming up?</h1>

<p>Tails 3.1 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for August 8.</p>

<p>Have a look at our <a href="https://tails.boum.org/contribute/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/donate/#3.0" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev" rel="nofollow">talk to us</a>!</p>

