title: 2022 Fundraising Results: thank you!
---
author: alsmith
---
pub_date: 2023-02-02
---
categories:

fundraising
---
summary: Everyone in our community deserves a big THANK YOU for supporting the Tor Project during the year-end campaign.
---
body:

Every year, the Tor Project asks our community for financial support during October, November, and December. We do this because we’re a 501(c)(3) nonprofit, and your help keeps Tor free for everyone to use. In the new year, we aim to publish clear and transparent results of our fundraising—that’s what this post is all about.

**First, everyone in our community deserves a big THANK YOU for supporting the Tor Project during the campaign.** Together, you raised $367,674 to power privacy online! Additionally please help us thank the Friends of Tor who provided the generous match during the campaign—[Aspiration](https://aspirationtech.org/), [Jon Callas](https://twitter.com/joncallas), [Craig Newmark](https://twitter.com/craignewmark), [Wendy Seltzer](https://twitter.com/wseltzer), and several anonymous supporters.

We’d also like to welcome and thank the 1,790 folks who made their first contribution to Tor during this time, whether you made a one-time gift or committed to monthly donations. We’re extremely grateful to have you here as part of our community.

By making a contribution to the Tor Project, you are:

* **Helping users overcome internet censorship** with localized user support, new bridge and Tor Browser distribution mechanisms with our [new Telegram channels](https://t.me/TorProject), better GetTor support, and improvements to the Tor Browser’s user interface and censorship circumvention tools based on user research.** **
* **Advancing the work to make Tor more secure and agile with Arti**, the project to rewrite Tor in Rust. In 2023, we anticipate releasing Arti 2.0.0, which is the next step in replacing the Tor C client with a more secure, easier to maintain Rust implementation.
* **Supporting the next generation Tor client. **In partnership with [LEAP and Guardian Project](https://blog.torproject.org/tor-community-partners/), we’ve been working on a [Tor VPN client for Android](https://www.youtube.com/watch?v=uSyBZ7GIzJY) over the last year. This is quite a big change for Tor, and one we prioritized because of the needs of our community and for the future sustainability of the Tor Project. Next year, our goal is to release a minimum viable product for internal tests—and your support makes that possible.
* **Powering privacy online for the millions of people who need it. **With your support, those who cannot make contributions are able to use Tor for free.

Now let’s take a look at the results of this year’s campaign compared to the last few years, what influences these results, and what’s next in 2023 with your support.

| Year | Contributions during year-end campaign |
| ---- | -------------------------------------- |
| 2020 | $376,315                           	|
| 2021 | $940,361                           	|
| 2022 | $367,674                           	|

Why is 2022 so different from 2021? The last several years have been unpredictable for everyone. In 2021, cryptocurrency gifts skyrocketed past any of our predictions—about 60% of the total raised in 2021 came in cryptocurrency. In 2022, we saw economic slowdowns, massive tech sector layoffs, cryptocurrency market collapse, and general uncertainty about the future. It’s clear that the results of fundraising efforts this year reflect the strain our community feels at this moment.

Over the last five years, the Tor Project has invested in strategic fundraising activities, financial oversight, board involvement, and operations infrastructure that has made the organization resilient during turbulence. To plan for unpredictable circumstances, we now account for a certain percentage of risk in every fundraising plan and budget. The payoff is that we’re strong, stable, and sustainable because we’ve accounted for rejected applications or a downturn in the economy. If you’ve followed Tor for a while, you will know that this hasn't always been the case—and the fact that we’re here is worth celebrating!

In 2022 specifically, we made a choice to consider the immense cryptocurrency gifts made during 2021 as an anomaly. We knew that the market was changing and that we could not build our budget for the next year on shifting sands. As such, we set realistic targets that we’re seeing match up closely to reality.

Even though there’s been a downturn, even though cryptocurrency markets are ever-changing, **the Tor Project is in a strong position**. We are still on track to meet (and likely exceed) our fundraising goals for the year (which ends in June, as a reminder). And even if external factors mean we don’t reach those goals, we have set the organization up to weather future uncertainty with healthy reserves.

Looking beyond individual giving, we have pushed our boundaries in terms of strengthening our community of supporters in new ways: from organizations rallying together to improve onion services; to welcoming [new funders](https://www.torproject.org/about/sponsors/) like [FUTO](https://futo.org/grants/), [#StartSmall](https://docs.google.com/spreadsheets/d/1-eGxq2mMoEGwgSpNVL5j2sa6ToojZUZ-Zun8h2oBAR4/edit#gid=0), and the [International Republican Institute](https://www.iri.org/); renewing members as part of our [membership program](https://www.torproject.org/about/membership/); and advancing existing projects with long-time supporters and [partner organizations](https://blog.torproject.org/tor-community-partners/).

In 2023, we look forward to our continued collaboration with you, our community, to stand up for the human right to privacy, freedom of expression, and access to information. [Keep up with progress and get the latest Tor news by subscribing to our newsletter](https://newsletter.torproject.org)!
