title: Arti 1.1.0 is released: Anti-censorship support!
---
author: nickm
---
pub_date: 2022-11-30
---
categories: announcements
---
summary:

Arti 1.1.0 is released and ready for download, with support for
bridges and pluggable transports!
---
body:

Arti is our ongoing project to create an next-generation Tor client in
Rust.  In September, we released [Arti 1.0.0], our first stable
release.  Now we're announcing the next feature release, **Arti 1.1.0**.

Arti 1.1.0's claim to fame is its support for Tor's anti-censorship
features: You can now use Arti with [bridges] and [pluggable transports]!
(We have tested it with [obfs4proxy] and [snowflake].)

Though it may only take a few lines to describe, achieving this goal has required a
large amount of work over the past months. For a complete list of all
the changes that we needed to make, have a look at our [CHANGELOG].

These features are still very new, so there are likely to be bugs, and
the user experience may not yet be optimal. (In particular, there are a
bunch of spurious warnings and error messages in the logs.) Nonetheless,
we believe that the quality of these features is good enough to be used.
When you find bugs, please report them [on our
bugtracker](https://gitlab.torproject.org/tpo/core/arti/).  You can
[request an account](https://gitlab.onionize.space/) or [report a bug
anonymously](https://anonticket.onionize.space/).

For more information on using Arti, see our top-level [README], and
the documentation for the [`arti` binary].  For (draft) information on
how to use bridges with Arti, see our [bridges HOWTO].

Thanks to everybody who has helped with this release, including
Alexander Færøy, arnabanimesh, breezykermo, Dimitris Apostolou,
EliTheCoder, Emil Engler, Gabriel de Perthuis, Jim Newsome, Reylaba, and
Trinity Pointard.

Also, our deep thanks to [Zcash Community Grants] for funding the
development of Arti!


[Arti 1.0.0]: https://blog.torproject.org/arti_100_released/
[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-110-30-november-2022
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[bridges]: https://bridges.torproject.org/
[bridges HOWTO]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/doc/bridges.md
[bug tracker]: https://gitlab.torproject.org/tpo/core/arti/-/issues/
[obfs4proxy]: https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/obfs4
[pluggable transports]: https://tb-manual.torproject.org/circumvention/
[snowflake]: https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake
