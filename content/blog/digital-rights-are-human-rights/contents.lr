title: Digital Rights are Human Rights
---
pub_date: 2019-12-10
---
author: ggus
---
tags:

take back the internet
internet freedom
surveillance
human rights
---
categories: human rights
---
summary:

In the recent years, the international debate around human rights started to include and analyze our relations and interactions with technology, including how we are able or not able to exercise our human rights when we're on the internet. We believe human rights apply to all spaces we occupy, offline and online. 
---
_html_body:

<p>In honor of the United Nations' proclamation and adoption of the Universal Declaration of Human Rights (UDHR) in 1948, December 10 is Human Rights Day. Unlike a linear and accumulative process, the struggle for and implementation of human rights follows an unplanned and unique path in each community, and there are often setbacks. In the recent years, the international debate around human rights started to include and analyze our relations and interactions with technology, and how we are able or not able to exercise our human rights when we're on the internet. We believe human rights apply to all spaces we occupy, offline and online. </p>
<p>Politics and technologies have also been closely intertwined: how we conceived our collective life has been fundamental to the ways in which we conceive our technologies. For the ways in which early internet activist movements articulated the importance of human rights online, the <a href="ttps://www.democracynow.org/2019/11/27/1999_wto_protests_20_years_later">anti-WTO protests in Seattle of nearly 20 years ago</a> serve as a powerful example. Although different people and movements have outlined what <a href="https://www.eff.org/cyberspace-independence">online utopias might look like</a>, the internet as a liberation tool has been hijacked to become <a href="https://blog.torproject.org/we-can-choose-internet-without-surveillance">a tool for mass surveillance</a>.  </p>
<p>Today academics, researchers, and hackers are discussing algorithms implemented in new digital technologies and how they are being weaponized with human targets. Automated systems are implemented to capture our digital traces, monitor our daily lives with no accountability. With complete lack of transparency, digital surveillance has real impact on human lives: discrimination, preventing people from organizing, enabling targeting and arrest of marginalized groups, and even blocking purchases and access to services, as well as access to information.</p>
<p>These biased, weaponized algorithms perpetuate unequal societies, marginalizing even more vulnerable groups. The right to access an open and free internet, use privacy enhancing technologies and, especially, the access to and defense of cryptography, have become critical and fundamental against a dystopic mass surveillance and censored society. The discussion can be expanded to how technologies are being built in ways that threaten human rights, and how we can build another internet which advances and respects human rights. </p>
<p>At the Tor Project, we build technologies that defend and promote the human rights to privacy and freedom. More than just a way to exercise an individual right, it’s a collective collaboration and movement that generates a common good for all. Everyone can use this open and secure network as infrastructure that has privacy as a <a href="https://blog.torproject.org/category/tags/heart-internet-freedom">default feature of its design</a>. The Tor network promotes a radical decentralization with <a href="https://community.torproject.org/onion-services/">onion services</a>, so you can run your own service without a dedicated IP address or having a domain name, all done privately and securely. Tor also promotes net neutrality, since it doesn't modify the traffic based on who is accessing it or which sites they are visiting. In other words, it's what we've always wanted the internet to be.</p>
<p>A few months ago, we told you how the <a href="https://blog.torproject.org/strength-numbers-usable-tools-dont-need-be-invasive">Community and UX teams</a> are implementing a <a href="https://blog.torproject.org/reaching-people-where-they-are">User Feedback Program</a>, a program that combines user experience research and digital security training for human rights defenders.  </p>
<p>This program is the result of more than two years of work organizing Tor trainings with human rights defenders in the Global South. We <a href="https://blog.torproject.org/pride-and-privacy">traveled to countries where governments outlaw or punish being LGBTQ+</a> and block trans and gays rights websites with accusations of immorality. We know that the tools developed by Tor protect many activists in <a href="https://www.qurium.org/fighters/albertine-watchdog-in-case-our-identities-were-discovered-the-risks-include-assassinations/">very hostile situations around the world</a>. We listened to <a href="https://blog.torproject.org/how-has-tor-helped-you-send-us-your-story">many stories</a> from activists telling us that the <a href="https://blog.torproject.org/better-internet-possible-ive-seen-it">internet wasn’t always like that</a> in their communities, and that they were glad to explore freely again after installing Tor Browser. </p>
<p>Fighting for human rights includes <a href="https://blog.torproject.org/take-back-internet-wins-victories">victories and setbacks</a>, but our selective memory sometimes forgets one or other. The only clear orientation is that we need is to stand for our rights. If you are a human rights defender, anonymity loves company, so come with us, <a href="https://torproject.org/download">use Tor</a>, and <a href="https://torproject.org/donate/donate-tbi-bp7">help us take back the internet</a>. </p>
<p>There are many ways to <a href="https://blog.torproject.org/there-are-many-ways-help-tor-take-back-internet">donate to us and support our work</a>. With your help, we can keep Tor growing and improving to be what the world needs: a way to help take back the internet for freedom and human rights. </p>
<p><img alt="donate button" src="/static/images/blog/inline-images/tor-donate-button-purple_4.png" width="250" class="align-center" /></p>

---
_comments:

<a id="comment-286072"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286072" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2019</p>
    </div>
    <a href="#comment-286072">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286072" class="permalink" rel="bookmark">&gt; But what you may not have…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; But what you may not have considered is the equally paramount importance of financial privacy.</p>
<p>And medical history.  And small-d democratic participation history (voter records, communications with elected officials and with government agencies).  And geolocation history.  </p>
<p>I strongly support framing TP's </p>
<p>o appeal for grass-roots donations from ordinary citizens around the world</p>
<p>o program for growing and diversifying (geographically and financially) the user base</p>
<p>in terms of threats faced by ordinary citizens.</p>
<p>But there is a second strategic avenue which I hope TP and its partners (Debian, Mozilla, Tails) can pursue: educating government and party officials (at all levels, in all nations), as well as medical providers, lawyers, schools, public utilities and other institutions holding vast amounts of dangerous information about everyone they serve, about how Tor products can be used right now to make essential information sharing safer.   For example, I believe that all doctors and lawyers should be taught in medical/law school how to teach their colleagues/clients to use OnionShare routinely for sharing sensitive personal information of the kinds most sought by spooks and others who wish to do inexpensive but devastating harm to ordinary citizens (or the nations of which they are citizens).</p>
<p>I would love to see privacy researchers start exploring how tools which are already available such as OnionShare could perhaps have been used, at little cost or effort to local government officials, to make municipal governments much more resistant to devastating data breaches.  In the US, some state Attorney Generals have been compiling public reports on major data breaches in their state, and I believe the EU has even more disclosure.   Reading some of these reports can quickly give officials a much clearer idea of the scope and depth of the problems, and disabuse them of the illusion that the most common response (ignore the harm to ordinary citizens and governments caused by breaches) is unacceptable and ultimately suicidal for their own employer, whether a local government agency, medical provider, credit union, or local public utility.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286073"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286073" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2019</p>
    </div>
    <a href="#comment-286073">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286073" class="permalink" rel="bookmark">&gt; These biased, weaponized…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; These biased, weaponized algorithms perpetuate unequal societies, marginalizing even more vulnerable groups.</p>
<p>For example:</p>
<p>o in Brazil, Bolsonaro openly advocates genocide of indigeneous peoples struggling to protect the Amazon rain forest from devastating deforestation,</p>
<p>o in Canada, it has just been revealed, the RCMP deployed snipers who targeted pipeline protesters, many of whom have been indigeneous people protesting ecologically dangerous pipelines being constructed on or near reservations.</p>
<p>These groups have already been subjected, in previous centuries, to previous waves of severe mistreatment, including genocidal attacks, and now are being subjected to a surge of 21st century genocidal threats.</p>
<p>Some endangered subpopulations appear to be coming under simulataneous life-threatening attacks in every part of the world, including persons suffering dehousing and persons suffering from a chronic mental illness (two groups with significant overlap, in all parts of the world).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286075"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286075" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2019</p>
    </div>
    <a href="#comment-286075">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286075" class="permalink" rel="bookmark">Just wanted to stress a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just wanted to stress a critically important point about human rights:</p>
<p>Everyone who has read Stanton's famous essay, "The Ten Stages of Genocide", appreciates why it is increasingly likely the major genocides are coming to the Americas, Europe, and possibly Asia (India and China) and Africa too.  It could be argued that this is already happening in parts of the MENA region (Middle East-North Africa).  Even Antarctica is in danger of becoming a "hot" conflict zone.</p>
<p>But it is crucial to understand that when it comes to the perpetrators of genocide, there are no "good-uns" and "bad-uns",  no "our guys" and "their guys"--- just killers.  Who must be brought to justice, if necessary in the ICC.</p>
<p>In the USA, the Drump admin (and also the political leadership of a number of EU countries) is taking exactly the opposite stance, by making explicit distinctions between religious and ethnic groups which are official recognized as deserving of greater protections--- or of greater state-sponsored abuses:</p>
<p>thehill.com<br />
Trump's executive order raises important questions about Jewish identity and free speech<br />
Glenn C. Altschuler and David Wippman, opinion contributors<br />
22 Dec 2019</p>
<p>&gt; On Dec. 11, President Donald Trump signed an Executive Order Combating Anti-Semitism. The order’s stated intent is to ensure vigorous enforcement of existing civil rights law against “prohibited forms of discrimination rooted in anti-Antisemitism.” While the goal of combating anti-Antisemitism is, of course, laudable, the order raises important concerns about defining Jews as a racial or national group and the suppression of free speech... [The] order focuses only on discrimination against Jews. It does not mention Muslims, Sikhs or other religious groups.</p>
<p>The full essay is well-worth reading.  It may also be valuable to recall that one of the few things GWB did right just after 9/11 was to nix the immediate calls for harsh retaliations targeting Muslims simply for following Islam.  To be sure, such things were immediately implemented by some states and local agencies such as NYPD (bugging mosques, Islamic civic organizations, Muslim-owned businesses, etc), but for some years were for the most part not implemented by the US federal government--- except in deepest secrecy.  Bush's public disapprobation of the open pursuit of anti-Islamic policies no doubt helped tamp down street violence targeting Muslims in America for some years.  </p>
<p>But, tragically, the Drump administration has done a one-eighty on this point.  The current administration seeks to exploit ancient hatreds to divide and conquer the ranks of potential political dissidents (from the Drumpian party line).  Hence the need for Jewish-Americans to respond to Drump's EO with a firm "no thanks!", while calling out to their Congressional representatives for a return to the principle that all threatened sub populations require and deserve protection from discrimination, harassment, and racist thuggery of the kind which Drump always goes out of his way to encourage at his own political rallies.</p>
<p>When it comes to the perpatrators of genocide, there are no "good-uns" and "bad-uns", no "our guys" and "their guys", just mass-murderers plain and simple.  The law should never make a distinction between hate crimes targeting "our people" versus "their people", because such laws always encourage rather than preventing genocide.</p>
<p>One of the most powerful aspects of privacy/anonymity enhancing software tools like Tor is that by their very nature these tools avoid explicit discrimination, and TP makes every effort (e.g. through translation and in-country trainings) to avoid unintentional bias.  This aspect of free open-source citizen-empowering software tools is a point worth stressing whenever NGOs reach out to American officials who are seriously considering implementing laws or policies which would make Tor or similar tools illegal in the US (and hence, effectively, throughout the world).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-286076"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286076" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2019</p>
    </div>
    <a href="#comment-286076">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286076" class="permalink" rel="bookmark">&gt;  the anti-WTO protests in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt;  the anti-WTO protests in Seattle of nearly 20 years ago serve as a powerful example. </p>
<p>For anyone who does not know what ggus is talking about, here is another useful link:</p>
<p><a href="https://en.wikipedia.org/wiki/1999_Seattle_WTO_protests" rel="nofollow">https://en.wikipedia.org/wiki/1999_Seattle_WTO_protests</a></p>
<p>Then Seattle Police Department chief Norm Stamper later wrote several op-eds expressing regret that his department had reacted with militarized police assaults on the protesters (including pepper-spraying people who were already in police custody, held in improvised cages), mass arrests of everyone found on the street, and choking entire neighborhoods with vast clouds of tear gas:</p>
<p><a href="https://en.wikipedia.org/wiki/Norm_Stamper" rel="nofollow">https://en.wikipedia.org/wiki/Norm_Stamper</a></p>
<p>The WTO protests were one of the first "un-authorized" protests in the US, and the police reaction was denounced around the world.  For better or worse, many police departments in many nations have used the disastrous 1999 SPD tactics as an example of what -not- to do.</p>
<p>Evidently police in India have not gotten the message, however:</p>
<p>truthdig.com<br />
Amid Citizenship Law Outcry, Indian Authorities Ban Protests<br />
20 Dec 2019</p>
<p>&gt; NEW DELHI — Police banned public gatherings in parts of the Indian capital and other cities for a third day Friday and cut internet services to try to stop growing protests against a new citizenship law that have left 11 people dead and more than 4,000 others detained.  Thousands of protesters stood inside and on the steps of New Delhi’s Jama Masijd, one of India’s largest mosques, after Friday afternoon prayers, waving Indian flags and shouting slogans against the government and the citizenship law, which critics say threatens India’s secular democracy in favor of a Hindu state.  Police had banned a proposed march from the mosque to an area near India’s Parliament, and a large number of officers were waiting outside the mosque. Late Friday, police sprayed protesters with a water cannon to keep them from marching toward a monument in Central Delhi where protests have been converging, about 4 kilometers (2 1/2 miles) away... Police fired tear gas and used batons to disperse the protesters in Muzzaffarnagar, Saharanpur, Firozabad and Gorakhpur.</p>
<p>US viewers may have noticed Indian police officers using wooden staves to beat protesters.  That happens to be a tactic which was carried over from the days of the Raj.  A factoid which emphasizes the point that when it comes to the suppression of dissidents, all governments think more or less alike.  Some, to be sure, are more openly brutal than most, such as the Russian government, where special police often inflict beatings which put large numbers of protesters in the hospital.</p>
<p>US mass media has widely reported on crude Chinese retaliations against bloggers (including sport stars and actors) who have spoken out against the increasingly horrific oppression of Uyghurs, which is laudable, but unfortunately these news organizations have largely omitted to call foul when other governments do the same thing. Here is a rare exception:</p>
<p>thehill.com<br />
Top Indian official canceled congressional meeting over inclusion of Jayapal: report<br />
Marty Johnson<br />
19 Dec 20191</p>
<p>&gt; India's external affairs minister called off a meeting with senior congressional members after the group of lawmakers refused to exclude Rep. Pramila Jayapal (D-Wash.), who has been critical of India's policies in Kashmir.</p>
<p>Jayapal, incidently, was born in India and is a member of the Human Rights Caucus (in the US Congress) who just happens to be the representative in the U.S. House of Tor Project, at least if you go by mailing address.  And she obviously has grasped a key point about state-sponsored oppression of minority groups:</p>
<p>When it comes to the oppression of minorities, there are no "good-uns" and "bad-uns", no "our guys" or "their guys", there are just government oppressors.  And all of us (ordinary citizens) must join ranks to speak out against all of them (the state-sponsored oppressors).</p>
<p>I hope Tor Project will consider reaching out to her with suggestions for how threatened peoples (and local government agencies, political parties, hospitals, NGOs, and small business such as medical or legal practices) can use tools like Tor to protect both themselves and the ordinary citizens they serve from dangerous phenomena such as real-time-bidding which are in effect building a system of corporate-enabled mass discrimination in America, which some are calling the "New Jim Crow".</p>
</div>
  </div>
</article>
<!-- Comment END -->
