title: Tor at the Heart: Onion Browser (and more iOS Tor)
---
pub_date: 2016-12-06
---
author: mtigas
---
tags:

mobile
heart of Internet freedom
Onion Browser
iphone
---
categories: onion services
---
_html_body:

<p><i>During the month of December, we're highlighting other organizations and projects that rely on Tor, build on Tor, or are accomplishing their missions better because Tor exists. Check out our blog each day to learn about our fellow travelers. And please support the Tor Project! We're at the heart of Internet freedom.</i><br />
<a href="https://torproject.org/donate/donate-blog6" rel="nofollow">Donate today</a>!</p>

<p><b>Onion Browser</b></p>

<p><a href="https://mike.tig.as/onionbrowser/" rel="nofollow">Onion Browser</a> is an open-source iOS web browser that connects to Tor. The app has been available in the Apple App Store since 2012; it was previously $0.99 but recently became <a href="https://mike.tig.as/blog/2016/12/05/onion-browser-now-free/" rel="nofollow">free of charge</a>. You can download it in the App Store <a href="https://itunes.apple.com/us/app/onion-browser/id519296448?mt=8&amp;at=10l9R4" rel="nofollow">here</a> and access the source code <a href="https://github.com/mtigas/OnionBrowser" rel="nofollow">on GitHub</a>.</p>

<p>In addition to Tor support, Onion Browser features an experimental NoScript-like mode, user agent spoofing, and (since August) support for obfs4 and meek bridges.</p>

<p>It’s primarily developed by <a href="https://mike.tig.as/" rel="nofollow">Mike Tigas</a>, who works as a developer and investigative journalist at <a href="https://www.propublica.org/" rel="nofollow">ProPublica</a> by day. (Did you know? ProPublica is one of the first major news sites to be <a href="https://www.propublica.org/nerds/item/a-more-secure-and-anonymous-propublica-using-tor-hidden-services" rel="nofollow">available via an onion site</a> — <a href="https://www.propub3r6espa33w.onion" rel="nofollow">propub3r6espa33w.onion</a>) The app is an independent community project and is supported by <a href="https://www.patreon.com/OnionBrowser" rel="nofollow">Patreon backers</a> and other donors (<a href="https://mike.tig.as/onionbrowser/#support-project" rel="nofollow">read more about supporting Onion Browser here</a>), with some key support also coming from the <a href="https://guardianproject.info/" rel="nofollow">Guardian Project</a>.</p>

<p>Onion Browser isn’t the prettiest app, lacking features like tabbed browsing, and it is starting to show it’s age a bit. But it still receives regular security updates and a new user interface is <a href="https://twitter.com/mtigas/status/805628113174589440" rel="nofollow">actively being developed</a> (discussed in full below).</p>

<p><b>Challenges on iOS</b></p>

<p>Tor hasn't been well-represented on iOS over the years for a variety of reasons, mostly due to system peculiarities on the iOS platform. And although there’s a version of Firefox for iOS, several challenges prevent the interoperability that Tor developers are accustomed to on other platforms.</p>

<p>The most glaring restriction on iOS is that you're not allowed to fork subprocesses. Tor must be compiled into the app binary and hacked to run as a thread <i>inside</i> the app process to work on iOS. Among other things, this means that a system-wide Tor app, like Orbot on Android, is simply not possible on the platform. (At least, not yet: read about iCepa below!) And simply relying on another app’s Tor instance — as some tools do with Tor Browser Bundle — also doesn’t work on iOS, since <i>all</i> of an app’s functionality is halted soon after a user switches out of the app.</p>

<p>Even after solving the problem of just getting Tor to run, several other quirks prevent a lot of the functionality of Tor Browser (or even Orfox) from being easily reimplemented on iOS:</p>

<ul>
<li>You're not allowed to implement your own browser engine and must use the WebKit framework built into the operating system. This separates Onion Browser from Tor Browser and Orfox, which are browsers based on Firefox Gecko. (On the other hand, this inadvertently made Onion Browser immune to the <a href="https://blog.mozilla.org/security/2016/11/30/fixing-an-svg-animation-vulnerability/" rel="nofollow">Firefox vulnerability</a> targeting Tor Browser users last week.)</li>
<li>Only the older WebKit API (<a href="https://developer.apple.com/reference/uikit/uiwebview" rel="nofollow">UIWebView</a>) allows control over the SOCKS settings of the browser stack, so that we can configure it to use Tor. The newer framework (<a href="https://developer.apple.com/reference/webkit/wkwebview" rel="nofollow">WKWebView</a>) always uses your system proxy settings and can’t be reconfigured by an app at runtime. The APIs also contain vastly different functionality so that it's not always possible to convert code relying on one API to use the other. Firefox for iOS uses the newer WKWebView framework, which unfortunately means that much of the work on Firefox for iOS is quite difficult to use in a Tor-supporting iOS browser.</li>
<li>The WebKit APIs don’t allow a lot of control over the rendering and execution of web pages, making a Tor Browser-style security slider very difficult to implement. Many multimedia features on iOS also bypass the browser network stack — in particular, the iOS video player doesn’t use the same network stack as WebKit and therefore any browser action that launches the native video player may possibly leak traffic outside of Tor. Onion Browser tries to provide <i>some</i> functionality to block JavaScript and multimedia, but these features aren’t yet as robust as on other platforms.</li>
</ul>

<p><b>iOS developments in the community</b></p>

<p>Despite the challenges, there are quite a few positive developments on the horizon — both around Onion Browser and the larger Tor iOS landscape.</p>

<p><a href="https://github.com/jcs/endless" rel="nofollow">Endless</a> is an open source browser for iOS that uses the older UIWebView API and thus can be modified to support Tor. It adds a lot of important features over the existing Onion Browser, like <a href="https://twitter.com/mtigas/status/805628113174589440" rel="nofollow">a nicer user interface with tabbed browsing</a>, HTTPS Everywhere, and HSTS Preloading. There’s a <b>new version of Onion Browser</b> in the works that’s based on Endless that will hopefully enter beta testing this month.</p>

<p>The <a href="https://developer.apple.com/reference/networkextension" rel="nofollow">NetworkExtension framework</a> introduced in iOS 9 allows writing custom VPN software that the iOS system can use. A small coalition of Tor iOS developers are working on <b>a tool called <a href="https://github.com/iCepa/iCepa" rel="nofollow">iCepa</a></b> to use this framework to provide a Tor VPN to the entire phone — similar to the VPN mode of Orbot on Android. The framework was introduced with a tiny 5MB memory limit — which wasn’t enough to run both Tor and the controller app. But the memory limits have been increased to usable levels in iOS 10 and <a href="https://conradkramer.com/" rel="nofollow">Conrad Kramer</a>, the lead iCepa developer, has been making a bit of progress in recent months.</p>

<p>There’s also work ongoing work to make Tor easier to implement in other apps, like <b><a href="https://github.com/iCepa/Tor.framework" rel="nofollow">Tor.framework</a></b> and <b><a href="https://github.com/ursachec/CPAProxy" rel="nofollow">CPAProxy</a></b>. <a href="https://chatsecure.org/" rel="nofollow">ChatSecure</a> for iOS uses CPAProxy to power encrypted XMPP instant messaging over Tor, and the next version of Onion Browser uses Tor.framework rather than a custom solution. Onion Browser’s obfs4/meek support comes from another similar reusable framework called <b><a href="https://github.com/mtigas/iObfs" rel="nofollow">iObfs</a></b>. Reusable pieces like this will hopefully encourage more developers to work on iOS software that supports Tor.</p>

---
_comments:

<a id="comment-224101"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224101" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
    <a href="#comment-224101">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224101" class="permalink" rel="bookmark">Thanks Mike!
So it sounds</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks Mike!</p>
<p>So it sounds like having some variant of Firefox on iOS is still really far out -- which means that porting Tor Browser to it (like Orfox on Android) isn't going to be feasible in the short or mid term either, which means that we'll be missing all the application-level privacy features that Tor Browser provides:<br />
<a href="https://www.torproject.org/projects/torbrowser/design/" rel="nofollow">https://www.torproject.org/projects/torbrowser/design/</a></p>
<p>A) Can you give us your sense of whether Apple will ever change its position there? Should we just give up on Firefox on iOS, or are there upcoming shifts inside Apple that might change things?</p>
<p>B) "Endless" sounds neat. Can you give us your guesses about how close it could reasonably become to the protections provided by Tor Browser? I'm thinking of more complicated things like per-domain isolation of browser state, rather than the simple things like "throws away cookies at exit".</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224125"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224125" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mtigas
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mtigas said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224101" class="permalink" rel="bookmark">Thanks Mike!
So it sounds</a> by arma</p>
    <a href="#comment-224125">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224125" class="permalink" rel="bookmark">A)
The consensus seems to be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A)<br />
The consensus seems to be that the "system WebKit only" policy is fairly set in stone.</p>
<p>In terms of allowing the newer WebKit APIs to allow some control over the network stack (so we can patch it to use Tor), there’s been a little bit of lobbying (mostly <a href="https://openradar.appspot.com/20545691" rel="nofollow">via tickets</a>) to regain some of that functionality.</p>
<p>The older UIWebView API allowed _full_ control to intercept and modify requests from the app -- which allows apps to do things like rewrite content or inject javascript into loaded pages. I’ve heard a rumor that there’s some hesitation to provide that much control in the new API due to concerns that some developers have been using the old API’s full access to inject their own tracking javascript to web views within their apps (though I haven’t myself verified these claims).</p>
<p>B)<br />
Browser state isolation is a tricky problem since we’re limited to what we can accomplish with the built in WebKit APIs.</p>
<ul>
<li>We <i>do</i> have full control over the headers and body content of requests, so we can easily filter out Cookie headers and handle things like that (and possibly do some content filtering for other things)</li>
<li>We <i>do</i> get access to where caches (including HTML5 localStorage) are stored on disk.</li>
<li>But some of these non-cookie features, like HTML5 localStorage, are more difficult to pin down since script-based features take place within WebKit and we don’t generally have access to customize what they do. There’s <i>some</i> internal shared state across all web views (i.e. "browser tabs") within an app as well (the internal cookie handler is a singleton), which may have some ramifications as well.</li>
</ul>
<p>Our isolation options are essentially limited to what we can control from the network stack (in terms of content processing and filtering) and via access to disk storage and browser cache.</p>
<p>In terms of other protections, <a href="https://web.archive.org/web/20140829041534/http://objectiveself.com/post/84817251648/uiwebviews-hidden-properties" rel="nofollow">there are some hidden options</a> to the UIWebView that allow disabling JavaScript some other multimedia features, controlling hardware acceleration on &lt;canvas&gt; elements, and a few other things like cache size. Combining those with our control of the network stack (since we can do things like inject Content-Security-Policy headers to all incoming HTTP responses to coerce WebKit into disabling things like remote fonts &amp; etc) we can get <i>some</i> of the way toward a "Security Level" slider like in Tor Browser.</p>
<p>However, since we lack control over a great portion of the rendering engine, the number of things we can protect against is always going to be a _lot_ more limited than on other platforms.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228676"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228676" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 25, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224101" class="permalink" rel="bookmark">Thanks Mike!
So it sounds</a> by arma</p>
    <a href="#comment-228676">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228676" class="permalink" rel="bookmark">Hi - can I get your opinion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi - can I get your opinion of "Red Browser" on iOS?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-224107"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224107" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
    <a href="#comment-224107">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224107" class="permalink" rel="bookmark">just want to say Thank You!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>just want to say Thank You!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224111"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224111" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
    <a href="#comment-224111">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224111" class="permalink" rel="bookmark">Thank you so much for all of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you so much for all of your work! Mobiles aren't the safest but bringing Tor to mobiles is a very critical step in blowing up the anonymity set. Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224119"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224119" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
    <a href="#comment-224119">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224119" class="permalink" rel="bookmark">Thank you so much. We really</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you so much. We really appreciate your work and your efforts for us. Thanks a lot!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224142"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224142" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
    <a href="#comment-224142">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224142" class="permalink" rel="bookmark">Thanks so much for all that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks so much for all that you do; I'm going to encourage my friends with iOS devices to use Onion Browser instead of Firefox.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224144"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224144" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
    <a href="#comment-224144">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224144" class="permalink" rel="bookmark">https://blog.torproject.org/a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://blog.torproject.org/archive" rel="nofollow">https://blog.torproject.org/archive</a><br />
Forbidden<br />
You don't have permission to access /archive on this server.<br />
Apache Server at blog.torproject.org Port 443</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224147"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224147" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224144" class="permalink" rel="bookmark">https://blog.torproject.org/a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224147">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224147" class="permalink" rel="bookmark">Yes, we disabled that part</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, we disabled that part of the blog because it opened users up to cross-site attacks. Our blog software is obsolete. We're in the process of fixing that but we're not there yet. Sorry that the duct tape is showing, but at least some of the duct tape is still sticky. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-224919"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224919" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
    <a href="#comment-224919">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224919" class="permalink" rel="bookmark">It so much work to do.It</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It so much work to do.It going to be a long trip but I wish you good luck. When you are going to finally make the app please use the classic onion icon that Tor browser have...it looks interesting</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224959"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224959" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
    <a href="#comment-224959">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224959" class="permalink" rel="bookmark">I downloaded The onion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I downloaded The onion browser for Ios but I don't know what to say about the interface....it needs some work in terms or design and functionality.<br />
In the app store is another browser called Vpn browser....is using tor and has more functions and a better design but I don't know if the app is trustfull because of the developer (Art Fusion). I need an advice. This app is safe or not?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228990"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228990" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-228990">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228990" class="permalink" rel="bookmark">I don&#039;t think anyone needs</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't think anyone needs Apple torbrowser because anyone needeng privacy won't use iOS because it has built-in surveillance. Anyone using Apple devices and OSes is Tim Cook's anal slave.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-234355"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-234355" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 05, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-228990" class="permalink" rel="bookmark">I don&#039;t think anyone needs</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-234355">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-234355" class="permalink" rel="bookmark">First of all, that&#039;s not an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>First of all, that's not an insult. Plenty of people enjoy anal sex, along the entire gender spectrum. So it makes no sense to mention it unless you're homophobic. Tim Cook is also gay so is that why? Get off it.</p>
<p>Segundo, yes, iOS is bad not good. But that doesn't mean that we should abandon the dawgs money who be illin wit dat. Sheeeeet. Everyone deserves access to freedom, not just those of us who use GNU and free systems. Are you also against Tor on Windows?</p>
<p>The bottom line is that we can either be traitors who refuse to meet people where they are, like the moron who tried to tell me no one is "stuck" using proprietary garbageware, or we can be decents who try to help people in a plurality and diversity of ways.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-231536"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-231536" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 18, 2017</p>
    </div>
    <a href="#comment-231536">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-231536" class="permalink" rel="bookmark">A small coalition of Tor iOS</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><cite>A small coalition of Tor iOS developers are working on a tool called iCepa to use this framework to provide a Tor VPN to the entire phone — similar to the VPN mode of Orbot on Android.</cite></p>
<p>But, unlike on Android, it doesn't have the ability to apply the VPN in a per-app manner. Apparently, Apple in all their infinite wisdom decided that particular feature should only apply to MDM'd iDevices, which instantly alienates the vast majority of iOS users. Sending all your app traffic through Tor is a Really Bad Idea. You think the iOS video player leaks data? What about every logged-in account on that device betraying your identity to anyone sniffing around an exit node?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271136"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271136" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Luca (not verified)</span> said:</p>
      <p class="date-time">August 30, 2017</p>
    </div>
    <a href="#comment-271136">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271136" class="permalink" rel="bookmark">Thanks for developing Onion…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for developing Onion Browser, Mike. Just tested in on my i7+, works great! Here's <a href="https://yalujailbreak.net/onion-browser/" rel="nofollow">my review of the Onion Browser</a>. Hope you like it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
