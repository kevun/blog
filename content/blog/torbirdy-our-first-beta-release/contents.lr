title: TorBirdy: our first beta release!
---
pub_date: 2013-02-11
---
author: ioerror
---
tags:

TorBirdy
Thunderbird
---
_html_body:

<p>Today we are happy to <a href="https://lists.torproject.org/pipermail/tor-talk/2013-February/027277.html" rel="nofollow">release our first beta</a> of <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy" rel="nofollow">TorBirdy</a>. It has been in development since April of last year and was released internally on the tor-talk mailing list. We think we've had just over five thousand users testing it in the last year.  We have polished it and we've made great progress.</p>

<p>What is <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy" rel="nofollow">TorBirdy</a>?</p>

<p><a href="https://trac.torproject.org/projects/tor/wiki/torbirdy" rel="nofollow">TorBirdy</a> is a Torbutton like extension for Thunderbird, Icedove and related Mozilla mail clients. It may also work with other non-web browser Mozilla programs such as Sunbird. We've also added support for JonDo, Whonix, Tails; if that means something to you, let us know how it works!</p>

<p>We offer two ways to install TorBirdy - either by visiting <a href="https://www.torproject.org/dist/torbirdy/torbirdy-0.1.0.xpi" rel="nofollow">our website</a> (<a href="https://www.torproject.org/dist/torbirdy/torbirdy-0.1.0.xpi.asc" rel="nofollow">sig</a>) or by visiting the <a href="https://addons.mozilla.org/en-US/thunderbird/addon/torbirdy/" rel="nofollow">Mozilla AddOn page for TorBirdy</a> (<a href="https://addons.mozilla.org/thunderbird/downloads/file/191748/torbirdy-0.1.0-tb.xpi?src=dp-btn-primary" rel="nofollow">xpi available here</a>).</p>

<p>As a general Anonymity and security note: We're still working on <a href="https://trac.torproject.org/projects/tor/ticket/6314" rel="nofollow">two</a> <a href="https://trac.torproject.org/projects/tor/ticket/6315" rel="nofollow">known</a> anonymity issues with <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=776397" rel="nofollow">Mozilla</a>. When our improvements to Thunderbird are accepted, it will be anonymity ready out of the box and we'll do a proper full release.</p>

<p>We'd love help with translations, programming or anything that you think will improve TorBirdy!</p>

<p>Thanks to all of our TorBirdy users and contributors - Sukhbir and I would especially like to tagnaq and Karsten N!</p>

---
_comments:

<a id="comment-18720"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18720" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2013</p>
    </div>
    <a href="#comment-18720">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18720" class="permalink" rel="bookmark">It works great. Thank you!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It works great. Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18723"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18723" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2013</p>
    </div>
    <a href="#comment-18723">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18723" class="permalink" rel="bookmark">Hello. 
I am unable to leave</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello. </p>
<p>I am unable to leave a comment on "blogger" blogs. The comment window is offered and you can submit the comment but the comment doesn't appear and the comment window simply just reloads. I noticed in another post on this blog someone mentioning this issue in August. Is there any fix for this?</p>
<p>Thanks so much.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18736"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18736" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 16, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18723" class="permalink" rel="bookmark">Hello. 
I am unable to leave</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18736">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18736" class="permalink" rel="bookmark">Same problem here.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Same problem here.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18785"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18785" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 21, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18723" class="permalink" rel="bookmark">Hello. 
I am unable to leave</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18785">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18785" class="permalink" rel="bookmark">They are moderating these</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>They are moderating these comments to prevent spamming, trolling and flaming.<br />
Only once they reviewed the comments to be appropriate, and approved them, then only the comments will appear.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19144"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19144" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 16, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18785" class="permalink" rel="bookmark">They are moderating these</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19144">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19144" class="permalink" rel="bookmark">If that was the case I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If that was the case I wouldn't have made this post . Of course they are not moderating the comments. If they were, it would not be possible to post comments through others means like with a regular browser or other proxy. Tor is the only one that fails.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-18725"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18725" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2013</p>
    </div>
    <a href="#comment-18725">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18725" class="permalink" rel="bookmark">Yes I can help you in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes I can help you in translation. How can i help you ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18976"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18976" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 27, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18725" class="permalink" rel="bookmark">Yes I can help you in</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18976">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18976" class="permalink" rel="bookmark">https://www.torproject.org/ge</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/getinvolved/translation.html.en" rel="nofollow">https://www.torproject.org/getinvolved/translation.html.en</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-18726"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18726" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2013</p>
    </div>
    <a href="#comment-18726">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18726" class="permalink" rel="bookmark">Oh and for the translation,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oh and for the translation, there is my email : <a href="mailto:cyril.charlier@gmail.com" rel="nofollow">cyril.charlier@gmail.com</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18977"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18977" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 27, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18726" class="permalink" rel="bookmark">Oh and for the translation,</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18977">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18977" class="permalink" rel="bookmark">https://www.torproject.org/ge</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/getinvolved/translation.html.en" rel="nofollow">https://www.torproject.org/getinvolved/translation.html.en</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-18735"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18735" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 16, 2013</p>
    </div>
    <a href="#comment-18735">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18735" class="permalink" rel="bookmark">/typo Sukhbir and I would</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>/typo Sukhbir and I would especially like to tagnaq and Karsten N!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18737"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18737" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 17, 2013</p>
    </div>
    <a href="#comment-18737">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18737" class="permalink" rel="bookmark">Hi im joe and new to using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi im joe and new to using apps i have read all the tips and everything else im not computer savy but i realy like all the stuff people are saying about tor but i cannot get it to work i have a samsung mini and no job i would donate or pay to get it working if somebody can help i will donate pnext week i realy like the idea of having privicy on the fone please help iv been trying since about 11 last night so im feeling prety dumb about now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18978"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18978" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 27, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18737" class="permalink" rel="bookmark">Hi im joe and new to using</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18978">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18978" class="permalink" rel="bookmark">hello joe
educate urself
no</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello joe<br />
educate urself<br />
no one do it for u</p>
<p><a href="http://survival.tacticaltech.org/" rel="nofollow">http://survival.tacticaltech.org/</a><br />
<a href="https://ssd.eff.org/" rel="nofollow">https://ssd.eff.org/</a><br />
<a href="https://www.eff.org/issues/transparency" rel="nofollow">https://www.eff.org/issues/transparency</a><br />
<a href="https://globalchokepoints.org/" rel="nofollow">https://globalchokepoints.org/</a><br />
<a href="http://www.privacysos.org/" rel="nofollow">http://www.privacysos.org/</a><br />
<a href="http://33bits.org/" rel="nofollow">http://33bits.org/</a><br />
<a href="https://www.tacticaltech.org/#privacy-and-expression" rel="nofollow">https://www.tacticaltech.org/#privacy-and-expression</a><br />
<a href="https://www.eff.org/free-speech-weak-link" rel="nofollow">https://www.eff.org/free-speech-weak-link</a><br />
<a href="http://mobileactive.org/howtos/mobile-surveillance-primer" rel="nofollow">http://mobileactive.org/howtos/mobile-surveillance-primer</a></p>
<p>that some shit to get u start<br />
ther more. a lot<br />
u find<br />
g'day</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-18757"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18757" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 18, 2013</p>
    </div>
    <a href="#comment-18757">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18757" class="permalink" rel="bookmark">Look&#039;n forward to the full</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Look'n forward to the full release!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18776"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18776" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 20, 2013</p>
    </div>
    <a href="#comment-18776">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18776" class="permalink" rel="bookmark">https://check.torproject.org
</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://check.torproject.org" rel="nofollow">https://check.torproject.org</a><br />
 showing "There is a security update available for the Tor Browser Bundle." these 2 days on Windows 7 and Linux but I am sure that I have the latest Stable and I have seen once the green message and then again yellow</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18779"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18779" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 20, 2013</p>
    </div>
    <a href="#comment-18779">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18779" class="permalink" rel="bookmark">Hello there, Tor crew. I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello there, Tor crew. I know this is a bit off topic..But I can't find a good answer to this privacy related question anywhere. I am looking for an alternative to Skype that is privacy oriented (encrypted calls/anonymity) and that's preferable open source or very trust worthy. Does a good skype alternative even exist? I know Jacob Appelbaum recommends Jitsi, but that is not exactly user friendly software and their FAQ sucks, it's very difficult to be able to tell what exactly is going on and what Jitsi is doing, or how it works.. Any non computer savvy person can't understand their FAQ on encryption for sure... and then with Jitsi you have to login with a Google or yahoo account? how is that anonymous. So what are the alternatives for talking to other people online in a private manner?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18975"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18975" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 27, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18779" class="permalink" rel="bookmark">Hello there, Tor crew. I</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18975">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18975" class="permalink" rel="bookmark">u no find anser? u no look</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>u no find anser? u no look enuf</p>
<p>irc.freenode.ne: #jitsi<br />
<a href="https://jitsi.org/Development/MailingLists#users" rel="nofollow">https://jitsi.org/Development/MailingLists#users</a><br />
<a href="https://www.youtube.com/user/APrivateLifeOnline" rel="nofollow">https://www.youtube.com/user/APrivateLifeOnline</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-22244"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-22244" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 27, 2013</p>
    </div>
    <a href="#comment-22244">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-22244" class="permalink" rel="bookmark">A few days ago I find that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A few days ago I find that thunderbird send some computer information that I don't want like hostname, hostdomain, ... How may I disable this behavior? I am not going on the street screaming my name or passport number but I feel that some of my programs do that!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-28500"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-28500" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 12, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-22244" class="permalink" rel="bookmark">A few days ago I find that</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-28500">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-28500" class="permalink" rel="bookmark">Use TorBirdy. It hides that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Use TorBirdy. It hides that information.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
