title: New Releases: Tor 0.4.0.2-alpha, 0.3.5.8, 0.3.4.11, and 0.3.3.12
---
pub_date: 2019-02-21
---
author: nickm
---
tags: tor releases
---
categories: releases
---
summary:

There are new source code releases available for download. 
---
_html_body:

<p>There are new source code releases available for download. If you build Tor from source, you can download the source code for 0.4.0.2-alpha and 0.3.5.8 from the <a href="https://www.torproject.org/download/download">download page</a>. You can find 0.3.4.11 and 0.3.3.12 at <a href="https://dist.torproject.org/">dist.torproject.org</a>. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely in the same timeframe.</p>
<p>These releases all fix <a href="https://trac.torproject.org/projects/tor/ticket/29168">TROVE-2019-001</a>, a possible security bug involving the KIST cell scheduler code in versions 0.3.2.1-alpha and later. We are not certain that it is possible to exploit this bug in the wild, but out of an abundance of caution, we recommend that all affected users upgrade once packages are available. The potential impact is a remote denial-of-service attack against clients or relays.</p>
<p>Also note: 0.3.3.12 is the last anticipated release in the 0.3.3.x series; that series will become unsupported next week. The remaining supported stable series will 0.2.9.x (long-term support until 2020), 0.3.4.x (supported until June), and 0.3.5.x (long-term support until 2022).</p>
<p>Below are the changes in Tor 0.3.5.8 and in 0.4.0.2-alpha. You can also read the <a href="https://gitweb.torproject.org/tor.git/plain/ChangeLog?h=tor-0.3.4.11">changelog for 0.3.4.11</a> and the <a href="https://gitweb.torproject.org/tor.git/plain/ChangeLog?h=tor-0.3.3.12">changelog for 0.3.3.12</a>.</p>
<h2>Changes in version 0.3.5.8 - 2019-02-21</h2>
<p>Tor 0.3.5.8 backports serveral fixes from later releases, including fixes for an annoying SOCKS-parsing bug that affected users in earlier 0.3.5.x releases.</p>
<p>It also includes a fix for a medium-severity security bug affecting Tor 0.3.2.1-alpha and later. All Tor instances running an affected release should upgrade to 0.3.3.12, 0.3.4.11, 0.3.5.8, or 0.4.0.2-alpha.</p>
<ul>
<li>Major bugfixes (cell scheduler, KIST, security):
<ul>
<li>Make KIST consider the outbuf length when computing what it can put in the outbuf. Previously, KIST acted as though the outbuf were empty, which could lead to the outbuf becoming too full. It is possible that an attacker could exploit this bug to cause a Tor client or relay to run out of memory and crash. Fixes bug <a href="https://bugs.torproject.org/29168">29168</a>; bugfix on 0.3.2.1-alpha. This issue is also being tracked as TROVE-2019-001 and CVE-2019-8955.</li>
</ul>
</li>
<li>Major bugfixes (networking, backport from 0.4.0.2-alpha):
<ul>
<li>Gracefully handle empty username/password fields in SOCKS5 username/password auth messsage and allow SOCKS5 handshake to continue. Previously, we had rejected these handshakes, breaking certain applications. Fixes bug <a href="https://bugs.torproject.org/29175">29175</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (compilation, backport from 0.4.0.2-alpha):
<ul>
<li>Compile correctly when OpenSSL is built with engine support disabled, or with deprecated APIs disabled. Closes ticket <a href="https://bugs.torproject.org/29026">29026</a>. Patches from "Mangix".</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the February 5 2019 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/29478">29478</a>.</li>
</ul>
</li>
<li>Minor features (testing, backport from 0.4.0.2-alpha):
<ul>
<li>Treat all unexpected ERR and BUG messages as test failures. Closes ticket <a href="https://bugs.torproject.org/28668">28668</a>.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v3, client, backport from 0.4.0.1-alpha):
<ul>
<li>Stop logging a "BUG()" warning and stacktrace when we find a SOCKS connection waiting for a descriptor that we actually have in the cache. It turns out that this can actually happen, though it is rare. Now, tor will recover and retry the descriptor. Fixes bug <a href="https://bugs.torproject.org/28669">28669</a>; bugfix on 0.3.2.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (IPv6, backport from 0.4.0.1-alpha):
<ul>
<li>Fix tor_ersatz_socketpair on IPv6-only systems. Previously, the IPv6 socket was bound using an address family of AF_INET instead of AF_INET6. Fixes bug <a href="https://bugs.torproject.org/28995">28995</a>; bugfix on 0.3.5.1-alpha. Patch from Kris Katterjohn.</li>
</ul>
</li>
<li>Minor bugfixes (build, compatibility, rust, backport from 0.4.0.2-alpha):
<ul>
<li>Update Cargo.lock file to match the version made by the latest version of Rust, so that "make distcheck" will pass again. Fixes bug <a href="https://bugs.torproject.org/29244">29244</a>; bugfix on 0.3.3.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (client, clock skew, backport from 0.4.0.1-alpha):
<ul>
<li>Select guards even if the consensus has expired, as long as the consensus is still reasonably live. Fixes bug <a href="https://bugs.torproject.org/24661">24661</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, backport from 0.4.0.1-alpha):
<ul>
<li>Compile correctly on OpenBSD; previously, we were missing some headers required in order to detect it properly. Fixes bug <a href="https://bugs.torproject.org/28938">28938</a>; bugfix on 0.3.5.1-alpha. Patch from Kris Katterjohn.</li>
</ul>
</li>
<li>Minor bugfixes (documentation, backport from 0.4.0.2-alpha):
<ul>
<li>Describe the contents of the v3 onion service client authorization files correctly: They hold public keys, not private keys. Fixes bug <a href="https://bugs.torproject.org/28979">28979</a>; bugfix on 0.3.5.1-alpha. Spotted by "Felixix".</li>
</ul>
</li>
<li>Minor bugfixes (logging, backport from 0.4.0.1-alpha):
<ul>
<li>Rework rep_hist_log_link_protocol_counts() to iterate through all link protocol versions when logging incoming/outgoing connection counts. Tor no longer skips version 5, and we won't have to remember to update this function when new link protocol version is developed. Fixes bug <a href="https://bugs.torproject.org/28920">28920</a>; bugfix on 0.2.6.10.</li>
</ul>
</li>
<li>Minor bugfixes (logging, backport from 0.4.0.2-alpha):
<ul>
<li>Log more information at "warning" level when unable to read a private key; log more information at "info" level when unable to read a public key. We had warnings here before, but they were lost during our NSS work. Fixes bug <a href="https://bugs.torproject.org/29042">29042</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (misc, backport from 0.4.0.2-alpha):
<ul>
<li>The amount of total available physical memory is now determined using the sysctl identifier HW_PHYSMEM (rather than HW_USERMEM) when it is defined and a 64-bit variant is not available. Fixes bug <a href="https://bugs.torproject.org/28981">28981</a>; bugfix on 0.2.5.4-alpha. Patch from Kris Katterjohn.</li>
</ul>
</li>
<li>Minor bugfixes (onion services, backport from 0.4.0.2-alpha):
<ul>
<li>Avoid crashing if ClientOnionAuthDir (incorrectly) contains more than one private key for a hidden service. Fixes bug <a href="https://bugs.torproject.org/29040">29040</a>; bugfix on 0.3.5.1-alpha.</li>
<li>In hs_cache_store_as_client() log an HSDesc we failed to parse at "debug" level. Tor used to log it as a warning, which caused very long log lines to appear for some users. Fixes bug <a href="https://bugs.torproject.org/29135">29135</a>; bugfix on 0.3.2.1-alpha.</li>
<li>Stop logging "Tried to establish rendezvous on non-OR circuit..." as a warning. Instead, log it as a protocol warning, because there is nothing that relay operators can do to fix it. Fixes bug <a href="https://bugs.torproject.org/29029">29029</a>; bugfix on 0.2.5.7-rc.</li>
</ul>
</li>
<li>Minor bugfixes (tests, directory clients, backport from 0.4.0.1-alpha):
<ul>
<li>Mark outdated dirservers when Tor only has a reasonably live consensus. Fixes bug <a href="https://bugs.torproject.org/28569">28569</a>; bugfix on 0.3.2.5-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (tests, backport from 0.4.0.2-alpha):
<ul>
<li>Detect and suppress "bug" warnings from the util/time test on Windows. Fixes bug <a href="https://bugs.torproject.org/29161">29161</a>; bugfix on 0.2.9.3-alpha.</li>
<li>Do not log an error-level message if we fail to find an IPv6 network interface from the unit tests. Fixes bug <a href="https://bugs.torproject.org/29160">29160</a>; bugfix on 0.2.7.3-rc.</li>
</ul>
</li>
<li>Minor bugfixes (usability, backport from 0.4.0.1-alpha):
<ul>
<li>Stop saying "Your Guard ..." in pathbias_measure_{use,close}_rate(). Some users took this phrasing to mean that the mentioned guard was under their control or responsibility, which it is not. Fixes bug <a href="https://bugs.torproject.org/28895">28895</a>; bugfix on Tor 0.3.0.1-alpha.</li>
</ul>
</li>
</ul>
<h2>Changes in version 0.4.0.2-alpha - 2019-02-21</h2>
<p>Tor 0.4.0.2-alpha is the second alpha in its series; it fixes several bugs from earlier versions, including several that had broken backward compatibility.</p>
<p>It also includes a fix for a medium-severity security bug affecting Tor 0.3.2.1-alpha and later. All Tor instances running an affected release should upgrade to 0.3.3.12, 0.3.4.11, 0.3.5.8, or 0.4.0.2-alpha.</p>
<ul>
<li>Major bugfixes (cell scheduler, KIST, security):
<ul>
<li>Make KIST consider the outbuf length when computing what it can put in the outbuf. Previously, KIST acted as though the outbuf were empty, which could lead to the outbuf becoming too full. It is possible that an attacker could exploit this bug to cause a Tor client or relay to run out of memory and crash. Fixes bug <a href="https://bugs.torproject.org/29168">29168</a>; bugfix on 0.3.2.1-alpha. This issue is also being tracked as TROVE-2019-001 and CVE-2019-8955.</li>
</ul>
</li>
<li>Major bugfixes (networking):
<ul>
<li>Gracefully handle empty username/password fields in SOCKS5 username/password auth messsage and allow SOCKS5 handshake to continue. Previously, we had rejected these handshakes, breaking certain applications. Fixes bug <a href="https://bugs.torproject.org/29175">29175</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Major bugfixes (windows, startup):
<ul>
<li>When reading a consensus file from disk, detect whether it was written in text mode, and re-read it in text mode if so. Always write consensus files in binary mode so that we can map them into memory later. Previously, we had written in text mode, which confused us when we tried to map the file on windows. Fixes bug <a href="https://bugs.torproject.org/28614">28614</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor features (compilation):
<ul>
<li>Compile correctly when OpenSSL is built with engine support disabled, or with deprecated APIs disabled. Closes ticket <a href="https://bugs.torproject.org/29026">29026</a>. Patches from "Mangix".</li>
</ul>
</li>
<li>Minor features (developer tooling):
<ul>
<li>Check that bugfix versions in changes files look like Tor versions from the versions spec. Warn when bugfixes claim to be on a future release. Closes ticket <a href="https://bugs.torproject.org/27761">27761</a>.</li>
<li>Provide a git pre-commit hook that disallows commiting if we have any failures in our code and changelog formatting checks. It is now available in scripts/maint/pre-commit.git-hook. Implements feature <a href="https://bugs.torproject.org/28976">28976</a>.</li>
</ul>
</li>
<li>Minor features (directory authority):
<ul>
<li>When a directory authority is using a bandwidth file to obtain bandwidth values, include the digest of that file in the vote. Closes ticket <a href="https://bugs.torproject.org/26698">26698</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the February 5 2019 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/29478">29478</a>.</li>
</ul>
</li>
<li>Minor features (testing):
<ul>
<li>Treat all unexpected ERR and BUG messages as test failures. Closes ticket <a href="https://bugs.torproject.org/28668">28668</a>.</li>
</ul>
</li>
<li>Minor bugfixes (build, compatibility, rust):
<ul>
<li>Update Cargo.lock file to match the version made by the latest version of Rust, so that "make distcheck" will pass again. Fixes bug <a href="https://bugs.torproject.org/29244">29244</a>; bugfix on 0.3.3.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix compilation warnings in test_circuitpadding.c. Fixes bug <a href="https://bugs.torproject.org/29169">29169</a>; bugfix on 0.4.0.1-alpha.</li>
<li>Silence a compiler warning in test-memwipe.c on OpenBSD. Fixes bug <a href="https://bugs.torproject.org/29145">29145</a>; bugfix on 0.2.9.3-alpha. Patch from Kris Katterjohn.</li>
</ul>
</li>
<li>Minor bugfixes (documentation):
<ul>
<li>Describe the contents of the v3 onion service client authorization files correctly: They hold public keys, not private keys. Fixes bug <a href="https://bugs.torproject.org/28979">28979</a>; bugfix on 0.3.5.1-alpha. Spotted by "Felixix".</li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp sandbox):
<ul>
<li>Fix startup crash when experimental sandbox support is enabled. Fixes bug <a href="https://bugs.torproject.org/29150">29150</a>; bugfix on 0.4.0.1-alpha. Patch by Peter Gerber.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Avoid logging that we are relaxing a circuit timeout when that timeout is fixed. Fixes bug <a href="https://bugs.torproject.org/28698">28698</a>; bugfix on 0.2.4.7-alpha.</li>
<li>Log more information at "warning" level when unable to read a private key; log more information at "info" level when unable to read a public key. We had warnings here before, but they were lost during our NSS work. Fixes bug <a href="https://bugs.torproject.org/29042">29042</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (misc):
<ul>
<li>The amount of total available physical memory is now determined using the sysctl identifier HW_PHYSMEM (rather than HW_USERMEM) when it is defined and a 64-bit variant is not available. Fixes bug <a href="https://bugs.torproject.org/28981">28981</a>; bugfix on 0.2.5.4-alpha. Patch from Kris Katterjohn.</li>
</ul>
</li>
<li>Minor bugfixes (onion services):
<ul>
<li>Avoid crashing if ClientOnionAuthDir (incorrectly) contains more than one private key for a hidden service. Fixes bug <a href="https://bugs.torproject.org/29040">29040</a>; bugfix on 0.3.5.1-alpha.</li>
<li>In hs_cache_store_as_client() log an HSDesc we failed to parse at "debug" level. Tor used to log it as a warning, which caused very long log lines to appear for some users. Fixes bug <a href="https://bugs.torproject.org/29135">29135</a>; bugfix on 0.3.2.1-alpha.</li>
<li>Stop logging "Tried to establish rendezvous on non-OR circuit..." as a warning. Instead, log it as a protocol warning, because there is nothing that relay operators can do to fix it. Fixes bug <a href="https://bugs.torproject.org/29029">29029</a>; bugfix on 0.2.5.7-rc.</li>
</ul>
</li>
<li>Minor bugfixes (scheduler):
<ul>
<li>When re-adding channels to the pending list, check the correct channel's sched_heap_idx. This issue has had no effect in mainline Tor, but could have led to bugs down the road in improved versions of our circuit scheduling code. Fixes bug <a href="https://bugs.torproject.org/29508">29508</a>; bugfix on 0.3.2.10.</li>
</ul>
</li>
<li>Minor bugfixes (tests):
<ul>
<li>Fix intermittent failures on an adaptive padding test. Fixes one case of bug <a href="https://bugs.torproject.org/29122">29122</a>; bugfix on 0.4.0.1-alpha.</li>
<li>Disable an unstable circuit-padding test that was failing intermittently because of an ill-defined small histogram. Such histograms will be allowed again after 29298 is implemented. Fixes a second case of bug <a href="https://bugs.torproject.org/29122">29122</a>; bugfix on 0.4.0.1-alpha.</li>
<li>Detect and suppress "bug" warnings from the util/time test on Windows. Fixes bug <a href="https://bugs.torproject.org/29161">29161</a>; bugfix on 0.2.9.3-alpha.</li>
<li>Do not log an error-level message if we fail to find an IPv6 network interface from the unit tests. Fixes bug <a href="https://bugs.torproject.org/29160">29160</a>; bugfix on 0.2.7.3-rc.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>In the manpage entry describing MapAddress torrc setting, use example IP addresses from ranges specified for use in documentation by RFC 5737. Resolves issue <a href="https://bugs.torproject.org/28623">28623</a>.</li>
</ul>
</li>
<li>Removed features:
<ul>
<li>Remove the old check-tor script. Resolves issue <a href="https://bugs.torproject.org/29072">29072</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-279959"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279959" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 21, 2019</p>
    </div>
    <a href="#comment-279959">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279959" class="permalink" rel="bookmark">&gt; Packages should be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Packages should be available over the coming WEEKS</p>
<p>As I wrote before, why can't you publish a blog AFTER you release a APT package!?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279960"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279960" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 21, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279959" class="permalink" rel="bookmark">&gt; Packages should be…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279960">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279960" class="permalink" rel="bookmark">The people who write 99% of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The people who write 99% of the Tor code are different than the people who package Tor for Debian. Same for Ubuntu, Fedora, Gentoo, Arch, macOS, etc. etc.</p>
<p>Announcement of a new $software version should not have to wait for the maintainer for $software's package on $myFavoriteDistro to get around to making a new package.</p>
<p>Be patient or help the people who package $software. And definitely don't pester them with "WhEn CaN I gEt My NeW VeRsIoN" as they are volunteers that have been doing this for years and are tired of Internet users demanding to get their free software faster.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279980"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279980" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 23, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279959" class="permalink" rel="bookmark">&gt; Packages should be…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279980">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279980" class="permalink" rel="bookmark">DEBs of 0.4.0.2-alpha and 0…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>DEBs of 0.4.0.2-alpha and 0.3.5.8 were on Tor Project's repository the day after this post and are usually up that quickly.  The lower numbered series aren't up yet, and you'll have to wait "over the coming weeks" for your OS distro maintainers to put them on their repositories if you use those.</p>
<p>Want them quickly?  Use Tor Project's repository.<br />
<a href="https://www.torproject.org/docs/debian.html.en" rel="nofollow">https://www.torproject.org/docs/debian.html.en</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-279967"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279967" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>tomasland (not verified)</span> said:</p>
      <p class="date-time">February 22, 2019</p>
    </div>
    <a href="#comment-279967">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279967" class="permalink" rel="bookmark">Hi there. Thanks for the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi there. Thanks for the information.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279990"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279990" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>KDQ (not verified)</span> said:</p>
      <p class="date-time">February 24, 2019</p>
    </div>
    <a href="#comment-279990">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279990" class="permalink" rel="bookmark">Of concern is that the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Of concern is that the ubuntu *LTS* versions expire for support prematurely.</p>
<p>I do not count the short expiry 1Yr Ubuntu Distros in any expiry list, only LTS.</p>
<p>Willing to build contrib packages, with some basic guidance of doing this with debs.<br />
trusty 14.0.4LTS is NOT YET EOL, and may go to extended support<br />
It is also a part of the SecurityOnion variant, likely to be in service for awhile longer.</p>
<p>I also build static tarballs and pkgs (debs, rarely rpm) with alternate ssl codebases to openssl.<br />
jessie and stretch are favourites here.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-280065"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280065" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Jeff (not verified)</span> said:</p>
      <p class="date-time">February 27, 2019</p>
    </div>
    <a href="#comment-280065">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280065" class="permalink" rel="bookmark">What is the command prompt…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is the command prompt directory for the Tor Browser bookmarks in Windows 8.1 please?   Windows won't boot and I want to backup the bookmarks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280080"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280080" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 01, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280065" class="permalink" rel="bookmark">What is the command prompt…</a> by <span>Jeff (not verified)</span></p>
    <a href="#comment-280080">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280080" class="permalink" rel="bookmark">Go to the folder TBB was…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Go to the folder TBB was extracted into.  Then,<br />
<strong>cd .\Browser\TorBrowser\Data\Browser\profile.default\</strong><br />
bookmarks are saved in <strong>places.sqlite</strong><br />
older backups are in <strong>\bookmarkbackups\</strong></p>
<p>When you can open Tor Browser, you can export a HTML backup by<br />
hamburger menu --&gt; Library --&gt; Bookmarks --&gt; Show All Bookmarks at the bottom (Ctrl+Shift+O)<br />
then in the Library window, Import and Backup --&gt; Export Bookmarks to HTML...<br />
The HTML file saves tags, keywords, and descriptions too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
