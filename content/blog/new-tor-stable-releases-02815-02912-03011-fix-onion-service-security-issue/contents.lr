title: New Tor stable releases (0.2.8.15, 0.2.9.12, 0.3.0.11) with fix for onion service security issue
---
pub_date: 2017-09-18
---
author: nickm
---
tags: onion services
---
categories: onion services
---
summary: Hello! We found a security issue in the onion service code (CVE-2017-0380, TROVE-2017-008) that can cause sensitive information to be written to your logs if you have set the SafeLogging option to 0. If you are not running an onion service, or you have not changed the SafeLogging option from its default, you are not affected. If you are running 0.2.5, you are not affected. (0.2.4, 0.2.6, and 0.2.7 are no longer supported.) For more information, including workaround steps, see the advisory.
---
_html_body:

<p>Hello! We found a security issue in the onion service code (CVE-2017-0380, TROVE-2017-008) that can cause sensitive information to be written to your logs if you have set the SafeLogging option to 0. If you are not running an onion service, or you have not changed the SafeLogging option from its default, you are not affected. If you are running 0.2.5, you are not affected. (0.2.4, 0.2.6, and 0.2.7 are no longer supported.) For more information, including workaround steps, see <a href="https://lists.torproject.org/pipermail/tor-talk/2017-September/043585.html">the advisory.</a></p>
<p>Because of this issue, we have released new Tor versions in the 0.2.8, 0.2.9, and 0.3.0 series. If you build Tor from source, you will find the source code for these releases at <a href="https://dist.torproject.org">https://dist.torproject.org/</a>. Packages should be available over the coming days -- in the meantime, please see the advisory linked above for information on working around this issue.</p>
<p>The release series 0.3.1.x has also become stable today; I'll announce that in a separate post.</p>
<p>Below are the changelog entries for the new releases mentioned in this email:</p>
<p>Tor 0.2.8.15 backports a collection of bugfixes from later Tor series.</p>
<p>Most significantly, it includes a fix for TROVE-2017-008, a security bug that affects onion services running with the SafeLogging option disabled. For more information, see <a href="https://trac.torproject.org/projects/tor/ticket/23490">https://trac.torproject.org/projects/tor/ticket/23490</a></p>
<p>Note that Tor 0.2.8.x will no longer be supported after 1 Jan 2018. We suggest that you upgrade to the latest stable release if possible. If you can't, we recommend that you upgrade at least to 0.2.9, which will be supported until 2020.</p>
<h2>Changes in version 0.2.8.15 - 2017-09-18</h2>
<ul>
<li>Major bugfixes (openbsd, denial-of-service, backport from 0.3.1.5-alpha):
<ul>
<li>Avoid an assertion failure bug affecting our implementation of inet_pton(AF_INET6) on certain OpenBSD systems whose strtol() handling of "0xx" differs from what we had expected. Fixes bug 22789; bugfix on 0.2.3.8-alpha. Also tracked as TROVE-2017-007.</li>
</ul>
</li>
<li>Minor features:
<ul>
<li>Update geoip and geoip6 to the September 6 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
</ul>
<!--break--><ul>
<li>Minor bugfixes (compilation, mingw, backport from 0.3.1.1-alpha):
<ul>
<li>Backport a fix for an "unused variable" warning that appeared in some versions of mingw. Fixes bug 22838; bugfix on 0.2.8.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (defensive programming, undefined behavior, backport from 0.3.1.4-alpha):
<ul>
<li>Fix a memset() off the end of an array when packing cells. This bug should be harmless in practice, since the corrupted bytes are still in the same structure, and are always padding bytes, ignored, or immediately overwritten, depending on compiler behavior. Nevertheless, because the memset()'s purpose is to make sure that any other cell-handling bugs can't expose bytes to the network, we need to fix it. Fixes bug 22737; bugfix on 0.2.4.11-alpha. Fixes CID 1401591.</li>
</ul>
</li>
<li>Build features (backport from 0.3.1.5-alpha):
<ul>
<li>Tor's repository now includes a Travis Continuous Integration (CI) configuration file (.travis.yml). This is meant to help new developers and contributors who fork Tor to a Github repository be better able to test their changes, and understand what we expect to pass. To use this new build feature, you must fork Tor to your Github account, then go into the "Integrations" menu in the repository settings for your fork and enable Travis, then push your changes. Closes ticket 22636.</li>
</ul>
</li>
</ul>
<p>Tor 0.2.9.12 backports a collection of bugfixes from later Tor series.</p>
<p>Most significantly, it includes a fix for TROVE-2017-008, a security bug that affects onion services running with the SafeLogging option disabled. For more information, see <a href="https://trac.torproject.org/projects/tor/ticket/23490">https://trac.torproject.org/projects/tor/ticket/23490</a></p>
<h2>Changes in version 0.2.9.12 - 2017-09-18</h2>
<ul>
<li>Major features (security, backport from 0.3.0.2-alpha):
<ul>
<li>Change the algorithm used to decide DNS TTLs on client and server side, to better resist DNS-based correlation attacks like the DefecTor attack of Greschbach, Pulls, Roberts, Winter, and Feamster. Now relays only return one of two possible DNS TTL values, and clients are willing to believe DNS TTL values up to 3 hours long. Closes ticket <a href="https://bugs.torproject.org/19769">19769</a>.</li>
</ul>
</li>
<li>Major bugfixes (crash, directory connections, backport from 0.3.0.5-rc):
<ul>
<li>Fix a rare crash when sending a begin cell on a circuit whose linked directory connection had already been closed. Fixes bug <a href="https://bugs.torproject.org/21576">21576</a>; bugfix on 0.2.9.3-alpha. Reported by Alec Muffett.</li>
</ul>
</li>
<li>Major bugfixes (DNS, backport from 0.3.0.2-alpha):
<ul>
<li>Fix a bug that prevented exit nodes from caching DNS records for more than 60 seconds. Fixes bug <a href="https://bugs.torproject.org/19025">19025</a>; bugfix on 0.2.4.7-alpha.</li>
</ul>
</li>
<li>Major bugfixes (linux TPROXY support, backport from 0.3.1.1-alpha):
<ul>
<li>Fix a typo that had prevented TPROXY-based transparent proxying from working under Linux. Fixes bug <a href="https://bugs.torproject.org/18100">18100</a>; bugfix on 0.2.6.3-alpha. Patch from "d4fq0fQAgoJ".</li>
</ul>
</li>
<li>Major bugfixes (openbsd, denial-of-service, backport from 0.3.1.5-alpha):
<ul>
<li>Avoid an assertion failure bug affecting our implementation of inet_pton(AF_INET6) on certain OpenBSD systems whose strtol() handling of "0xx" differs from what we had expected. Fixes bug <a href="https://bugs.torproject.org/22789">22789</a>; bugfix on 0.2.3.8-alpha. Also tracked as TROVE-2017-007.</li>
</ul>
</li>
<li>Minor features (code style, backport from 0.3.1.3-alpha):
<ul>
<li>Add "Falls through" comments to our codebase, in order to silence GCC 7's -Wimplicit-fallthrough warnings. Patch from Andreas Stieger. Closes ticket <a href="https://bugs.torproject.org/22446">22446</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the September 6 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor bugfixes (bandwidth accounting, backport from 0.3.1.1-alpha):
<ul>
<li>Roll over monthly accounting at the configured hour and minute, rather than always at 00:00. Fixes bug <a href="https://bugs.torproject.org/22245">22245</a>; bugfix on 0.0.9rc1. Found by Andrey Karpov with PVS-Studio.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, backport from 0.3.1.5-alpha):
<ul>
<li>Suppress -Wdouble-promotion warnings with clang 4.0. Fixes bug <a href="https://bugs.torproject.org/22915">22915</a>; bugfix on 0.2.8.1-alpha.</li>
<li>Fix warnings when building with libscrypt and openssl scrypt support on Clang. Fixes bug <a href="https://bugs.torproject.org/22916">22916</a>; bugfix on 0.2.7.2-alpha.</li>
<li>When building with certain versions the mingw C header files, avoid float-conversion warnings when calling the C functions isfinite(), isnan(), and signbit(). Fixes bug <a href="https://bugs.torproject.org/22801">22801</a>; bugfix on 0.2.8.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, backport from 0.3.1.7):
<ul>
<li>Avoid compiler warnings in the unit tests for running tor_sscanf() with wide string outputs. Fixes bug <a href="https://bugs.torproject.org/15582">15582</a>; bugfix on 0.2.6.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, mingw, backport from 0.3.1.1-alpha):
<ul>
<li>Backport a fix for an "unused variable" warning that appeared in some versions of mingw. Fixes bug <a href="https://bugs.torproject.org/22838">22838</a>; bugfix on 0.2.8.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller, backport from 0.3.1.7):
<ul>
<li>Do not crash when receiving a HSPOST command with an empty body. Fixes part of bug <a href="https://bugs.torproject.org/22644">22644</a>; bugfix on 0.2.7.1-alpha.</li>
<li>Do not crash when receiving a POSTDESCRIPTOR command with an empty body. Fixes part of bug <a href="https://bugs.torproject.org/22644">22644</a>; bugfix on 0.2.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (coverity build support, backport from 0.3.1.5-alpha):
<ul>
<li>Avoid Coverity build warnings related to our BUG() macro. By default, Coverity treats BUG() as the Linux kernel does: an instant abort(). We need to override that so our BUG() macro doesn't prevent Coverity from analyzing functions that use it. Fixes bug <a href="https://bugs.torproject.org/23030">23030</a>; bugfix on 0.2.9.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (defensive programming, undefined behavior, backport from 0.3.1.4-alpha):
<ul>
<li>Fix a memset() off the end of an array when packing cells. This bug should be harmless in practice, since the corrupted bytes are still in the same structure, and are always padding bytes, ignored, or immediately overwritten, depending on compiler behavior. Nevertheless, because the memset()'s purpose is to make sure that any other cell-handling bugs can't expose bytes to the network, we need to fix it. Fixes bug <a href="https://bugs.torproject.org/22737">22737</a>; bugfix on 0.2.4.11-alpha. Fixes CID 1401591.</li>
</ul>
</li>
<li>Minor bugfixes (file limits, osx, backport from 0.3.1.5-alpha):
<ul>
<li>When setting the maximum number of connections allowed by the OS, always allow some extra file descriptors for other files. Fixes bug <a href="https://bugs.torproject.org/22797">22797</a>; bugfix on 0.2.0.10-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp2 sandbox, backport from 0.3.1.5-alpha):
<ul>
<li>Avoid a sandbox failure when trying to re-bind to a socket and mark it as IPv6-only. Fixes bug <a href="https://bugs.torproject.org/20247">20247</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp2 sandbox, backport from 0.3.1.4-alpha):
<ul>
<li>Permit the fchmod system call, to avoid crashing on startup when starting with the seccomp2 sandbox and an unexpected set of permissions on the data directory or its contents. Fixes bug <a href="https://bugs.torproject.org/22516">22516</a>; bugfix on 0.2.5.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay, backport from 0.3.0.5-rc):
<ul>
<li>Avoid a double-marked-circuit warning that could happen when we receive DESTROY cells under heavy load. Fixes bug <a href="https://bugs.torproject.org/20059">20059</a>; bugfix on 0.1.0.1-rc.</li>
</ul>
</li>
<li>Minor bugfixes (voting consistency, backport from 0.3.1.1-alpha):
<ul>
<li>Reject version numbers with non-numeric prefixes (such as +, -, or whitespace). Disallowing whitespace prevents differential version parsing between POSIX-based and Windows platforms. Fixes bug <a href="https://bugs.torproject.org/21507">21507</a> and part of 21508; bugfix on 0.0.8pre1.</li>
</ul>
</li>
<li>Build features (backport from 0.3.1.5-alpha):
<ul>
<li>Tor's repository now includes a Travis Continuous Integration (CI) configuration file (.travis.yml). This is meant to help new developers and contributors who fork Tor to a Github repository be better able to test their changes, and understand what we expect to pass. To use this new build feature, you must fork Tor to your Github account, then go into the "Integrations" menu in the repository settings for your fork and enable Travis, then push your changes. Closes ticket <a href="https://bugs.torproject.org/22636">22636</a>.</li>
</ul>
</li>
</ul>
<p>Tor 0.3.0.11 backports a collection of bugfixes from Tor the 0.3.1 series.</p>
<p>Most significantly, it includes a fix for TROVE-2017-008, a security bug that affects onion services running with the SafeLogging option disabled. For more information, see <a href="https://trac.torproject.org/projects/tor/ticket/23490">https://trac.torproject.org/projects/tor/ticket/23490</a></p>
<h2>Changes in version 0.3.0.11 - 2017-09-18</h2>
<ul>
<li>Minor features (code style, backport from 0.3.1.7):
<ul>
<li>Add "Falls through" comments to our codebase, in order to silence GCC 7's -Wimplicit-fallthrough warnings. Patch from Andreas Stieger. Closes ticket <a href="https://bugs.torproject.org/22446">22446</a>.</li>
</ul>
</li>
<li>Minor features:
<ul>
<li>Update geoip and geoip6 to the September 6 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, backport from 0.3.1.7):
<ul>
<li>Avoid compiler warnings in the unit tests for calling tor_sscanf() with wide string outputs. Fixes bug <a href="https://bugs.torproject.org/15582">15582</a>; bugfix on 0.2.6.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller, backport from 0.3.1.7):
<ul>
<li>Do not crash when receiving a HSPOST command with an empty body. Fixes part of bug <a href="https://bugs.torproject.org/22644">22644</a>; bugfix on 0.2.7.1-alpha.</li>
<li>Do not crash when receiving a POSTDESCRIPTOR command with an empty body. Fixes part of bug <a href="https://bugs.torproject.org/22644">22644</a>; bugfix on 0.2.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (file limits, osx, backport from 0.3.1.5-alpha):
<ul>
<li>When setting the maximum number of connections allowed by the OS, always allow some extra file descriptors for other files. Fixes bug <a href="https://bugs.torproject.org/22797">22797</a>; bugfix on 0.2.0.10-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging, relay, backport from 0.3.1.6-rc):
<ul>
<li>Remove a forgotten debugging message when an introduction point successfully establishes an onion service prop224 circuit with a client.</li>
<li>Change three other log_warn() for an introduction point to protocol warnings, because they can be failure from the network and are not relevant to the operator. Fixes bug <a href="https://bugs.torproject.org/23078">23078</a>; bugfix on 0.3.0.1-alpha and 0.3.0.2-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-271416"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271416" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 19, 2017</p>
    </div>
    <a href="#comment-271416">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271416" class="permalink" rel="bookmark">&quot;(0.2.4, 0.2.6, and 0.2.7…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"(0.2.4, 0.2.6, and 0.2.7 are no longer supported.)"</p>
<p>There are a lot of Relays with these version numbers. What<br />
does that mean?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-271975"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271975" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-271416" class="permalink" rel="bookmark">&quot;(0.2.4, 0.2.6, and 0.2.7…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-271975">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271975" class="permalink" rel="bookmark">These are old systems that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>These are old systems that may be vulnerable for some attacks.   As far as I know, version 0.2.9.12 is the last one that successfully compiles with gcc-3 .  It is strongly recommended to update.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
