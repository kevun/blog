title: False Positives in 0.2.0.30:  RISING found Trojan.PSW.Win32.Undef.adp
---
pub_date: 2008-08-06
---
author: phobos
---
tags:

vidalia
false positive
virus
trojan
windows bundles
privoxy
---
categories: applications
---
_html_body:

<p>I've noticed a <a href="https://blog.torproject.org/blog/june-2008-progress-report#comment-154" rel="nofollow">few comments</a> about a Chinese anti-virus program, RISING, reporting that Vidalia.exe and Privoxy.exe are infected with Trojan.PSW.Win32.Undef.adp.    In both cases, I suspect that RISING is reporting false positives.  These executables as packaged and available on the Tor download page are not infected.</p>

<p>I've looked at the MD5 and SHA-1 sums of these programs as included in the Vidalia bundle and they match what the source packages produce as executables.  The privoxy.exe included in the bundles is the exact same one as found at the <a href="http://downloads.sourceforge.net/ijbswa/privoxy_setup_3_0_6.exe?modtime=1164015760&amp;big_mirror=0" rel="nofollow">Sourceforge Privoxy Download Page</a>.</p>

<p>The Vidalia.exe is the same as the one included in the <a href="http://www.vidalia-project.net/dist/vidalia-0.1.7.exe" rel="nofollow">Vidalia Download Page</a>.</p>

<p>Feel free to confirm this is true for you.  Better yet, let us know if these individual packages (Vidalia.exe from Vidalia and Privoxy.exe from Sourceforge) also show up as infected.</p>

---
_comments:

<a id="comment-165"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-165" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://slashdot.org">Anonymous (not verified)</a> said:</p>
      <p class="date-time">August 06, 2008</p>
    </div>
    <a href="#comment-165">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-165" class="permalink" rel="bookmark">update tor nodes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you fix the tor nodes to use a newer version of TOR because many are using old versions that generating weak keys.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-167"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-167" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 06, 2008</p>
    </div>
    <a href="#comment-167">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-167" class="permalink" rel="bookmark">an old version of tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>an old version of tor doesn't mean they have weak keys.  Only debian systems which were installed during the period of vulnerability are risky.  We believe we've blacklisted all bad keys, so those servers would have dropped off the network long ago.</p>
<p>Old versions of tor may have other bugs, however.  Anyone running 1.1 is probably vulnerable.  0.2.0.30 is the new stable, 0.2.1.x is the new alpha.</p>
<p>Here are all the versions I currently see as tor nodes:</p>
<p>0.1.1.19-rc<br />
0.1.1.20<br />
0.1.1.21<br />
0.1.1.22<br />
0.1.1.23<br />
0.1.1.24<br />
0.1.1.25<br />
0.1.1.26<br />
0.1.2.10-rc<br />
0.1.2.12-rc<br />
0.1.2.13<br />
0.1.2.14<br />
0.1.2.15<br />
0.1.2.16<br />
0.1.2.17<br />
0.1.2.18<br />
0.1.2.19<br />
0.1.2.2-alpha<br />
0.1.2.3-alpha<br />
0.2.0.12-alpha<br />
0.2.0.18-alpha<br />
0.2.0.20-rc<br />
0.2.0.21-rc<br />
0.2.0.22-rc<br />
0.2.0.23-rc<br />
0.2.0.24-rc<br />
0.2.0.25-rc<br />
0.2.0.26-rc<br />
0.2.0.27-rc<br />
0.2.0.28-rc<br />
0.2.0.29-rc<br />
0.2.0.2-alpha<br />
0.2.0.30<br />
0.2.0.4-alpha<br />
0.2.0.5-alpha<br />
0.2.0.6-alpha<br />
0.2.0.7-alpha<br />
0.2.0.8-alpha<br />
0.2.0.9-alpha<br />
0.2.1.0-alpha-dev<br />
0.2.1.1-alpha<br />
0.2.1.2-alpha<br />
0.2.1.2-alpha-dev<br />
0.2.1.3-alpha<br />
0.2.1.4-alpha<br />
0.2.1.4-alpha-dev</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-184"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-184" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 13, 2008</p>
    </div>
    <a href="#comment-184">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-184" class="permalink" rel="bookmark">ah, RISING anti-virus,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ah, RISING anti-virus, always miss true virus but report false positives, no one should be surprised about this. it even reports the open source edition of Qt4.4.1 installer has some trojan/malware downloader. really, it'd be abnormal if RISING don't report false positives all the time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-185"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-185" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 14, 2008</p>
    </div>
    <a href="#comment-185">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-185" class="permalink" rel="bookmark">It&#039;s CHINESE Anti-Virus Software</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The Chinese government is most likely not a fan of Tor, so imagine their pressure to domestic Chinese anti-virus companies include false positives on Tor software. It perhaps would be an effective way to stop the new Chinese dissident from using Tor to discover uncensored news beyond China's "Great Firewall".</p>
<p>Just a little conspiracy theory for you all. It happens all the time.</p>
<p>I bid you all adieu!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9302"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9302" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 16, 2011</p>
    </div>
    <a href="#comment-9302">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9302" class="permalink" rel="bookmark">On my school antivirus, the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>On my school antivirus, the TOR program is found as a virus...</p>
</div>
  </div>
</article>
<!-- Comment END -->
