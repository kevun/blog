title: Tails 1.2.1 is out
---
pub_date: 2014-12-03
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 1.2.1, is out.</p>

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_1.2/" rel="nofollow">numerous security issues</a> and all users must <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a> as soon as possible.</p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Security fixes
<ul>
<li>Upgrade Linux to 3.16.7-1.</li>
<li>Install Tor Browser 4.0.2 (based on Firefox 31.3.0esr).</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Restore mouse scrolling in KVM/Spice (<a href="https://labs.riseup.net/code/issues/7426" rel="nofollow">ticket #7426</a>).</li>
<li>Suppress excessive (and leaky!) Torbutton logging (<a href="https://labs.riseup.net/code/issues/8160" rel="nofollow">ticket #8160</a>).</li>
<li>Don't break the Unsafe and I2P Browsers after installing incremental upgrades (<a href="https://labs.riseup.net/code/issues/8152" rel="nofollow">ticket #8152</a>, <a href="https://labs.riseup.net/code/issues/8158" rel="nofollow">ticket #8158</a>).</li>
<li>External links in various applications should now open properly in the Tor Browser (<a href="https://labs.riseup.net/code/issues/8153" rel="nofollow">ticket #8153</a>, <a href="https://labs.riseup.net/code/issues/8153" rel="nofollow">ticket #8186</a>).</li>
<li>Fix clearsigning of text including non-ASCII characters in gpgApplet (<a href="https://labs.riseup.net/code/issues/7968" rel="nofollow">ticket #7968</a>).</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Upgrade I2P to 0.9.17-1~deb7u+1.</li>
<li>Make GnuPG configuration closer to the best practices (<a href="https://labs.riseup.net/code/issues/7512" rel="nofollow">ticket #7512</a>).</li>
<li>Remove TrueCrypt support and document how to open TrueCrypt volumes using cryptsetup (<a href="https://labs.riseup.net/code/issues/5373" rel="nofollow">ticket #5373</a>).</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<ul>
<li>Users of the <i>GnuPG keyrings and configuration</i> persistence feature should follow some <a href="https://tails.boum.org/news/version_1.2.1/#index3h1" rel="nofollow">manual steps</a> after upgrading a Tails USB stick or SD card installation to Tails 1.2.1.</li>
<li><a href="https://tails.boum.org/support/known_issues/" rel="nofollow">Longstanding</a> known issues.</li>
</ul>

<p><strong>I want to try it or to upgrade!</strong></p>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for January 14.</p>

<p>Have a look at our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Do you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk to us!</p>

<p><strong>Support and feedback</strong></p>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

