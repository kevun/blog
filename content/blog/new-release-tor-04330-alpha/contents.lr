title: New Release: Tor 0.4.3.3-alpha (with security fixes!)
---
pub_date: 2020-03-18
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.3.3-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming days, including a new alpha Tor Browser.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.4.3.3-alpha fixes several bugs in previous releases, including TROVE-2020-002, a major denial-of-service vulnerability that affected all released Tor instances since 0.2.1.5-alpha. Using this vulnerability, an attacker could cause Tor instances to consume a huge amount of CPU, disrupting their operations for several seconds or minutes. This attack could be launched by anybody against a relay, or by a directory cache against any client that had connected to it. The attacker could launch this attack as much as they wanted, thereby disrupting service or creating patterns that could aid in traffic analysis. This issue was found by OSS-Fuzz, and is also tracked as CVE-2020-10592.</p>
<p>We do not have reason to believe that this attack is currently being exploited in the wild, but nonetheless we advise everyone to upgrade as soon as packages are available.</p>
<p>There are also new stable releases coming out today; I'll describe them in an upcoming post.</p>
<h2>Changes in version 0.4.3.3-alpha - 2020-03-18</h2>
<ul>
<li>Major bugfixes (security, denial-of-service):
<ul>
<li>Fix a denial-of-service bug that could be used by anyone to consume a bunch of CPU on any Tor relay or authority, or by directories to consume a bunch of CPU on clients or hidden services. Because of the potential for CPU consumption to introduce observable timing patterns, we are treating this as a high-severity security issue. Fixes bug <a href="https://bugs.torproject.org/33119">33119</a>; bugfix on 0.2.1.5-alpha. Found by OSS-Fuzz. We are also tracking this issue as TROVE-2020-002 and CVE-2020-10592.</li>
</ul>
</li>
<li>Major bugfixes (circuit padding, memory leak):
<ul>
<li>Avoid a remotely triggered memory leak in the case that a circuit padding machine is somehow negotiated twice on the same circuit. Fixes bug <a href="https://bugs.torproject.org/33619">33619</a>; bugfix on 0.4.0.1-alpha. Found by Tobias Pulls. This is also tracked as TROVE-2020-004 and CVE-2020-10593.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major bugfixes (directory authority):
<ul>
<li>Directory authorities will now send a 503 (not enough bandwidth) code to clients when under bandwidth pressure. Known relays and other authorities will always be answered regardless of the bandwidth situation. Fixes bug <a href="https://bugs.torproject.org/33029">33029</a>; bugfix on 0.1.2.5-alpha.</li>
</ul>
</li>
<li>Minor features (diagnostic):
<ul>
<li>Improve assertions and add some memory-poisoning code to try to track down possible causes of a rare crash (32564) in the EWMA code. Closes ticket <a href="https://bugs.torproject.org/33290">33290</a>.</li>
</ul>
</li>
<li>Minor features (directory authorities):
<ul>
<li>Directory authorities now reject descriptors from relays running Tor versions from the 0.2.9 and 0.4.0 series. The 0.3.5 series is still allowed. Resolves ticket <a href="https://bugs.torproject.org/32672">32672</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (usability):
<ul>
<li>Include more information when failing to parse a configuration value. This should make it easier to tell what's going wrong when a configuration file doesn't parse. Closes ticket <a href="https://bugs.torproject.org/33460">33460</a>.</li>
</ul>
</li>
<li>Minor bugfix (relay, configuration):
<ul>
<li>Warn if the ContactInfo field is not set, and tell the relay operator that not having a ContactInfo field set might cause their relay to get rejected in the future. Fixes bug <a href="https://bugs.torproject.org/33361">33361</a>; bugfix on 0.1.1.10-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (coding best practices checks):
<ul>
<li>Allow the "practracker" script to read unicode files when using Python 2. We made the script use unicode literals in 0.4.3.1-alpha, but didn't change the codec for opening files. Fixes bug <a href="https://bugs.torproject.org/33374">33374</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (continuous integration):
<ul>
<li>Remove the buggy and unused mirroring job. Fixes bug <a href="https://bugs.torproject.org/33213">33213</a>; bugfix on 0.3.2.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v3, client):
<ul>
<li>Remove a BUG() warning that would cause a stack trace if an onion service descriptor was freed while we were waiting for a rendezvous circuit to complete. Fixes bug <a href="https://bugs.torproject.org/28992">28992</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion services v3):
<ul>
<li>Fix an assertion failure that could result from a corrupted ADD_ONION control port command. Found by Saibato. Fixes bug <a href="https://bugs.torproject.org/33137">33137</a>; bugfix on 0.3.3.1-alpha. This issue is also tracked as TROVE-2020-003.</li>
</ul>
</li>
<li>Documentation (manpage):
<ul>
<li>Alphabetize the Server and Directory server sections of the tor manpage. Also split Statistics options into their own section of the manpage. Closes ticket <a href="https://bugs.torproject.org/33188">33188</a>. Work by Swati Thacker as part of Google Season of Docs.</li>
<li>Document the __OwningControllerProcess torrc option and specify its polling interval. Resolves issue <a href="https://bugs.torproject.org/32971">32971</a>.</li>
</ul>
</li>
<li>Testing (Travis CI):
<ul>
<li>Remove a redundant distcheck job. Closes ticket <a href="https://bugs.torproject.org/33194">33194</a>.</li>
<li>Sort the Travis jobs in order of speed: putting the slowest jobs first takes full advantage of Travis job concurrency. Closes ticket <a href="https://bugs.torproject.org/33194">33194</a>.</li>
<li>Stop allowing the Chutney IPv6 Travis job to fail. This job was previously configured to fast_finish (which requires allow_failure), to speed up the build. Closes ticket <a href="https://bugs.torproject.org/33195">33195</a>.</li>
<li>When a Travis chutney job fails, use chutney's new "diagnostics.sh" tool to produce detailed diagnostic output. Closes ticket <a href="https://bugs.torproject.org/32792">32792</a>.</li>
</ul>
</li>
</ul>

