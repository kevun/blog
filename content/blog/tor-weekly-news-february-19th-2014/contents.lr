title: Tor Weekly News — February 19th, 2014
---
pub_date: 2014-02-19
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the seventh issue of Tor Weekly News in 2014, the weekly newsletter that covers what is happening in the <a href="http://redd.it/1y5y49" rel="nofollow">inked</a> Tor community.</p>

<h1>Tor 0.2.5.2-alpha is out</h1>

<p>Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032150.html" rel="nofollow">announced</a> the second alpha release in the Tor 0.2.5 series. As well as incorporating “all the fixes from 0.2.4.18-rc and 0.2.4.20, like the “poor random number generation” fix and the “building too many circuits” fix”, this release brings with it several new features of its own, among them the forced inclusion of at least one relay capable of the NTor handshake in every three-hop circuit, which should reduce the chance “that we’re building a circuit that’s worth attacking by an adversary who finds breaking 1024-bit crypto doable”, as Roger wrote.</p>

<p>You can read the full changelog in Roger’s announcement, and download the new release from <a href="https://www.torproject.org/dist/" rel="nofollow">the Tor Project website</a>.</p>

<h1>Tor Browser 3.5.2.1 is released</h1>

<p>A <a href="https://blog.torproject.org/blog/tor-browser-3521-released" rel="nofollow">new point release of the Tor Browser Bundle was put out</a> on February 15th. A change in how Mozilla tags Firefox releases <a href="https://bugs.torproject.org/10895" rel="nofollow">broke the localization</a> of the browser interface. This release restores proper behavior for languages other than English.</p>

<p>Apart from the localization fix and the removal of unneeded libraries from the Windows bundles, no other changes have been made.</p>

<h1>Help draft a proposal for partnership with the Wikimedia Foundation</h1>

<p>The relationship between Tor users and the Wikimedia Foundation, which operates Wikipedia and related projects, is currently not as good as it could be: many Tor users feel they should be able to contribute to Wikipedia anonymously, while many Wikipedia editors are wary of dealing with a tool that could enable untraceable or unblockable vandalism of pages and articles.</p>

<p>As a prelude to resolving this conflict, Lane Rasberry has <a href="https://meta.wikimedia.org/wiki/Grants:IdeaLab/Partnership_between_Wikimedia_community_and_Tor_community" rel="nofollow">opened a discussion</a> on the Wikimedia Foundation’s IdeaLab, a forum in which ideas can be discussed and debated before being collaboratively developed into full grant proposals. As Lane wrote, “persons using Tor (the anonymity network) should be able to create Wikipedia accounts and contribute to Wikipedia while logged into those accounts. Some technical problems currently disallow this and some social problems prevent the technical problems from being addressed. Anyone with a proposal of what kind of relationship Tor and the Wikimedia movement should have should describe it here.”</p>

<p>If you are interested in helping to resolve this issue, please see the IdeaLab page, and add your comments!</p>

<h1>Only as good as your weakest transport?</h1>

<p>Delton Barnes <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003906.html" rel="nofollow">pointed out</a> that although the ScrambleSuit pluggable transport protocol includes a certain amount of protection against active probing for bridges by censorship systems like the Chinese “Great Firewall”, bridge operators who run more vulnerable protocols like obfs3 alongside ScrambleSuit may be increasing the risk that censors will discover their relay and block connections to it of any kind.</p>

<p>In reply, Philipp Winter <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003907.html" rel="nofollow">conceded</a> that although the Chinese censorship system currently seems to block bridges by IP:port tuples, rather than by IP address alone, the mere presence of an easily-discoverable pluggable transport protocol (or a public relay) on a given machine makes it more likely that a censor will be motivated to try and defeat protections such as those offered by ScrambleSuit. “So you are right, only running ScrambleSuit gives your bridge more protection than running other protocols at the same time — at the cost of attracting less users, however”, he concluded.</p>

<h1>Miscellaneous news</h1>

<p>The Tails team has published <a href="https://tails.boum.org/news/report_2014_01/" rel="nofollow">its report for January 2014</a>. A lot is happening in the growing community of Tails developers. Have a look!</p>

<p>Qingping Hou sent out the beginnings of a proposal to increase the speed of connections to Tor Hidden Services by using circuits of only five hops, and <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006198.html" rel="nofollow">asked the community for feedback</a>.</p>

<p>Yawning Angel called for help with testing obfsclient, a C++ pluggable transport client, and clarified the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006211.html" rel="nofollow">next steps in the development process</a>.</p>

<p>David Fifield <a href="https://lists.torproject.org/pipermail/tor-qa/2014-February/000338.html" rel="nofollow">made available a second updated set of the experimental Tor Pluggable Transport Bundle with tor-fw-helper</a>, which fixes several of the errors encountered in the first version.</p>

<p>Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006230.html" rel="nofollow">called for help with reviewing proposal 227</a>, which involves “extending the Tor consensus document to include digests of the latest versions of one or more package files, to allow software using Tor to determine its up-to-dateness, and help users verify that they are getting the correct software”.</p>

<p>Efforts to include the FTE protocol in the Tor Pluggable Transport Bundles have taken a step forward, as Kevin P. Dyer <a href="https://lists.torproject.org/pipermail/tor-dev/2014-February/006223.html" rel="nofollow">announced</a> the release of a patch including fteproxy that not only works, but also builds deterministically, in keeping with the Tor Project’s focus on <a href="https://blog.torproject.org/blog/deterministic-builds-part-one-cyberwar-and-global-compromise" rel="nofollow">build security</a>.</p>

<p>Rusty Bird <a href="https://lists.torproject.org/pipermail/tor-talk/2014-February/032152.html" rel="nofollow">announced</a> the release of <a href="https://github.com/rustybird/corridor" rel="nofollow">corridor</a>, a Tor traffic whitelisting gateway. corridor will turn a Linux system into a router that “allows only connections to Tor relays to pass through (no clearnet leaks!)”. However, unlike transparent proxying solutions, “client computers are themselves responsible for torifying their own traffic.”</p>

<p>One relay operator was looking for an <span class="geshifilter"><code class="php geshifilter-php">init<span style="color: #339933;">.</span>d</code></span> script able to start multiple tor instances. Johannes Fürmann <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003915.html" rel="nofollow">pointed out</a> that one was <a href="https://github.com/torservers/setup-automation/blob/master/config/initd-tor" rel="nofollow">available in the torservers.net Git repository</a>.</p>

<p>TorBirdy, the Tor-enabling extension for the Thunderbird mail client, currently disables the automated account configuration system for security reasons. <a href="https://bugs.torproject.org/1083" rel="nofollow">Progress is being made</a> in changing the behavior to make it fit the expectations of Tor users.</p>

<p>Andrea Shepard submitted seven status reports, covering her activity since July 2013 (<a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000455.html" rel="nofollow">July</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000456.html" rel="nofollow">August</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000457.html" rel="nofollow">September</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000458.html" rel="nofollow">October</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000459.html" rel="nofollow">November</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000460.html" rel="nofollow">December</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-February/000461.html" rel="nofollow">January</a>).</p>

<h1>Tor help desk roundup</h1>

<p>Users often ask for help updating their Tor Browser Bundle. Some users download and try to run their new Tor Browser without first closing their current Tor Browser. This causes an error message, since Tor is already running. The Tor Browser Bundle update is not an upgrade that can be applied while Tor Browser is still running. Users must close their current Tor Browser before running their newly downloaded Tor Browser Bundle.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, harmony, and Matt Pagan.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

