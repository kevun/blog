title: Strength in Numbers: An Entire Ecosystem Relies on Tor
---
pub_date: 2018-12-13
---
author: al
---
tags:

Strength in Numbers
Guardian Project
onion services
Onion Browser
---
categories:

onion services
partners
---
summary: If the Tor Project, the Tor network, and Tor Browser were to disappear, what would happen? Not only would millions of global, daily users lose access to Tor’s software, but the diverse ecosystem of privacy, security, and anti-censorship applications that rely on the Tor network would cease to function.
---
_html_body:

<p><em>This post is one in a series of blogs to complement our 2018 crowdfunding campaign, Strength in Numbers. Anonymity loves company and we are all safer and stronger when we work together. <a href="https://torproject.org/donate/donate-sin-ec-bp">Please contribute today</a>, and your gift will be matched by Mozilla.</em></p>
<p>If the Tor Project, the Tor network, and Tor Browser were to disappear, what would happen? Not only would millions of global, daily users lose access to Tor’s software, but the diverse ecosystem of privacy, security, and anti-censorship applications that rely on the Tor network would <strong>cease to function</strong>.</p>
<p>The same network and technologies that allow you to use the internet anonymously power the anonymity, circumvention, and privacy features of many third-party web browsers, communications apps, secure operating systems, monitoring tools, and file sharing apps.</p>
<p><a href="https://torproject.org/donate/donate-sin-ec-bp">Your donation</a> to the Tor Project will have a positive ripple effect on the many vital anonymity and privacy applications that rely on the Tor network and technologies, like:</p>
<h2>Whistleblowing, file sharing, and reporting tools that are secure and anonymous</h2>
<p><a href="https://securedrop.org/">SecureDrop</a> is an open-source whistleblower submission system developed by many Tor core contributors that media organizations can install to accept documents from anonymous sources. <a href="https://www.globaleaks.org/">GlobaLeaks</a> is an open-source, free software tool intended to enable secure and anonymous whistleblowing initiatives. <a href="https://onionshare.org/">OnionShare</a> is an open source tool developed by a Tor core contributor that lets you securely and anonymously share a file of any size. All of these tools rely on the Tor network.</p>
<p>More than <strong>75</strong> <a href="https://securedrop.org/directory">news outlets</a>, including Al Jazeera, the Associated Press, CBC, The Guardian, The New York Times, and the Washington Post rely on SecureDrop and the Tor network to provide their sources and journalists with a safer way to communicate. More than <strong>60</strong> <a href="https://www.globaleaks.org/who-uses-it/">projects and initiatives utilize GlobaLeaks</a> for activism, anti-corruption, investigative journalism, and corporate whistleblowing work.</p>
<p><strong>Because of the Tor network, we can call attention to injustice with reduced risk.</strong></p>
<h2>Pluggable transports, an open source censorship circumvention technique</h2>
<p>Many governments determined to block access to the Tor network order ISPs to block the publicly listed IP addresses of Tor relays. In response, we developed <a href="https://www.torproject.org/docs/bridges.html.en">bridge relays</a> (“bridges”), which are Tor nodes that are not listed publicly, and are thus more difficult for governments to find and block. As government censorship increases globally, repressive regimes become more sophisticated in their tactics. Some governments employ a censorship technique called Deep Packet Inspection (DPI) to classify Internet traffic flows by protocol. DPI enables a censor to recognize and filter a wide variety of tools, including Tor traffic, even when it connects through a bridge.</p>
<p>In response to these kinds of censorship techniques, the Tor Project developed a flexible circumvention framework known as <a href="https://www.torproject.org/docs/pluggable-transports.html.en">pluggable transports</a>. Pluggable transports mask Tor traffic to make it look like other kinds of internet traffic, essentially making Tor traffic invisible to adversaries and resistant against DPI censorship tactics.</p>
<p>The Tor Project is the origin of the pluggable transports concept. Popular circumvention and VPN tools like <a href="https://getlantern.org/en_US/">Lantern</a> and <a href="https://www.tunnelbear.com/">Tunnelbear</a> directly reuse Tor’s open source <a href="https://github.com/Yawning/obfs4/blob/master/doc/obfs4-spec.txt">obfs4 pluggable transport system</a> for their own censorship circumvention services. Developers can take advantage of <a href="https://blog.torproject.org/strength-numbers-fighting-internet-censorship">OONI’s dataset on internet censorship</a>, likely the largest publicly-available resource to-date, to inform the circumvention methods they develop.</p>
<p><strong>Because of the Tor Project, we all have a robust ecosystem of options for circumvention in the face of sophisticated censorship.</strong></p>
<h2>Onion services, a method for further anonymizing internet use</h2>
<p>Most privacy protection tools provide end-to-end encryption, which can protect users against surveillance techniques that are focused on the content of their conversations. But these tools don’t protect metadata--information like: who talks to who, when, and how much they say--which can be used to build behavioral maps (revealing a user’s contacts and the frequency with which they communicate, for example). As government censorship becomes more sophisticated, metadata surveillance has become a technique used to track individuals and their activities.</p>
<p>In fact, some <a href="https://www.eff.org/issues/mass-surveillance-technologies">human rights organizations report that many surveillance tools target metadata</a>, and bad actors can use this metadata to support further actions of oppression, such as public defamation or doxing, arrest, and censorship.</p>
<p>Our solution for this problem is <a href="https://www.torproject.org/docs/onion-services.html.en">onion services</a>. An onion service is a website or any other internet service that is available through the secure Tor network.</p>
<p>Take <a href="https://onion.debian.org/">Debian</a>’s onion service, for example: when you keep your <a href="https://www.debian.org/">Debian-based operating system</a> up-to-date via automated updates through Debian’s onion service, you ensure that the Debian Project cannot target your computer specifically. If you update your Debian machine at home via your ordinary, public, relatively unchanging IP address, in theory, a bad actor Debian administrator could target you specifically by shipping you a backdoored package. But when you update your system via Debian’s onion service, you become an anonymous user accessing their anonymous services. A bad actor would have to attack <strong>all </strong>users that access the onion service, which would likely be discovered and mitigated more quickly than a targeted attack on a single individual.</p>
<p>Another example of the use of onion services is <a href="https://guardianproject.github.io/haven/">Haven</a>, an app developed by the Guardian Project with <a href="https://freedom.press/">Freedom of the Press Foundation</a> and Edward Snowden. Haven can turn any Android device into a motion, sound, vibration, and light detector, watching for unexpected guests and unwanted intruders. All event logs and captured media can be remotely and privately accessed through an onion service.</p>
<p>Many other important projects and organizations--including <a href="https://www.facebook.com/facebookcorewwwi">Facebook</a>, <a href="https://open.nytimes.com/https-open-nytimes-com-the-new-york-times-as-a-tor-onion-service-e0d0b67b7482">The New York Times</a>, <a href="https://riseup.net/en/tor">Riseup.net</a>, <a href="https://www.propublica.org/nerds/a-more-secure-and-anonymous-propublica-using-tor-hidden-services">ProPublica</a>, <a href="https://blog.torproject.org/privacy-international-protects-partners-its-onion-address">Privacy International</a>, and <a href="https://onionshare.org/">OnionShare</a>--have adopted Tor’s onion services by also offering their websites as .onion addresses so people can access their sites safely and securely while protecting themselves from the negative consequences that exposure brings.</p>
<p><strong>Because of the Tor Project, we have methods for protecting ourselves with end-to-end encryption and against metadata surveillance.</strong></p>
<h2>Dozens of other privacy, security, and anti-censorship web browsers, chat apps, and related tools</h2>
<p>Tor is at the heart of many interconnected efforts to make the lives of people better around the world.</p>
<p>In addition to Tor Browser and Tor Browser for Android (alpha), there are a number of <strong>web browsers</strong> (and more on the way) that integrate Tor and provide associated privacy protections:</p>
<ul>
<li><a href="https://onionbrowser.com/">Onion Browser</a> (iOS)</li>
<li><a href="https://brave.com/">Brave</a> (Desktop)</li>
</ul>
<p>From peer-to-peer messaging apps to email plugins, there are a number of <strong>private communication apps</strong> that you can use to anonymize your communications. The following applications utilize the Tor network to route your messages and offer privacy and security:</p>
<ul>
<li><a href="https://briarproject.org/">Briar</a> (Android)</li>
<li><a href="https://ricochet.im/">Ricochet</a> (Desktop)</li>
<li><a href="https://chatsecure.org/">ChatSecure</a> (iOS)</li>
<li><a href="https://addons.thunderbird.net/en-US/thunderbird/addon/torbirdy/">TorBirdy </a>(Email, desktop)</li>
</ul>
<p>Currently, there are a number of Linux-based <strong>operating systems</strong> that aim to provide anonymity and privacy to their users and utilize the Tor network in part to do so:</p>
<ul>
<li><a href="https://tails.boum.org/">Tails</a> (Desktop operating system)</li>
<li><a href="https://www.qubes-os.org/">Qubes</a> (Desktop operating system)</li>
<li><a href="https://www.whonix.org/">Whonix </a>(Desktop operating system)</li>
</ul>
<p>The security ecosystem even offers tools to allow you to remain secure when you’re using apps that don’t send your traffic over the Tor network. The following applications allow you to <strong>tunnel third-party app internet traffic over the Tor network</strong>, whether by working as a proxy--like Orbot, developed by our friends and partners at the Guardian Project--or as a "system-wide VPN."</p>
<ul>
<li><a href="https://guardianproject.info/apps/orbot/">Orbot</a> (Android)</li>
<li><a href="https://github.com/iCepa">iCepa</a> (iOS)</li>
</ul>
<p>Your gift to the Tor Project has a positive ripple effect on the many vital anonymity and privacy applications that rely on the Tor network and technologies. When you support Tor, you support all of these projects as well. <strong>Plus: Every donation from now until December 31, 2018, will be matched 1:1 by Mozilla. Your impact—doubled.</strong></p>
<p><a href="https://torproject.org/donate/donate-sin-ec-bp"><img alt="donate button" src="/static/images/blog/inline-images/tor-donate-button_8.png" class="align-center" /></a></p>
<p> </p>

---
_comments:

<a id="comment-279037"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279037" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Confused (not verified)</span> said:</p>
      <p class="date-time">December 14, 2018</p>
    </div>
    <a href="#comment-279037">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279037" class="permalink" rel="bookmark">Thank you for this summary…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for this summary of the many good things enabled by Tor.</p>
<p>A half dozen are associated with either Android or iOS smart phones.  I have a question about those, for users experienced in using Tor on a desktop but who haven't owned a smart phone:</p>
<p>Would Haven or Orbot be unreliable if the Android phone is not a brand new phone just purchased?  Does one need to sign up with Google to use Haven on an Android phone?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279061"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279061" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 15, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279037" class="permalink" rel="bookmark">Thank you for this summary…</a> by <span>Confused (not verified)</span></p>
    <a href="#comment-279061">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279061" class="permalink" rel="bookmark">Would Haven or Orbot be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>Would Haven or Orbot be unreliable if the Android phone is not a brand new phone just purchased?</p></blockquote>
<p>Nope not really. Whether an Android phone is usable for Orbot or Haven depends on the internet connectivity of the device (4G/Wi-Fi, whether Tor is blocked in said country of use...). Of course, if the device is compromised then all bets are off. Bare minimum you should do is to wipe the purchased device (factory reset) and reflash the entire OS before configuring it for use.</p>
<blockquote><p>Does one need to sign up with Google to use Haven on an Android phone?</p></blockquote>
<p>Nope. You can directly download and install Haven on the desired Android device via Fdroid. Refer to the main Haven install page/guide at <a href="https://guardianproject.github.io/haven/#install" rel="nofollow">https://guardianproject.github.io/haven/#install</a> for more info.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279140"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279140" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Still confused (not verified)</span> said:</p>
      <p class="date-time">December 19, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279061" class="permalink" rel="bookmark">Would Haven or Orbot be…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279140">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279140" class="permalink" rel="bookmark">Thanks for the link. 
My…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the link. </p>
<p>My use case scenario for Haven is similar to Movilizatorio (described by Andy Greenberg).</p>
<p>Do I understand correctly that I could purchase a *new* low-end Android for $100, install Haven without needing to involve Google or a phone company, leave home for a few hours, and when I return, use my laptop to access on onion (hosted on the phone and accessible only by me) in order to review what Haven says happened in my home while I was out?  Or does the phone need a phone number or internet access or what else?  Could I buy the phone for cash?  In a store not owned by Google or another company likely to surveil or ID customers?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-279063"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279063" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Farrt Slimson (not verified)</span> said:</p>
      <p class="date-time">December 16, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279037" class="permalink" rel="bookmark">Thank you for this summary…</a> by <span>Confused (not verified)</span></p>
    <a href="#comment-279063">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279063" class="permalink" rel="bookmark">I use android - not familiar…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I use android - not familiar with ios - using various phone models and have never signed up with google. I remove as many google apps, including system apps as I can. On some models I have rooted them giving me admin control. On models to problematic to root I just uninstall apps. I also use f-droid as repository rather than play store. Have never used google play store. Haven is on f-droid. Hope this reply helps.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-279040"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279040" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>peter the fox (not verified)</span> said:</p>
      <p class="date-time">December 14, 2018</p>
    </div>
    <a href="#comment-279040">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279040" class="permalink" rel="bookmark">Very good.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very good.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279064"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279064" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>vart slimson (not verified)</span> said:</p>
      <p class="date-time">December 16, 2018</p>
    </div>
    <a href="#comment-279064">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279064" class="permalink" rel="bookmark">No need to sign up with them…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No need to sign up with them to use haven on android. Available from f-droid repository.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279141"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279141" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279064" class="permalink" rel="bookmark">No need to sign up with them…</a> by <span>vart slimson (not verified)</span></p>
    <a href="#comment-279141">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279141" class="permalink" rel="bookmark">f-droid repository?  Is that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>f-droid repository?  Is that an internet site?  Does the phone need to have a phone number or internet access?  Or can I download something using a laptop over Tor and then somehow transfer it to the phone?  Via a USB port or something like that?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279208"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279208" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279141" class="permalink" rel="bookmark">f-droid repository?  Is that…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279208">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279208" class="permalink" rel="bookmark">So what this refers to is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So what this refers to is the alternative "app store" for Android called F-Droid, which provides a repository of free and open-source apps for users who do not want to be reliant on Google services and the Play Store's ecosystem.</p>
<p>Your phone doesn't need a phone number to access F-Droid, but it does need Internet access to download and install the F-Droid store APK (similar to the Play Store), which keeps the apps you download from the F-Droid repository updated. Yep, you can also download the F-Droid APK from your laptop, transfer it to your phone and install it there (also known as "sideloading"), incase you can't get Internet access for your phone at the moment.</p>
<p>The F-Droid store can be accessed at: <a href="https://f-droid.org/en/" rel="nofollow">https://f-droid.org/en/</a>. Just download and install the APK, follow the onscreen instructions and you should be up and running with the F-Droid repository/app store. You can then search for Haven within the F-Droid application and install it from there.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279213"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279213" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="random answerer for this q.">random answere… (not verified)</span> said:</p>
      <p class="date-time">December 30, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279141" class="permalink" rel="bookmark">f-droid repository?  Is that…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279213">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279213" class="permalink" rel="bookmark">F-Droid is an internet site,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>F-Droid is an internet site, yes.<br />
Can download these apps from F-Droid using phone's WiFi for internet connection without any cell service provider.  Also recommended to download F-Droid's own f-droid app for future app downloads from F-Droid to benefit from relatively automatic app updates for security fixes.</p>
<p>Or use a computer and download the actual app files (.apk) you want to a micro SD card and then plug the card into your phone and install from there. (Except if your Android phone imitates Apple by lacking a memory card slot.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-279157"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279157" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2018</p>
    </div>
    <a href="#comment-279157">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279157" class="permalink" rel="bookmark">Yes, if Tor were to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, if Tor were to disappear, my ability to use the internet would disappear along with it. Much appreciation for everyone's hard work!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279273"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279273" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Donation (not verified)</span> said:</p>
      <p class="date-time">January 05, 2019</p>
    </div>
    <a href="#comment-279273">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279273" class="permalink" rel="bookmark">What could Tor Project do…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What could Tor Project do with $500.000.000.00 ? What would Tor Project really do?<br />
Maybe more secure core 10 Gbps bare metal servers all over the place?<br />
Maybe some cover up front end like CloudFlare to make it "expensive" to block it?<br />
Maybe do like music and movie industry and do lobbying all over the place to allow less surveillance and more individual, organization and corporate freedoms?</p>
</div>
  </div>
</article>
<!-- Comment END -->
