title: The Debian OpenSSL flaw: what does it mean for Tor clients?
---
pub_date: 2008-05-14
---
author: arma
---
tags:

openssl
security advisory
---
_html_body:

<p>There have been a lot of questions today about just what <a href="http://lists.debian.org/debian-security-announce/2008/msg00152.html" rel="nofollow">the<br />
recent Debian OpenSSL</a> flaw means for Tor clients. Here's an attempt to<br />
explain it in a bit more detail. (Go read <a href="http://archives.seul.org/or/announce/May-2008/msg00000.html" rel="nofollow">the Tor security advisory</a> before<br />
reading this post.)</p>

<p>First, let's look at the security/anonymity implications for users who<br />
<i>aren't</i> running on Debian, Ubuntu, or similar. These implications all<br />
stem from the fact that some of the Tor relays and v3 directory authorities<br />
have weak keys, so the Tor network isn't able to provide as much anonymity<br />
as we would like.</p>

<p>The biggest issue is that perhaps 300 Tor relays were running with<br />
weak keys and weak crypto, out of the roughly 1500-2000 total running<br />
relays. What can an attacker do from this? If you happen to pick three<br />
weak relays in a row for your circuit, then somebody watching your local<br />
network connection (or watching the first relay you pick) could break all<br />
the layers of Tor encryption and read the traffic as if they were watching<br />
it at the exit relay. (I don't want to say they could read the plaintext,<br />
because if you used end-to-end encryption like SSL they wouldn't be able<br />
to see inside of that -- unless of course the webserver you contact is<br />
running Debian and affected by this bug!) Because this attacker can read<br />
the traffic inside Tor, they would also break your anonymity: they know<br />
both you and the destination(s) you asked for.</p>

<p>Worse, this attack works against past traffic too: what if an attacker<br />
logged traffic over the past two years? As long as there's a single<br />
non-weak non-colluding Tor relay in your circuit, you're fine -- that<br />
relay will provide encryption that the attacker can't break, then or<br />
now. But if you ever picked a path that consisted entirely of relays<br />
with broken RNGs, and an attacker logged this traffic, then he can unwrap<br />
the traffic from his logs using the same approach as above.</p>

<p>(Somebody who knows a Tor relay's private key could also impersonate that<br />
relay. So he can do a man-in-the-middle attack, intercepting your traffic<br />
to the "real" Tor relay and handling it himself. But this wouldn't give<br />
him anything that he can't already do just by watching. Another attack would<br />
be to create a fake descriptor and upload that. But this wouldn't give him<br />
anything he can't do by starting his own relay and uploading a descriptor for it.)</p>

<p>This evening we've begun the process of making the directory authorities<br />
reject all uploaded descriptors that are signed using these keys, so<br />
we will effectively cut them out of the network. Peter Palfrader, our<br />
Tor debian package maintainer, has also put out a new deb package that<br />
will automatically discard the old relay keys when the relays upgrade,<br />
so they'll automatically generate new safe keys.</p>

<p>The next big issue is that three of the six v3 directory authorities<br />
were using weak keys for their directory votes and signatures. This<br />
issue doesn't affect Tor clients running the 0.1.2.x (stable) series,<br />
since those clients use the v2 directory system, none of whose keys<br />
(I think) are weak.</p>

<p>What can three v3 authorities do? If they could forge a new v3<br />
networkstatus consensus, they could trick users into using their own<br />
fabricated Tor network, which would totally ruin their anonymity. Worse,<br />
they could do this in a way that would be very hard to detect, by just<br />
giving out their forged consensus to a few target users and giving out<br />
the "real" consensus the rest of the time. But fortunately, Tor clients<br />
require a majority of signatures before they'll believe the consensus --<br />
and that's four of six. (Whew, that was close!)</p>

<p>Now, three v3 authorities <i>can</i> still influence the consensus a lot. As<br />
one example, they could pick their favorite relays (say, because they<br />
operate those relays, or because those are the ones that are easiest to<br />
monitor), and put in three votes claiming that all the other relays are<br />
unusable. The resulting consensus will then list only those relays as<br />
"Running", since no other relays got enough votes. Then we're back to<br />
the above scenario. But in this case at least one other v3 authority<br />
would need to participate in building the consensus, so this couldn't be<br />
a selective one-off attack. The whole consensus would appear different<br />
to everybody, and hopefully somebody would notice.</p>

<p>The 0.2.0.26-rc release resolves these concerns by replacing those<br />
three weak identity keys with new strong ones. Once you upgrade, your<br />
new Tor won't trust any of those old keys -- so if anybody tries the<br />
above attacks on you, your Tor won't buy it.</p>

<p>And the last issue that affects non-Debian users? If you use a hidden<br />
service that generated its ".onion" key on a weak system, then you can<br />
no longer be sure that you're actually talking to the original person<br />
who generated the key. That's because somebody else might have figured<br />
out the private key for that service, and started advertising it himself.<br />
(Ordinarily, hidden services guarantee that nobody can intercept and read<br />
or modify your communications with the service, because only the person<br />
on the other end knows the private key that generated the ".onion" name.)</p>

<p>Now, what about the effects on Tor clients that run Debian, Ubuntu,<br />
or the like? Well, first they're affected by all the above attacks. And<br />
on top of that, any encryption they do can be considered to have no real<br />
effect. So for example if an attacker can observe your traffic either<br />
locally or at the first relay, he can see right through it all.</p>

<p>Similarly, if anybody has logs of traffic coming out of a Debian or Ubuntu<br />
Tor client, they can strip it of its encryption, and thus retroactively<br />
break the anonymity.</p>

<p>And lastly, don't forget there are plenty of other issues that this<br />
OpenSSL bug causes that are unrelated to Tor. As a simple example, if your<br />
bank generated its SSL cert on Debian or Ubuntu, then your SSL connection<br />
to your bank (likely including your password) is readable. Or if you ever<br />
tried to ssh into (or out of) an affected system, you have a problem. I<br />
imagine some broader lists of examples will start appearing soon.</p>

---
_comments:

<a id="comment-65"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-65" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 16, 2008</p>
    </div>
    <a href="#comment-65">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-65" class="permalink" rel="bookmark">Attack confirmed - Tor leaks like a sieve.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"What can three v3 authorities do? If they could forge a new v3<br />
networkstatus consensus, they could trick users into using their own<br />
fabricated Tor network, which would totally ruin their anonymity. Worse,<br />
they could do this in a way that would be very hard to detect, by just<br />
giving out their forged consensus to a few target users and giving out<br />
the "real" consensus the rest of the time. But fortunately, Tor clients<br />
require a majority of signatures before they'll believe the consensus --<br />
and that's four of six. (Whew, that was close!)"</p>
<p>So if te attacker had control of one of the other v3 authorities ALSO this attack would definitely work.. not really *Whew* then is it, more of a - "we've been scuppered".</p>
<p>This attack was done and was successful.</p>
<p>Let **nobody** tell you different.</p>
<p>Also the true IP address of **all* "hidden services" is being identified (by USA .edu ops), after less than 24hrs up time for a brand new "hidden service" (non-debian), when only 1 access (single page request) of the hidden service from the tor network in that time.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-68"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-68" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 19, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-65" class="permalink" rel="bookmark">Attack confirmed - Tor leaks like a sieve.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-68">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-68" class="permalink" rel="bookmark">FUD vs Proof</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"So if te attacker had control of one of the other v3 authorities ALSO this attack would definitely work.. not really *Whew* then is it, more of a - "we've been scuppered"."</p>
<p>Could you expand upon how this is possible with just one?  One of seven does not a consensus make.</p>
<p>"This attack was done and was successful."</p>
<p>Proof of this?</p>
<p>"Also the true IP address of **all* "hidden services" is being identified (by USA .edu ops), after less than 24hrs up time for a brand new "hidden service" (non-debian), when only 1 access (single page request) of the hidden service from the tor network in that time"</p>
<p>Feel free to provide some proof of this actually working.  I'm happy to setup a hidden service and have it identified by IP address as a test.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-74"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Nick Mathewson (not verified)</span> said:</p>
      <p class="date-time">May 20, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-74">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74" class="permalink" rel="bookmark">&gt; One of seven does not a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; One of seven does not a consensus make.</p>
<p>They're probably saying that if an attacker had broken into one of the unaffected machines and stolen its keys, and if that attacker had also known about the Debian OpenSSL vulnerability, they'd be able forge a consensus.  That much is possible.</p>
<p>&gt;&gt; This attack was done and was successful.<br />
&gt; Proof of this?</p>
<p>Agreed.  Frankly, it doesn't seem very likely that somebody would report an actual authority compromise by saying "An authority has been compromised but I won't tell you when, how, how I know about it, or how you can confirm that I'm telling you the truth."  If somebody knew about a compromise and they wanted to help Tor, they would report it so it could be fixed.  If they knew about a compromise and wanted to attack Tor users, they would keep it secret so they could exploit it.  This looks like random FUD to me too.</p>
<p>As for this hidden-service-identifying attack, could you maybe provide some kind of a link to what you're talking about?  When we speak anonymously, it's very important to provide evidence for contentious claims, since we can't very well expect people to accept them based on our reputation.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-75"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-75" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">May 20, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-75">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-75" class="permalink" rel="bookmark">re: FUD vs proof</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Even better would be to have published research on the topic.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-66"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="https://www.pgpru.com">unknown (not verified)</a> said:</p>
      <p class="date-time">May 16, 2008</p>
    </div>
    <a href="#comment-66">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66" class="permalink" rel="bookmark">Backward decryption of Tor traffic after Debian OpenSSL bug disc</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I send this question in the or-talk list before read here with care.</p>
<p>Could you answer in or-talk?</p>
<p>-----BEGIN PGP SIGNED MESSAGE-----<br />
Hash: SHA1</p>
<p>// Backward decryption of Tor traffic after Debian OpenSSL bug disclosure</p>
<p>Let some passive adversary haves a records of traffic between users Debian<br />
GNU/Linux tor-client and servers of Tor-network (a lot of Debian's too).<br />
The records dated 2006-may 2008.</p>
<p>Now Debian OpenSSL PRNG bug disclosed. All ~250000 "pseudorandom" values known.</p>
<p>Is it possible to adversary use this data to backward partially decryption of<br />
recorded and stored users traffic?</p>
<p>- From predicted states of broken PRNG he can compute Diffie-Hellman params,<br />
reconstructs ephemerial keys and extract session AES keys between nodes in circuit<br />
if two of circuit has broken PRNG's.</p>
<p>Is it real? Or openSSL PRNG used in tor for generating auth. keys only and not<br />
for session keys material in the case of tor?<br />
-----BEGIN PGP SIGNATURE-----</p>
<p>iD8DBQFILcYLRkm9ZEvRLEARApaoAKCHz8Pk4H8jLI4xgzbCnK1EgRzH1gCffINB<br />
tto9W39Qr3hb4cq978zBC0s=<br />
=vUFM<br />
-----END PGP SIGNATURE-----</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-842"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-842" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2009</p>
    </div>
    <a href="#comment-842">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-842" class="permalink" rel="bookmark">Switch to more secure operating system?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Maybe the directory authorities should switch to OpenBSD or FreeBSD. They both do code audits, which should help prevent this sort of thing from happening. OpenBSD's code audits are more thorough than FreeBSD's code audits.</p>
</div>
  </div>
</article>
<!-- Comment END -->
