title: New Release: Tor 0.3.3.9 (Bridge Authority Update)
---
pub_date: 2018-07-16
---
author: arma
---
tags:

tor releases
bridges
---
categories:

circumvention
releases
---
_html_body:

<p> </p>
<p>Tor 0.3.3.9 moves to a new bridge authority. The only people who need to upgrade are people who <a href="https://www.torproject.org/docs/bridges#RunningABridge">operate a bridge relay</a> — so their bridge address can resume being given out to censored users, and so their stats can resume being included in the metrics pages.</p>
<h2>Changes in version 0.3.3.9 - 2018-07-13</h2>
<ul>
<li>Directory authority changes:
<ul>
<li>The "Bifroest" bridge authority has been retired; the new bridge authority is "Serge", and it is operated by George from the <a href="https://www.torbsd.org/">Tor BSD Diversity project</a>. Closes ticket <a href="https://bugs.torproject.org/26771">26771</a>.</li>
</ul>
</li>
</ul>
<p>We also put out new oldstable releases for 0.2.9.16 and 0.3.2.11, and those releases include other backported fixes:<br />
<a href="https://gitweb.torproject.org/tor.git/tree/ReleaseNotes?h=tor-0.2.9.16">https://gitweb.torproject.org/tor.git/tree/ReleaseNotes?h=tor-0.2.9.16</a><br />
<a href="https://gitweb.torproject.org/tor.git/tree/ReleaseNotes?h=tor-0.3.2.11">https://gitweb.torproject.org/tor.git/tree/ReleaseNotes?h=tor-0.3.2.11</a></p>
<p>If for whatever reason you can't upgrade yet, you can also manually switch to advertising your bridge descriptor to the new bridge authority by using this <a href="https://www.torproject.org/docs/faq#torrc">torrc</a> line:</p>
<p>AlternateBridgeAuthority Serge orport=9001 bridge 66.111.2.131:9030 BA44 A889 E64B 93FA A2B1 14E0 2C2A 279A 8555 C533</p>

---
_comments:

<a id="comment-276152"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276152" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="after updating to 0.3.3.9">after updating… (not verified)</span> said:</p>
      <p class="date-time">July 16, 2018</p>
    </div>
    <a href="#comment-276152">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276152" class="permalink" rel="bookmark">all v3 hidden services are…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>all v3 hidden services are dead (cannot find any HsDir):</p>
<p>[notice] Closed 2 streams for service [scrubbed].onion for reason resolve failed. Fetch status: No more HSDir available to query.</p>
<p>old onion services are OKAY</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276154"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276154" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 17, 2018</p>
    </div>
    <a href="#comment-276154">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276154" class="permalink" rel="bookmark">We also put out new…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>We also put out new oldstable releases for 0.2.9.16 and 0.3.2.11</p></blockquote>
<p>oldstable - for 0.3.2.x, LTS - for 0.2.9.x</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276165"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276165" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="Ana Carmen Vázquez Martínez">Ana Carmen Váz… (not verified)</span> said:</p>
      <p class="date-time">July 19, 2018</p>
    </div>
    <a href="#comment-276165">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276165" class="permalink" rel="bookmark">7/19/2018 9:36:45 AM.400 …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>7/19/2018 9:36:45 AM.400 [WARN] Proxy Client: unable to connect to 154.35.22.12:4304 ("general SOCKS server failure")</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276166"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276166" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Philip (not verified)</span> said:</p>
      <p class="date-time">July 19, 2018</p>
    </div>
    <a href="#comment-276166">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276166" class="permalink" rel="bookmark">The proxy server is refusing…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The proxy server is refusing connections</p>
<p>Firefox is configured to use a proxy server that is refusing connections.</p>
<p>    Check the proxy settings to make sure that they are correct.<br />
    Contact your network administrator to make sure the proxy server is working.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-276168"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276168" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 19, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-276166" class="permalink" rel="bookmark">The proxy server is refusing…</a> by <span>Philip (not verified)</span></p>
    <a href="#comment-276168">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276168" class="permalink" rel="bookmark">Sounds like you have an…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sounds like you have an antivirus system that is reaching in and messing with Tor Browser. If so, you'll need to disable it or maybe even uninstall it.</p>
<p>For more things to try consider also<br />
<a href="https://www.torproject.org/about/contact#support" rel="nofollow">https://www.torproject.org/about/contact#support</a><br />
<a href="https://support.torproject.org/" rel="nofollow">https://support.torproject.org/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-276268"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276268" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2018</p>
    </div>
    <a href="#comment-276268">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276268" class="permalink" rel="bookmark">If you care why sometimes…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you care why sometimes users say Tor "stalls" during bootstrap, see:</p>
<p>[07-29 14:05:34] Torbutton INFO: tor SOCKS: <a href="https://www.https-rulesets.org/v1//latest-rulesets-timestamp" rel="nofollow">https://www.https-rulesets.org/v1//latest-rulesets-timestamp</a> via<br />
                       --unknown--:7c78468ddbed6e418db8215adc6237b1<br />
Tor NOTICE: Bootstrapped 10%: Finishing handshake with directory server<br />
Tor NOTICE: Bootstrapped 15%: Establishing an encrypted directory connection<br />
Tor NOTICE: Bootstrapped 20%: Asking for networkstatus consensus<br />
Tor NOTICE: Bootstrapped 25%: Loading networkstatus consensus<br />
[07-29 14:06:39] TorLauncher INFO: NOTICE CONSENSUS_ARRIVED</p>
<p>More than a minute nothing happens: no cpu/disk/net activity. By what wonderful way does consensus arrive here?!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276328"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276328" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>tmidrii (not verified)</span> said:</p>
      <p class="date-time">August 06, 2018</p>
    </div>
    <a href="#comment-276328">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276328" class="permalink" rel="bookmark">Bridges metrics gone crazy…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Bridges metrics gone crazy right after update, it says that their amount has decreased from near 1500 to about 500, why? <strong>Are 1000 bridges really become disconnected from TOR Network</strong> or it's just metrics bug?</p>
</div>
  </div>
</article>
<!-- Comment END -->
