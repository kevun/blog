title: Secure Communication is Gaining Momentum in India
---
pub_date: 2018-01-30
---
author: antonela
---
tags:

UX
meetup
India
---
categories:

community
usability
---
_html_body:

<p> </p>
<p>Meetups are the best place to link faces with nicknames. And for sure, to share knowledge and discuss specific topics. We had our first Tor Meetup in Mumbai last weekend and thanks to the organizers, it was a pretty energizing one.</p>
<p>Zainab, Anish, and Kondi from <a href="https://hasgeek.com ">HasGeek </a>were responsible for organizing everything and worked wonders. The venue was situated in Lower Parel, Mumbai, on super cool offices shared by <a href="http://oml.in/">OML (Only Much Louder)</a> in an old refurbished silk silo. </p>
<p>The event started with quick introductions; we shared our backgrounds, how we are helping involved in the Tor community, and offered some words about ourselves. We also asked the members in the audience about what interested them about the workshop (voluntary; no names required).</p>
<p>I introduced the work that the UX team is doing. Then I ran user tests on the new onion services indicators (ticket <a href="https://trac.torproject.org/projects/tor/ticket/23247">#23247</a>) and the next improvements for the new Tor Launcher which was introduced last week in <a href="https://blog.torproject.org/tor-browser-75-released">Tor Browser 7.5</a>.</p>
<p>The community was ready to collaborate and excited to be part of the tests. Sharing our internal processes with the people who use Tor every day allow us to have the real spectrum of their expectations and experiences.</p>
<p>After that Sukhbir did a master class about Tor where he gave an introduction to Tor and a demo of Tor Browser (<a href="https://people.torproject.org/~sukhbir/talks/mumbai-tormeetup-2018-jan.pdf">slides</a>), followed by discussions on digital security and privacy.</p>
<p>Then <a href="https://kushaldas.in">Kushal Das</a>, from Freedom of the Press Foundation, took the projector and did a demo about how journalists are using <a href="https://blog.torproject.org/tor-heart-securedrop">SecureDrop</a>, which is built using onion services, for secure communication with their sources, such as whistleblowers. We then briefly discussed the current state of investigative journalism in India, the lack of SecureDrop installations at newspapers in India, and what we can do about it.</p>
<p>Privacy became a headline topic in the last few weeks in India because of <a href="https://twitter.com/Snowden/status/955050455490547712">surveillance and abuse</a> related to <a href="https://twitter.com/NewIndianVoice/status/955051477877313536">Aadhaar</a>, the 12 digit biometric identifier. January also happened to be <a href="https://twitter.com/hashtag/privacymonth?src=hash">Privacy Month</a>, led by Mozilla India. Each day in January, they've featured a tip for protecting your privacy online. </p>
<p>Secure tools for communication are more relevant now than ever. We are glad that we found an active Tor community working on online privacy and safety in India.</p>

---
_comments:

<a id="comment-273832"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273832" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>id (not verified)</span> said:</p>
      <p class="date-time">February 01, 2018</p>
    </div>
    <a href="#comment-273832">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273832" class="permalink" rel="bookmark">&quot;Aadhaar&quot; : an individual…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Aadhaar" : an individual survey code transmitted in real time tattooed on your face.</p>
<p>It is like a public card with all your private code account _ the same exist in a lot of administrations &amp; countries but aadhaar is a special concept invented by &amp; for the alien enterprises who are the real head &amp; boss of the government (less corrupted, less violent, more lazy than in the e.u or in the usa).<br />
Aadhaar is an obligation for all (but few exceptions) , it means that every one is registered with their dna linked to their identity.</p>
<p>The purpose of this card is still experimental : famine, money, disease, build a modern database ... no, just a proposition from some companies : building a world of consumers &amp; managing their need.<br />
A resistance could be self-organized using tor &amp; a better network respectful of the privacy could be the next challenge of india.</p>
<p>usa presentation : <a href="https://en.wikipedia.org/wiki/Aadhaar" rel="nofollow">https://en.wikipedia.org/wiki/Aadhaar</a></p>
<p>official presentation : <a href="https://economictimes.indiatimes.com/news/economy/policy/11-questions-on-aadhaar-and-its-misuse-answered-by-the-uidai/articleshow/62538926.cms" rel="nofollow">https://economictimes.indiatimes.com/news/economy/policy/11-questions-o…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273871"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273871" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Pari (not verified)</span> said:</p>
      <p class="date-time">February 04, 2018</p>
    </div>
    <a href="#comment-273871">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273871" class="permalink" rel="bookmark">Yeayy! It is very exciting…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yeayy! It is very exciting to know things are picking up here in India..</p>
</div>
  </div>
</article>
<!-- Comment END -->
