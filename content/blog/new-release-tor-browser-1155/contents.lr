title: New Release: Tor Browser 11.5.5 (Android, Windows, macOS, Linux)
---
pub_date: 2022-10-25
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5.5 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5.5 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5.5/).

Tor Browser 11.5.5 backports the following security updates from Firefox ESR 102.4 to to Firefox ESR 91.13 on Windows, macOS and Linux:
- [CVE-2022-40674: libexpat before 2.4.9 has a use-after-free in the doContent function in xmlparse.c](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-40674)
- [CVE-2022-42927: Same-origin policy violation could have leaked cross-origin URLs](https://www.mozilla.org/en-US/security/advisories/mfsa2022-45/#CVE-2022-42927)
- [CVE-2022-42928: Memory Corruption in JS Engine](https://www.mozilla.org/en-US/security/advisories/mfsa2022-45/#CVE-2022-42928)
- [CVE-2022-42929: Denial of Service via window.print](https://www.mozilla.org/en-US/security/advisories/mfsa2022-45/#CVE-2022-42929)
- [CVE-2022-42932: Memory safety bugs fixed in Firefox 106 and Firefox ESR 102.4](https://www.mozilla.org/en-US/security/advisories/mfsa2022-45/#CVE-2022-42932)

Tor Browser 11.5.5 updates GeckoView on Android to 102.4.0esr and includes [important security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-45/). There were no Android-specific security updates to backport from the Firefox 106 release.

The full changelog since [Tor Browser 11.5.4](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.5) is:

- All Platforms
  - Update Translations
  - [Bug tor-browser-build#40649](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40649): Update meek default bridge
  - [Bug tor-browser-build#40654](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40654): Enable uTLS and use the full bridge line for snowflake
- Windows + macOS + Linux
  - Update Manual
  - [Bug tor-browser#40465](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40465): Onion Authentication fails when connecting to a subdomain
  - [Bug tor-browser#41355](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41355): Amends to YEC 2022 Takeover Desktop Stable 11.5.5
  - [Bug  tor-browser#41359](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41359): Backport ESR 102.4 security fixes to 91.13-based Tor Browser
  - [Bug tor-browser#41364](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41364): Continued amends to YEC 2022 Takeover Desktop Stable 11.5.5
- Android
  - [Bug tor-browser-build#40650](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40650): Rebase geckoview-102.3.0esr-11.5-1 to ESR 102.4
  - [Bug tor-browser#41360](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41360): Backport Android-specific Firefox 106 to ESR 102.4-based Tor Browser
  - [Bug tor-browser#41365](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41365): Amends to YEC 2022 Takeover on Android
- Build
  - Windows + macOS + Linux
    - Update Go to 1.18.7
    - [Bug tor-browser-build#40464](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40464): go 1.18 fails to build on macOS
