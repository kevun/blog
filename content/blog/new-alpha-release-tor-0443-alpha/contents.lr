title: New alpha release: Tor 0.4.4.3-alpha
---
pub_date: 2020-07-27
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.4.3-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release by mid-August.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.4.4.3-alpha fixes several annoyances in previous versions, including one affecting NSS users, and several affecting the Linux seccomp2 sandbox.</p>
<h2>Changes in version 0.4.4.3-alpha - 2020-07-27</h2>
<ul>
<li>Major features (fallback directory list):
<ul>
<li>Replace the 148 fallback directories originally included in Tor 0.4.1.4-rc (of which around 105 are still functional) with a list of 144 fallbacks generated in July 2020. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40061">40061</a>.</li>
</ul>
</li>
<li>Major bugfixes (NSS):
<ul>
<li>When running with NSS enabled, make sure that NSS knows to expect nonblocking sockets. Previously, we set our TCP sockets as nonblocking, but did not tell NSS, which in turn could lead to unexpected blocking behavior. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40035">40035</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (linux seccomp2 sandbox):
<ul>
<li>Fix a regression on sandboxing rules for the openat() syscall. The fix for bug <a href="https://bugs.torproject.org/tpo/core/tor/25440">25440</a> fixed the problem on systems with glibc &gt;= 2.27 but broke with versions of glibc. We now choose a rule based on the glibc version. Patch from Daniel Pinto. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/27315">27315</a>; bugfix on 0.3.5.11.</li>
<li>Makes the seccomp sandbox allow the correct syscall for opendir according to the running glibc version. This fixes crashes when reloading torrc with sandbox enabled when running on glibc 2.15 to 2.21 and 2.26. Patch from Daniel Pinto. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40020">40020</a>; bugfix on 0.3.5.11.</li>
</ul>
</li>
<li>Minor bugfixes (relay, usability):
<ul>
<li>Adjust the rules for when to warn about having too many connections to other relays. Previously we'd tolerate up to 1.5 connections per relay on average. Now we tolerate more connections for directory authorities, and raise the number of total connections we need to see before we warn. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33880">33880</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Replace most http:// URLs in our code and documentation with https:// URLs. (We have left unchanged the code in src/ext/, and the text in LICENSE.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/31812">31812</a>. Patch from Jeremy Rand.</li>
</ul>
</li>
<li>Removed features:
<ul>
<li>Our "check-local" test target no longer tries to use the Coccinelle semantic patching tool parse all the C files. While it is a good idea to try to make sure Coccinelle works on our C before we run a Coccinelle patch, doing so on every test run has proven to be disruptive. You can still run this tool manually with "make check-cocci". Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40030">40030</a>.</li>
</ul>
</li>
</ul>

