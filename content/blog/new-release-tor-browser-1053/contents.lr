title: New Release: Tor Browser 10.5.3 (Android)
---
pub_date: 2021-07-20
---
author: sysrqb
---
tags:

tor browser
tbb-10.5
tbb
---
categories: applications
---
summary: Tor Browser 10.5.3 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 10.5.3 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5.3/">distribution directory</a>.</p>
<p>This version updates Firefox to 90.1.1. This version includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-28/">security updates</a> to Firefox.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser will <em><b>stop</b> supporting <u>version 2 onion services</u></em> later <em><b>this year</b></em>. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a>. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.5">Tor Browser 10.5.1</a>:</p>
<ul>
<li>Android
<ul>
<li>Update HTTPS Everywhere to 2021.7.13</li>
<li>Update Fenix to 90.1.1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40172">Bug 40172</a>: Find the Quit button</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40173">Bug 40173</a>: Rebase fenix patches to fenix v90.0.0-beta.6</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40177">Bug 40177</a>: Hide Tor icons in settings</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40179">Bug 40179</a>: Show Snowflake bridge option on Release</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40180">Bug 40180</a>: Rebase fenix patches to fenix v90.1.1</li>
</ul>
</li>
<li>Build System
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40312">Bug 40312</a>: Update components for mozilla90</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292420"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292420" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
    <a href="#comment-292420">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292420" class="permalink" rel="bookmark">it is ver.10.5.3, why is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it is ver.10.5.3, why is arch64-android TOR is still 0.4.1.5rc??? Do anyone bother to update it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292430"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292430" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ffffff (not verified)</span> said:</p>
      <p class="date-time">July 22, 2021</p>
    </div>
    <a href="#comment-292430">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292430" class="permalink" rel="bookmark">In my country obsf4 in not…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In my country obsf4 in not yet blocked .<br />
From an anonymity perspective is it better to use snowflake or obsf4 or they are the same ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292444"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292444" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 23, 2021</p>
    </div>
    <a href="#comment-292444">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292444" class="permalink" rel="bookmark">I&#039;m still on 10.0.18 from a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm still on 10.0.18 from a month ago using the guardian project's F-droid repo. Is that normal? what is the recommended way to update tor browser on android?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292445"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292445" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>boyaki (not verified)</span> said:</p>
      <p class="date-time">July 23, 2021</p>
    </div>
    <a href="#comment-292445">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292445" class="permalink" rel="bookmark">Google Play offers Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Google Play offers Tor Browser 10.5, but on F-Droid the latest available version is 10.0.18. When will 10.5 start to be distributed by the Guardian Project's F-Droid Repository?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292463"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292463" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 27, 2021</p>
    </div>
    <a href="#comment-292463">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292463" class="permalink" rel="bookmark">Hello. I am trying to use…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello. I am trying to use the Whonix version of Tor Browser on Qubes, but I have noticed something weird. Tor is always choosing exit nodes on the same country. When I visit the Tor website, I am getting German exit nodes, and when I visit YouTube, I am getting Ukrainian nodes. Always. Was that supposed to be normal?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292506"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292506" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 03, 2021</p>
    </div>
    <a href="#comment-292506">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292506" class="permalink" rel="bookmark">Hello
Just would like to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello<br />
Just would like to warn you that the F-Droid Guardian Project Official Releases repository seems to not have provided the latest version of Tor Browser.</p>
<p>The Latest version available for Tor Browser is the version 10.5.3 on Android while on the F-Droid Repo the latest is 10.0.18.<br />
Please fix that with them because it's a security risk to use an older version of Tor Browser and I didn't know there was a new version...</p>
<p>Thanks again for your great work :)</p>
<p>Ps: I use Foxy Droid if that matters ;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
