title: Vidalia 0.2.2 Released
---
pub_date: 2009-08-15
---
author: phobos
---
tags:

bug fixes
vidalia release
openssl fixes
usability enhancements
---
categories:

releases
usability
---
_html_body:

<p>Vidalia 0.2.2 is released.  It addresses an issue with openssl which causes the geoip lookups to fail on various versions of Windows.  It also switches from the Nullsoft Installer to the Microsoft System Installer for better compatibility with Microsoft Windows.</p>

<p>There are now separate Apple OS X builds, one for PowerPC architectures and one for i386 architectures.  No more Universal binary bloat to download.</p>

<p>The changes are:</p>

<ul>
<li>When the user clicks "Browse" in the Advanced settings page to locate<br />
    a new torrc, set the initial directory shown in the file dialog to the<br />
    current location of the user's torrc. (Ticket #505)</li>
<li>Use 'ditto' to strip the architectures we don't want from the Qt<br />
    frameworks installed into the app bundle with the dist-osx,<br />
    dist-osx-bundle and dist-osx-split-bundle build targets.</li>
<li>Fix a bug in the CMakeLists.txt files for ts2po and po2ts that caused<br />
    build errors on Panther for those two tools.</li>
<li>Include rebuilt OpenSSL libraries in the Windows packages that are<br />
    built with the static (/MT) version of the Microsoft Visual C++<br />
    Runtime. Otherwise, we would require users to install the MSVC<br />
    Redistributable, which doesn't work for portable installations such as<br />
    the Tor Browser Bundle.</li>
<li>Remove the NSIS file for the Vidalia installer since we now ship<br />
    MSI-based installers on Windows.</li>
</ul>

---
_comments:

<a id="comment-2130"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2130" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2009</p>
    </div>
    <a href="#comment-2130">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2130" class="permalink" rel="bookmark">I believe there is an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I believe there is an spelling error, unless our beloved software have changed name.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2131"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2131" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2130" class="permalink" rel="bookmark">I believe there is an</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2131">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2131" class="permalink" rel="bookmark">fixed!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2325"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2325" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 01, 2009</p>
    </div>
    <a href="#comment-2325">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2325" class="permalink" rel="bookmark">Vidalia for Universal Binary.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Vidalia needs to be updated as a Universal Binary.  It's running on PPC, and all Macs are Intel these days</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2333"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2333" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 01, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2325" class="permalink" rel="bookmark">Vidalia for Universal Binary.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2333">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2333" class="permalink" rel="bookmark">re: vidalia</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Vidalia is a universal binary.  Future versions will be either x86 or PPC, no more universal binaries.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2372"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2372" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 02, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2325" class="permalink" rel="bookmark">Vidalia for Universal Binary.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2372">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2372" class="permalink" rel="bookmark">re: vidalia x86</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>And see, <a href="https://blog.torproject.org/blog/major-changes-os-x-vidalia-bundle-0221alpha" rel="nofollow">https://blog.torproject.org/blog/major-changes-os-x-vidalia-bundle-0221…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
