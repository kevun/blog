title: New Alpha Release: Tor Browser 12.0a1 (Windows, macOS, Linux)
---
pub_date: 2022-08-09
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 12.0a1 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 12.0a1 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/12.0a1/).

Tor Browser 12.0a1 updates Firefox on Windows, macOS, and Linux to 91.12.0esr.

We use the opportunity as well to update various other components of Tor Browser:
- Tor Launcher 0.2.38
- OpenSSL 1.1.1q
- Go 1.17.13

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-29/) to Firefox.

The full changelog since [Tor Browser 11.5a13](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=main) is:

- Windows + OS X + Linux
  - Update Firefox to 91.12.0esr
  - Update Tor-Launcher to 0.2.38
  - Update Translations
  - Update OpenSSL to 1.1.1q
  - Update Manual
  - [Bug tor-browser#21740](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/21740): Make sure Mozilla's own emoji font on Windows/Linux does not interfere with our font fingerprinting defense
  - [Bug tor-browser#27719](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/27719): Treat unsafe renegotiation as broken
  - [Bug tor-browser-build#40584](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40584): Update tor-browser manual to latest
  - [Bug tor-browser#40966](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40966): Investigate problems with Twemoji Mozilla
  - [Bug tor-browser#41011](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41011): The Internet and Tor status are visible when opening the settings
  - [Bug tor-browser#41035](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41035): OnionAliasService should use threadsafe ISupports
  - [Bug tor-browser#41036](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41036): Add a preference to disable OnionAlias
  - [Bug tor-browser#41037](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41037): User onboarding still points to about:preferences#tor
  - [Bug tor-browser#41044](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41044): Content exceeding the height of the connection settings modals
  - [Bug tor-browser#41049](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41049): QR codes in connection settings aren't recognized by some readers in dark theme
  - [Bug tor-browser#41050](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41050): "Continue to HTTP Site" button doesn't work on IP addresses
  - [Bug tor-browser#41053](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41053): remove HTTPS-Everywhere entry from browser.uiCustomization.state pref
  - [Bug tor-browser#41054](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41054): Improve color contrast of purple elements in connection settings in dark theme
  - [Bug tor-browser#41055](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41055): Icon fix from #40834 is missing in 11.5 stable
  - [Bug tor-browser#41058](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41058): Hide `currentBridges` description when the section itself is hidden
  - [Bug tor-browser#41059](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41059): Bridge cards aren't displaying, and toggle themselves off
  - [Bug tor-browser#41067](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41067): Cherry-pick fixes for HTTPS-Only mode
  - [Bug tor-browser#41070](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41070): Google vanished from default search options from 11.0.10 onwards
  - [Bug tor-browser#41089](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41089): Add tor-browser build scripts + Makefile to tor-browser
  - [Bug tor-browser#41095](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41095): "Learn more" link in onboarding slideshow still points to 11.0 release post
- Windows
  - [Bug tor-browser#30589](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/30589): Tor Browser on Windows is lacking fonts to render some kind of scripts
  - [Bug tor-browser#41039](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41039): Set 'startHidden' flag on tor process in tor-launcher
- OS X
  - [Bug tor-browser#41004](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41004): The Bangla font does not display correctly on MacOs
- Linux
  - [Bug tor-browser#41043](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41043): Investigate why STIX Two becomes the default font on Linux
- Build System
  - Windows + OS X + Linux
    - Update Go to 1.17.13
    - [Bug tor-browser-build#40499](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40499): Update firefox to enable building from new 'base-browser' tag
    - [Bug tor-browser-build#40500](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40500): Add base-browser package project
    - [Bug tor-browser-build#40501](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40501): Makefile updates to support building base-browser packages
    - [Bug tor-browser-build#40547](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40547): Remove container/remote_* from rbm.conf
    - [Bug tor-browser-build#40581](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40581): Update reference to master branches
