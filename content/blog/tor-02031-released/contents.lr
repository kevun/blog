title: Tor 0.2.0.31 Released
---
pub_date: 2008-09-09
---
author: phobos
---
tags:

stable release
bug fixes
---
categories: releases
---
_html_body:

<p>A better formatted version of this can be found at the <a href="http://archives.seul.org/or/announce/Sep-2008/msg00000.html" rel="nofollow">OR-Announce Archives</a>.</p>

<p>Tor 0.2.0.31 addresses two potential anonymity issues, starts to fix<br />
a big bug we're seeing where in rare cases traffic from one Tor stream<br />
gets mixed into another stream, and fixes a variety of smaller issues.</p>

<p><a href="https://www.torproject.org/download.html" rel="nofollow">https://www.torproject.org/download.html</a></p>

<p>Changes in version 0.2.0.31 - 2008-09-03<br />
  o Major bugfixes:<br />
    - Make sure that two circuits can never exist on the same connection<br />
      with the same circuit ID, even if one is marked for close. This<br />
      is conceivably a bugfix for bug 779. Bugfix on 0.1.0.4-rc.<br />
    - Relays now reject risky extend cells: if the extend cell includes<br />
      a digest of all zeroes, or asks to extend back to the relay that<br />
      sent the extend cell, tear down the circuit. Ideas suggested<br />
      by rovv.<br />
    - If not enough of our entry guards are available so we add a new<br />
      one, we might use the new one even if it overlapped with the<br />
      current circuit's exit relay (or its family). Anonymity bugfix<br />
      pointed out by rovv.</p>

<p>  o Minor bugfixes:<br />
    - Recover 3-7 bytes that were wasted per memory chunk. Fixes bug<br />
      794; bug spotted by rovv. Bugfix on 0.2.0.1-alpha.<br />
    - Correctly detect the presence of the linux/netfilter_ipv4.h header<br />
      when building against recent kernels. Bugfix on 0.1.2.1-alpha.<br />
    - Pick size of default geoip filename string correctly on windows.<br />
      Fixes bug 806. Bugfix on 0.2.0.30.<br />
    - Make the autoconf script accept the obsolete --with-ssl-dir<br />
      option as an alias for the actually-working --with-openssl-dir<br />
      option. Fix the help documentation to recommend --with-openssl-dir.<br />
      Based on a patch by "Dave". Bugfix on 0.2.0.1-alpha.<br />
    - Disallow session resumption attempts during the renegotiation<br />
      stage of the v2 handshake protocol. Clients should never be trying<br />
      session resumption at this point, but apparently some did, in<br />
      ways that caused the handshake to fail. Bug found by Geoff Goodell.<br />
      Bugfix on 0.2.0.20-rc.<br />
    - When using the TransPort option on OpenBSD, and using the User<br />
      option to change UID and drop privileges, make sure to open<br />
      /dev/pf before dropping privileges. Fixes bug 782. Patch from<br />
      Christopher Davis. Bugfix on 0.1.2.1-alpha.<br />
    - Try to attach connections immediately upon receiving a RENDEZVOUS2<br />
      or RENDEZVOUS_ESTABLISHED cell. This can save a second or two<br />
      on the client side when connecting to a hidden service. Bugfix<br />
      on 0.0.6pre1. Found and fixed by Christian Wilms; resolves bug 743.<br />
    - When closing an application-side connection because its circuit is<br />
      getting torn down, generate the stream event correctly. Bugfix on<br />
      0.1.2.x. Anonymous patch.</p>

---
_comments:

<a id="comment-245"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-245" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 14, 2008</p>
    </div>
    <a href="#comment-245">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-245" class="permalink" rel="bookmark">Bug?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi.</p>
<p>i've blocked a tor node with ExcludeNode command in my torrc with it's fingerprint and name but the node keeps coming back.</p>
<p>is this a bug?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-251"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-251" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 21, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-245" class="permalink" rel="bookmark">Bug?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-251">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-251" class="permalink" rel="bookmark">possibly.  please open a bug</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>possibly.  please open a bug report at <a href="http://bugs.torproject.org" rel="nofollow">http://bugs.torproject.org</a>.  Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
