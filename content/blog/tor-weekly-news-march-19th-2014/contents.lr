title: Tor Weekly News — March 19th, 2014
---
pub_date: 2014-03-19
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the eleventh issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Accessing the Tor network from China</h1>

<p>In a new blog post <a href="https://blog.torproject.org/blog/how-to-read-our-china-usage-graphs" rel="nofollow">How to read our China usage graphs</a>, Roger Dingledine looks at the current situation of how Tor is able to circumvent censorship on Chinese Internet accesses. Indeed, if one only looks at the <a href="https://metrics.torproject.org/users.html?graph=userstats-bridge-country&amp;start=2011-10-18&amp;end=2014-01-16&amp;country=cn#userstats-bridge-country" rel="nofollow">current bridge users graph</a>, one might believe that Tor is not a solution for users in China.</p>

<p>“The correct interpretation of the graph is ‘obfs3 bridges have not been deployed enough to keep up with the demand in China’. So it isn’t that Tor is blocked — it’s that we haven’t done much of a deployment for obfs3 bridges or ScrambleSuit bridges, which are the latest steps in the arms race” writes Roger.</p>

<p>The upcoming version — <a href="https://lists.torproject.org/pipermail/tor-qa/2014-March/000364.html" rel="nofollow">currently in QA phase</a> — of the Tor Browser will include support for the <a href="https://www.torproject.org/docs/pluggable-transports.html" rel="nofollow">pluggable transports</a> <a href="https://gitweb.torproject.org/pluggable-transports/obfsproxy.git/blob/refs/heads/master:/doc/obfs3/obfs3-protocol-spec.txt" rel="nofollow">obfs3</a>, <a href="https://fteproxy.org/" rel="nofollow">FTE</a> and <a href="https://crypto.stanford.edu/flashproxy/" rel="nofollow">Flashproxy</a>. Having these transports ready to be used in a couple of clicks should help Chinese users.</p>

<p>The “obfs3” protocol is still vulnerable to active probing attacks.  The deployment of its replacement, <a href="http://www.cs.kau.se/philwint/scramblesuit/" rel="nofollow">ScrambleSuit</a>, is on-going.  As Roger highlighted, “we need to get more addresses”. <a href="https://blog.torproject.org/blog/strategies-getting-more-bridge-addresses" rel="nofollow">Several ways have been thoughts in the past</a>, but until there is more cooperation from ISP and network operators, your can make a difference by <a href="https://lists.torproject.org/pipermail/tor-relays/2014-February/003886.html" rel="nofollow">running a bridge</a> if you can!</p>

<p>On another front, work is currently on-going on the <a href="https://gitweb.torproject.org/bridgedb.git" rel="nofollow">bridge distributor</a> to improve how censored users can get a hand on bridge addresses. Yawning Angel also <a href="https://lists.torproject.org/pipermail/tor-dev/2014-March/006476.html" rel="nofollow">just released</a> the first version of <a href="https://github.com/Yawning/obfsclient" rel="nofollow">obfsclient</a> which should help making ScrambleSuit available on Android devices. All in all, the Tor community can hope to welcome back more users from China in a near future.</p>

<h1>Circumventing censorship through “too-big-too-block” websites</h1>

<p>Late January, David Fifield <a href="https://lists.torproject.org/pipermail/tor-dev/2014-January/006159.html" rel="nofollow">introduced</a> a new pluggable transport called <a href="https://trac.torproject.org/projects/tor/wiki/doc/meek" rel="nofollow">meek</a>. It can be described as “a transport that uses HTTP for carrying bytes and TLS for obfuscation. Traffic is relayed through a third-party server (Google App Engine). It uses a trick to talk to the third party so that it looks like it is talking to an unblocked server.” The approach is close to the <a href="https://trac.torproject.org/projects/tor/wiki/doc/GoAgent" rel="nofollow">GoAgent</a> proxy that has a certain popularity in China.</p>

<p>With the current version, using Google App Engine, the transport requires no additional configuration. But David also mentioned that <a href="https://bugs.torproject.org/10984" rel="nofollow">a PHP script</a> could also be a good candidate to relay the traffic.  Combined to <a href="http://www.cs.kau.se/philwint/scramblesuit/" rel="nofollow">ScrambleSuit</a>, it could allow “a real web site with real pages and everything” to be used as a bridge if a user can provide the shared secret.</p>

<p>David has made available <a href="https://lists.torproject.org/pipermail/tor-qa/2014-February/000340.html" rel="nofollow">experimental versions</a> of the Tor Browser for anyone to try. The <a href="https://gitweb.torproject.org/pluggable-transports/meek.git" rel="nofollow">source code</a> has recently <a href="https://lists.torproject.org/pipermail/tor-dev/2014-March/006506.html" rel="nofollow">moved</a> to the Tor Project’s infrastructure, and is ready for more eyes and fingers to play with it.</p>

<h1>Switching to a single guard node?</h1>

<p>Last October, Roger Dingledine called for research on <a href="https://blog.torproject.org/blog/improving-tors-anonymity-changing-guard-parameters" rel="nofollow">improving Tor’s anonymity by changing guard parameters </a>. One of these parameters is the number of guard nodes used simultaneously by a Tor client.</p>

<p>Following up on the <a href="http://freehaven.net/~arma/cogs-wpes.pdf" rel="nofollow">paper written by Tariq Elahi et al.</a>, Roger’s blog post, and recent discussions during the winter dev. meeting, George Kadianakis made a detailed <a href="https://lists.torproject.org/pipermail/tor-dev/2014-March/006458.html" rel="nofollow">analysis of the implications of switching to a single guard node </a>. He studied the performance implications of switching to a single guard, the performance implications of raising the minimum guard bandwidth for both clients and the overall network, and how the change would affect the overall anonymity and fingerprintability of Tor users.</p>

<p>Jumping to conclusions: “It seems that the performance implications of switching to 1 guard are not terrible. […] A guard bandwidth threshold of 2MB/s […] seems like it would considerably improve client performance without screwing terribly with the security or the total performance of the network. The fingerprinting problem will be improved in some cases, but still remains unsolved for many of the users […] A proper solution might involve <a href="https://bugs.torproject.org/9273#comment:4" rel="nofollow">guard node buckets</a>”.</p>

<p>For a better understanding, be sure to look at George’s work which includes graphs and proper explanations.</p>

<h1>Miscellaneous news</h1>

<p>George Kadianakis <a href="https://lists.torproject.org/pipermail/tor-relays/2014-March/004074.html" rel="nofollow">announced</a> obfsproxy version 0.2.7. The new release fixes <a href="https://bugs.torproject.org/11100" rel="nofollow">an important bug</a> “where scramblesuit would basically reject clients if they try to connect a second time after a short amount of time has passed.” Bridge operators are strongly advised to upgrade from <a href="https://gitweb.torproject.org/pluggable-transports/obfsproxy.git/commit/6cdbc64" rel="nofollow">source</a>, <a href="https://pypi.python.org/pypi/obfsproxy/0.2.7" rel="nofollow">pip</a>, or the upcoming Debian packages.</p>

<p>The submission deadline for this year’s <a href="https://blog.torproject.org/blog/tor-google-summer-code-2014" rel="nofollow">Google Summer of Code</a> is the 21st: this Friday. Several students already showed up on the tor-dev mailing list, but as Damian Johnson <a href="https://lists.torproject.org/pipermail/tor-dev/2014-March/006498.html" rel="nofollow">says</a>: “If you’re procrastinating until the last minute then please don’t!”</p>

<p>Tails <a href="https://tails.boum.org/news/" rel="nofollow">logo contest</a> is happily on-going. Several submissions have already been received and can be seen on the <a href="https://tails.boum.org/blueprint/logo/" rel="nofollow">relevant blueprint</a>.</p>

<p>Kelley Misata and Karen Reilly <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000485.html" rel="nofollow">attended the South by Southwest (SXSW) Interactive festival</a> in Austin, Texas.</p>

<p>Relay and bridge operators might be interested in Ramo’s <a href="https://lists.torproject.org/pipermail/tor-relays/2014-March/004062.html" rel="nofollow">first release</a> of <a href="https://github.com/goodvikings/tor_nagios" rel="nofollow">a Tor plugin for Nagios</a>. It can currently check for a page fetch through the SOCKS proxy port, the hibernation state, the current bandwidth, ORPort reachability, DirPort reachability, and the bytes remaining until hibernation.</p>

<p>Nicolas Vigier sent his <a href="https://lists.torproject.org/pipermail/tor-reports/2014-March/000486.html" rel="nofollow">monthly report for February</a>.</p>

<p>Tails <a href="https://twitter.com/accessnow/status/441043400708857856" rel="nofollow">won the 2014 Endpoint Security prize</a> from Access. The prize <a href="https://www.accessnow.org/prize" rel="nofollow">recognizes</a> Tails “unique positive impact on the endpoint security of at-risk users in need”. Congrats!</p>

<p>The Format-Transforming Encryption project at Portland State University <a href="http://www.oregonlive.com/silicon-forest/index.ssf/2014/03/psu_professor_wins_surprise_10.html" rel="nofollow">received</a> an unexpected 100,000 USD grant from Eric Schmidt.</p>

<h1>Tor help desk roundup</h1>

<p>The help desk has seen an increase in Russian language support requests amidst news that the Russian Federation began censoring a number of websites. Unfortunately, the help desk is not able to provide support in Russian for now. Changes in the number of Tor users by country can be observed on the project’s <a href="https://metrics.torproject.org/users.html" rel="nofollow">metrics page</a>.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, Matt Pagan and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter.  We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

