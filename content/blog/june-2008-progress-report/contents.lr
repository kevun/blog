title: June 2008 Progress Report
---
pub_date: 2008-07-23
---
author: phobos
---
tags:

tor
vidalia
progress report
bridges
openssl
tor browser bundle
translations
---
categories:

applications
circumvention
localization
network
reports
---
_html_body:

<p>Torbutton 1.2.0rc1 (released June 1), the first release candidate for the next stable series of the security-enhanced Torbutton Firefox extension, features functional support for Firefox 3. However, this support has not been extensively tested. In particular, timezone masking does not work at all. The workaround is to manually set the environment variable 'TZ' to 'UTC' before starting Firefox. This works on both Linux and Windows:<br />
<a href="http://archives.seul.org/or/talk/Jun-2008/msg00044.html" rel="nofollow">http://archives.seul.org/or/talk/Jun-2008/msg00044.html</a></p>

<p>Tor 0.2.0.27-rc (released June 3) adds a few features we left out of the earlier release candidates. In particular, we now include an IP-to-country GeoIP database, so controllers can easily look up what country a given relay is in, and so bridge relays can give us some sanitized summaries about which countries are making use of bridges. (See proposal 126-geoip-fetching.txt for details.)<br />
<a href="http://archives.seul.org/or/talk/Jun-2008/msg00055.html" rel="nofollow">http://archives.seul.org/or/talk/Jun-2008/msg00055.html</a></p>

<p>Torbutton 1.2.0rc2 (released June 8) features a fix for an annoying bug on MacOS, and adds much clamored for options to start Firefox in a specific Tor state:<br />
<a href="http://archives.seul.org/or/talk/Jun-2008/msg00103.html" rel="nofollow">http://archives.seul.org/or/talk/Jun-2008/msg00103.html</a></p>

<p>Tor 0.2.0.28-rc (released June 13) fixes an anonymity-related bug, fixes a hidden-service performance bug, and fixes a bunch of smaller bugs.<br />
<a href="http://archives.seul.org/or/talk/Jun-2008/msg00165.html" rel="nofollow">http://archives.seul.org/or/talk/Jun-2008/msg00165.html</a></p>

<p>Tor 0.2.1.1-alpha (released June 13) fixes a lot of memory fragmentation problems that were making the Tor process bloat especially on Linux; makes our TLS handshake blend in better; sends "bootstrap phase" status events to the controller, so it can keep the user informed of progress (and problems) fetching directory information and establishing circuits; and adds a variety of smaller features. <a href="http://archives.seul.org/or/talk/Jun-2008/msg00185.html" rel="nofollow">http://archives.seul.org/or/talk/Jun-2008/msg00185.html</a></p>

<p>Vidalia 0.1.4 (released June 13) adds a bootstrap progress bar, UPnP support, a new set of freely licensed GUI icons, and fixes a few bugs.<br />
<a href="http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.4/CHANGELOG" rel="nofollow">http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.4/CHAN…</a></p>

<p>The Tor Browser Bundle 1.1.0 (released June 13) replaces startup batch script with application (RelativeLink) so there is a helpful icon, optionally installs Pidgin (for Tor IM Browser Bundle), optionally uses WinRAR to produce a self-extracting split bundle, and includes upgraded versions of Tor, Vidalia, and Torbutton.<br />
<a href="https://svn.torproject.org/svn/torbrowser/trunk/README" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/README</a></p>

<p>Tor 0.2.1.2-alpha (released June 20) includes a new "TestingTorNetwork" config option to make it easier to set up your own private Tor network; fixes several big bugs with using more than one bridge relay; fixes a big bug with offering hidden services quickly after Tor starts; and uses a better API for reporting potential bootstrapping problems to the controller.<br />
<a href="http://archives.seul.org/or/talk/Jun-2008/msg00247.html" rel="nofollow">http://archives.seul.org/or/talk/Jun-2008/msg00247.html</a></p>

<p>Vidalia 0.1.5 (released June 21) switches Vidalia's internal string representation so it can use the new Pootle-based translation system.<br />
<a href="http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.5/CHANGELOG" rel="nofollow">http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.5/CHAN…</a></p>

<p>Torbutton 1.2.0rc3 and 1.2.0rc4 (both released June 27) provide improved addon compatibility, better preservation of Firefox preferences that we touch, fixing issues with Tor toggle breaking for some option combos, and an improved 'Restore Defaults' button.<br />
<a href="https://torbutton.torproject.org/dev/CHANGELOG" rel="nofollow">https://torbutton.torproject.org/dev/CHANGELOG</a></p>

<p>We finally got around to writing down the details of many of our architecture and technical design changes:</p>

<p>Proposal 137 ("Keep controllers informed as Tor bootstraps") modifies Tor so it keeps Vidalia informed of each "bootstrap phase" -- that is, progress Tor makes at learning directory information, making connections to the network, etc. Now Vidalia has a progress bar on Tor startup that explains what's going on. Further, Tor reports "bootstrap problems" when it believes it's having troubles starting up correctly, and Vidalia can now tell the user. All of this is in as of the Tor 0.2.1.2-alpha release (June 20).<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/137-bootstrap-phases.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/137-bootstr…</a></p>

<p>Proposal 138 ("Remove routers that are not Running from consensus documents") modifies the directory "networkstatus consensus" documents so they no longer list relays that are believed to be unusable. They used to list these relays so clients could decide for themselves, but in practice clients just ignored them. This change saves 30% to 40% in download bandwidth for consensus documents. It is included in the 0.2.1.2-alpha release (June 20).<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/138-remove-down-routers-from-consensus.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/138-remove-…</a></p>

<p>Proposal 139 ("Download consensus documents only when it will be trusted") tries to make Tor clients better handle the case when new directory authorities have been added to the system, or when directory authorities have changed (for example, this could happen if we have another bug like the one in May that caused us to change keys for half the directory authorities). Now clients specify which directory authorities they trust, so the directory mirrors can give them a consensus document they'll be willing to use. This change is included in Tor 0.2.1.1-alpha, and a bugfix on it was included in Tor 0.2.1.2-alpha.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/139-conditional-consensus-download.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/139-conditi…</a></p>

<p>Proposal 140 ("Provide diffs between consensuses") is still under development, but is scheduled to be included in the Tor 0.2.1.x tree. The idea is that most parts of the consensus document don't change from one hour to the next, so we can give clients a diff on the previous one rather than a whole new document, changing the size of the document every client must download every few hours from 92KB on average to 13KB on average.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/140-consensus-diffs.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/140-consens…</a></p>

<p>Proposal 141 ("Download server descriptors on demand") is still under discussion, and may not be ready until for inclusion until Tor 0.2.2.x. This is the more detailed version of our "grand scaling plan" first mentioned in April. The idea is to have clients download networkstatus consensus documents as they do now, but rather than preemptively fetching every relay descriptor just in case, they fetch descriptors "just in time" only when they need them.  The trick is to keep the bandwidth overhead low while not introducing too many new anonymity attacks e.g. due to leaking which relays you're picking.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/141-jit-sd-downloads.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/141-jit-sd-…</a></p>

<p>We've instrumented a Tor client to collect stats on how much bandwidth we use now for directory overhead and how much we'd save with this new approach:<br />
<a href="http://archives.seul.org/or/dev/Jun-2008/msg00024.html" rel="nofollow">http://archives.seul.org/or/dev/Jun-2008/msg00024.html</a></p>

<p>Proposals 142 ("Combine Introduction and Rendezvous Points") and 143 ("Improvements of Distributed Storage for Tor Hidden Service Descriptors") are still in the discussion phase. Their goal is to improve the experience for clients accessing Tor hidden services, both by making the handshake faster and by making hidden service reachability more reliable and more robust.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/142-combine-intro-and-rend-points.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/142-combine…</a><br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/143-distributed-storage-improvements.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/143-distrib…</a></p>

<p>The "spoofing Firefox cipher suites and extensions" features are now in the Tor 0.2.1.1-alpha release, meaning they're in the Tor Browser Bundle 1.1.0 release also. From the 0.2.1.1-alpha ChangeLog:<br />
"More work on making our TLS handshake blend in: modify the list of ciphers advertised by OpenSSL in client mode to even more closely resemble a common web browser. We cheat a little so that we can advertise ciphers that the locally installed OpenSSL doesn't know about."</p>

<p>We've done some initial security auditing (though there's always room for more, and we plan to do some more concrete auditing in July).</p>

<p>Nick also wrote some early thoughts on doing pass-through to an Apache server to improve scanning resistance:<br />
<a href="http://archives.seul.org/or/dev/Jun-2008/msg00014.html" rel="nofollow">http://archives.seul.org/or/dev/Jun-2008/msg00014.html</a></p>

<p>The Tor Browser Bundle 1.1.0 (released June 13) replaces startup batch script with application (RelativeLink) so there is a helpful icon, optionally installs Pidgin (for Tor IM Browser Bundle), optionally uses WinRAR to produce a self-extracting split bundle, and includes upgraded versions of Tor, Vidalia, and Torbutton.</p>

<p>We also looked into running two Firefoxes in parallel:<br />
<a href="https://svn.torproject.org/svn/torbrowser/trunk/docs/two-firefox.txt" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/docs/two-firefox.txt</a><br />
and we even hacked in some Torbutton fixes that will come out in version 1.2.0rc3 that should get us closer:<br />
<a href="http://archives.seul.org/or/cvs/Jun-2008/msg00213.html" rel="nofollow">http://archives.seul.org/or/cvs/Jun-2008/msg00213.html</a></p>

<p>Speaking of which, we also hacked in another feature in Torbutton 0.1.2rc2, to add a "locked" mode so Tor Browser Bundle can start Torbutton and not fear that the user will click and disable Tor. I believe TBB 1.1.0 doesn't use this feature yet though.<br />
<a href="http://archives.seul.org/or/cvs/Jun-2008/msg00186.html" rel="nofollow">http://archives.seul.org/or/cvs/Jun-2008/msg00186.html</a></p>

<p>From the Tor 0.2.1.2-alpha ChangeLog:<br />
"If you have more than one bridge but don't know their digests, you would only learn a request for the descriptor of the first one on your list. (Tor considered launching requests for the others, but found that it already had a connection on the way for $0000...0000 so it didn't open another.) Bugfix on 0.2.0.x."<br />
"If you have more than one bridge but don't know their digests, and the connection to one of the bridges failed, you would cancel all pending bridge connections. (After all, they all have the same digest.) Bugfix on 0.2.0.x."<br />
"If you're using bridges, generate "bootstrap problem" warnings as soon as you run out of working bridges, rather than waiting for ten failures -- which will never happen if you have less than ten bridges."</p>

<p>We put up a new webpage to describe bridges, how to fetch bridge relay addresses, etc:<br />
<a href="https://www.torproject.org/bridges" rel="nofollow">https://www.torproject.org/bridges</a></p>

<p>We also modified the BridgeDB database (that is, the server that runs <a href="https://bridges.torproject.org/" rel="nofollow">https://bridges.torproject.org/</a> and answers mail to <a href="mailto:bridges@torproject.org" rel="nofollow">bridges@torproject.org</a>) to autodetect if the address hitting <a href="https://bridges.torproject.org/" rel="nofollow">https://bridges.torproject.org/</a> is currently a Tor exit relay, and if so to treat it specially -- that is, we reserve a set of bridge addresses and give those out only to folks coming in over Tor.</p>

<p>The updated BridgeDB version now makes sure to give out at least one bridge that's listed as Stable in the bridge authority's networkstatus document, and at least one bridge that listens on port 443. The goal here is to increase the odds that at least one of the bridges we give the user will be usable even if he's in a tightly firewalled situation.</p>

<p>From the Tor 0.2.0.27-rc ChangeLog:<br />
"Include an IP-to-country GeoIP file in the tarball, so bridge relays can report sanitized summaries of the usage they're seeing."</p>

<p>We finished work on a patch for OpenSSL that will make it keep less buffer space around. Currently fast Tor relays use (waste) as much as 100M of memory in OpenSSL's buffers. This patch was accepted and included in the main OpenSSL tree in June:<br />
<a href="http://marc.info/?l=openssl-cvs&amp;m=121246471627426&amp;w=2" rel="nofollow">http://marc.info/?l=openssl-cvs&amp;m=121246471627426&amp;w=2</a></p>

<p>The Vidalia 0.1.4 release has folded the UPnP library and GUI changes into the main Vidalia tree, along with a "test" button to try speaking UPnP at the local router and tell the user whether it worked; these features will be available by default in the 0.2.0.x stable release.</p>

<p>We've put a lot of effort into reducing Tor's memory footprint again. The main issue was a "memory fragmentation" problem in Linux's memory allocator, which was causing Tor servers on Linux to slowly grow without bound. As of Tor 0.2.1.2-alpha, the issue appears to be substantially better. Many more details are here:<br />
<a href="http://archives.seul.org/or/dev/Jun-2008/msg00001.html" rel="nofollow">http://archives.seul.org/or/dev/Jun-2008/msg00001.html</a></p>

<p>From the Tor 0.2.1.2-alpha ChangeLog:<br />
"New TestingTorNetwork config option to allow adjustment of previously constant values that, while reasonable, could slow bootstrapping. Implements proposal 135. Patch from Karsten Loesing."<br />
"When building a consensus, do not include routers that are down. This will cut down 30% to 40% on consensus size. Implements proposal 138."</p>

<p>From the Tor 0.2.1.2-alpha ChangeLog:<br />
"New TestingTorNetwork config option to allow adjustment of previously constant values that, while reasonable, could slow bootstrapping. Implements proposal 135. Patch from Karsten Loesing."<br />
"When building a consensus, do not include routers that are down. This will cut down 30% to 40% on consensus size. Implements proposal 138."</p>

<p>We've added clear user-oriented instructions for the Tor Browser Bundle split-download page:<br />
<a href="https://www.torproject.org/torbrowser/split.html.en" rel="nofollow">https://www.torproject.org/torbrowser/split.html.en</a></p>

<p>We're starting work on a "gettor" email auto-responder script that will let people mail <a href="mailto:gettor@torproject.org" rel="nofollow">gettor@torproject.org</a> and retrieve a copy of Tor from their mailbox. More info forthcoming in July.</p>

<p>More generally, we have a new <a href="https://www.torproject.org/finding-tor" rel="nofollow">https://www.torproject.org/finding-tor</a> page that describes various mechanisms such as mirrors.</p>

<p>In July we plan to deploy a more automated mechanism for tracking which Tor mirrors are up-to-date.</p>

<p>We have our translation server up and online:<br />
<a href="https://translation.torproject.org/" rel="nofollow">https://translation.torproject.org/</a></p>

<p>We have imported the strings from Vidalia, Torbutton, and Torcheck, and we currently have active translations for Spanish, French, German, Italian, Polish, Romanian, Swedish, Turkish, Finnish, Russian, Chinese, and Arabic.</p>

<p>We have a more useful overall translation tutorial here:<br />
<a href="https://www.torproject.org/translation-portal" rel="nofollow">https://www.torproject.org/translation-portal</a></p>

<p>And we have internal documentation here for how to deal with the translation stuff behind the scenes:<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/translations.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/translations.txt</a></p>

<p>In July we plan to add the strings for Vidalia's installer; the challenge is that we need to write a script to convert from the "nsh" (nullscript installer language) format to the "po" (preferred by Pootle) format and back.</p>

<p>In July we also expect to see the first version of our "wml to po and back" conversion tool, that will allow us to start putting our website pages into the translation server.</p>

---
_comments:

<a id="comment-146"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-146" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Loyal Tor User (not verified)</span> said:</p>
      <p class="date-time">July 26, 2008</p>
    </div>
    <a href="#comment-146">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-146" class="permalink" rel="bookmark">Vidalia Package Suggestion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,<br />
As you know Tor (including Vidalia, Privoxy) is a great package for the anonymous user. Working together these individual components are providing core services for anonymity. I would like to bring an attention sideways to the recognized area:</p>
<p>It is --- the newest version of i2p software. It may be worthwhile endeavor to bundle i2p with Tor as a complementary service. There are some areas where Tor is good at (anonymous web browsing). And maybe others areas which may be simpler to use the newer i2p network. </p>
<p>To help support the anonymity efforts i hope you will evaluate the i2p software for packaging because it will help strengthen i2p as an option for the users. It does not need to be a burden for the Tor maintainers either because i2p team is doing most of the hard work. Many user dont know about it but i2p very good now since version 0.6.  The headless* package very is easy to install. Almost no work needed - this forum thread describes installation proceedure:</p>
<p><a href="http://forum.i2p2.de/viewtopic.php?t=1625&amp;sid=756d4ae34bc5162c1383b39f1a7b9d75" rel="nofollow">http://forum.i2p2.de/viewtopic.php?t=1625&amp;sid=756d4ae34bc5162c1383b39f1a7b9d75</a></p>
<p>Its 1 script to start i2p daemon and hooked into the "Start Vidalia" will add i2p onto the existing Tor startup. You may need a button in the control panel to let users select the i2p daemon ( for example "Enable i2p" option ). </p>
<p>You should know that the i2p does not interfere with Tor in any way.  For FoxyProxy. The setting is localhost:4444. And FoxyProxy whitelist pattern=*://*.i2p/* will catch the site ending with .i2p extension.</p>
<p>*i have tested the headless package which is recommended, version 0.62 or later. (POSIX)<br />
*** For software license - please speak to <a href="http://www.i2p2.i2p/index.html" rel="nofollow">i2p development team</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-152"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-152" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 01, 2008</p>
    </div>
    <a href="#comment-152">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-152" class="permalink" rel="bookmark">Tor is not working for me anymore :(</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi<br />
I've installed tor on my ubuntu 8.04 AMD64 and it works for my fine but now a few days that tor is not works for my any more:( it says that :<br />
Aug 01 12:40:37.409 [notice] I learned some more directory information, but not enough to build a circuit.<br />
Aug 01 12:40:45.269 [notice] We now have enough directory information to build circuits.</p>
<p>and then nothing just scilence. It didn't work anymore. And i'm in trouble fetching news in my country.</p>
<p>Can my contry state block tor itself?<br />
Please help me in by response in your blog or by mailing me or even by fixing any bug in your very helpfull project Tor.<br />
Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-154"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-154" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 02, 2008</p>
    </div>
    <a href="#comment-154">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-154" class="permalink" rel="bookmark">RISING found Trojan.PSW.Win32.Undef.adp in vidalia programme</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The RISING [version 20.55.51, updated on 2008-08-02] found out a Trojan.PSW.Win32.Undef.adp in the vidalia programme. The proxy.exe file had been deleted automatically by the RISING. Trojan.PSW.Win32.Undef.adp also found out in the stable version vidalia-bundle-0.2.0.30-0.1.6.exe and cannot be installed. Please give some advice for solving the problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-155"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-155" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 02, 2008</p>
    </div>
    <a href="#comment-155">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-155" class="permalink" rel="bookmark">RISING found Trojan.PSW.Win32.Undef.adp in vidalia programme</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The RISING [version 20.55.51, updated on 2008-08-02] found out a Trojan.PSW.Win32.Undef.adp in the vidalia programme. The proxy.exe file had been deleted automatically by the RISING. Trojan.PSW.Win32.Undef.adp also found out in the stable version vidalia-bundle-0.2.0.30-0.1.6.exe and cannot be installed. Please give some advice for solving the problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-158"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-158" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 03, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-155" class="permalink" rel="bookmark">RISING found Trojan.PSW.Win32.Undef.adp in vidalia programme</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-158">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-158" class="permalink" rel="bookmark">Maybe because the exe file</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Maybe because the exe file is using the same binary structure/packer/crypter as most of the trojans.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-162"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-162" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 05, 2008</p>
    </div>
    <a href="#comment-162">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-162" class="permalink" rel="bookmark">We use the NullSoft</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We use the NullSoft installer (<a href="https://www.torproject.org/svn/trunk/contrib/tor-mingw.nsi.in" rel="nofollow">https://www.torproject.org/svn/trunk/contrib/tor-mingw.nsi.in</a>) for building the Windows exe installer.  It uses "SetCompressor /SOLID LZMA" as the compressor to shrink the total package size down.</p>
<p>There is no proxy.exe file in the official Tor packages.</p>
</div>
  </div>
</article>
<!-- Comment END -->
