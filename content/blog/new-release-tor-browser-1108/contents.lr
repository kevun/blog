title: New Release: Tor Browser 11.0.8 (Android)
---
pub_date: 2022-03-12
---
author: aguestuser
---
categories:

applications
releases
---
summary: Tor Browser 11.0.8 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.8 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.8/).

This version includes important security updates to Firefox:
- https://www.mozilla.org/en-US/security/advisories/mfsa2022-09/
- https://www.mozilla.org/en-US/security/advisories/mfsa2022-11/

Tor Browser 11.0.8 updates Firefox to 96.0.3 and includes bufixes and stability improvements. Notably, we fixed a bug preventing downloads on Android 10 and above.

We also use the opportunity to update various components of Tor Browser, bumping Tor to 0.4.6.10, NoScript to 11.3.7, while switching to the latest stable version of Go (1.16.15) for building our Go-related projects.

The full changelog since the last Android update in [Tor Browser 11.0.6](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Android
  - Update Fenix to 96.3.0
  - Update Tor to 0.4.6.10
  - Update NoScript to 11.3.7
  - [Bug android-components#40075](https://gitlab.torproject.org/tpo/applications/android-components/-/issues/40075): Fix bug preventing downloads on Android Q and higher
  - [Bug tor-browser#40829](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40829): Port fixes for Mozilla security advisory mfsa2022-09 to Android
- Build System
  - Android
    - Update Go to 1.16.15
    - [Bug tor-browser-build#40056](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40056): Download .aar and .jar files for all .pom files
    - [Bug tor-browser-build#40298](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40298): Add documentation about subuid and subgid
    - [Bug tor-browser-build#40385](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40385): Add android platform-31_r0
    - [Bug tor-browser-build#40422](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40422): Remove projects/ed25519
    - [Bug tor-browser-build#40431](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40431): added license info for edwards25519 and edwards25519-extra
    - [Bug tor-browser-build#40432](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40432): Support bug reports by displaying Android Components commit hash in "About Tor Browser" display
    - [Bug tor-browser-build#40436](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40436): Use intermediate files for default bridge lines
    - [Bug tor-browser-build#40438](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40438): Use the same list of bridges for desktop and android
    - [Bug tor-browser-build#40440](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40440): Bump version of snowflake to to include PT LOG events
    - [Bug tor-browser-build#40441](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40441): Add aguestuser as valid git-tag signer
