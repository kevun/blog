title: Vidalia 0.2.17 is out!
---
pub_date: 2012-02-13
---
author: chiiph
---
tags:

vidalia
stable
release
---
categories:

applications
releases
---
_html_body:

<p>Hello everyone, it's been a while since we had a Vidalia release, so we thought "why not make two?".</p>

<p>On a more serious comment, the only difference between 0.2.16 and 0.2.17 is what translations are in. Since we changed our translations workflow, we needed to update the availability policy. Basically, every new translation that has more than 90% done gets enabled. If any of the enabled translations drop bellow the 75% done, we take them out.</p>

<p>Remember that if you find any bugs, you can report them at <a href="https://trac.torproject.org/" rel="nofollow">https://trac.torproject.org/</a>.</p>

<p>Here's what changed since 0.2.15:</p>

<p><b>0.2.17  11-Feb-2012</b></p>

<ul>
<li> Improve the translation policy: do not remove translations that<br />
    are not under 75% done. This re enables Polish and Catalan.</li>
</ul>

<p><b>0.2.16  11-Feb-2012</b></p>

<ul>
<li> Make the default data directory in windows be located in the Local<br />
    AppData instead of the Roaming one. Fixes bug 2319.</li>
<li> Do not launch Firefox with every CIRCUIT_ESTABLISHED signal, do it<br />
    only if Firefox isn't open yet. Fixes bug 2943.</li>
<li> Uses TAKEOWNERSHIP and __OwningControllerProcess to avoid leaving<br />
    tor running in background if Vidalia exits unexpectedly. Fixes bug<br />
    3463.</li>
<li> Attempt to remove port.conf file before using it to avoid a race<br />
    condition between tor and Vidalia. Fixes bug 4048.</li>
<li> Do not allow users to check the "My ISP blocks..." checkbox<br />
    without entering any bridges. Also updates the<br />
    documentation. Fixes bug 4290.</li>
<li> Check that the authentication-cookie file length is exactly 32<br />
    bytes long. Fixes bug 4304.</li>
<li> Explicitly disable ControlPort auto. Fixes bug 4379.</li>
<li> Make the non exit relay option backward compatible with Vidalia &lt;<br />
    0.2.14 so that it doesn't confuse users. Fixes bug 4642.</li>
<li> Sets the preferred size for the GUI layout so it doesn't squeeze<br />
    widges when the size isn't big enough. Fixes bug 4656.</li>
<li> Removes the option to have only HTTPProxy since it does not work<br />
    any more as it used to do with older tor versions. Users should<br />
    use HTTP/HTTPSProxy instead. Fixes bug 4724.</li>
<li> Add a hidden configuration option called SkipVersionCheck so<br />
    systems like Tails can force Vidalia to skip checking tor's<br />
    version. Resolves ticket 4736.</li>
<li> When Tor has cached enough information it bootstraps faster than<br />
    what takes Vidalia connect to it, so Vidalia does not see the<br />
    event to update the progress bar. Now Vidalia explicitly asks for<br />
    bootstrap-phase when it connects to Tor, and updates the progress<br />
    to what is actually happening instead of hanging in<br />
    "Authenticating to Tor". Fixes bug 4827.</li>
<li> Fix size hints in the main window layout so that tilling window<br />
    managers display the window properly. Thanks to Mike Warren for<br />
    the fix. Fixes bug 4907.</li>
<li> Vidalia only validates IPv4 bridge lines. IPv6 bridges are now<br />
    available, and there will be pluggable transport bridge lines. So<br />
    the validation is now delegated to Tor through SETCONF.</li>
<li> Explicitly disable SocksPort auto by setting it to its default<br />
    (9050). Fixes bug 4598.</li>
<li> Sets __ReloadTorrcOnSIGHUP to 0 if SAVECONF failed, which means<br />
    the user can't write the torrc file. Fixes bug 4833.</li>
<li> Enable new translations that are &gt;90% done. The new languages are:<br />
    Bulgarian, Czech, Hebrew, Greek, Indonesian, Korean,<br />
    Dutch. Resolves ticket 5051.</li>
<li> Remove translations that aren't ready enough: Japanese, Thai,<br />
    Albanian, Vietnamese, Chinese (Taiwan), Polish, Catalan and<br />
    Burmese.</li>
</ul>

---
_comments:

<a id="comment-14586"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14586" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 14, 2012</p>
    </div>
    <a href="#comment-14586">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14586" class="permalink" rel="bookmark">Hi.
&quot;Sets</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi.</p>
<p>"Sets __ReloadTorrcOnSIGHUP to 0 if SAVECONF failed, which means<br />
the user can't write the torrc file. Fixes bug 4833."</p>
<p>?????</p>
<p>Where can I now add the exit nodes that I was using on the previous version?  I need to adjust it to work with Foxyproxy/Firefox.  </p>
<p>I'm using Mac OS X.</p>
<p>Also, where can we set up the "SkipVersionCheck"?</p>
<p>Please help.  Thanks</p>
<p>:S</p>
</div>
  </div>
</article>
<!-- Comment END -->
