title: Tor Weekly News — January 21st, 2015
---
pub_date: 2015-01-21
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the third issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the <a href="https://guardianproject.info/2015/01/02/2015-is-the-year-of-bore-sec/" rel="nofollow">boring</a> Tor community.</p>

<h1>Tor Browser 4.0.3 and 4.5a3 are out</h1>

<p>Georg Koppen announced two new releases by the Tor Browser team. <a href="https://blog.torproject.org/blog/tor-browser-403-released" rel="nofollow">Version 4.0.3</a> of the privacy-preserving browser is based on Firefox 31.4.0esr, and also contains updates to NoScript, meek, and Tor Launcher.</p>

<p>The <a href="https://blog.torproject.org/blog/tor-browser-45a3-released" rel="nofollow">third release in the 4.5-alpha series</a> allows the secure in-browser update mechanism to handle signed update files, and will reject unsigned ones from now on. It also restores functionality for meek, which was broken in previous 4.5-alpha releases, and offers other improvements and bugfixes — please see Georg’s announcement for the full changelog.</p>

<p>These releases contain important security updates, so users of both the stable and alpha series should upgrade as soon as possible. Furthermore, Tor Browser 4.5a3 is signed by a new Tor Browser Developers signing key rather than the personal key of an individual developer. If you want to verify your download of the new alpha (and you should!), you will need to retrieve the new key (fingerprint EF6E 286D DA85 EA2A 4BA7 DE68 4E2C 6E87 9329 8290) from a keyserver before doing so.</p>

<h1>Miscellaneous news</h1>

<p>Anthony G. Basile <a href="https://lists.torproject.org/pipermail/tor-talk/2015-January/036526.html" rel="nofollow">announced</a> version 20150114 of Tor-ramdisk, the uClibc-based micro Linux distribution whose only purpose is to host a Tor relay in an environment that maximizes security and privacy. This release includes updates to Tor, Libevent, and other key software.</p>

<p>Nik <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008174.html" rel="nofollow">announced</a> oppy, an onion proxy implemented in Python: “oppy works like a regular Tor client”, though “there are a number of simplifications made, with the major ones primarily centering around circuit management/build logic and how and when network status documents are collected”. Nik also asked for suggestions on how to take the project forward: “Whether or not I continue hacking on oppy to make it a solid piece of software (rather than just a prototype) or just leave it as is as a reference depends on whether or not the Tor development community sees any real uses or future potential for the project”.</p>

<p>meejah <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008166.html" rel="nofollow">announced</a> a new one-to-one encrypted and anonymous voice chat feature for “<a href="https://github.com/meejah/carml.git" rel="nofollow">carml</a>”, the command-line Tor control utility: “ [It] essentially cross-connects the mic + speakers of each side via an Opus + OGG stream over a single Tor TCP connection.” As meejah warns, it is “NOT FOR REAL USE at all yet”, but if you have experience with gstreamer and/or OGG then please see meejah’s message for some unresolved questions.</p>

<p>Following suggestions from <a href="https://lists.torproject.org/pipermail/tor-relays/2015-January/006240.html" rel="nofollow">Sebastian Urbach</a> and <a href="https://lists.torproject.org/pipermail/tor-relays/2015-January/006248.html" rel="nofollow">grarpamp</a>, Karsten Loesing <a href="https://bugs.torproject.org/14257" rel="nofollow">altered</a> the main unit of data rate measurement for the <a href="https://metrics.torproject.org/" rel="nofollow">Tor Metrics portal</a> from MiB/s (mebibytes per second) to the more common Gbit/s (gigabits per second).</p>

<p>Philipp Winter <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008156.html" rel="nofollow">published</a> preliminary statistics and analysis obtained by running a Go implementation of <a href="https://gitweb.torproject.org/doctor.git/" rel="nofollow">Doctor</a>’s sybil-hunting script over archived consensuses: “I’ll have a more detailed analysis at some point in the future.”</p>

<p>The Tails team <a href="https://mailman.boum.org/pipermail/tails-dev/2015-January/007919.html" rel="nofollow">published</a> instructions for running an nginx webserver as a hidden service using a copy of Tails: “Feedback is welcome!”</p>

<p>Thanks to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2015-January/000850.html" rel="nofollow">Frédéric Cornu</a> for running a mirror of the Tor Project’s website and software!</p>

<h1>This week in Tor history</h1>

<p><a href="https://lists.torproject.org/pipermail/tor-news/2014-January/000029.html" rel="nofollow">A year ago this week</a>, the “<a href="http://www.cs.kau.se/philwint/spoiled_onions/" rel="nofollow">Spoiled Onions</a>” project published its preliminary technical report. The goal of the project was to monitor Tor exit relays in order to “expose, document, and thwart malicious or misconfigured relays”; the researchers turned up 65 such relays over the course of their investigation, with the culprits engaging in attacks such as “SSH and HTTPS MitM, HTML injection, SSL stripping, and traffic sniffing”, or unintentionally interfering with traffic as a result of upstream censorship.</p>

<p>Events such as the <a href="https://blog.torproject.org/blog/tor-security-advisory-relay-early-traffic-confirmation-attack" rel="nofollow">RELAY_EARLY traffic confirmation attack</a> and the <a href="https://lists.torproject.org/pipermail/tor-consensus-health/2014-December/005381.html" rel="nofollow">sybil attacks late last year</a> have only highlighted the importance of monitoring for malicious relays in the Tor network. The <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/bad-relays" rel="nofollow">bad-relays mailing list</a> serves as a reporting channel for Tor community members who believe particular relays are up to no good (messages sent to the list are not publicly visible, for <a href="https://lists.torproject.org/pipermail/tor-news/2014-August/000057.html" rel="nofollow">various reasons</a>); David Fifield has been experimenting with <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008095.html" rel="nofollow">data visualizations of significant network events</a>; and Philipp Winter, a “Spoiled Onions” co-author, has been working on additional tools (such as the above-mentioned Go sybil hunter and “<a href="https://gitweb.torproject.org/user/phw/zoossh.git/" rel="nofollow">zoossh</a>”, a speedy Tor network document parser) to make these checks more efficient — to give only a few examples of recent work by the community on this issue.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

