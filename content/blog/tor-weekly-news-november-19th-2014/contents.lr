title: Tor Weekly News — November 19th, 2014
---
pub_date: 2014-11-19
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the forty-sixth issue in 2014 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor Browser 4.5-alpha-1 is out</h1>

<p>Mike Perry <a href="https://blog.torproject.org/blog/tor-browser-45-alpha-1-released" rel="nofollow">announced</a> the first alpha release in the Tor Browser 4.5 series. This version goes some way to restoring one of the features most missed by users following the removal of the now-defunct Vidalia interface from Tor Browser — the ability to quickly visualize the Tor circuit that the current page is using. Clicking on the green Torbutton icon in the Tor Browser window now brings up a small diagram showing the IP addresses of all relays in a circuit, and the states in which they are located; this may help users evaluate the suitability of the circuits their Tor has selected, and also to quickly identify a malicious exit relay if they notice unusual behavior in downloaded pages and files.</p>

<p>Another key user-facing innovation in this release is the “security slider”. Users can now choose from four security settings in Torbutton’s “Preferences” window — “low (default)”, “medium-low”, “medium-high”, and “high” — that allow them to customize their Tor Browser based on their own security and usability needs, while still working to prevent “partitioning” attacks, which try to identify users based on their unusual browser configuration.</p>

<p>For other important additions in this series, please see the full changelog in Mike’s post. If you want to try out this alpha version, you can find it on the Tor Browser <a href="https://www.torproject.org/projects/torbrowser.html#downloads-alpha" rel="nofollow">project page</a> or in the <a href="https://www.torproject.org/dist/torbrowser/4.5-alpha-1/" rel="nofollow">distribution directory</a>; please report any bugs you find!</p>

<h1>Tor Browser on 32-bit Macs approaches end-of-life</h1>

<p>Now that Apple has discontinued support for the last remaining 32-bit Mac systems, Mike Perry <a href="https://blog.torproject.org/blog/end-life-plan-tor-browser-32-bit-macs" rel="nofollow">announced</a> that the Tor Browser team will soon stop distributing 32-bit builds of its software. This week’s 4.5-alpha-1, like all future releases in the 4.5 series, is only available in a 64-bit build, and all support for 32-bit systems will end once 4.5 supersedes 4.0.</p>

<p>“32-bit Mac users likely have a month or two to decide what to do”, wrote Mike. “If your actual Mac hardware is 64-bit capable, you can upgrade to either the 64-bit edition of OSX 10.6 (which we will continue to support for a bit longer), or use the app store to upgrade to 10.9 or 10.10. If your hardware is not 64-bit capable and won’t run these newer Mac operating systems, you should still be able to use <a href="https://tails.boum.org" rel="nofollow">Tails</a>, which contains the Tor Browser.”</p>

<p>As a side effect of this transition, Tor Browser 4.0’s experimental in-browser secure updater will not handle the upgrade to the 64-bit build correctly for any Mac user; the old version must instead be replaced manually with the new one.</p>

<h1>Miscellaneous news</h1>

<p><a href="https://blog.torproject.org/blog/traffic-correlation-using-netflows" rel="nofollow">Roger Dingledine</a> and <a href="https://blog.torproject.org/blog/traffic-correlation-using-netflows#comment-78918" rel="nofollow">Sambuddho Chakravarty</a> responded on the Tor blog to inaccurate reports of a new attack against Tor, based on a recent study co-authored by Sambuddho. “It’s great to see more research on traffic correlation attacks, especially on attacks that don’t need to see the whole flow on each side. But it’s also important to realize that traffic correlation attacks are not a new area”, wrote Roger.</p>

<p>The Tails team set out the December <a href="https://mailman.boum.org/pipermail/tails-dev/2014-November/007422.html" rel="nofollow">release schedule</a> for version 1.2.1 of the anonymous live operating system.</p>

<p>Giovanni Pellerano <a href="https://lists.torproject.org/pipermail/tor-talk/2014-November/035742.html" rel="nofollow">announced</a> version 3.1.30 of Tor2web, which now supports web access to Tor hidden services over TLS. Access to the Facebook hidden service, the most high-profile instance of an HTTPS-enabled .onion site, is blocked in this version, as Tor2web offers no benefit in cases where there exists an identical service on the regular or “naked” web, and may actually present additional risk of compromise.</p>

<p>Griffin Boyce <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007798.html" rel="nofollow">requested feedback</a> on a “very rough” version of <a href="https://github.com/glamrock/Stormy" rel="nofollow">Stormy</a>, the simple hidden service setup wizard. “I’d love to get feedback on places where it breaks and where it could use a major structural change […] the current setup is entirely for development and should not be used as-is.”</p>

<p>Virgil Griffith <a href="https://lists.torproject.org/pipermail/tor-talk/2014-November/035658.html" rel="nofollow">started a discussion</a> on the suitability of the name “hidden services” as opposed to other possible terms like “onion service” or “onion site”. Among the many responses, Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-talk/2014-November/035660.html" rel="nofollow">suggested</a> that an alternative name like “onion service” “makes people have to learn what it is rather than guessing (and often guessing wrong)”, while Nathan Freitas <a href="https://lists.torproject.org/pipermail/tor-talk/2014-November/035662.html" rel="nofollow">pointed out</a> that as “typical users don’t talk about web services, they talk about web sites or pages”, “onion site” might be a term worth adopting.</p>

<p>Tom Ritter <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007786.html" rel="nofollow">put forward</a> a number of improvements to the integration of HTTPS certificates and hidden services, following “a spirited debate on IRC”.</p>

<p>The Wikimedia Foundation is the latest high-profile organization to <a href="https://lists.torproject.org/pipermail/tor-talk/2014-November/035655.html" rel="nofollow">set up a non-exit Tor relay</a>. “It’s just a small contribution to the network. Really — anyone can do it.”</p>

<p>This issue of Tor Weekly News has been assembled by Harmony and Lunar.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the team <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">mailing list</a> if you want to get involved!</p>

