title: Strength in Numbers: Why Every Dollar Counts
---
pub_date: 2018-11-27
---
author: al
---
tags:

Strength in Numbers
Giving Tuesday
---
summary: If you've never given to the Tor Project, we have some exciting news for Giving Tuesday. A donor has offered to match all first-time gifts, up to $20,000. So if you've never given to the Tor Project before, your gift gets matched twice. A $25 donation becomes $75.
---
_html_body:

<p><em>This post is one in a series of blogs to complement our 2018 crowdfunding campaign, Strength in Numbers. Anonymity loves company, and the internet freedom movement is stronger when we fight together. <a href="https://torproject.org/donate/donate-sin-gt-bp">Please contribute today</a>, and your gift will be matched by Mozilla.</em></p>
<p><strong>Something you already know about the Tor Project: we’re good at what we do.</strong></p>
<p>The Tor Project and members of our community are at the forefront of the <a href="https://blog.torproject.org/strength-numbers-internet-freedom-line">internet freedom movement</a>, fighting against digital censorship and protecting the privacy of millions online. We’re on the ground speaking with activists, journalists, and internet users in countries all over the world, teaching them about Tor and online security.</p>
<p>Our Open Observatory of Network Interference (<a href="https://www.ooni.torproject.org/">OONI</a>) project is one of the few initiatives that monitors network interference happening globally. Through a combination of contributions by volunteers worldwide and our data collection methodologies, we’re able to identify when, where, and how digital censorship is happening while protecting the privacy and security of the individuals who help us gather our data.</p>
<p>Our Network and Browser teams have been working hard in 2018 to provide better support for mobile, as most of the world primarily accesses the internet access through cell phones. We now offer <a href="https://play.google.com/store/apps/details?id=org.torproject.torbrowser_alpha">Tor Browser for Android</a> (now in alpha), and our Tor network daemon has been optimized for mobile so it’s easier for application developers to embed Tor in their apps.</p>
<p><strong>Something you <em>should</em> know about the Tor Project: we rely on donations to operate.</strong></p>
<p>We are here today, in part, because of the strong support of individual donors like you.</p>
<p>Your contribution can help ensure that the Tor Project and our community have a long life of building privacy, security, and anti-censorship technologies. Unrestricted money, given by donors like you, allows us to be flexible with our development. While most of our projects are funded by grants, your donation allows us to address new and changing priorities, like security issues or emergent censorship events.</p>
<p>Your donations also help us fund our participation at conferences, sending our trainers to different locations around the world to provide digital security trainings, and bringing members from our community to <a href="https://blog.torproject.org/hack-us-mexico-city-hackea-con-tor-en-mexico">Tor meetings</a>.</p>
<p>And most important, if one of our other funding sources disappears, your support ensures the Tor Project will not fall apart without it.</p>
<p><strong>Your individual gift makes the Tor Project strong in a way no other kind of donation can.</strong></p>
<p><a href="https://torproject.org/donate/donate-sin-gt-bp"><img alt="donate-button" src="/static/images/blog/inline-images/tor-donate-button_5.png" class="align-center" /></a></p>
<p><strong><a href="https://torproject.org/donate/donate-sin-gt-bp">Give today</a>, and Mozilla will match your donation, dollar for dollar. Every gift--doubled. </strong></p>
<p>And if you've never given to the Tor Project, we have some exciting news for <a href="https://www.givingtuesday.org/">Giving Tuesday</a>. A donor has offered to <strong>match all first-time gifts, up to $20,000</strong>. So if you've never given to the Tor Project before, <a href="https://torproject.org/donate/donate-gt-sin-em2">your gift</a> gets matched twice. A $25 donation becomes $75.</p>
<p>Join the movement; there is Strength in Numbers. Thanks in advance for your support!</p>

---
_comments:

<a id="comment-278616"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278616" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 27, 2018</p>
    </div>
    <a href="#comment-278616">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278616" class="permalink" rel="bookmark">Looks like this page needs…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Looks like this page needs to be updated:</p>
<p><a href="https://donate.torproject.org/donor-faq" rel="nofollow">https://donate.torproject.org/donor-faq</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278625"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278625" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 28, 2018</p>
    </div>
    <a href="#comment-278625">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278625" class="permalink" rel="bookmark">Cool, I will donate.
Who is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Cool, I will donate.<br />
Who is the donor who will match my first-time gift?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278626"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278626" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">November 28, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278625" class="permalink" rel="bookmark">Cool, I will donate.
Who is…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278626">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278626" class="permalink" rel="bookmark">It&#039;s someone from our…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you! </p>
<p>It's someone from our community who asked to remain anonymous. We can respect that ;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
