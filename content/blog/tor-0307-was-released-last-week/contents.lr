title: Tor 0.3.0.7 was released last week!
---
pub_date: 2017-05-26
---
author: nickm
---
tags:

tor
stable
release
---
categories:

network
releases
---
_html_body:

<p>Hello! This release came out 11 days ago, but since the blog was down at the time, I was only able to announce on the tor-announce@ mailing list. Nevertheless, I'm copying it here in case anyone didn't see it.</p>
<p>Tor 0.3.0.7 fixes a medium-severity security bug in earlier versions of Tor 0.3.0.x, where an attacker could cause a Tor relay process to exit. Relays running earlier versions of Tor 0.3.0.x should upgrade; clients are not affected.</p>
<p>If you build Tor from source, you can find it at the usual place on the website. Packages should be ready over the next weeks.</p>
<h2>Changes in version 0.3.0.7 - 2017-05-15</h2>
<ul>
<li>Major bugfixes (hidden service directory, security):
<ul>
<li>Fix an assertion failure in the hidden service directory code, which could be used by an attacker to remotely cause a Tor relay process to exit. Relays running earlier versions of Tor 0.3.0.x should upgrade. This security issue is tracked as TROVE-2017-002. Fixes bug <a href="https://bugs.torproject.org/22246">22246</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor features:
<ul>
<li>Update geoip and geoip6 to the May 2 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor features (future-proofing):
<ul>
<li>Tor no longer refuses to download microdescriptors or descriptors if they are listed as "published in the future". This change will eventually allow us to stop listing meaningful "published" dates in microdescriptor consensuses, and thereby allow us to reduce the resources required to download consensus diffs by over 50%. Implements part of ticket <a href="https://bugs.torproject.org/21642">21642</a>; implements part of proposal 275.</li>
</ul>
</li>
<li>Minor bugfixes (Linux seccomp2 sandbox):
<ul>
<li>The getpid() system call is now permitted under the Linux seccomp2 sandbox, to avoid crashing with versions of OpenSSL (and other libraries) that attempt to learn the process's PID by using the syscall rather than the VDSO code. Fixes bug <a href="https://bugs.torproject.org/21943">21943</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-268757"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268757" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Nah (not verified)</span> said:</p>
      <p class="date-time">May 27, 2017</p>
    </div>
    <a href="#comment-268757">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268757" class="permalink" rel="bookmark">So, hm, what&#039;s the deal with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So, hm, what's the deal with no .*debs showing up for stable 0.3.x?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-268766"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268766" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 28, 2017</p>
    </div>
    <a href="#comment-268766">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268766" class="permalink" rel="bookmark">Can you please update expert…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you please update expert bundle link on download page?<br />
<a href="https://dist.torproject.org/torbrowser/7.0a4/tor-win32-0.3.0.6.zip" rel="nofollow">https://dist.torproject.org/torbrowser/7.0a4/tor-win32-0.3.0.6.zip</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-268770"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268770" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 28, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-268766" class="permalink" rel="bookmark">Can you please update expert…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-268770">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268770" class="permalink" rel="bookmark">The expert bundle gets…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The expert bundle gets updated when Tor Browser gets updated. So presumably when there's a 7.0a5 out is when there will be a newer one.</p>
<p>I agree that's not perfect. In the meantime, see also Linus's nightly builds:<br />
<a href="https://people.torproject.org/~linus/builds/" rel="nofollow">https://people.torproject.org/~linus/builds/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-268786"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268786" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>really? (not verified)</span> said:</p>
      <p class="date-time">May 31, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-268786">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268786" class="permalink" rel="bookmark">Non-English nightly builds…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Non-English nightly builds are awful. It looks like no one uses them.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-268792"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268792" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 31, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-268786" class="permalink" rel="bookmark">Non-English nightly builds…</a> by <span>really? (not verified)</span></p>
    <a href="#comment-268792">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268792" class="permalink" rel="bookmark">I would believe that no one…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would believe that no one uses them. </p>
<p>If there is something wrong with them, please consider opening a ticket on <a href="https://bugs.torproject.org/" rel="nofollow">https://bugs.torproject.org/</a> ?</p>
<p>Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-268805"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268805" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">June 02, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-268786" class="permalink" rel="bookmark">Non-English nightly builds…</a> by <span>really? (not verified)</span></p>
    <a href="#comment-268805">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268805" class="permalink" rel="bookmark">In what regard are they…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In what regard are they awful? Which locale are you talking about? I just gave the russian bundle a try on Linux and Windows it looks to me not that different from then en-US one and a quick test did not show a different behavior.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-268795"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268795" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>beenanewbi (not verified)</span> said:</p>
      <p class="date-time">June 01, 2017</p>
    </div>
    <a href="#comment-268795">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268795" class="permalink" rel="bookmark">Struggling to get into Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Struggling to get into Tor for a large part of May 2017. Still can't get in. By the time it reaches the "checktor." Site, it has timed out. </p>
<p>No I have not downloaded the fix. I saw the message but only the message. There was no download link.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-268800"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268800" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  pastly
  </article>
    <div class="comment-header">
      <p class="comment__submitted">pastly said:</p>
      <p class="date-time">June 01, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-268795" class="permalink" rel="bookmark">Struggling to get into Tor…</a> by <span>beenanewbi (not verified)</span></p>
    <a href="#comment-268800">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268800" class="permalink" rel="bookmark">Could you be more specific…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Could you be more specific about the issues you are seeing? And what fix you are talking about? </p>
<p>Logs could also be very helpful. When Tor Browser is open, click the green onion button to the left of the URL bar, then click Tor Network Settings, then click Copy Tor Log To Clipboard. If you could paste the logs to somewhere like <a href="https://paste.debian.net" rel="nofollow">https://paste.debian.net</a> and share the resulting link, that could be very helpful.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-268811"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-268811" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ISP BLOCK TOR! (not verified)</span> said:</p>
      <p class="date-time">June 03, 2017</p>
    </div>
    <a href="#comment-268811">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-268811" class="permalink" rel="bookmark">Error. Page cannot be…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Error. Page cannot be displayed. Please contact your service provider for more details. (32)</p>
<p>ISP BLOCKING TOR!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-269494"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-269494" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>John Butcher (not verified)</span> said:</p>
      <p class="date-time">June 28, 2017</p>
    </div>
    <a href="#comment-269494">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-269494" class="permalink" rel="bookmark">I have been away for a few…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have been away for a few weeks and on my return TOR updated but will not open now any ideas please?</p>
</div>
  </div>
</article>
<!-- Comment END -->
