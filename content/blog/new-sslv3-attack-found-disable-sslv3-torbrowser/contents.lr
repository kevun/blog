title: New SSLv3 attack found: Disable SSLv3 in TorBrowser
---
pub_date: 2014-10-15
---
author: nickm
---
tags:

tor browser
ssl
tls
sslv3
poodle
---
categories: applications
---
_html_body:

<p>Hi!  It's a new month, so that means there's a new attack on TLS.</p>

<p>This time, the attack is that many clients, when they find a server that doesn't support TLS, will downgrade to the ancient SSLv3.  And SSLv3 is subject to a new padding oracle attack.</p>

<p>There is a readable summary of the issue at <a href="https://www.imperialviolet.org/2014/10/14/poodle.html" rel="nofollow">Adam Langley's blog</a>; it links to other descriptions of the attack.</p>

<p>Tor itself is not affected: all released versions for a long time have shipped with TLSv1 enabled, and we have never had a fallback mechanism to SSLv3. Furthermore, Tor does not send the same secret encrypted in the same way in multiple connection attempts, so even if you could make Tor fall back to SSLv3, a padding oracle attack probably wouldn't help very much.</p>

<p>TorBrowser, on the other hand, is based on Firefox, and has the same protocol downgrade mechanisms as Firefox.  I expect and hope the TorBrowser team will be<br />
releasing a new version soon with SSLv3 disabled.  But in the meantime, I think you can disable SSLv3 yourself by changing the value of the "security.tls.version.min" preference to "1".  (The default value is "0".)</p>

<p>To do that:</p>

<ol>
<li> Enter "about:config" in the URL bar.
</li>
<li> Then you click "I'll be careful, I promise".
</li>
<li> Then enter "security.tls.version.min" in the preference "search"<br />
field underneath the URL bar.  (Not the search box next to the URL<br />
bar.)
</li>
<li> You should see an entry that says "security.tls.version.min" under<br />
"Preference Name".  Double-click on it, then enter the value "1" and<br />
click okay.
</li>
</ol>

<p>You should now see that the value of "security.tls.version.min" is set to one.</p>

<p>(Note that I am not a Firefox developer or a TorBrowser developer: if you're cautious, you might want to wait until one of them says something here before you try this workaround.  On the other hand, if you believe me, you should probably do this in your regular Firefox as well.)</p>

<p>Obviously, this isn't a convenient way to do this; if you are uncertain of your ability to do so, waiting for an upgrade might be a good move.  In the meantime, if you have serious security requirements and you cannot disable SSLv3, it might be a good idea to avoid using the Internet for a week or two while this all shakes out.</p>

<p>Best wishes to other residents of these interesting times.</p>

---
_comments:

<a id="comment-76587"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76587" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 14, 2014</p>
    </div>
    <a href="#comment-76587">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76587" class="permalink" rel="bookmark">Have done this,thanks!!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Have done this,thanks!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-76594"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76594" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 14, 2014</p>
    </div>
    <a href="#comment-76594">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76594" class="permalink" rel="bookmark">https://www.imperialviolet.or</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.imperialviolet.org/2014/10/14/poodle.html" rel="nofollow">https://www.imperialviolet.org/2014/10/14/poodle.html</a></p>
<p>This way of doing it is confirmed to work.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-76602"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76602" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 15, 2014</p>
    </div>
    <a href="#comment-76602">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76602" class="permalink" rel="bookmark">I think &#039;ll just wait for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think 'll just wait for the next release</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-76851"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76851" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 17, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-76602" class="permalink" rel="bookmark">I think &#039;ll just wait for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-76851">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76851" class="permalink" rel="bookmark">And, it&#039;s</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>And, it's ready:<br />
<a href="https://blog.torproject.org/blog/tor-browser-40-released" rel="nofollow">https://blog.torproject.org/blog/tor-browser-40-released</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-76608"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76608" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 15, 2014</p>
    </div>
    <a href="#comment-76608">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76608" class="permalink" rel="bookmark">but _remember_ disabling</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>but _remember_ disabling sslv3 you will be distinct from others dummies</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-76611"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76611" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 15, 2014</p>
    </div>
    <a href="#comment-76611">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76611" class="permalink" rel="bookmark">Totally unrelated comment</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Totally unrelated comment here: Of sites where you can ask general questions of actual lawyers, (JustAnswer.com, Avvo.com, and Justia.com, there may be more) only Justia.com allows account creation and posting via TorBrowser with Javascript and Flash fully disabled in about:config &amp; about:addons.  SSL implementation is inconsistent, though.  Accounts.justia.com got an F from SSL Labs.  Fortunately, there's no real name policy.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-76618"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76618" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 15, 2014</p>
    </div>
    <a href="#comment-76618">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76618" class="permalink" rel="bookmark">Official Mozilla Security</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Official Mozilla Security Blog entry <a href="https://blog.mozilla.org/security/2014/10/14/the-poodle-attack-and-the-end-of-ssl-3-0/" rel="nofollow">https://blog.mozilla.org/security/2014/10/14/the-poodle-attack-and-the-…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-76976"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76976" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 19, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-76618" class="permalink" rel="bookmark">Official Mozilla Security</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-76976">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76976" class="permalink" rel="bookmark">The blog links to an add-on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The blog links to an add-on that can turn off SSL 3.0 for you: <a href="https://addons.mozilla.org/firefox/addon/ssl-version-control/" rel="nofollow">https://addons.mozilla.org/firefox/addon/ssl-version-control/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-76982"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76982" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 19, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-76976" class="permalink" rel="bookmark">The blog links to an add-on</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-76982">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76982" class="permalink" rel="bookmark">No, just move to Tor Browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, just move to Tor Browser 4.0.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77134"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77134" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 20, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-77134">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77134" class="permalink" rel="bookmark">Even better.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Even better. Strike-out/remove my comment #comment-76976 above?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-76626"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76626" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 15, 2014</p>
    </div>
    <a href="#comment-76626">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76626" class="permalink" rel="bookmark">http://www.metafilter.com/131</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://www.metafilter.com/131948/FBI-Admits-It-Controlled-Tor-Servers-Behind-Mass-Malware-Attack" rel="nofollow">http://www.metafilter.com/131948/FBI-Admits-It-Controlled-Tor-Servers-B…</a></p>
<p>FBI malwares Tor servers!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-76850"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76850" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 17, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-76626" class="permalink" rel="bookmark">http://www.metafilter.com/131</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-76850">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76850" class="permalink" rel="bookmark">The people who wrote those</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The people who wrote those articles either didn't understand much about Tor, or they intentionally wrote ambiguous and confusing titles.</p>
<p>"FBI malwares web servers" is the correct title. Some people ran some web servers, and some FBI people broke into them.</p>
<p>Nothing to do with Tor servers, sorry.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-76627"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76627" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 15, 2014</p>
    </div>
    <a href="#comment-76627">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76627" class="permalink" rel="bookmark">NSA targets privacy people</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>NSA targets privacy people (all of TOR)</p>
<p><a href="https://gigaom.com/2014/07/03/nsa-targeted-tor-server-administrator-in-germany-reports-claim/" rel="nofollow">https://gigaom.com/2014/07/03/nsa-targeted-tor-server-administrator-in-…</a></p>
<p>NSA wants you to use Tor</p>
<p><a href="http://www.counterpunch.org/2014/07/18/the-nsa-wants-you-to-trust-tor-should-you/" rel="nofollow">http://www.counterpunch.org/2014/07/18/the-nsa-wants-you-to-trust-tor-s…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-76852"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76852" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 17, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-76627" class="permalink" rel="bookmark">NSA targets privacy people</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-76852">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76852" class="permalink" rel="bookmark">See all the discussion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>See all the discussion at<br />
<a href="https://blog.torproject.org/blog/being-targeted-nsa" rel="nofollow">https://blog.torproject.org/blog/being-targeted-nsa</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-76628"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76628" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 15, 2014</p>
    </div>
    <a href="#comment-76628">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76628" class="permalink" rel="bookmark">I&#039;ll give it a try. Thanks.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'll give it a try. Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-76847"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76847" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 17, 2014</p>
    </div>
    <a href="#comment-76847">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76847" class="permalink" rel="bookmark">did you delete the counter?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>did you delete the counter?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77062"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77062" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 20, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-76847" class="permalink" rel="bookmark">did you delete the counter?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77062">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77062" class="permalink" rel="bookmark">Counter?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Counter?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
