title: New stable Tor releases: 0.3.1.8, 0.3.0.12, 0.2.9.13, 0.2.8.16, 0.2.5.15
---
pub_date: 2017-10-25
---
author: nickm
---
tags: stable release
---
categories: releases
---
summary: Tor 0.3.1.8 is the second stable release in the 0.3.1 series. It includes several bugfixes, including a bugfix for a crash issue that had affected relays under memory pressure. It also adds a new directory authority, Bastet.
---
_html_body:

<p>Hi, all!</p>
<p>There are new stable Tor releases available for download.  If you build Tor from source, you can find the source for the latest stable release on our Download page. You can find the older releases at <a href="https://dist.torproject.org">https://dist.torproject.org/</a>. Packages should be available over the coming days, including a planned TorBrowser release in November.</p>
<p>These releases backport stability fixes from later Tor releases, and add the key for the latest directory authority, "bastet".</p>
<p>The ChangeLog for 0.3.1.8 follows below. For the changelogs for other releases, see the <a href="https://lists.torproject.org/pipermail/tor-announce/2017-October/000143.html">announcement email</a>.</p>
<p>Tor 0.3.1.8 is the second stable release in the 0.3.1 series. It includes several bugfixes, including a bugfix for a crash issue that had affected relays under memory pressure. It also adds a new directory authority, Bastet.</p>
<h2>Changes in version 0.3.1.8 - 2017-10-25</h2>
<ul>
<li>Directory authority changes:
<ul>
<li>Add "Bastet" as a ninth directory authority to the default list. Closes ticket <a href="https://bugs.torproject.org/23910">23910</a>.</li>
<li>The directory authority "Longclaw" has changed its IP address. Closes ticket <a href="https://bugs.torproject.org/23592">23592</a>.</li>
</ul>
</li>
<li>Major bugfixes (relay, crash, assertion failure, backport from 0.3.2.2-alpha):
<ul>
<li>Fix a timing-based assertion failure that could occur when the circuit out-of-memory handler freed a connection's output buffer. Fixes bug <a href="https://bugs.torproject.org/23690">23690</a>; bugfix on 0.2.6.1-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (directory authorities, backport from 0.3.2.2-alpha):
<ul>
<li>Remove longclaw's IPv6 address, as it will soon change. Authority IPv6 addresses were originally added in 0.2.8.1-alpha. This leaves 3/8 directory authorities with IPv6 addresses, but there are also 52 fallback directory mirrors with IPv6 addresses. Resolves 19760.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the October 4 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, backport from 0.3.2.2-alpha):
<ul>
<li>Fix a compilation warning when building with zstd support on 32-bit platforms. Fixes bug <a href="https://bugs.torproject.org/23568">23568</a>; bugfix on 0.3.1.1-alpha. Found and fixed by Andreas Stieger.</li>
</ul>
</li>
<li>Minor bugfixes (compression, backport from 0.3.2.2-alpha):
<ul>
<li>Handle a pathological case when decompressing Zstandard data when the output buffer size is zero. Fixes bug <a href="https://bugs.torproject.org/23551">23551</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory authority, backport from 0.3.2.1-alpha):
<ul>
<li>Remove the length limit on HTTP status lines that authorities can send in their replies. Fixes bug <a href="https://bugs.torproject.org/23499">23499</a>; bugfix on 0.3.1.6-rc.</li>
</ul>
</li>
<li>Minor bugfixes (hidden service, relay, backport from 0.3.2.2-alpha):
<ul>
<li>Avoid a possible double close of a circuit by the intro point on error of sending the INTRO_ESTABLISHED cell. Fixes bug <a href="https://bugs.torproject.org/23610">23610</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (memory safety, backport from 0.3.2.3-alpha):
<ul>
<li>Clear the address when node_get_prim_orport() returns early. Fixes bug <a href="https://bugs.torproject.org/23874">23874</a>; bugfix on 0.2.8.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (unit tests, backport from 0.3.2.2-alpha):
<ul>
<li>Fix additional channelpadding unit test failures by using mocked time instead of actual time for all tests. Fixes bug <a href="https://bugs.torproject.org/23608">23608</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-272098"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272098" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 25, 2017</p>
    </div>
    <a href="#comment-272098">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272098" class="permalink" rel="bookmark">Drop support for 0.2.8 and 0…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Drop support for 0.2.8 and 0.2.5 on Dec 24, 2017.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272183"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272183" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">October 27, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272098" class="permalink" rel="bookmark">Drop support for 0.2.8 and 0…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-272183">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272183" class="permalink" rel="bookmark">Why?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272188"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272188" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to nickm</p>
    <a href="#comment-272188">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272188" class="permalink" rel="bookmark">Three years is enough.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Three years is enough.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-272131"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272131" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>SHOTTI (not verified)</span> said:</p>
      <p class="date-time">October 25, 2017</p>
    </div>
    <a href="#comment-272131">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272131" class="permalink" rel="bookmark">I want to visit tor it&#039;s my…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I want to visit tor it's my drem<br />
....plese</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272174"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272174" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272131" class="permalink" rel="bookmark">I want to visit tor it&#039;s my…</a> by <span>SHOTTI (not verified)</span></p>
    <a href="#comment-272174">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272174" class="permalink" rel="bookmark">...what?…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>...what?</p>
<p>Just download Tor from this site and you can "visit Tor".</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272182"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272182" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>TorLiker (not verified)</span> said:</p>
      <p class="date-time">October 27, 2017</p>
    </div>
    <a href="#comment-272182">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272182" class="permalink" rel="bookmark">I have been unable to use…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have been unable to use Tor for over a month now. Windows 10 did an update and it has not worked since. I've been looking all over the net for a fix, but I get the same error each time. It gives me a box that says to reset or copy/paste the info and send it to tor, but nothing gets saved. Anyone have an idea why this is?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272206"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272206" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 29, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272182" class="permalink" rel="bookmark">I have been unable to use…</a> by <span>TorLiker (not verified)</span></p>
    <a href="#comment-272206">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272206" class="permalink" rel="bookmark">Maybe it&#039;s your antivirus or…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Maybe it's your antivirus or something</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272210"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272210" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>TorLikerAnswer (not verified)</span> said:</p>
      <p class="date-time">October 29, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272182" class="permalink" rel="bookmark">I have been unable to use…</a> by <span>TorLiker (not verified)</span></p>
    <a href="#comment-272210">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272210" class="permalink" rel="bookmark">There is a windows…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is a windows essentials tool for exploring an executable, try googling for that and opening the tor.exe (or whatever tor is called on windows) via it and check what dlls it is requesting and whether they are found or not.</p>
<p>I ran into an issue with Libreoffice the other day that was due to a missing dll thanks to the latest version being compiled with the visual studio 2017 redist which the installer didn't check for while installing. If something similiar happened with tor that might be the cause of your issue. Another possibility is a package you had installed was rolled back as a result of updates. In the latter case, unless it shows up in the dynamic link checking tool, you will be best helped by joining the mailing list and asking there, or filing a bug on the bugtracker.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272214"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272214" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Jean L (not verified)</span> said:</p>
      <p class="date-time">October 29, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272182" class="permalink" rel="bookmark">I have been unable to use…</a> by <span>TorLiker (not verified)</span></p>
    <a href="#comment-272214">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272214" class="permalink" rel="bookmark">Use linux instead : Windows …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Use linux instead : Windows (all versions) is full of US spies and malware (that is THE speed, or conection problem, on Window's computers).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272184"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272184" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>qr (not verified)</span> said:</p>
      <p class="date-time">October 27, 2017</p>
    </div>
    <a href="#comment-272184">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272184" class="permalink" rel="bookmark">Do you plan to :…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you plan to :<br />
- insert a warning or visual/alert color icon for a suspicious relays ( but how to know that ?)<br />
- the flag of the country (aesthetic improvement ? )<br />
- invite the dev of tor_tools to update their app (is it not first their responsibility ?)<br />
- block the connection if several tabs have the same relays (security improvement ?)<br />
- block Tor if a malfunction ( https / noscript ) happens<br />
- an automatic check tester in case of problem (a command line that i do not know ... i can't open Tor without connection so a bug report is not possible ?)<br />
- alert the user to do not run Tor as root  so prevent it by blocking Tor ! (and if i open a terminal as root when Tor runs ; it interferes with Tor ).</p>
<p>you propose a lot of technical improvement at every release : thx.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272276"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272276" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>David Mishra (not verified)</span> said:</p>
      <p class="date-time">November 02, 2017</p>
    </div>
    <a href="#comment-272276">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272276" class="permalink" rel="bookmark">I am still unable to use Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am still unable to use Tor browser in my Win 10. I don't understand but when I click on the icon the <a href="https://show-box.ooo/" rel="nofollow">app</a> it doesn't respond and absolutely nothing happens is it something comfortability issue or what do I need to do to fix it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272280"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272280" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">November 02, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272276" class="permalink" rel="bookmark">I am still unable to use Tor…</a> by <span>David Mishra (not verified)</span></p>
    <a href="#comment-272280">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272280" class="permalink" rel="bookmark">So, this is just an issue…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So, this is just an issue with that particular app and otherwise Tor Browser is working for you? Could you give me exact steps to reproduce your problem?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272627"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272627" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon (not verified)</span> said:</p>
      <p class="date-time">November 19, 2017</p>
    </div>
    <a href="#comment-272627">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272627" class="permalink" rel="bookmark">Please, Ubuntu upgrade PPA.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please, Ubuntu upgrade PPA.</p>
</div>
  </div>
</article>
<!-- Comment END -->
