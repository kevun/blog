title: Torbutton-alpha 1.3.1 released for testing
---
pub_date: 2011-01-04
---
author: phobos
---
tags:

torbutton
alpha release
firefox
---
categories:

applications
releases
---
_html_body:

<p>Torbutton 1.3.1-alpha has been released at:<br />
<a href="https://www.torproject.org/dist/torbutton/torbutton-1.3.1-alpha.xpi" rel="nofollow">https://www.torproject.org/dist/torbutton/torbutton-1.3.1-alpha.xpi</a> and .asc</p>

<p>This release features a fix for the nasty pref dialog issue in 1.3.0 (bug #2011), as well as Firefox 4.0 support. Thanks to new APIs in Firefox 3.5 and better privacy options in Firefox 4, Torbutton has now been simplified as well. While we still provide a number of XPCOM components, the number of native Firefox components we replace has shrunk from 5 to just one.</p>

<p>However, the amount of changes involved in supporting Firefox 4 were substantial, and it is likely that these changes as well as the removal of old code has introduced new bugs. We've done our best to test out operation on Firefox 3.6 and 4.0, but we have not tested Firefox 3.0, and may have missed other issues as well. Please report any issues you notice on our bugtracker: <a href="https://trac.torproject.org/projects/tor/report/14" rel="nofollow">https://trac.torproject.org/projects/tor/report/14</a></p>

<p>Here is the complete changelog:<br />
 * bugfix: bug 1894: Amnesia is now called TAILS (patch from intrigeri)<br />
 * bugfix: bug 2315: Remove reference to TorVM (patch from intrigeri)<br />
 * bugfix: bug 2011: Fix preference dialog issues (patch from chrisdoble)<br />
 * bugfix: Fix some incorrect log lines in RefSpoofer<br />
 * new: Support Firefox 4.0 (many changes)<br />
 * new: Place button in the nav-bar (FF4 killed the status-bar)<br />
 * misc: No longer reimplement the session store, use new APIs instead<br />
 * misc: Simplify crash detection and startup mode settings</p>

