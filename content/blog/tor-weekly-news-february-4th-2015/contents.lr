title: Tor Weekly News — February 4th, 2015
---
pub_date: 2015-02-04
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the fifth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Mozilla’s Tor relays go live</h1>

<p>Late last year, Mozilla <a href="https://lists.torproject.org/pipermail/tor-news/2014-November/000071.html" rel="nofollow">announced</a> its partnership with the Tor Project in the <a href="https://blog.mozilla.org/privacy/2014/11/10/introducing-polaris-privacy-initiative-to-accelerate-user-focused-privacy-online/" rel="nofollow">Polaris Privacy Initiative</a>, a program aimed at “giving users more control, awareness and protection in their Web experiences”. One of the two Tor-related “experiments” Mozilla planned to carry out was the operation of several high-capacity middle relays, and last week Mozilla’s Network Engineering team announced that the <a href="https://globe.torproject.org/#/search/query=mozilla" rel="nofollow">new nodes</a> had been running for several weeks, in a <a href="https://blog.mozilla.org/it/2015/01/28/deploying-tor-relays/" rel="nofollow">blog post</a> that explores the technical decisions and challenges they faced in setting them up.</p>

<p>The team discussed the hardware and infrastructure chosen for the relays, configuration management using Ansible (publishing their <a href="https://github.com/XioNoX/moz-tor-relays/" rel="nofollow">playbook</a> at the same time), and security practices, as well as announcing their intention to continue posting about their experiences and findings during the experiment.</p>

<p>Thanks to Mozilla for this contribution to the Tor network!</p>

<h1>Monthly status reports for January 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of January has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000748.html" rel="nofollow">Juha Nurmi</a> released his report first, followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-January/000749.html" rel="nofollow">Philipp Winter</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-February/000750.html" rel="nofollow">Damian Johnson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-February/000751.html" rel="nofollow">Sherief Alaa</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2015-February/000752.html" rel="nofollow">Georg Koppen</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-February/000753.html" rel="nofollow">David Goulet</a>.</p>

<h1>Miscellaneous news</h1>

<p>meejah <a href="https://lists.torproject.org/pipermail/tor-dev/2015-February/008227.html" rel="nofollow">announced</a> version 0.12.0 of txtorcon, the Tor controller client in Twisted; please see the announcement for a full list of improvements.</p>

<p>Nick Mathewson submitted a draft of <a href="https://lists.torproject.org/pipermail/tor-dev/2015-February/008223.html" rel="nofollow">proposal 241</a>, which aims to protect users against adversaries who are able to attack their connectivity to the Tor network and force them to use malicious guard nodes. Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-dev/2015-February/008226.html" rel="nofollow">offered</a> some further thoughts.</p>

<p>Nick also set out the schedule for the Tor 0.2.6 <a href="https://lists.torproject.org/pipermail/tor-dev/2015-January/008216.html" rel="nofollow">feature freeze</a>.</p>

<p>After reports from users that Tor Browser’s default obfs4 bridges are no longer usable in China, David Fifield <a href="https://lists.torproject.org/pipermail/tor-dev/2015-February/008222.html" rel="nofollow">estimated</a> the time it takes for the “Great Firewall” to react to new circumvention systems as lying “between 2 and 10 weeks”, and asked for additional data to narrow the range further.</p>

<p>Isis Lovecruft “would be super stoked if we could make it as easy and seamless as possible for the bridge operators who are still running obfs2 (!!) to move to supporting better, newer pluggable transports”, such as obfs4, and <a href="https://lists.torproject.org/pipermail/tor-relays/2015-February/006346.html" rel="nofollow">wondered</a> how to make it possible for operators running Debian stable to get the necessary dependencies onto their system without extensive backporting: “If someone has done this, or has another simple solution, would you mind writing up some short how-to on the steps you took, please?”</p>

<p>The Tails team continued to <a href="https://mailman.boum.org/pipermail/tails-dev/2015-February/008003.html" rel="nofollow">discuss</a> the advantages and disadvantages of removing AdBlock Plus from Tails’ version of Tor Browser.</p>

<p>Sadia Afroz compiled a timeline of Tor blocking events, and <a href="https://lists.torproject.org/pipermail/ooni-dev/2015-February/000241.html" rel="nofollow">shared</a> it with the ooni-dev mailing list, along with a request for missing data points; Collin Anderson <a href="https://lists.torproject.org/pipermail/ooni-dev/2015-February/000242.html" rel="nofollow">responded</a> with some additional information.</p>

<p>Patrick Schleizer <a href="https://lists.torproject.org/pipermail/tor-talk/2015-February/036675.html" rel="nofollow">announced</a> that the Whonix team is looking for “a sponsor who is willing to donate a suitable sized virtual or root server”, and gave a list of planned requirements. If you can meet this need, please see Patrick’s message for next steps.</p>

<p>Konstantin Müller <a href="https://lists.torproject.org/pipermail/tor-talk/2015-February/036709.html" rel="nofollow">shared</a> a report written as part of a Master’s degree, offering an introduction to “the past, present, and future” of Tor hidden services.</p>

<h1>News from Tor StackExchange</h1>

<p>Windy wanted to know <a href="https://tor.stackexchange.com/q/6087/88" rel="nofollow">how often a client fetches consensus data from a directory server</a>. Jens Kubieziel provided some information by walking through the source code, and concluded: “every minute it is checked if the consensus document is too old. If it is older than the current time a new one will be fetched.”</p>

<p>This issue of Tor Weekly News has been assembled by qbi and Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the project page <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow"></a>, write down your name and subscribe to the team mailing list <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow"></a> if you want to get involved!</p>

