title: June 2009 Progress Report
---
pub_date: 2009-07-12
---
author: phobos
---
tags:

progress report
anonymity advocacy
releases
censorship circumvention
bug fixes
---
categories:

advocacy
circumvention
releases
reports
---
_html_body:

<p><strong>New releases</strong></p>

<p>On June 20th we released Tor 0.2.1.16-rc.<br />
On June 21st, we released Tor Browser Bundle 1.2.1.<br />
On June 23rd, we released Tor Browser Bundle 1.2.2.<br />
On June 24th, we released Tor 0.2.0.35-stable.  We expect that this release is the last of the 0.2.0.x -stable series, soon to be replaced with the 0.2.1.x series.<br />
On June 30th, we released Vidalia 0.1.14.  </p>

<p><strong>Censorship circumvention</strong></p>

<p>Packaged rpms for Red Flag Linux version 6.  Red Flag Linux is reported to be the new operating system for all Internet cafe's in China.  So far, no one has seen this conversion actually happen, but now we're ready if it does.</p>

<p>Our email autoresponder, gettor , received a number of patches to deal with dkim issues, including finding a dkim bug that prevented yahoo email users from fetching Tor. This bug has been fixed. Additionally, we've whitelisted some domains where we<br />
see we're having lots of use but dkim isn't always configured properly.  We've had thousands of users from China using gettor.</p>

<p><strong>Outreach/Advocacy</strong></p>

<p>Andrew, Roger, and Wendy attended Computers, Freedom, and Privacy 2009 Conference (<a href="http://www.cfp2009.org" rel="nofollow">http://www.cfp2009.org</a>).  Andrew presented a “quicktake” talk on “Who uses Tor?”.  Andrew and Roger, along with Paul Syverson, and a North African blogger,  hosted a panel on “It Takes A Village To Be Anonymous”.  Due to the sensitive situation surrounding the blogger, this panel was not recorded.</p>

<p>Andrew talked with the Committee to Protect Journalists (<a href="http://cpj.org" rel="nofollow">http://cpj.org</a>) about online security and circumvention tools.</p>

<p>Jillian C. York blogged at KnightPulse about “Average citizens browse anonymously<br />
”, <a href="http://www.knightpulse.org/blog/09/06/04/average-citizens-browse-anonymously" rel="nofollow">http://www.knightpulse.org/blog/09/06/04/average-citizens-browse-anonym…</a></p>

<p>Due to Iranian's usage of Tor during the recent election, the general press/media conducted a few interviews with Andrew:</p>

<ol>
<li>Computer World, <a href="http://www.computerworld.com/action/article.do?command=viewArticleBasic&amp;articleId=9134471&amp;intsrc=news_ts_head" rel="nofollow">http://www.computerworld.com/action/article.do?command=viewArticleBasic…</a></li>
<li>Cnet News, <a href="http://news.cnet.com/8301-13578_3-10267287-38.html" rel="nofollow">http://news.cnet.com/8301-13578_3-10267287-38.html</a></li>
<li>Deutche Welle, <a href="http://www.dw-world.de/dw/article/0,,4400882,00.html" rel="nofollow">http://www.dw-world.de/dw/article/0,,4400882,00.html</a></li>
<li>Technology Review, <a href="http://www.technologyreview.com/web/22893/" rel="nofollow">http://www.technologyreview.com/web/22893/</a></li>
<li>Origo, in Hungary, <a href="http://www.origo.hu/techbazis/internet/20090618-a-kiberforradalmarok-fegyverei-eszkozok-anonim-netezeshez.html" rel="nofollow">http://www.origo.hu/techbazis/internet/20090618-a-kiberforradalmarok-fe…</a></li>
<li>O'Reilly, <a href="http://radar.oreilly.com/2009/06/tor-and-the-legality-of-runnin.html" rel="nofollow">http://radar.oreilly.com/2009/06/tor-and-the-legality-of-runnin.html</a></li>
<li>Washington Times, <a href="http://www.washingtontimes.com/news/2009/jun/26/protesters-use-navy-technology-to-avoid-censorship/?feat=home_headlines" rel="nofollow">http://www.washingtontimes.com/news/2009/jun/26/protesters-use-navy-tec…</a></li>
<li>Arte TV video interview, the 30-minute video interview can't be put online, but will be shown to their viewers in late June/early July 2009.  <a href="http://www.arte.tv" rel="nofollow">http://www.arte.tv</a></li>
<li>EFF, <a href="http://www.eff.org/deeplinks/2009/06/help-protesters-iran-run-tor-relays-bridges" rel="nofollow">http://www.eff.org/deeplinks/2009/06/help-protesters-iran-run-tor-relay…</a></li>
<li>A Houston Radio station did an on-air interview, but didn't put the interview online.</li>
<li>A Romanian newspaper did an interview, but didn't put the story online.</li>
<li>Public Rado International did a more in-depth interview.  They expect it to be on PBS Radio and BBC Radio 4 in early July 2009.</li>
</ol>

<p>A number of blogs and other media picked up these original interviews and spread the word even further:</p>

<ol>
<li>Wall Street Journal, <a href="http://blogs.wsj.com/digits/2009/06/18/iranians-using-tor-to-anonymize-web-use/" rel="nofollow">http://blogs.wsj.com/digits/2009/06/18/iranians-using-tor-to-anonymize-…</a></li>
<li>CBS News, <a href="http://www.cbsnews.com/blogs/2009/06/17/politics/politicalhotsheet/entry5094825.shtml" rel="nofollow">http://www.cbsnews.com/blogs/2009/06/17/politics/politicalhotsheet/entr…</a></li>
<li><a href="http://curtisschweitzer.net/blog/?p=2995" rel="nofollow">http://curtisschweitzer.net/blog/?p=2995</a></li>
<li><a href="http://voices.allthingsd.com/20090618/iranians-using-tor-to-anonymize-web-use/" rel="nofollow">http://voices.allthingsd.com/20090618/iranians-using-tor-to-anonymize-w…</a></li>
<li><a href="http://www.dailyfinance.com/2009/06/24/nokia-and-siemens-in-iran-controversy/" rel="nofollow">http://www.dailyfinance.com/2009/06/24/nokia-and-siemens-in-iran-contro…</a></li>
<li><a href="http://www.muslimnews.co.uk/news/news.php?article=16360" rel="nofollow">http://www.muslimnews.co.uk/news/news.php?article=16360</a></li>
</ol>

<p><strong>Preconfigured privacy (circumvention) bundles for USB or LiveCD.</strong></p>

<p>Tor Browser Bundle 1.2.1 and 1.2.2 released in June.  Planning a migration of the base operating system for the Incognito LiveCD to switch from Gentoo to an Ubuntu variant.  We can always use help in maintaining Incognito.</p>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong></p>

<p>June was spent documenting, stabilizing, and streamlining the bandwidth authority scanner, which has been runningfor a while on the Directory Authority named ides.</p>

<p>It is good enough to start running on multiple authorities now to produce actual results for clients to use.</p>

<p><strong>More reliable (e.g. split) download mechanism.</strong></p>

<p>Our email autoresponder, gettor , received a number of patches to deal with dkim issues, including finding a dkim bug that prevented yahoo email users from fetching Tor. This bug<br />
has been fixed. Additionally, we've whitelisted some domains where we see we're having lots of use but dkim isn't always configured properly.  We've had thousands of users from China using gettor.</p>

<p>The Tor Check website (check.torproject.org) had a few bugs and we've fixed all but two. We sometimes still have false negatives (because the Tor client doesn't know to fetch the consensus at any specific time) and we also still sometimes barf python exceptions because mod_python has some weird exception from time to time. We also accepted a patch from Marcus Greip that extends the TorBulkExitList to allow arbitrary ports rather than just port 80.</p>

<p><strong>Footprints from Tor Browser Bundle.</strong></p>

<p>Reduced the scanning for plugins Portable Firefox can do on launch of the application.  There is still an issue where Firefox displays other plugins to users, but they aren't actually valid plugins and won't run on command.  Firefox acquires the names through queries to the Windows Registry.</p>

<p><strong>Translations</strong></p>

<p>16 Polish website updates<br />
8 Italian website updates<br />
3 German website updates</p>

---
_comments:

<a id="comment-1796"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1796" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>JustMe (not verified)</span> said:</p>
      <p class="date-time">July 12, 2009</p>
    </div>
    <a href="#comment-1796">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1796" class="permalink" rel="bookmark">Less Progress, }-(.
Having</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Less Progress, }-(.</p>
<p>Having trouble getting tor to run.<br />
Would be nice for a little more laymans if possible instructions.<br />
Could the problem be with my current Anti Virus programs and how to deal with that?</p>
<p>Norton and Spyware Doctor.</p>
<p>Herb</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1797"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1797" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>JUstMe (not verified)</span> said:</p>
      <p class="date-time">July 12, 2009</p>
    </div>
    <a href="#comment-1797">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1797" class="permalink" rel="bookmark">When I say layman I mean</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When I say layman I mean somewhere there was a note about, Opening a port.</p>
<p>Come on guys and gals some of us would like to set up a relay know a little about the tech stuff but not a lot? Thus the how to do for the layman?</p>
<p>Herb</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1800"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1800" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">July 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1797" class="permalink" rel="bookmark">When I say layman I mean</a> by <span>JUstMe (not verified)</span></p>
    <a href="#comment-1800">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1800" class="permalink" rel="bookmark">When I say layman I mean</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Use Vidalia, point and click and use the "Automatic Port Configuration" option, it will try to do everything for you.  See <a href="https://www.torproject.org/docs/tor-doc-relay.html.en#setup" rel="nofollow">https://www.torproject.org/docs/tor-doc-relay.html.en#setup</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1899"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1899" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>rmac (not verified)</span> said:</p>
      <p class="date-time">July 21, 2009</p>
    </div>
    <a href="#comment-1899">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1899" class="permalink" rel="bookmark">Tor, Tork, Incognito</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>First I would like to thank everyone who made these programs available to someone like me who doesn't have the necessary skills to do this on my own. I first came across the Tor Project when I was looking into the stories of North Korea and the recent DNS attacks. I wanted to know more about how true this was. From what I have read a high school student can do this on his home PC and it could look like it was an attack by a hostile government. Anyway, I was also concerned about the new NSA mega billion dollar listening operation the US is building. Not that I have any state secrets to hide, mostly I just want to keep my bank account and credit card info secure when I go online. So now I have Tor, Incognito and Back Track 3. I used Back Track 3 to monitor myself to see how much info I was leaking to who knows? While I am quite satisfied with using Incognito on my laptop for anything that has to be absolutely secure I did have some questions about being "attacked". I wanted to know if there was a way for me to monitor surveillance of my PC. Is there something that would give an alert that an attempt was being made to scan my transmissions or PC? How would I know if someone was using a program like Back Track to capture my information? For example If I was in China, Iran, or Honduras and I was sending info that was being censored how would I know if the military or police was going to be knocking down my door? Hope this makes sense. Any references where I could do some research on this would be appreciated.</p>
<p>Again thanks for TOR and Incognito</p>
<p>rmac</p>
</div>
  </div>
</article>
<!-- Comment END -->
