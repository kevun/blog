title: Tor Browser 5.5a5 is released
---
pub_date: 2015-12-18
---
author: gk
---
tags:

tor browser
tbb
tbb-5.5
---
categories:

applications
releases
---
_html_body:

<p>A new alpha Tor Browser release is available for download in the <a href="https://dist.torproject.org/torbrowser/5.5a5/" rel="nofollow">5.5a5 distribution directory</a> and on the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha" rel="nofollow">alpha download page</a>.</p>

<p>This release features important <a href="https://www.mozilla.org/en-US/security/known-vulnerabilities/firefox-esr/#firefoxesr38.5" rel="nofollow">security updates</a> to Firefox.</p>

<p>Additionally, we included updated versions for Tor (0.2.7.6), OpenSSL (1.0.1q) and NoScript (2.7). Moreover, we fixed an annoying bug in our circuit display (circuits weren't visible sometimes), isolated SharedWorkers to the first-party domain and improved our font fingerprinting defense.</p>

<p>On the usability side we improved the about:tor experience and started to use the bundled changelog to display new features and bug fixes after an update (instead of loading the blog post into a new tab). We'd love to hear feedback about both.</p>

<p>Tor Browser 5.5a5 comes with a banner supporting our donations campaign. The banner is visible on the about:tor page and features either Roger Dingledine, Laura Poitras or Cory Doctorow which is chosen randomly.</p>

<p>Here is the complete changelog since 5.5a4:</p>

<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 38.5.0esr
   </li>
<li>Update Tor to 0.2.7.6
   </li>
<li> Update OpenSSL to 1.0.1q
   </li>
<li>Update NoScript to 2.7
   </li>
<li>Update Torbutton to 1.9.4.2
<ul>
<li>Bug 16940: After update, load local change notes
       </li>
<li>Bug 16990: Avoid matching '250 ' to the end of node name
       </li>
<li>Bug 17565: Tor fundraising campaign donation banner
       </li>
<li>Bug 17770: Fix alignments on donation banner
       </li>
<li>Bug 17792: Include donation banner in some non en-US Tor Browsers
       </li>
<li>Bug 17108: Polish about:tor appearance
       </li>
<li>Bug 17568: Clean up tor-control-port.js
       </li>
<li>Translation updates
     </li>
</ul>
</li>
<li>Bug 9659: Avoid loop due to optimistic data SOCKS code (fix of #3875)
   </li>
<li>Bug 15564: Isolate SharedWorkers by first-party domain
   </li>
<li>Bug 16940: After update, load local change notes
   </li>
<li>Bug 17759: Apply whitelist to local fonts in @font-face (fix of #13313)
   </li>
<li>Bug 17747: Add ndnop3 as new default obfs4 bridge
   </li>
<li>Bug 17009: Shift and Alt keys leak physical keyboard layout (fix of #15646)
   </li>
<li>Bug 17369: Disable RC4 fallback
   </li>
<li>Bug 17442: Remove custom updater certificate pinning
   </li>
<li>Bug 16863: Avoid confusing error when loop.enabled is false
   </li>
<li>Bug 17502: Add a preference for hiding "Open with" on download dialog
   </li>
<li>Bug 17446: Prevent canvas extraction by third parties (fixup of #6253)
   </li>
<li>Bug 16441: Suppress "Reset Tor Browser" prompt
 </li>
</ul>
</li>
<li>Windows
<ul>
<li>Bug 13819: Ship expert bundles with console enabled
     </li>
<li>Bug 17250: Fix broken Japanese fonts
    </li>
</ul>
</li>
<li>OS X
<ul>
<li>Bug 17661: Whitelist font .Helvetica Neue DeskInterface
   </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-142414"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142414" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2015</p>
    </div>
    <a href="#comment-142414">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142414" class="permalink" rel="bookmark">Fonts have long been one of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fonts have long been one of my worst measurements on panopticlick. I always wondered why Tor Browser doesn't just bundle the most common ones and ignore all of the system installed fonts.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-142441"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142441" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2015</p>
    </div>
    <a href="#comment-142441">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142441" class="permalink" rel="bookmark">Hooray, you fixed bug</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hooray, you fixed bug #13819! This basically prevented me from using standalone Tor on Windows (for usability reasons). Now I can get Tor up and running the way I like again, thanks so much for the awesome new release!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-142619"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142619" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 18, 2015</p>
    </div>
    <a href="#comment-142619">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142619" class="permalink" rel="bookmark">i&#039;ll share [Later]</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i'll share [Later] a-long-ago bug.. but i think it's on Mozilla's Firefox core not Tor's :)</p>
<p>when you agree about it with me.. then u'd be able to contact them to fix it, cuz it might be faster to get response from'em than me :)</p>
<p>Contrary.. that bug not showing in ie.. and it's the only good thing found by M$'s-IE<br />
:))</p>
<p>Later..</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-143078"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143078" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-142619" class="permalink" rel="bookmark">i&#039;ll share [Later]</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-143078">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143078" class="permalink" rel="bookmark">Hi All,
That&#039;s me who</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi All,<br />
That's me who -above- wrote about that "funny" bug,<br />
&amp; will continue as promised :)</p>
<p>it's simple!</p>
<p>you'll click a link.. or (icon) that you can't copy its address..</p>
<p>.. in ie, while page is -trying- to load but otherwise you'll press ESC to stop it before it is fully/partially loaded ..</p>
<p>then y'd be able to see -instantly- the address that ie intend to open! </p>
<p>it can be copied to use for any other reason..</p>
<p>say, you want to open it with Tor Browser :) .. for example..</p>
<p>NOW you got the whole picture..<br />
Right?</p>
<p>So, WHY firefox not doing the same..</p>
<p>while page is -trying- to load but otherwise you'll press ESC to stop it before it is fully/partially loaded ..<br />
&gt;&gt;&gt; <strong>NO MORE ADDRESS..</strong> </p>
<p>nothing, just <strong>about:blank</strong>!</p>
<p>FF passed above ver40's and they seem to never notice/care about that 'shameful' bug!</p>
<p>it's OUR right to now where a link is taking us to while a new tab or page is trying to load!</p>
<p>isn't it?</p>
<p>Please do something in your NEXT release of Tor browser to solve this..</p>
<p>OR contact Mozilla if it's a core-thingy..</p>
<p>also, thank you for posting above notice 2 days ago.. that -internally- has nothing other than just a promise..<br />
and i did it..</p>
<p>bye for now..</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-142695"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-142695" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 19, 2015</p>
    </div>
    <a href="#comment-142695">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-142695" class="permalink" rel="bookmark">this may not matter but I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>this may not matter but I have a question about google I lost my password and tried to recover my google email acct, now they want to know my browser I use (which is tor) and when and where I sign onto and my internet provider...all I did was forget my password and now I feel like I am living in a third world country, really no privacy google?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-143039"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143039" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2015</p>
    </div>
    <a href="#comment-143039">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143039" class="permalink" rel="bookmark">https://mozorg.cdn.mozilla.ne</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://mozorg.cdn.mozilla.net/media/img/firefox/organizations/release-overview-high-res.7f1fea43e9e2.png" rel="nofollow">https://mozorg.cdn.mozilla.net/media/img/firefox/organizations/release-…</a></p>
<p>Will Tor Browser be updated to ESR 45.0?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-143066"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-143066" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 20, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-143039" class="permalink" rel="bookmark">https://mozorg.cdn.mozilla.ne</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-143066">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-143066" class="permalink" rel="bookmark">Yes eventually, but probably</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes eventually, but probably not until around the time 38 goes end-of-life.</p>
<p>The Tor Browser developers balance their time between fixing longer-term privacy issues in the browser, and fixing shorter-term bugs and mis-features that Mozilla put in to try to keep up with Chrome, etc in the browser arms races.</p>
<p>I imagine sometime in January or February, one of them will start working on the master list of all privacy and security disasters that are new in ESR 45 (or maybe they already made this list? I haven't been keeping track), and then they'll slowly work their way through it, finishing about the time that they're forced to upgrade since ESR 38 is getting dropped.</p>
<p>More resources (read: money and developers) would let them do it in a less frantic time table. Another thing that would help a lot is for Mozilla to believe that privacy is important -- important enough for them to merge the Tor Browser fixes into Firefox itself. This part is happening, but slowly.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-145702"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-145702" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 30, 2015</p>
    </div>
    <a href="#comment-145702">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-145702" class="permalink" rel="bookmark">Hello tbb,
Ive noticed that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello tbb,</p>
<p>Ive noticed that the tor circuit bug still do exist. After prolong use with multiple tabs, the circuit map still does disappear. </p>
<p>Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
