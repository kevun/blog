title: New Release: Tails 4.20
---
pub_date: 2021-07-13
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
summary: Tails 4.20 completely changes how to connect to the Tor network from Tails.
---
_html_body:

<h2 id="tor-connection"><i>Tor Connection</i> assistant</h2>
<p>Tails 4.20 completely changes how to connect to the Tor network from Tails.</p>
<p>After connecting to a local network, a <em>Tor Connection</em> assistant helps you connect to the Tor network.</p>
<p>This new assistant is most useful for users who are at high risk of physical surveillance, under heavy network censorship, or on a poor Internet connection:</p>
<ul>
<li>
<p>It protects better the users who need to go unnoticed if using Tor could look suspicious to someone who monitors their Internet connection (parental control, abusive partner, school or work network, etc.).</p>
</li>
<li>
<p>It allows people who need to connect to Tor using bridges to configure them without having to change the default configuration in the Welcome Screen.</p>
</li>
<li>
<p>It helps first-time users understand how to connect to a local Wi-Fi network.</p>
</li>
<li>
<p>It provides feedback while connecting to Tor and helps troubleshoot network problems.</p>
</li>
</ul>
<p>We know that this assistant is still far from being perfect, even if we have been working on this assistant since February. If anything is unclear, confusing, or not working as you would expect, please send your feedback to <a href="mailto:tails-dev@boum.org">tails-dev@boum.org</a> (public mailing list).</p>
<p>This first release of the <em>Tor Connection</em> assistant is only a first step. We will add more improvements to it in the coming months to:</p>
<ul>
<li>
<p>Save Tor bridges to the Persistent Storage (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/5461">#5461</a>)</p>
</li>
<li>
<p>Help detect when Wi-Fi is not working (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/14534">#14534</a>)</p>
</li>
<li>
<p>Detect if you have to sign in to the local network using a captive portal (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/5785">#5785</a>)</p>
</li>
<li>
<p>Synchronize the clock to make it easier to use Tor bridges in Asia (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/15548">#15548</a>)</p>
</li>
<li>
<p>Make it easier to learn about new Tor bridges (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18219">#18219</a>, <a href="https://gitlab.tails.boum.org/tails/tails/-/issues/15331">#15331</a>)</p>
</li>
</ul>
<h2 id="changes">Changes and updates</h2>
<ul>
<li>
<p>Update <em>OnionShare</em> from 1.3.2 to <a href="https://github.com/micahflee/onionshare/blob/develop/CHANGELOG.md#22">2.2</a>.</p>
<p>This major update adds a feature to <a href="https://docs.onionshare.org/2.3.2/en/features/#host-a-website">host a website</a> accessible from a Tor onion service.</p>
</li>
<li>
<p>Update <em>KeePassXC</em> from 2.5.4 to <a href="https://github.com/keepassxreboot/keepassxc/blob/master/CHANGELOG.md#262-2020-10-21">2.6.2</a>.</p>
<p>This major update comes with a redesign of the interface.</p>
</li>
<li>
<p>Update <em>Tor Browser</em> to <a href="https://blog.torproject.org/new-release-tor-browser-1052">10.5.2</a>.</p>
</li>
<li>
<p>Update <em>Thunderbird</em> to <a href="https://www.thunderbird.net/en-US/thunderbird/78.11.0/releasenotes/">78.11.0</a>.</p>
</li>
<li>
<p>Update <em>Tor</em> to 0.4.5.9.</p>
</li>
<li>
<p>Update the <em>Linux</em> kernel to 5.10.46. This should improve the support for newer hardware (graphics, Wi-Fi, and so on).</p>
</li>
<li>
<p>Rename <em>MAC address spoofing</em> as <em>MAC address anonymization</em> in the Welcome Screen.</p>
</li>
</ul>
<h2 id="fixes">Fixed problems</h2>
<h3>Automatic upgrades</h3>
<ul>
<li>
<p>Made the download of upgrades and the handling of errors more robust. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18162">#18162</a>)</p>
</li>
<li>
<p>Display an error message when failing to check for available upgrades. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18238">#18238</a>)</p>
</li>
</ul>
<h3>Tails Installer</h3>
<ul>
<li>
<p>Made the display of the <strong>Reinstall</strong> button more robust. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18300">#18300</a>)</p>
</li>
<li>
<p>Make the <strong>Install</strong> and <strong>Upgrade</strong> unavailable after a USB stick is removed. (<a href="https://gitlab.tails.boum.org/tails/tails/-/issues/18346">#18346</a>)</p>
</li>
</ul>
<p>For more details, read our <a href="https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog">changelog</a>.</p>
<h2 id="issues">Known issues</h2>
<p><a id="broken-upgrades"></a></p>
<ul>
<li>
<p>Automatic upgrades are broken from Tails 4.14 and earlier.</p>
<p>To upgrade from Tails 4.14 or earlier, you can either:</p>
<ul>
<li>
<p>Do a <a href="https://tails.boum.org/doc/upgrade/#manual">manual upgrade</a>.</p>
</li>
<li>
<p>Fix the automatic upgrade from a terminal. To do so:</p>
<ol>
<li>
<p>Start Tails and set up an <a href="https://tails.boum.org/doc/first_steps/welcome_screen/administration_password/">administration password</a>.</p>
</li>
<li>
<p>In a terminal, execute the following command:</p>
<pre>
<span class="geshifilter"><code class="php geshifilter-php">torsocks curl <span style="color: #339933;">--</span>silent https<span style="color: #339933;">:</span><span style="color: #666666; font-style: italic;">//tails.boum.org/isrg-root-x1-cross-signed.pem | sudo tee --append /usr/local/etc/ssl/certs/tails.boum.org-CA.pem &amp;amp;&amp;amp; systemctl --user restart tails-upgrade-frontend </span></code></span></pre><p>This command is a single command that wraps across several lines. Copy and paste the entire block at once and make sure that it executes as a single command.</p>
</li>
<li>
<p>Approximately 30 seconds later, you should be prompted to upgrade to the latest version of Tails. If no prompt appears, you might already be running the latest version of Tails.</p>
</li>
</ol>
</li>
</ul>
</li>
</ul>
<p>See the list of <a href="https://tails.boum.org/support/known_issues/">long-standing issues</a>.</p>
<h2 id="get">Get Tails 4.20</h2>
<h3>To upgrade your Tails USB stick and keep your persistent storage</h3>
<ul>
<li>
<p>Automatic upgrades are broken from Tails 4.14 and earlier. See the <a href="#broken-upgrades">known issue above</a>.</p>
</li>
<li>
<p>Automatic upgrades are available from Tails 4.14 or later to 4.20.</p>
<p>You can <a href="https://tails.boum.org/doc/upgrade/#reduce">reduce the size of the download</a> of future automatic upgrades by doing a manual upgrade to the latest version.</p>
</li>
<li>
<p>If you cannot do an automatic upgrade or if Tails fails to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/doc/upgrade/#manual">manual upgrade</a>.</p>
</li>
</ul>
<h3>To install Tails on a new USB stick</h3>
<p>Follow our installation instructions:</p>
<ul>
<li><a href="https://tails.boum.org/install/win/">Install from Windows</a></li>
<li><a href="https://tails.boum.org/install/mac/">Install from macOS</a></li>
<li><a href="https://tails.boum.org/install/linux/">Install from Linux</a></li>
</ul>
<p>The Persistent Storage on the USB stick will be lost if you install instead of upgrading.</p>
<h3>To download only</h3>
<p>If you don't need installation or upgrade instructions, you can download Tails 4.20 directly:</p>
<ul>
<li><a href="https://tails.boum.org/install/download/">For USB sticks (USB image)</a></li>
<li><a href="https://tails.boum.org/install/download-iso/">For DVDs and virtual machines (ISO image)</a></li>
</ul>
<h2 id="next">What's coming up?</h2>
<p>Tails 4.21 is <a href="https://tails.boum.org/contribute/calendar/">scheduled</a> for August 10.</p>
<p>Have a look at our <a href="https://tails.boum.org/contribute/roadmap">roadmap</a> to see where we are heading to.</p>
<h2>Support and feedback</h2>
<p>For support and feedback, visit the <a href="https://tails.boum.org/support/">Support section</a> on the Tails website.</p>

