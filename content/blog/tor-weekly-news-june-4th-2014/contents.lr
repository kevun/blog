title: Tor Weekly News — June 4th, 2014
---
pub_date: 2014-06-04
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-second issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Tails moves to Wheezy</h1>

<p>The <a href="https://tails.boum.org/" rel="nofollow">Tails live system</a> is a <a href="https://wiki.debian.org/Derivatives" rel="nofollow">Debian derivative</a> aiming at preserving the privacy and anonymity of its users.</p>

<p>The first Tails releases were based on Debian <a href="https://www.debian.org/releases/lenny/" rel="nofollow">Lenny</a> (2009-2012); since version 0.7, Tails has been based on Debian <a href="https://www.debian.org/releases/squeeze/" rel="nofollow">Squeeze</a> (2011-). Meanwhile, Debian has released a new stable version dubbed <a href="https://www.debian.org/releases/wheezy/" rel="nofollow">Wheezy</a>, and the upcoming Tails 1.1 will be the first release to be based on the latter.</p>

<p>The general set of features should not change much from the previous Tails release, but almost every software component has been updated. On<br />
May 30th, the Tails team <a href="https://tails.boum.org/news/test_1.1-beta1/" rel="nofollow">released a beta image</a>; given the number of changes, testing is even more welcome than usual.</p>

<p>Testers can also try out the new UEFI support, which enables Tails to boot on recent hardware and on Macs.</p>

<p><a href="https://tails.boum.org/news/test_1.1-beta1/#index3h1" rel="nofollow">Several issues</a> with the current beta image have already been identified, so be sure to have a look at the list before <a href="https://tails.boum.org/doc/first_steps/bug_reporting/" rel="nofollow">reporting</a>.</p>

<p>The details of the release schedule are <a href="https://mailman.boum.org/pipermail/tails-dev/2014-May/005917.html" rel="nofollow">still being discussed</a> at the time of writing, but Tails 1.1 is likely to be out by the end of July. Please help make it a great release!</p>

<h1>Stem 1.2 brings interactive interaction with the Tor daemon</h1>

<p>On June 1st, Damian Johnson <a href="https://blog.torproject.org/blog/stem-release-12" rel="nofollow">announced</a> the release of <a href="https://stem.torproject.org/" rel="nofollow">Stem</a> 1.2. Stem is a Python library for interacting with the Tor daemon. It is now used by <a href="https://stem.torproject.org/tutorials/double_double_toil_and_trouble.html" rel="nofollow">several applications</a> like the <a href="https://www.atagar.com/arm/" rel="nofollow">arm</a> status monitor and Philipp Winter’s <a href="http://www.cs.kau.se/philwint/spoiled_onions/" rel="nofollow">exit scanner</a>.</p>

<p>The new version brings an interactive control interpreter, “a new method for interacting with Tor’s control interface that combines an interactive python interpreter with raw access similar to telnet”. This should make Tor hackers happy by saving them from having to manually poke the control port through telnet or create complete Stem scripts.</p>

<p>For the complete list of changes, head over to the <a href="https://stem.torproject.org/change_log.html#version-1-2" rel="nofollow">changelog</a>.</p>

<h1>Monthly status reports for May 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of May has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2014-May/000539.html" rel="nofollow">Pearl Crescent</a> released their report first, followed by <a href="https://lists.torproject.org/pipermail/tor-reports/2014-May/000540.html" rel="nofollow">Sherief Alaa</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000542.html" rel="nofollow">Damian Johnson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000543.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000544.html" rel="nofollow">Colin C.</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000545.html" rel="nofollow">Georg Koppen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000546.html" rel="nofollow">Lunar</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000548.html" rel="nofollow">Arlo Breault</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000550.html" rel="nofollow">Matt Pagan</a>.</p>

<p>Lunar also reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000541.html" rel="nofollow">help desk</a>, while Arturo Filastò did likewise for the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000547.html" rel="nofollow">OONI team</a>, and Mike Perry for the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-June/000549.html" rel="nofollow">Tor Browser team</a>.</p>

<h1>Miscellaneous news</h1>

<p>Pups, a chat system implemented by Sherief Alaa for real-time invitation-based user support, has <a href="https://bugs.torproject.org/11657" rel="nofollow">gone live</a>, and can now be used by Tor’s support assistants when that method promises a quicker resolution of an issue.</p>

<p>In response to a question about the writing of unit tests for tor, Nick Mathewson <a href="https://lists.torproject.org/pipermail/tor-dev/2014-June/006933.html" rel="nofollow">shared</a> a brief guide to identifying lines in tor’s codebase that have not yet been covered by tests.</p>

<p>Nick also put out a call (<a href="https://lists.torproject.org/pipermail/tor-relays/2014-May/004617.html" rel="nofollow">relayed</a> by Moritz Bartl) for Tor relay operators running version 0.2.5.4-alpha or later to profile their relays, in order to identify potential bottlenecks. Basic instructions for doing so on Debian and Ubuntu can be found in the comments to the <a href="https://bugs.torproject.org/11332" rel="nofollow">relevant ticket</a>.</p>

<p>During a discussion on the role of JavaScript hooks in Tor Browser, Mike Perry <a href="https://lists.torproject.org/pipermail/tbb-dev/2014-June/000074.html" rel="nofollow">clarified</a> the merits of writing direct C++ Firefox patches over using such hooks, as well as the possibility of incorporating Torbutton’s privacy features into either Firefox itself or a dedicated add-on.</p>

<p>Andrew Lewman <a href="https://lists.torproject.org/pipermail/tor-reports/2014-May/000538.html" rel="nofollow">reported</a> on his trip to Stockholm to address Sida and the Stockholm Internet Forum.</p>

<p>Juha Nurmi sent the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-May/000537.html" rel="nofollow">second weekly report</a> for the ahmia.fi Google Summer of Code project .</p>

<p>Marc Juarez is working on website fingerprinting countermeasures in the form of a pluggable transport. Marc wants to “implement a set of primitives that any link padding-based defense would benefit of” and is <a href="https://lists.torproject.org/pipermail/tor-dev/2014-May/006918.html" rel="nofollow">looking for feedback</a> on the envisaged primitives.</p>

<p>Philipp Winter <a href="https://lists.torproject.org/pipermail/tor-relays/2014-May/004620.html" rel="nofollow">announced</a> that <a href="https://atlas.torproject.org" rel="nofollow">Atlas</a>, the web application to learn about currently running Tor relays, will now display information about a relay’s IPv6 exit policy, as well as the already-existing IPv4 exit summary.</p>

<h1>Tor help desk roundup</h1>

<p>Sometimes users with no network obstacles will email the help desk to ask how to configure their Tor Browser. Often these users will not need to configure anything, and clicking “Connect” is all that is necessary. Discussion on this problem is taking place on the <a href="https://bugs.torproject.org/12164" rel="nofollow">bug tracker</a>.</p>

<h1>Easy development tasks to get involved with</h1>

<p>The <a href="https://bridges.torproject.org/" rel="nofollow">bridge distributor BridgeDB</a> populates its database from the cached descriptor files copied over from the bridge authority. There’s a <a href="https://bugs.torproject.org/11216" rel="nofollow">small bug</a> in BridgeDB where a line that is included in two different cached descriptor files gets added twice to the database. The ticket says this bug is easily reproducible and even contains commands for reproducing it. If you enjoy digging into unknown Python/Twisted codebases to find the few spots that need fixing, this bug may be for you. Be sure to comment on the ticket when you have a fix!</p>

<p>This issue of Tor Weekly News has been assembled by harmony, Lunar, Matt Pagan and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

