title: 2021 Fundraising results: thank you!
---
author: alsmith
---
pub_date: 2022-01-31
---
categories: fundraising
---
summary:

THANK YOU to everyone who contributed to this campaign! You helped raise more for privacy online than we dreamed possible, and we’re extremely grateful for your trust and support.
---
body:

During the last months of each year, the Tor Project (a 501(c)(3) nonprofit) holds a fundraiser to ask for your support. You probably recognize these graphics by now from our blog, social media, or about:tor. I’m here today to share some of the results of this effort, and to talk about what you’ve made possible for Tor and privacy online.

Let’s start with the basics—first being THANK YOU to everyone who contributed to this campaign! You helped raise more for privacy online than we dreamed possible, and we’re extremely grateful for your trust and support.

On to the numbers. [Last year during the same fundraising period](https://blog.torproject.org/use-a-mask-use-tor-thank-you/), you contributed $376,315. This year, you contributed $940,361 towards helping people exercise their human right to privacy. Year over year, that’s a 150% increase! This includes a generous match from our Friends of Tor—[Aspiration](https://aspirationtech.org/), [Jon Callas](https://twitter.com/joncallas), [Craig Newmark](https://twitter.com/craignewmark), [Jesse Powell](https://twitter.com/jespow), [Wendy Seltzer](https://twitter.com/wseltzer), and several anonymous supporters.

Last year, you donated $58,296 in cryptocurrency. This year, about 58% of the total amount you contributed came to us in the form of cryptocurrencies, for a total of $548,647—that’s an 841% increase. Wow! The top four currencies were Bitcoin (68% of all cryptocurrency donations), Etherum (28%), DAI (2%), and Monero (1%).

Thank you to the cryptocurrency community for your outstanding support, and a special thank you to Gitcoin Grants for including us in grant rounds that intersected with this campaign. Everyone who made an individual gift on the [Gitcoin platform](https://gitcoin.co/grants/2805/the-tor-project) before Dec. 31 is included in this figure.
  
By making a donation this year and over the last several years, you’ve invested in building Tor’s Anti-Censorship team and anti-censorship features, like [adding Snowflake bridges into Tor Browser stable](https://blog.torproject.org/new-release-tor-browser-105/). You’ve helped establish a [Tor Forum](https://forum.torproject.net) where we connect directly with users experiencing censorship and shutdowns. You've helped us engage translators to [localize guides for routing around censorship into Russian](https://forum.torproject.net/t/tor-blocked-in-russia-how-to-circumvent-censorship/982). You’ve helped us keep the back-end of all of this infrastructure healthy, and you’ve made it possible for Tor to work in an agile way, responding immediately to censorship emergencies.

**I want to share a brief story of what you’ve *already* made possible.**

At the end of 2021, the [Russian government began ramping up censorship against Tor](https://blog.torproject.org/tor-censorship-in-russia/), starting by blocking [www.torproject.org](http://www.torproject.org), and escalating to blocking the Tor network using a variety of methods. Each ISP responded to this directive differently, and we saw censorship roll out across the country in a variety of forms.

We acted quickly. We mobilized our community to spin up hundreds of new bridges. The Anti-Censorship and Applications teams rolled out changes to Tor Browser and our circumvention tools that counteracted Russian blocks. Volunteers set up mirror versions of our websites. We answered hundreds of users via [frontdesk@torproject.org](mailto:frontdesk@torproject.org) to help them circumvent the block. We saw thousands of users access our newly-translated Russian guides for circumventing this censorship—and we’ve seen that these efforts are keeping Russian users connected to Tor.

![Bridge users from Russia](bridgeusers.png)

Source: https://metrics.torproject.org/userstats-bridge-country.html?start=2021-09-14&end=2022-01-13&country=ru

**You made it possible to keep users in Russia online.**

Without your support, we would not have been able to leap into action when users in Russia needed our immediate help. Thank you to everyone who made a donation during 2021. You make privacy online for millions of people possible. We look forward to sharing more progress on our [2022 plans](https://blog.torproject.org/tor-in-2022/).

[Stay tuned by subscribing to our newsletter](https://newsletter.torproject.org)!
