title: New Stable Releases: Tor 0.3.5.16, 0.4.5.10 and 0.4.6.7
---
pub_date: 2021-08-16
---
author: dgoulet
---
tags: stable, release, tor, security
---
_html_body:

<p>Greetings!</p>
<p>We have a new stable release today. If you build Tor from source, you can download the source code for the latest stable release on the <a href="https://www.torproject.org/download/tor/">download page</a>. Packages should be available within the next several weeks, with a new Tor Browser later this week.</p>
<p>The ChangeLog for 0.4.6.7 follows below. For the changelogs for other releases, see the <a href="https://lists.torproject.org/pipermail/tor-announce/2021-August/000228.html">announcement email</a>. These releases backport stability fixes from later Tor releases, and a security issue classified as HIGH per our <a href="https://gitlab.torproject.org/tpo/core/team/-/wikis/NetworkTeam/SecurityPolicy">policy.</a></p>
<p>Tor 0.4.6.7 fixes several bugs from earlier versions of Tor, including one that could lead to a denial-of-service attack. Everyone running an earlier version, whether as a client, a relay, or an onion service, should upgrade to Tor 0.3.5.16, 0.4.5.10, or 0.4.6.7.</p>
<h2>Changes in version 0.4.6.7 - 2021-08-16</h2>
<ul>
<li>Major bugfixes (cryptography, security):
<ul>
<li>Resolve an assertion failure caused by a behavior mismatch between our batch-signature verification code and our single-signature verification code. This assertion failure could be triggered remotely, leading to a denial of service attack. We fix this issue by disabling batch verification. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40078">40078</a>; bugfix on 0.2.6.1-alpha. This issue is also tracked as TROVE-2021-007 and CVE-2021-38385. Found by Henry de Valence.</li>
</ul>
</li>
<li>Minor feature (fallbackdir):
<ul>
<li>Regenerate fallback directories list. Close ticket <a href="https://bugs.torproject.org/tpo/core/tor/40447">40447</a>. </li>
</ul>
</li>
</ul>
<ul>
<li>Minor features (geoip data):
<ul>
<li>Update the geoip files to match the IPFire Location Database, as retrieved on 2021/08/12.</li>
</ul>
</li>
<li>Minor bugfix (crypto):
<ul>
<li>Disable the unused batch verification feature of ed25519-donna. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40078">40078</a>; bugfix on 0.2.6.1-alpha. Found by Henry de Valence.</li>
</ul>
</li>
<li>Minor bugfixes (onion service):
<ul>
<li>Send back the extended SOCKS error 0xF6 (Onion Service Invalid Address) for a v2 onion address. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40421">40421</a>; bugfix on 0.4.6.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Reduce the compression level for data streaming from HIGH to LOW in order to reduce CPU load on the directory relays. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40301">40301</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (timekeeping):
<ul>
<li>Calculate the time of day correctly on systems where the time_t type includes leap seconds. (This is not the case on most operating systems, but on those where it occurs, our tor_timegm function did not correctly invert the system's gmtime function, which could result in assertion failures when calculating voting schedules.) Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40383">40383</a>; bugfix on 0.2.0.3-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292617"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292617" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>NoName (not verified)</span> said:</p>
      <p class="date-time">August 17, 2021</p>
    </div>
    <a href="#comment-292617">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292617" class="permalink" rel="bookmark">I hope this time around the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I hope this time around the deb.torproject.org actually upgrades to 0.4.6.7 for all (the last few releases of 0.4.6.* have not been updated other than as experimental despite them being stable!).</p>
<p>As was noted on gitlab.torproject.org/tpo/tpa/team/-/issues/40221#note_2745110 the newer versions were held off due to Debian 11 Bullseye freeze, but now that it has been released that should not be any blocking issue for upgrading to 0.4.6.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292795"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292795" class="contextual-region comment js-comment unpublished by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 18, 2021</p>
    </div>
    <a href="#comment-292795">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292795" class="permalink" rel="bookmark">Zero ed_pubkey lets an…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Zero ed_pubkey lets an attacker force the relay to hold open a new TLS connection for _some_ extend request.</p>
<p>circuit_extend_add_ed25519_helper:</p>
<blockquote><p>  /* Fill in ed_pubkey if it was not provided and we can infer it from<br />
   * our networkstatus */<br />
  if (ed25519_public_key_is_zero(&amp;ec-&gt;ed_pubkey)) {<br />
    const node_t *node = node_get_by_id((const char*)ec-&gt;node_id);<br />
    const ed25519_public_key_t *node_ed_id = NULL;<br />
    if (node &amp;&amp;<br />
        node_supports_ed25519_link_authentication(node, 1) &amp;&amp;<br />
        (node_ed_id = node_get_ed25519_id(node))) {<br />
      ed25519_pubkey_copy(&amp;ec-&gt;ed_pubkey, node_ed_id);<br />
    }<br />
  }</p></blockquote>
<p>ed_pubkey is optional if no node information.</p>
<p>channel_get_for_extend:</p>
<blockquote><p>    /* The Ed25519 key has to match too */<br />
    if (!channel_remote_identity_matches(chan, rsa_id_digest, ed_id)) {<br />
      continue;<br />
    }</p></blockquote>
<p>ed_pubkey is not optional for channel reuse.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292959"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292959" class="contextual-region comment js-comment unpublished by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="Concerned Relay admin">Concerned Rela… (not verified)</span> said:</p>
      <p class="date-time">October 08, 2021</p>
    </div>
    <a href="#comment-292959">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292959" class="permalink" rel="bookmark">There is an issue with io…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There is an issue with io timeouts and connection resets when compiling with libnss</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-293331"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293331" class="contextual-region comment js-comment unpublished by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Gleb (not verified)</span> said:</p>
      <p class="date-time">November 11, 2021</p>
    </div>
    <a href="#comment-293331">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293331" class="permalink" rel="bookmark">What is it? Why 3 new…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is it? Why 3 new versions? 0.3.5.16, 0.4.5.10 and 0.4.6.7.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-293332"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-293332" class="contextual-region comment js-comment unpublished by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Gleb (not verified)</span> said:</p>
      <p class="date-time">November 11, 2021</p>
    </div>
    <a href="#comment-293332">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-293332" class="permalink" rel="bookmark">What is it? Why 3 new…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is it? Why 3 new versions? 0.3.5.16, 0.4.5.10 and 0.4.6.7.</p>
</div>
  </div>
</article>
<!-- Comment END -->
