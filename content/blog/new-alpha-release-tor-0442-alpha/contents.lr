title: New alpha release: Tor 0.4.4.2-alpha
---
pub_date: 2020-07-09
---
author: nickm
---
tags: security advisory
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.4.2-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release around the end of the month.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>This is the second alpha release in the 0.4.4.x series. It fixes a few bugs in the previous release, and solves a few usability, compatibility, and portability issues.</p>
<p>This release also fixes TROVE-2020-001, a medium-severity denial of service vulnerability affecting all versions of Tor when compiled with the NSS encryption library. (This is not the default configuration.) Using this vulnerability, an attacker could cause an affected Tor instance to crash remotely. This issue is also tracked as CVE-2020- 15572. Anybody running a version of Tor built with the NSS library should upgrade to 0.3.5.11, 0.4.2.8, 0.4.3.6, or 0.4.4.2-alpha or later. If you're running with OpenSSL, this bug doesn't affect your Tor.</p>
<h2>Changes in version 0.4.4.2-alpha - 2020-07-09</h2>
<ul>
<li>Major bugfixes (NSS, security):
<ul>
<li>Fix a crash due to an out-of-bound memory access when Tor is compiled with NSS support. Fixes bug <a href="https://bugs.torproject.org/33119">33119</a>; bugfix on 0.3.5.1-alpha. This issue is also tracked as TROVE-2020-001 and CVE-2020-15572.</li>
</ul>
</li>
<li>Minor features (bootstrap reporting):
<ul>
<li>Report more detailed reasons for bootstrap failure when the failure happens due to a TLS error. Previously we would just call these errors "MISC" when they happened during read, and "DONE" when they happened during any other TLS operation. Closes ticket <a href="https://bugs.torproject.org/32622">32622</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (directory authority):
<ul>
<li>Authorities now recommend the protocol versions that are supported by Tor 0.3.5 and later. (Earlier versions of Tor have been deprecated since January of this year.) This recommendation will cause older clients and relays to give a warning on startup, or when they download a consensus directory. Closes ticket <a href="https://bugs.torproject.org/32696">32696</a>.</li>
</ul>
</li>
<li>Minor features (entry guards):
<ul>
<li>Reinstate support for GUARD NEW/UP/DOWN control port events. Closes ticket <a href="https://bugs.torproject.org/40001">40001</a>.</li>
</ul>
</li>
<li>Minor features (linux seccomp2 sandbox, portability):
<ul>
<li>Allow Tor to build on platforms where it doesn't know how to report which syscall caused the linux seccomp2 sandbox to fail. This change should make the sandbox code more portable to less common Linux architectures. Closes ticket <a href="https://bugs.torproject.org/34382">34382</a>.</li>
<li>Permit the unlinkat() syscall, which some Libc implementations use to implement unlink(). Closes ticket <a href="https://bugs.torproject.org/33346">33346</a>.</li>
</ul>
</li>
<li>Minor bugfix (CI, Windows):
<ul>
<li>Use the correct 64-bit printf format when compiling with MINGW on Appveyor. Fixes bug <a href="https://bugs.torproject.org/40026">40026</a>; bugfix on 0.3.5.5-alpha.</li>
</ul>
</li>
<li>Minor bugfix (onion service v3 client):
<ul>
<li>Remove a BUG() warning that could occur naturally. Fixes bug <a href="https://bugs.torproject.org/34087">34087</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfix (SOCKS, onion service client):
<ul>
<li>Detect v3 onion service addresses of the wrong length when returning the F6 ExtendedErrors code. Fixes bug <a href="https://bugs.torproject.org/33873">33873</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compiler warnings):
<ul>
<li>Fix a compiler warning on platforms with 32-bit time_t values. Fixes bug <a href="https://bugs.torproject.org/40028">40028</a>; bugfix on 0.3.2.8-rc.</li>
</ul>
</li>
<li>Minor bugfixes (control port, onion service):
<ul>
<li>Consistently use 'address' in "Invalid v3 address" response to ONION_CLIENT_AUTH commands. Previously, we would sometimes say 'addr'. Fixes bug <a href="https://bugs.torproject.org/40005">40005</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>Downgrade a noisy log message that could occur naturally when receiving an extrainfo document that we no longer want. Fixes bug <a href="https://bugs.torproject.org/16016">16016</a>; bugfix on 0.2.6.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion services v3):
<ul>
<li>Avoid a non-fatal assertion failure in certain edge-cases when opening an intro circuit as a client. Fixes bug <a href="https://bugs.torproject.org/34084">34084</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Deprecated features (onion service v2):
<ul>
<li>Add a deprecation warning for version 2 onion services. Closes ticket <a href="https://bugs.torproject.org/40003">40003</a>.</li>
</ul>
</li>
<li>Removed features (IPv6, revert):
<ul>
<li>Revert the change in the default value of ClientPreferIPv6OrPort: it breaks the torsocks use case. The SOCKS resolve command has no mechanism to ask for a specific address family (v4 or v6), and so prioritizing IPv6 when an IPv4 address is requested on the SOCKS interface resulted in a failure. Tor Browser explicitly sets PreferIPv6, so this should not affect the majority of our users. Closes ticket <a href="https://bugs.torproject.org/33796">33796</a>; bugfix on 0.4.4.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-288677"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288677" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>TorUser (not verified)</span> said:</p>
      <p class="date-time">July 11, 2020</p>
    </div>
    <a href="#comment-288677">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288677" class="permalink" rel="bookmark">How do I install the debian…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How do I install the debian package from the official repository? I add deb <a href="https://deb.torproject.org/torproject.org" rel="nofollow">https://deb.torproject.org/torproject.org</a> tor-experimental-0.4.4.x-buster main to /etc/sources.list but apt update doesn't find the update.<br />
I think it's because this file is empty <a href="https://deb.torproject.org/torproject.org/dists/tor-experimental-0.4.4.x-buster/main/binary-amd64/Packages" rel="nofollow">https://deb.torproject.org/torproject.org/dists/tor-experimental-0.4.4…</a><br />
It shouldn't be empty.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288703"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288703" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>John (not verified)</span> said:</p>
      <p class="date-time">July 15, 2020</p>
    </div>
    <a href="#comment-288703">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288703" class="permalink" rel="bookmark">Since I updated Tor from 8.5…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Since I updated Tor from 8.5 to 9.5 I can't access v3 onions, can this be fixed on my end or what do I do?, thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288850"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288850" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 30, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288703" class="permalink" rel="bookmark">Since I updated Tor from 8.5…</a> by <span>John (not verified)</span></p>
    <a href="#comment-288850">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288850" class="permalink" rel="bookmark">I believe you mean &quot;Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I believe you mean "Tor Browser" or "Tor Browser Bundle". The title of this blog post indicates it's for "tor", the network daemon. Since your question appears to be for Tor Browser Bundle and not the daemon, please ask your question in a post whose title says "Tor Browser" so the proper team can find you: <a href="https://blog.torproject.org/category/tags/tbb" rel="nofollow">https://blog.torproject.org/category/tags/tbb</a><br />
Since you asked about 9.5 which is not an alpha version (in 10.0a4, "a" means alpha), ask in a post whose title does not indicate alpha versions.</p>
<p>The Tor Browser Bundle includes the tor network daemon and a heavily modified version of Mozilla Firefox rebranded as Tor Browser. Capital-T "Tor" is the network of devices, and lowercase-t "tor" is the executable proxy daemon.</p>
<p>Onions go down very frequently. If you really think it's on your end, you could export or backup any custom settings or bookmarks, uninstall or delete the folder, reinstall, and import.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
