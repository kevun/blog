title: New Release: Tor Browser 11.0.14 (Android, Windows, macOS, Linux)
---
pub_date: 2022-06-07
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.0.14 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.14 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.14/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-21/) to Firefox.

Tor Browser 11.0.14 updates Firefox on Windows, macOS, and Linux to 91.10.0esr.

We use the opportunity as well to update various other components of Tor Browser:
- NoScript 11.4.6

The full changelog since [Tor Browser 11.0.13](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- All Platforms
  - Updated NoScript to 11.4.6
  - [Bug tor-browser-build#40474](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40474): Bump version of pion/webrtc to v3.1.41
- Windows + OS X + Linux
  - Update Firefox to 91.10.0esr
- Build System
  - All Platforms
    - Update Go to 1.17.11
    - [Bug tor-browser-build#40511](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40511): Changlog in stable is missing 11.5ax stanzas
