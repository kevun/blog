title: Tor Weekly News — July 9th, 2014
---
pub_date: 2014-07-09
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-seventh issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>On being targeted by the NSA</h1>

<p>Das Erste has <a href="http://daserste.ndr.de/panorama/aktuell/nsa230_page-1.html" rel="nofollow">published an article</a> and <a href="http://daserste.ndr.de/panorama/xkeyscorerules100.txt" rel="nofollow">supporting material</a> showing how the NSA explicitly targets Tor and Tails user through the XKEYSCORE Deep Packet Inspection system. Several other media picked up the news, and it was also discussed in various threads on the tor-talk mailing list (<a href="https://lists.torproject.org/pipermail/tor-talk/2014-June/033473.html" rel="nofollow">1</a>, <a href="https://lists.torproject.org/pipermail/tor-talk/2014-July/033564.html" rel="nofollow">2</a>, <a href="https://lists.torproject.org/pipermail/tor-talk/2014-July/033640.html" rel="nofollow">3</a>, <a href="https://lists.torproject.org/pipermail/tor-talk/2014-July/033642.html" rel="nofollow">4</a>, <a href="https://lists.torproject.org/pipermail/tor-talk/2014-July/033656.html" rel="nofollow">5</a>, <a href="https://lists.torproject.org/pipermail/tor-talk/2014-July/033703.html" rel="nofollow">6</a>, <a href="https://lists.torproject.org/pipermail/tor-talk/2014-July/033749.html" rel="nofollow">7</a>).</p>

<p>The Tor Project’s view has been <a href="https://blog.torproject.org/blog/being-targeted-nsa" rel="nofollow">reposted</a> on the blog. To a comment that said “I felt like i am caught in the middle of a two gigantic rocks colliding each other”, Roger Dingledine <a href="https://blog.torproject.org/blog/being-targeted-nsa#comment-64376" rel="nofollow">replied</a>: “You’re one of the millions of people every day who use Tor. And because of the diversity of users […], just because they know you use Tor doesn’t mean they know <em>why</em> you use Tor, or what you do with it. That’s still way better than letting them watch all of your interactions with all websites on the Internet.”</p>

<h1>More monthly status reports for June 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of June continued, with submissions from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000576.html" rel="nofollow">Georg Koppen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000577.html" rel="nofollow">Lunar</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000578.html" rel="nofollow">Noel David Torres Taño</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000579.html" rel="nofollow">Matt Pagan</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000580.html" rel="nofollow">Colin C.</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000583.html" rel="nofollow">Arlo Breault</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000585.html" rel="nofollow">George Kadianakis</a>.</p>

<p>Mike Perry reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000584.html" rel="nofollow">Tor Browser team</a>.</p>

<h1>Miscellaneous news</h1>

<p>An Austrian Tor exit node operator <a href="https://network23.org/blackoutaustria/2014/07/01/to-whom-it-may-concern-english-version/" rel="nofollow">interpreted</a> their conviction in a first ruling as judging them “guilty of complicity, because he enabled others to transmit content of an illegal nature through the service”. Moritz Bartl from <a href="https://www.torservers.net/" rel="nofollow">Torservers.net</a> <a href="https://lists.torproject.org/pipermail/tor-talk/2014-July/033613.html" rel="nofollow">commented</a>: “We strongly believe that it can be easily challenged. […] We will definitely try and find some legal expert in Austria and see what we can do to fight this.”</p>

<p>Linus Nordberg is expanding the idea of public, append-only, untrusted log à la <a href="http://www.certificate-transparency.org/" rel="nofollow">Certificate Transparency</a> to the Tor consensus. Linus submitted <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007092.html" rel="nofollow">a new draft proposal</a> to the tor-dev mailing list for reviews.</p>

<p>Miguel Freitas <a href="https://lists.torproject.org/pipermail/tor-talk/2014-July/033580.html" rel="nofollow">reported</a> that <a href="http://twister.net.co/" rel="nofollow">twister</a> — a fully decentralized P2P microblogging platform — was now able to run over Tor. As Miguel wrote, “running twister on top of Tor was a long time goal, […] the Tor support allows a far more interesting threat model”.</p>

<p>Google Summer of Code students have sent a new round of reports after the mid-term: Israel Leiva on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007074.html" rel="nofollow">GetTor revamp</a>, Amogh Pradeep on <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007083.html" rel="nofollow">Orbot and Orfox improvements</a>, Mikhail Belous on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007086.html" rel="nofollow">multicore tor daemon</a>, Daniel Martí on <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007087.html" rel="nofollow">incremental updates to consensus documents</a>, Sreenatha Bhatlapenumarthi on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007091.html" rel="nofollow">Tor Weather rewrite</a>, Quinn Jarrell on the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007094.html" rel="nofollow">pluggable transport combiner</a>, Noah Rahman on <a href="https://lists.torproject.org/pipermail/tor-dev/2014-July/007095.html" rel="nofollow">Stegotorus enhancements</a>, Marc Juarez on <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000581.html" rel="nofollow">website fingerprinting defenses </a>, development, Juha Nurmi on the ahmia.fi project <a href="https://lists.torproject.org/pipermail/tor-reports/2014-July/000582.html" rel="nofollow"></a>, and Zack Mullaly on the <a href="https://lists.eff.org/pipermail/https-everywhere/2014-July/002182.html" rel="nofollow">HTTPS Everywhere secure ruleset update mechanism</a>.</p>

<p>sajolida, tchou and Giorgio Maone from NoScript <a href="https://mailman.boum.org/pipermail/tails-dev/2014-July/006288.html" rel="nofollow">drafted a specification</a> for a Firefox extension to download and verify Tails.</p>

<h1>Tor help desk roundup</h1>

<p>One way to volunteer for Tor is to run a <a href="https://www.torproject.org/getinvolved/mirrors.html" rel="nofollow">mirror</a> of the Tor Project website. <a href="https://www.torproject.org/docs/running-a-mirror.html.en" rel="nofollow">Instructions</a> are available for anyone wanting to run a mirror. Mirrors are useful for those who, for one reason or another, cannot access or use the main Tor Project website. Volunteers who have successfully set up a synced a mirror can report their mirror to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-mirrors" rel="nofollow">tor-mirrors mailing list</a> to get it included in the full mirrors list.</p>

<h1>Easy development tasks to get involved with</h1>

<p>ooniprobe is a tool for conducting network measurements that are useful for detecting network interference. When ooniprobe starts <a href="https://bugs.torproject.org/11983" rel="nofollow">it should perform checks to verify that the config file is correct</a>. If that is not the case, it should fail gracefully at startup. The ticket indicates where this check should be added to the <a href="https://gitweb.torproject.org/ooni-probe.git" rel="nofollow">ooniprobe codebase</a>. If you’d like to do some easy Python hacking, be sure to give this ticket a try.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, harmony, Matt Pagan, and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

