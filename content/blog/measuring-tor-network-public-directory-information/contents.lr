title: Measuring the Tor Network from Public Directory Information
---
pub_date: 2009-06-16
---
author: karsten
---
tags:

research
censorship circumvention
performance
---
categories:

circumvention
network
research
---
_html_body:

<p>On this year's <a href="http://petsymposium.org/2009/hotpets.php" rel="nofollow">HotPETs workshop</a> (August 5-7 in Seattle, WA, USA) I'm going to present some results on <a href="http://freehaven.net/~karsten/metrics/measuring-tor-public-dir-info-final.pdf" rel="nofollow">Measuring the Tor Network from Public Directory Information</a>. The main idea is to observe trends in the Tor network without having to measure any data other than <a href="https://git.torproject.org/checkout/tor/master/doc/spec/dir-spec.txt" rel="nofollow">public directory information</a>. These data are there anyway as they are required for clients to make good path selection decisions and build circuits. The results of this paper reveal problems in the current Tor network that need to be addressed, e.g., by <a href="https://git.torproject.org/checkout/metrics/master/report/dirarch/flagrequirements-2009-04-11.pdf" rel="nofollow">lowering requirements for assigning certain flags</a>, facilitating the upgrade process, improving support for dynamic IP addresses, possibly calculating bandwidth capacity more reliably, and clarifying legal issues for running relays in view of data retention laws. The next step in understanding the problems of the Tor network requires an <a href="https://blog.torproject.org/blog/performance-measurements-and-blockingresistance-analysis-tor-network" rel="nofollow">extension of network measurements</a> to improve <a href="https://blog.torproject.org/blog/why-tor-is-slow" rel="nofollow">performance</a> and blocking-resistance of Tor.</p>

---
_comments:

<a id="comment-1544"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1544" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 17, 2009</p>
    </div>
    <a href="#comment-1544">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1544" class="permalink" rel="bookmark">Canadian Tor proxy bridge for all Iranians 174.0.84.20:443 55590</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Canadian Tor proxy bridge for all Iranians 174.0.84.20:443 555905FEDB75920E1056B0C158402EF32E668B68  Please spread word!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
