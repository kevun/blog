title: New releases (with security fixes): Tor 0.3.5.14, 0.4.4.8, and 0.4.5.7
---
pub_date: 2021-03-16
---
author: nickm
---
tags: security
---
_html_body:

<p>We have a new stable release today. If you build Tor from source, you can download the source code for 0.4.5.7 on the <a href="https://www.torproject.org/download/tor/">download page</a>. Packages should be available within the next several weeks, with a new Tor Browser coming next week.</p>
<p>Also today, Tor 0.3.5.14 (<a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.3.5.14">changelog</a>) and Tor 0.4.4.8 (<a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.4.4.8">changelog</a>) have also been released; you can find them (and source for older Tor releases) at <a href="https://dist.torproject.org/">https://dist.torproject.org</a>.</p>
<p>These releases fix a pair of denial-of-service issues, described below. One of these issues is authority-only.  The other issue affects all Tor instances, and is most damaging on directory authorities and relays.  We recommend that everybody should upgrade to one of these versions once packages are available.</p>
<p>Tor 0.4.5.7 fixes two important denial-of-service bugs in earlier versions of Tor.</p>
<p>One of these vulnerabilities (TROVE-2021-001) would allow an attacker who can send directory data to a Tor instance to force that Tor instance to consume huge amounts of CPU. This is easiest to exploit against authorities, since anybody can upload to them, but directory caches could also exploit this vulnerability against relays or clients when they download. The other vulnerability (TROVE-2021-002) only affects directory authorities, and would allow an attacker to remotely crash the authority with an assertion failure. Patches have already been provided to the authority operators, to help ensure network stability.</p>
<p>We recommend that everybody upgrade to one of the releases that fixes these issues (0.3.5.14, 0.4.4.8, or 0.4.5.7) as they become available to you.</p>
<p>This release also updates our GeoIP data source, and fixes a few smaller bugs in earlier releases.</p>
<h2>Changes in version 0.4.5.7 - 2021-03-16</h2>
<ul>
<li>Major bugfixes (security, denial of service):
<ul>
<li>Disable the dump_desc() function that we used to dump unparseable information to disk. It was called incorrectly in several places, in a way that could lead to excessive CPU usage. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40286">40286</a>; bugfix on 0.2.2.1-alpha. This bug is also tracked as TROVE-2021- 001 and CVE-2021-28089.</li>
<li>Fix a bug in appending detached signatures to a pending consensus document that could be used to crash a directory authority. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40316">40316</a>; bugfix on 0.2.2.6-alpha. Tracked as TROVE-2021-002 and CVE-2021-28090.</li>
</ul>
</li>
<li>Minor features (geoip data):
<ul>
<li>We have switched geoip data sources. Previously we shipped IP-to- country mappings from Maxmind's GeoLite2, but in 2019 they changed their licensing terms, so we were unable to update them after that point. We now ship geoip files based on the IPFire Location Database instead. (See <a href="https://location.ipfire.org/">https://location.ipfire.org/</a> for more information). This release updates our geoip files to match the IPFire Location Database as retrieved on 2021/03/12. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40224">40224</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (directory authority):
<ul>
<li>Now that exit relays don't allow exit connections to directory authority DirPorts (to prevent network reentry), disable authorities' reachability self test on the DirPort. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40287">40287</a>; bugfix on 0.4.5.5-rc.</li>
</ul>
</li>
<li>Minor bugfixes (documentation):
<ul>
<li>Fix a formatting error in the documentation for VirtualAddrNetworkIPv6. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40256">40256</a>; bugfix on 0.2.9.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (Linux, relay):
<ul>
<li>Fix a bug in determining total available system memory that would have been triggered if the format of Linux's /proc/meminfo file had ever changed to include "MemTotal:" in the middle of a line. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40315">40315</a>; bugfix on 0.2.5.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (metrics port):
<ul>
<li>Fix a BUG() warning on the MetricsPort for an internal missing handler. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40295">40295</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service):
<ul>
<li>Remove a harmless BUG() warning when reloading tor configured with onion services. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40334">40334</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (portability):
<ul>
<li>Fix a non-portable usage of "==" with "test" in the configure script. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40298">40298</a>; bugfix on 0.4.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Remove a spammy log notice falsely claiming that the IPv4/v6 address was missing. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40300">40300</a>; bugfix on 0.4.5.1-alpha.</li>
<li>Do not query the address cache early in the boot process when deciding if a relay needs to fetch early directory information from an authority. This bug resulted in a relay falsely believing it didn't have an address and thus triggering an authority fetch at each boot. Related to our fix for 40300.</li>
</ul>
</li>
<li>Removed features (mallinfo deprecated):
<ul>
<li>Remove mallinfo() usage entirely. Libc 2.33+ now deprecates it. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40309">40309</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291362"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291362" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 16, 2021</p>
    </div>
    <a href="#comment-291362">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291362" class="permalink" rel="bookmark">Both major fixes are for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Both major fixes are for directory authorities. Those are run by people who communicate closely with Tor Project and so can deeply analyze bugs together. Are metrics enough to find and analyze these sorts of deep bugs in the procedures of relays? Are we missing more bugs in relay operations than are found for directory authorities?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291364"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291364" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 16, 2021</p>
    </div>
    <a href="#comment-291364">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291364" class="permalink" rel="bookmark">Critical problem since  0.4…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Critical problem since  0.4.5.6.     Torbrowser can not create circuit through  transparent proxy <a href="https://learn.adafruit.com/onion-pi/overview" rel="nofollow">https://learn.adafruit.com/onion-pi/overview</a><br />
<a href="https://github.com/grugq/PORTALofPi" rel="nofollow">https://github.com/grugq/PORTALofPi</a><br />
<a href="https://github.com/grugq/portal" rel="nofollow">https://github.com/grugq/portal</a></p>
<p>[NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
[NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
[NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
[NOTICE] Opening Socks listener on 127.0.0.1:9150<br />
[NOTICE] Opened Socks listener connection (ready) on 127.0.0.1:9150<br />
[NOTICE] Bootstrapped 5% (conn): Connecting to a relay<br />
[NOTICE] Bootstrapped 10% (conn_done): Connected to a relay<br />
[WARN] Problem bootstrapping. Stuck at 10% (conn_done): Connected to a relay. (TLS_ERROR; TLS_ERROR; count 10; recommendation warn; host XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX at xxx.xxx.xxx.xxx:xxx)<br />
[WARN] 10 connections have failed:<br />
[WARN] 10 connections died in state handshaking (TLS) with SSL state SSLv3/TLS write client hello in HANDSHAKE<br />
[NOTICE] Closing no-longer-configured Socks listener on 127.0.0.1:9150<br />
[NOTICE] DisableNetwork is set. Tor will not make or accept non-control network connections. Shutting down all existing connections.<br />
[NOTICE] Delaying directory fetches: DisableNetwork is set.</p>
<p>Few  exit circuits created by tor daemon on PORTALofPi  allow  torbrowser work.  Majority give  SSLv3/TLS write client hello in HANDSHAKE error when torbrowser on desktop computer attempts to connect through transparent proxy Pi.   Someone show how create bug report?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291383"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291383" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291364" class="permalink" rel="bookmark">Critical problem since  0.4…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291383">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291383" class="permalink" rel="bookmark">&gt; exit circuits created by…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; exit circuits created by tor daemon</p>
<p>Do you mean "exit circuit" in contrast to an "onion service circuit"?</p>
<p>&gt; torbrowser on desktop computer attempts to connect through transparent proxy Pi.</p>
<p>Maybe don't do that.<br />
<a href="https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TorifyHOWTO#tor-over-tor" rel="nofollow">https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TorifyHOWTO#tor-o…</a><br />
<a href="https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TorPlusVPN#you-tor-x" rel="nofollow">https://gitlab.torproject.org/legacy/trac/-/wikis/doc/TorPlusVPN#you-to…</a></p>
<p>In <a href="https://blog.torproject.org/node/1990" rel="nofollow">0.3.5.13, 0.4.3.8, and 0.4.4.7</a>, backported from 0.4.5.5-rc: "Re-entry into the network is now denied at the Exit level to all relays' ORPorts and authorities' ORPorts and DirPorts. This change should help mitgate a set of denial-of-service attacks. Closes ticket 2667."</p>
<p>Another thing is that your guard could have become inaccessible at no fault of your LAN. Search if your guard is offline by pasting its IP or fingerprint into <a href="https://metrics.torproject.org/rs.html" rel="nofollow">Relay Search</a> on the Metrics portal. If your guard is offline, you could wait for it to come back online, or you could change your guard. It is usually safer to wait. Your guard is saved in your "state" file in the same directory as your <a href="https://support.torproject.org/tbb/tbb-editing-torrc/" rel="nofollow">torrc file</a>. If you're running the daemon by itself as implied by the OS links you pasted, search for the default locations of the torrc file in the <a href="https://2019.www.torproject.org/docs/tor-manual.html.en" rel="nofollow">tor manual</a>. However, you said you were doing both: running Tor Browser which is bundled with a tor daemon on your local device, as well as running a tor daemon on a Pi.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
