title: New Release: Tor Browser 11.5.2 (Android,Windows, macOS, Linux)
---
pub_date: 2022-08-29
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5.2 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5.2 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5.2/).

Tor Browser 11.5.2 updates Firefox on Windows, macOS, and Linux to 91.13.0esr.

This version includes important security updates to Firefox:
- https://www.mozilla.org/en-US/security/advisories/mfsa2022-35/

We use the opportunity as well to update various other components of Tor Browser:
- Tor 0.4.7.10
- NoScript 11.4.9

The full changelog since [Tor Browser 11.5.1](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=tbb-11.5.2-build1) is:

- All Platforms
  - Update Tor to 0.4.7.10
  - Update NoScript to 11.4.9
- Windows + macOS + Linux
  - Update Firefox to 91.13.0esr
  - [Bug tor-browser#27719](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/27719): Treat unsafe renegotiation as broken
  - [Bug tor-browser#40242](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40242): Tor Browser has two default bridges that share a fingerprint, and Tor ignores one
  - [Bug tor-browser#41067](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41067): Cherry-pick fixes for HTTPS-Only mode
  - [Bug tor-browser#41070](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41070): Google vanished from default search options from 11.0.10 onwards
  - [Bug tor-browser#41075](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41075): The Tor Browser is showing caution sign but your document said it won't
  - [Bug tor-browser#41089](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41089): Add tor-browser build scripts + Makefile to tor-browser
  - [Bug tor-browser#41095](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41095): "Learn more" link in onboarding slideshow still points to 11.0 release post
- Build System
  - Windows + macOS + Linux
    - Update Go to 1.17.3
  - Android
    - Update Go to 1.18.5
