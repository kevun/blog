title: Torbutton 1.4.1 Released
---
pub_date: 2011-08-29
---
author: mikeperry
---
tags:

torbutton
tor browser bundle
tor browser
firefox
new identity
hotmail fixes
safecache
---
categories:

applications
releases
---
_html_body:

<p>Torbutton 1.4.1 has been released at: <a href="https://www.torproject.org/torbutton/" rel="nofollow">https://www.torproject.org/torbutton/</a></p>

<p>This release features a "New Identity" menu option that clears browser state, closes tabs, and obtains a fresh Tor circuit for future requests. It also features a fix for breakage with Hotmail, and further isolates browser state and identifiers to the url bar domain (see <a href="https://blog.torproject.org/blog/improving-private-browsing-modes-do-not-track-vs-real-privacy-design" rel="nofollow">https://blog.torproject.org/blog/improving-private-browsing-modes-do-no…</a>).</p>

<p>However, the New Identity button and the Hotmail fix are only available to Tor Browser Bundle users. If you are still using Torbutton with a vanilla Mozilla Firefox, we strongly recommend you download the Tor Browser Bundle 2.2.x alphas from <a href="https://www.torproject.org/dist/torbrowser/" rel="nofollow">https://www.torproject.org/dist/torbrowser/</a> and report problems you experience, as we will be declaring them stable within the next release or two.</p>

<p>Stay tuned for a new Tor Browser release that contains Torbutton 1.4.1 tomorrow.</p>

<p>Here is the complete changelog:<br />
 * bug 523: Implement New Identity (for TBB only)<br />
 * bug 3580: Fix hotmail/live breakage (for TBB only)<br />
 * bug 3748: Disable 3rd party HTTP auth<br />
 * bug 3665: Fix several corner cases SafeCache isolation<br />
 * bug 3739: Fix https-&gt;http CORS failure for SafeCache<br />
 * bug 3414: Isolate window.name based on referrer policy<br />
 * bug 3809: Disable referer spoofing (fixes navigation issues)<br />
 * bug 3819: Fix API issue with cookie protections<br />
 * bug 3820: Fix warning w/ session store filter</p>

---
_comments:

<a id="comment-11262"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11262" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 29, 2011</p>
    </div>
    <a href="#comment-11262">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11262" class="permalink" rel="bookmark">Can we get rid of Vidalia</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can we get rid of Vidalia for the Browser Bundle? Do Browser Bundle get into tweaking all the fiddly bits, or is it just a heavy way to launch tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-11266"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11266" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 29, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11262" class="permalink" rel="bookmark">Can we get rid of Vidalia</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-11266">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11266" class="permalink" rel="bookmark">Some TBB users need to set a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some TBB users need to set a proxy or a bridge in order to use Tor. So we can't do away with a GUI interface entirely.</p>
<p>If somebody wanted to make a lighter-weight Vidalia, or work on a way to make Vidalia lighter-weight, that'd be great. Keep in mind that it would be best for the interface to work on all platforms (as Vidalia does via Qt).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-11278"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11278" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 30, 2011</p>
    </div>
    <a href="#comment-11278">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11278" class="permalink" rel="bookmark">Please, will there ever be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please, will there ever be released Google Chrome (Chromium) Tor Browser?<br />
It would be great, many people like this browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15730"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15730" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 24, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11278" class="permalink" rel="bookmark">Please, will there ever be</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15730">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15730" class="permalink" rel="bookmark">in case you aren&#039;t</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>in case you aren't trolling:</p>
<p>there's no way chrome could provide real anonymity.<br />
if you want to visit .onion sites over chrome use tor2web.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15982"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15982" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 05, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11278" class="permalink" rel="bookmark">Please, will there ever be</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15982">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15982" class="permalink" rel="bookmark">great idea but it will open</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>great idea but it will open the range for spying on people<br />
you know how much google company spend the money about spying activities &amp; with this integration (with tor browser) they will control the world by proxies &amp; spying with exellent service interface</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15985"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15985" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 05, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11278" class="permalink" rel="bookmark">Please, will there ever be</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15985">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15985" class="permalink" rel="bookmark">https://www.torproject.org/do</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/docs/faq#TBBOtherBrowser" rel="nofollow">https://www.torproject.org/docs/faq#TBBOtherBrowser</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-11291"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11291" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 31, 2011</p>
    </div>
    <a href="#comment-11291">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11291" class="permalink" rel="bookmark">A newer better version is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A newer better version is good.<br />
But whats with users running older windows versions?<br />
#Expert Bundle<br />
#...., ME, and Windows 98SE ?<br />
Probably surfing with older outdated operating system/browser.<br />
How can they use tor safe(er)?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11378"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11378" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 04, 2011</p>
    </div>
    <a href="#comment-11378">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11378" class="permalink" rel="bookmark">1.41 breaks Firefox on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>1.41 breaks Firefox on Windows. Both installed 3.62 and portable. No response can't even get into addons to remove it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11554"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11554" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 10, 2011</p>
    </div>
    <a href="#comment-11554">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11554" class="permalink" rel="bookmark">Why does the &quot;New Identity&quot;</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why does the "New Identity" feature of torbutton only work in TBB?  Can it be implemented without needing TBB?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11956"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11956" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 01, 2011</p>
    </div>
    <a href="#comment-11956">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11956" class="permalink" rel="bookmark">Is it possible to make the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it possible to make the "new identity" feature more random?  ie not keep bringing up the same few identities.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11972"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11972" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 02, 2011</p>
    </div>
    <a href="#comment-11972">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11972" class="permalink" rel="bookmark">Im not a very experienced</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Im not a very experienced user, but this question: ive FF7, and Tor. Ive downloaded a new Torbutton after installing FF7 (and JondoFox profile (switcher)).<br />
The torbutton icon is green ('tor enabled') or it has a red cross ('disabled'). In both cases my ip is the same (being an 'tor'-ip), theres no difference to find. Am i doing something wrong? Or what do i need that torbutton for then..? (Tor is running and im connected)?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13425"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13425" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2012</p>
    </div>
    <a href="#comment-13425">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13425" class="permalink" rel="bookmark">I downloaded the Tor bundle,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I downloaded the Tor bundle, and everything is there, included the icon in the doc for Firefox, however firefox can't connect to the internet for some reason?  Any ideas how to get that connection?  Was there something else I was suppose to do? </p>
<p>I found this article here that tells me I have to fill in the ports and host information, and it tells me what to put in those fields.  Is this information accurate?  </p>
<p><a href="http://www.simplehelp.net/2007/09/06/how-to-surf-the-web-anonymously-using-os-x/" rel="nofollow">http://www.simplehelp.net/2007/09/06/how-to-surf-the-web-anonymously-us…</a></p>
<p>Is it also true based on this article I could use safari with Tor, and still have the true benefit of the service?? </p>
<p>Any help would be much appreciated!!  Alot of this stuff is just simply over my head.  The more I read too, sometimes the more confused I am getting.  </p>
<p>I am also trying to partition my Apple hard drive so that I can get off of os-x and browse and use Linux Ubuntu as my OS.  Having a hard time accomphlishing that as well!!!  These days, it is computer science I wish I would have gone into years ago!!... :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14498"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14498" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 03, 2012</p>
    </div>
    <a href="#comment-14498">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14498" class="permalink" rel="bookmark">Can someone please</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can someone please officially comment on this discussion?  The issue is that the tor browser is not available as a seperate download, and comments about the unmaintainability of the current tbb's design.</p>
<p><a href="http://ci3hn2uzjw2wby3z.onion/talk/27y" rel="nofollow">http://ci3hn2uzjw2wby3z.onion/talk/27y</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16045"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16045" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 08, 2012</p>
    </div>
    <a href="#comment-16045">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16045" class="permalink" rel="bookmark">About June 1 using Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>About June 1 using Tor bundle 32 at wireless connection a McDonalds  a Tor button message came up..could not find the Tor Button then trying to connect to a site<br />
got a message "Proxy Server refusing connection..."</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-16055"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16055" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">June 08, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-16045" class="permalink" rel="bookmark">About June 1 using Tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-16055">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16055" class="permalink" rel="bookmark">What is &quot;Tor bundle 32&quot;?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What is "Tor bundle 32"?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
