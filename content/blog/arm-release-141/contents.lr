title: Arm Release 1.4.1
---
pub_date: 2011-01-08
---
author: atagar
---
tags: arm
---
categories: network
---
_html_body:

<p>Hi all. A new release of <a href="http://www.atagar.com/arm/" rel="nofollow">arm</a> is available, including enhancements targeted at performance and cross platform compatibility. In particular, this release provides...</p>

<ul>
<li>Vastly Better Resolver Performance
<p>By far the most expensive thing that arm does is ps and netstat/lsof/etc lookups. While wandering around development forums I discovered <a href="https://code.google.com/p/psutil/" rel="nofollow">psutil</a>, an awesome library for cross platform resolution of system and process information. For OSX and BSD they're using ps and lsof lookups just like arm. However, for Linux they had a very different approach, querying proc contents directly. I <a href="https://svn.torproject.org/svn/arm/trunk/src/util/procTools.py" rel="nofollow">adapted the functions for arm</a> and it cut the runtime for resource and connection resolution by 90%. Many thanks to the authors of psutil (Jay Loden, Dave Daeschler, and Giampaolo Rodola')!</p>
</li>
<li>BSD Compatibility
<p>For a long time FreeBSD has been arm's nemesis. Its variant of netstat can't get connection pids, the ss resolving utility belongs to a spreadsheet program instead, and even pid resolution failed (breaking resource stats and numerous other things). However, thanks to patches and testing by Fabian Keil and Hans Schnehl arm now has BSD counterparts for all of these, plus autodetection for BSD Jails.</p>
</li>
<li>Expanded Distribution
<p>Peter and I have finished revisions for the arm deb and it's now pending feedback from the <a href="http://ftp-master.debian.org/new.html" rel="nofollow">Debian FTP admins</a>. Arm is also now <a href="http://aur.archlinux.org/packages.php?ID=44172" rel="nofollow">available on ArchLinux</a> thanks to Spider.007 and Fabian mentioned that he might be interested in doing a FreeBSD port.</p>
</li>
<li>Volunteer Recruiting
<p>Being the lone developer of arm is kinda lonely. I'd love to find other people interested in hacking on the code with me. To this end, and in anticipation of GSOC 2011, I've added a project to <a href="https://www.torproject.org/getinvolved/volunteer.html.en" rel="nofollow">Tor's volunteer page</a> ("Client Mode Use Cases for Arm").</p>
</li>
</ul>

<p>Plus numerous other fixes and improvements (for details see the <a href="http://www.atagar.com/arm/log.php#releaseNotes" rel="nofollow">release notes</a>). As always, screenshots and downloads are available from the project's homepage:<br />
<a href="http://www.atagar.com/arm/" rel="nofollow">http://www.atagar.com/arm/</a></p>

<p>Cheers! -Damian</p>

