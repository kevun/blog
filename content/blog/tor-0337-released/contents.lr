title: Tor 0.3.3.7 is released!
---
pub_date: 2018-06-12
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>Hello, everyone!</p>
<p>We have a new stable release today. If you build Tor from source, you can<br />
download the source code for 0.3.3.7 on the website.  Packages<br />
should be available within the next several weeks, with a new Tor Browser over the next several weeks.</p>
<p> </p>
<p>Tor 0.3.3.7 backports several changes from the 0.3.4.x series, including fixes for bugs affecting compatibility and stability.</p>
<h2>Changes in version 0.3.3.7 - 2018-06-12</h2>
<ul>
<li>Directory authority changes:
<ul>
<li>Add an IPv6 address for the "dannenberg" directory authority. Closes ticket <a href="https://bugs.torproject.org/26343">26343</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the June 7 2018 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/26351">26351</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (compatibility, openssl, backport from 0.3.4.2-alpha):
<ul>
<li>Work around a change in OpenSSL 1.1.1 where return values that would previously indicate "no password" now indicate an empty password. Without this workaround, Tor instances running with OpenSSL 1.1.1 would accept descriptors that other Tor instances would reject. Fixes bug <a href="https://bugs.torproject.org/26116">26116</a>; bugfix on 0.2.5.16.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, backport from 0.3.4.2-alpha):
<ul>
<li>Silence unused-const-variable warnings in zstd.h with some GCC versions. Fixes bug <a href="https://bugs.torproject.org/26272">26272</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller, backport from 0.3.4.2-alpha):
<ul>
<li>Improve accuracy of the BUILDTIMEOUT_SET control port event's TIMEOUT_RATE and CLOSE_RATE fields. (We were previously miscounting the total number of circuits for these field values.) Fixes bug <a href="https://bugs.torproject.org/26121">26121</a>; bugfix on 0.3.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (hardening, backport from 0.3.4.2-alpha):
<ul>
<li>Prevent a possible out-of-bounds smartlist read in protover_compute_vote(). Fixes bug <a href="https://bugs.torproject.org/26196">26196</a>; bugfix on 0.2.9.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (path selection, backport from 0.3.4.1-alpha):
<ul>
<li>Only select relays when they have the descriptors we prefer to use for them. This change fixes a bug where we could select a relay because it had _some_ descriptor, but reject it later with a nonfatal assertion error because it didn't have the exact one we wanted. Fixes bugs 25691 and 25692; bugfix on 0.3.3.4-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-275728"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275728" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 12, 2018</p>
    </div>
    <a href="#comment-275728">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275728" class="permalink" rel="bookmark">@nickm
What&#039;s up with the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@nickm</p>
<p>What's up with the PKI certificate for blog,torproject.org?   Not owned by Tor Project?  "Authenticates" multiple domains including political campaigns, forensicon.com, etc?</p>
<p>TIA for any information you can provide.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-275809"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275809" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Jesus H Christ (not verified)</span> said:</p>
      <p class="date-time">June 20, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-275728" class="permalink" rel="bookmark">@nickm
What&#039;s up with the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-275809">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275809" class="permalink" rel="bookmark">I think this is the 3rd time…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think this is the 3rd time that I read your demand for an explanation about this. Ever heard of shared hosting/ssl certs? If you would have checked the cert you would have seen that it belongs to a wp/drupal hosting provider.</p>
<p>Hosting a f***ing drupal based blog with no sensitive data on it in-house is a waste of manpower/resources when you have a rather small team and as much on your plate as the Tor guys do.</p>
<p>I hope you can find peace at night again now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-275729"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275729" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 12, 2018</p>
    </div>
    <a href="#comment-275729">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275729" class="permalink" rel="bookmark">Can OONI detect blocking of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can OONI detect blocking of Tor traffic?  Everything seems slower and many news sites no longer work with Tor, since the demise of NN.  Any ideas?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-275755"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275755" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 13, 2018</p>
    </div>
    <a href="#comment-275755">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275755" class="permalink" rel="bookmark">Venezuela is ramping up…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Venezuela is ramping up censorship, there is probably deep packet inspection involved to detect and block Tor. Just a couple of days ago you could still connect directly, but not anymore. It works if you switch to obfs4 bridges...</p>
<p>In recent days they started blocking more news/opinion media sites by using a different method than simple DNS, and at the same time normal Tor operation has been interrupted.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-276124"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276124" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ces (not verified)</span> said:</p>
      <p class="date-time">July 12, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-275755" class="permalink" rel="bookmark">Venezuela is ramping up…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-276124">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276124" class="permalink" rel="bookmark">In venezuela only censoring…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In venezuela only censoring with CANTV and MOVILNET, same chinesse format are the famous firewall called "the great chinesse wall" the venezuelan version called "The Great Guacaipuro Wall" or other diferent name. Go to configure "In my country censorship" then select "Bridge one builded" and select "meek-azure" same format in China.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-275765"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275765" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 14, 2018</p>
    </div>
    <a href="#comment-275765">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275765" class="permalink" rel="bookmark">We are Anonymous.  We are…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We are Anonymous.  We are Legion.  We do not forgive.  We do not forget.  Expect us!</p>
<p>CAPTCHA</p>
<p>6 + 2 = Only proves that your moderation is betrayal!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-275802"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275802" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 18, 2018</p>
    </div>
    <a href="#comment-275802">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275802" class="permalink" rel="bookmark">As always, gratitude for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As always, gratitude for your work.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-275813"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275813" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>hhhwa (not verified)</span> said:</p>
      <p class="date-time">June 20, 2018</p>
    </div>
    <a href="#comment-275813">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275813" class="permalink" rel="bookmark">it&#039;s great job. I&#039;m from…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it's great job. I'm from China. thank you. grateful for your works.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-275843"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275843" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>browser (not verified)</span> said:</p>
      <p class="date-time">June 25, 2018</p>
    </div>
    <a href="#comment-275843">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275843" class="permalink" rel="bookmark">New Firefox, 52.9.0,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>New Firefox, 52.9.0, released. A new TBB will released, too?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-275853"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275853" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 25, 2018</p>
    </div>
    <a href="#comment-275853">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275853" class="permalink" rel="bookmark">is it safe to update this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>is it safe to update to this via Tails?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-275872"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275872" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 26, 2018</p>
    </div>
    <a href="#comment-275872">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275872" class="permalink" rel="bookmark">Why is Tails 3.8 still using…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why is Tails 3.8 still using 0.3.2.10-1~d90.stretch+1 and not 0.3.3.7?</p>
<p>Is it safe to update to the current version of Tor within Tails/Synaptic?</p>
</div>
  </div>
</article>
<!-- Comment END -->
