title: New Tor Browser Bundles for Mac OS X
---
pub_date: 2012-05-01
---
author: erinn
---
tags:

osx
tor browser
tor browser bundle
tbb
---
categories: applications
---
_html_body:

<p>We recently switched our build machine to Lion (OS X 10.7) which had some unintended effects on the Firefox/TorBrowser build. After consulting with Mozilla developers, Sebastian Hahn was able to nail down the problem and provide a <a href="https://gitweb.torproject.org/torbrowser.git/commitdiff/1b56e945bbd5a772f895dd9d3a818f2e606a430d" rel="nofollow">fix</a>. The Mac OS X Tor Browser Bundles have been updated so they should stop crashing for everyone now. Thanks for your patience!</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p><strong>Tor Browser Bundle (2.2.35-10)</strong></p>

<ul>
<li>Make TorBrowser stop crashing on random websites by building with clang instead of llvm-gcc. (closes: <a href="https://trac.torproject.org/projects/tor/ticket/5697" rel="nofollow">#5697</a>)</li>
</ul>

---
_comments:

<a id="comment-15297"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15297" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 01, 2012</p>
    </div>
    <a href="#comment-15297">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15297" class="permalink" rel="bookmark">Version 10 is utterly</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Version 10 is utterly unuseble. </p>
<p>To quote another user comment for previous (-9.1) version:</p>
<p>"Meh do you guys test your releases at all!? Mac torbundle 2.2.35-9.1: if you right-click the firefox toolbar and click 'customize' results in immediate crash of the browser. ADDENDUM: actually no it's way worse than that. you can't click on the noscript button on the toolbar or it crashes. You can't even HOVER over the noscript button or it crashes. Come on guys."</p>
<p>All these critical bugs are still there in the new version. I can add that the previous page button also crashes the browser. All this must mean that the problem hasn't even been recognized let alone adressed! (The reply to the above comment was that you were aware of the problem and then refer to a ticket which seems to have nothing to do with these problems!)<br />
Is it a Mac-only problem? Snow Leopard (which I use) only?<br />
I fully realize that you are a small team with limited resources but this lack of organization is seriously worrisome.<br />
Don't you test new versions across platforms at all before release???<br />
I know you're looking for a manager to tidy up this mess at the moment and that's a good start. When you have to read about critical bugs in blog comments and these comments are generally ignored/misunderstood you are doing yourself no favors. Many pointed out (a long time ago) that TBB versions (even those that work) install with a default setting of Javascript "globally allowed" via the NoScript extension. As a bundled security solution aimed at people who are not experts but need security, this is a disaster. Yet nothing has been done about it.<br />
Instead of this geeky non-user friendly "ticket" system why don't you set up a simple bug list (that non-programmer users actually can find)? Keep it TBB only and identify if bugs are platform/os dependent or universal.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15331"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15331" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15297" class="permalink" rel="bookmark">Version 10 is utterly</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15331">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15331" class="permalink" rel="bookmark">While I agree with most of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>While I agree with most of what you wrote, especially your point about how the devs seems to *NOT* test all releases on all OSes, your point about NoScript and global scripts is ignorant. The Tor devs, specifically Mike Perry, have addressed this point MANY times. TBB (via patches and TorButton) prevents malicious JavaScript code, so it's safe to use TBB with NoScript globally allowing all scripts.</p>
<p>That said, I for one disallow global scripts because I don't like ads and other junk on web pages; and for obscure/new JS attacks TBB can't prevent/mitigate. However, for non-English speakers and non-tech computer users, there would much confusion if TBB shipped NoScript to block all scripts, and the users may then decide "Tor's broken", and not try to figure out what's going on...and recent comments by Andrew wrt to his unofficial user testing recently bears out that point.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15376"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15376" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 03, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15331" class="permalink" rel="bookmark">While I agree with most of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15376">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15376" class="permalink" rel="bookmark">With Tor enabled javascript</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>With Tor enabled javascript may be less of an issue. But I have never understood the purpose of Tor button in TBB.<br />
It would be a lot more safe if TBB forced users to use Tor, i.e. no switch to turn Tor off as non-Tor mode and javascript on is dangerous. Make it clear that this browser is configured to use Tor and if the pre-configured settings/preferences are untampered with you are surfing safely and anonymously. I never ever use TBB with the proxy off.<br />
Much better to use a separate browser for non-Tor browsing rather than mixing them up. Not as smooth perhaps but it does lessen the risk of users who are non-technical doing something wrong. And security trumps smooth in any security solution.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-15345"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15345" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15297" class="permalink" rel="bookmark">Version 10 is utterly</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15345">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15345" class="permalink" rel="bookmark">&gt; default setting of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; default setting of Javascript "globally allowed" via the NoScript extension</p>
<p>"We configure NoScript to allow JavaScript by default in the Tor Browser Bundle because many websites will not work with JavaScript disabled. Most users would give up on Tor entirely if a website they want to use requires JavaScript, because they would not know how to allow a website to use JavaScript (or that enabling JavaScript might make a website work). "</p>
<p><a href="https://www.torproject.org/docs/faq.html.en#TBBJavaScriptEnabled" rel="nofollow">https://www.torproject.org/docs/faq.html.en#TBBJavaScriptEnabled</a></p>
<p>And allow JavaScript allows the user to blend with the crowd. The goal of Tor is to provide anonymity which loves company. Since the crowd uses JavaScript, allowing it makes you part of the crowd of normal users.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15347"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15347" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15297" class="permalink" rel="bookmark">Version 10 is utterly</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15347">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15347" class="permalink" rel="bookmark">Tor browser is TOTALLY</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor browser is TOTALLY UNUSABLE NOW. PLEASE FIX. I HAVE TRIED 8 9 AND 10.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-15299"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15299" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 01, 2012</p>
    </div>
    <a href="#comment-15299">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15299" class="permalink" rel="bookmark">snow lion eh? that&#039;s an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>snow lion eh? that's an animal i'd like to see.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15306"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15306" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 01, 2012</p>
    </div>
    <a href="#comment-15306">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15306" class="permalink" rel="bookmark">My browser still crashes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My browser still crashes regularly. I'm using Mac OS X 10.7.3</p>
<p>I find it crashes whenever I attempt to go back to a page viewed previously.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15309"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15309" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 01, 2012</p>
    </div>
    <a href="#comment-15309">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15309" class="permalink" rel="bookmark">Ouch! My faith in an end</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ouch! My faith in an end product is not enhanced by the presence of a simple error in a technical announcement: it's either 10.6 "Snow Leopard" or 10.7 "Lion" - there is no "Snow Lion". :(<br />
On the other hand: thanks for the swift update. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15310"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15310" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 01, 2012</p>
    </div>
    <a href="#comment-15310">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15310" class="permalink" rel="bookmark">Crashing randomly here</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Crashing randomly here :-(<br />
TBB 2.2.35-10 on Lion 10.7.3</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15312"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15312" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 01, 2012</p>
    </div>
    <a href="#comment-15312">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15312" class="permalink" rel="bookmark">still keeps crashing for me,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>still keeps crashing for me, whats up?</p>
<p>am running on 10.5.8 mac</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15313"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15313" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 01, 2012</p>
    </div>
    <a href="#comment-15313">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15313" class="permalink" rel="bookmark">Nope it&#039;s still crashing on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nope it's still crashing on lion</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15315"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15315" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 01, 2012</p>
    </div>
    <a href="#comment-15315">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15315" class="permalink" rel="bookmark">YAY thanks so much for this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>YAY thanks so much for this update!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15343"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15343" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15315" class="permalink" rel="bookmark">YAY thanks so much for this</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15343">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15343" class="permalink" rel="bookmark">i wouldn&#039;t be happy that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i wouldn't be happy that much as it crashes constantly infact u can't even have it running for 3 minutes :))</p>
<p>than it just crashes :)))</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-15316"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15316" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15316">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15316" class="permalink" rel="bookmark">Snow Lion?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Snow Lion?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15318"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15318" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15318">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15318" class="permalink" rel="bookmark">no more crashes on lion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>no more crashes on lion</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15321"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15321" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15321">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15321" class="permalink" rel="bookmark">&quot;Many pointed out (a long</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Many pointed out (a long time ago) that TBB versions (even those that work) install with a default setting of Javascript "globally allowed" via the NoScript extension. As a bundled security solution aimed at people who are not experts but need security, this is a disaster. Yet nothing has been done about it."</p>
<p>Jesus Christ bananas! Is this true? </p>
<p>Does turning Java off manually have any effect I'm wondering?</p>
<p>Snow Lion is simply a Lion playing the the Snow! Meoow!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15326"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15326" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15326">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15326" class="permalink" rel="bookmark">Still crashing here</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Still crashing here also<br />
Instead of trotting around the world telling people about tor why not stay at home<br />
and getting these packages tested and tested again?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15332"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15332" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15332">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15332" class="permalink" rel="bookmark">Tor really needs to FOCUS on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor really needs to FOCUS on a FEW products, otherwise all of your MANY products suffer, e.g., you should CONCENTRATE on:</p>
<p>a) Tor base code<br />
b) Solutions to Tor blocking, e.g., bridges and Obfuscate proxy<br />
c) Hidden Services (speed, connectivity, etc)<br />
d) TorBrowserBundle (incl. TorButton)<br />
e) user-outreach  (e.g., make a user forum, damn it!)<br />
f) Increasing node number and geographic diversity</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15335"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15335" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15335">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15335" class="permalink" rel="bookmark">Snow Lion - Wikipedia, the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Snow Lion - Wikipedia, the free encyclopedia<br />
en.wikipedia.org/wiki/Snow_Lion<br />
The Snow Lion, sometimes also Snowlion, (Tibetan: གངས་སེང་གེ་, Wylie: gangs seng ge; Chinese: 瑞獅; pinyin: ruìshī) is a celestial animal of Tibet.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15338"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15338" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15338">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15338" class="permalink" rel="bookmark">it still crashes for me as</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it still crashes for me as well 10.6.8 .. </p>
<p>Shame really</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15339"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15339" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15339">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15339" class="permalink" rel="bookmark">Uploaded &quot;FIX&quot; for 10. still</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Uploaded "FIX" for 10. still crashes, and always when I hit return to previous page.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15340"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15340" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15340">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15340" class="permalink" rel="bookmark">New fix for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>New fix for TorBrowser-2.2.35-10-osx-i386-en-US.zip  crashes on Mac OS X, version 10.6.8</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15341"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15341" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15341">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15341" class="permalink" rel="bookmark">I just used Tor for the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I just used Tor for the first time on my Mac Lion 10.7  and it crashed after several tries. Always when pressing "previous page" and usually after the second page I visit.Vidalia acts like its still on but the Tor icon is gone.<br />
﻿</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15348"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15348" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15348">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15348" class="permalink" rel="bookmark">As of May 2nd, 2012 7:21</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As of May 2nd, 2012 7:21 EST, OS X 64-bit browser-bundle fails verification.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15359"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15359" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2012</p>
    </div>
    <a href="#comment-15359">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15359" class="permalink" rel="bookmark">Mac Lion 10.7.3. Crashing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Mac Lion 10.7.3. Crashing after a search or two. Vidalia is still runner after the browser has crashed. This is a repeating event.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15386"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15386" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 03, 2012</p>
    </div>
    <a href="#comment-15386">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15386" class="permalink" rel="bookmark">Why is the new versions of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why is the new versions of TBB coming with Firefox instead of Aurora? What happened with Aurora that was used in several versions? why swtich?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15393"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15393" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 03, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-15386" class="permalink" rel="bookmark">Why is the new versions of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-15393">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15393" class="permalink" rel="bookmark">We&#039;ve never actually</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We've never actually switched. It's just a question of what name the browser uses when you compile it from source.</p>
<p><a href="https://www.torproject.org/docs/faq#TBBBrowserName" rel="nofollow">https://www.torproject.org/docs/faq#TBBBrowserName</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-15412"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15412" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 04, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-15412">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15412" class="permalink" rel="bookmark">I&#039;m using the pre-lastest</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm using the pre-lastest release of Tor Browser Bundle (2.2.35-8), with Firefox 11.0. It clearly says Mozilla Firefox at the top and when I press Help it says "About Firefox". It used to say Aurora at the top and "About Aurora" in Help in earlier versions.</p>
<p>Should it say Aurora in 2.2.35-8? Is there something wrong with my version?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-15407"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15407" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 03, 2012</p>
    </div>
    <a href="#comment-15407">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15407" class="permalink" rel="bookmark">you know I had downloaded</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>you know I had downloaded tor about a month or two ago for mac 10.6.8 and it was running great. I finally realize there is an update for the past couple days it's been there i've just ignored it, so i download it and now all the crashing.</p>
<p>me so sad :( Hope there are fixes soon</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15486"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15486" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 09, 2012</p>
    </div>
    <a href="#comment-15486">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15486" class="permalink" rel="bookmark">I&#039;ve been Tor Browser Bundle</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've been Tor Browser Bundle for a while. I sometimes have trouble with Firefox/Tor Browser not starting up properly after the Vidalia Control Panel is connected to the Tor network. So I would, after it connected to the Tor network, sometimes go to manually open tor browser (firefox) in the folders: FirefoxPortable -&gt; App  -&gt; Firefox  -&gt; tbb-firefox.exe . Everything seemed to be OK for a while as Firefox would start up. (but every time I turned it on like this it would should the Mozilla "Thanks for Upgrading" screen. I later found out that all the Bookmarks from my other (non tor) firefox were in the tbb-firefox. And the proxy settings were not activated!!!!! In Firefox: Tools  -&gt; Advanced  -&gt; Network  -&gt; Connection (Settings), the option "Use System Proxy Settings" was enabled, instead of "Manual proxy configuration"!!!!!! Is this a huge bug?!?! I went to Tor Check website and it wasn't routing my traffic through Tor! My privacy has been leaked because of something I don't understand. Can anyone help me please??????</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15504"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15504" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 10, 2012</p>
    </div>
    <a href="#comment-15504">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15504" class="permalink" rel="bookmark">Still crashing for me.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Still crashing for me.</p>
</div>
  </div>
</article>
<!-- Comment END -->
