title: Tor Weekly News — September 4th, 2013
---
pub_date: 2013-09-04
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the tenth issue of Tor Weekly News, the weekly newsletter that covers what is happening in the skyrocketing Tor community.</p>

<h1>Serious network overload</h1>

<blockquote><p>
&lt;borealis&gt; if it really is a coordinated attack from a bot twice the size of the regular tor network i'm much surprised tor is still usable at all
</p></blockquote>

<p>— borealis in #tor, 2013-09-02 18:38 UTC</p>

<p>The tremendous <a href="https://metrics.torproject.org/users.html?graph=direct-users&amp;start=2013-08-15&amp;end=2013-09-02#direct-users" rel="nofollow">influx of new clients that started mid-August</a> is stretching the current Tor network and software to its limits.</p>

<p>Several relay operators <a href="https://lists.torproject.org/pipermail/tor-relays/2013-August/002594.html" rel="nofollow">reported their relays to be saturated</a> by the amount of connections and circuits that <a href="https://lists.torproject.org/pipermail/tor-relays/2013-August/002589.html" rel="nofollow">relays currently have to handle</a>.</p>

<p>Mike Perry wishing to “compare load characteristics since 8/19 for nodes with different types of flags” issued a <a href="https://lists.torproject.org/pipermail/tor-relays/2013-August/002612.html" rel="nofollow">call to relay operators</a>: “especially useful [are] links/graph images for connection counts, bandwidth, and CPU load since 8/19.”</p>

<p>It was reported on IRC that on some relays, only one circuit was successfully created out of four attempts. This unfortunately implies that clients retry to build more circuits, resulting in even more load on Tor relays.</p>

<p>The tor 0.2.4 series introduced a new circuit extension handshake dubbed “<a href="https://gitweb.torproject.org/torspec.git/blob_plain/HEAD:/proposals/216-ntor-handshake.txt" rel="nofollow">ntor</a>”.  This new handshake is faster (especially on the relay side) than the original circuit extension handshake, “TAP”. Roger Dingledine came up with a <a href="https://bugs.torproject.org/9574#comment:10" rel="nofollow">patch to prioritize circuit creations using ntor over TAP</a>. Various observers reported that these overwhelming unidentified new clients were likely to be using Tor 0.2.3. Prioritizing ntor is then likely to make them less a burden for the network, and should help the network to function despite being overloaded by circuit creations.</p>

<p>Sathya and Isis both reported the patch to work. Nick Mathewson pointed out a <a href="https://bugs.torproject.org/9574#comment:12" rel="nofollow">few issues</a> in the current implementation but overall it looks like a band-aid good enough for the time being.</p>

<h1>Latest findings regarding traffic correlation attacks</h1>

<p>Erik de Castro Lopo <a href="https://lists.torproject.org/pipermail/tor-talk/2013-September/029755.html" rel="nofollow">pointed tor-talk readers</a> to a new well written paper named <a href="http://www.ohmygodel.com/publications/usersrouted-ccs13.pdf" rel="nofollow">Users Get Routed: Traffic Correlation on Tor by Realistic Adversaries</a>. To be presented at the upcoming <a href="http://www.sigsac.org/ccs/CCS2013/" rel="nofollow">CCS 2013 conference</a> this November in Berlin, Aaron Johnson, Chris Wacek, Rob Jansen, Micah Sherr, and Paul Syverson describe their experiments on traffic correlation attacks.</p>

<p>This research paper follows on a long series of earlier research papers to better understand how Tor is vulnerable to adversaries controlling portions of the Tor network or monitoring users and relays at the network level.</p>

<p>Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-talk/2013-September/029756.html" rel="nofollow">wrote to tor-talk readers</a>: “Yes, a big enough adversary can screw Tor users. But we knew that. I think it’s great that the paper presents the dual risks of relay adversaries and link adversaries, since most of the time when people are freaking out about one of them they’re forgetting the other one. And we really should raise the guard rotation period. If you do their compromise graphs again with guards rotated every nine months, they look way different.”</p>

<p>One tricky question with <a href="https://bugs.torproject.org/8240" rel="nofollow">raising guard rotation period</a> is: “How do we <a href="https://bugs.torproject.org/9321" rel="nofollow">keep clients properly balanced</a> to match the guard capacities?” It is also probably another signal for any Tails supporter that wishes to help implementing <a href="https://labs.riseup.net/code/issues/5462" rel="nofollow">guard persistence</a>.</p>

<p>“I have plans for writing a blog post about the paper, to explain what it means, what it doesn’t mean, what we should do about it, and what research questions remain open” wrote Roger. Let’s stay tuned!</p>

<h1>A peek inside the Pirate Browser</h1>

<p>Torrent-sharing website The Pirate Bay started shipping a custom browser — the Pirate Browser — on August 10th. They advertised using Tor to circumvent censorship but unfortunately did not provide any source code for their project.</p>

<p>Matt Pagan <a href="https://lists.torproject.org/pipermail/tor-talk/2013-August/029703.html" rel="nofollow">examined the contents of the package</a> in order to get a better idea of what it was. He compared the contents of the Pirate Browser 0.6b archive using cryptographic checksums to the contents of the Tor Browser Bundle 2.3.25-12 (en-US version).</p>

<p>According to Matt’s findings the Pirate Browser includes unmodified versions of tor 0.2.3.25 and Vidalia 0.2.20. The tor configuration contains slight deviation from the one shipped with the Tor Browser Bundle. One section labeled “Configured for speed” unfortunately shows wrong understanding of the Tor network. Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-talk/2013-August/029729.html" rel="nofollow">commented in a subsequent email</a>: “Just for the record, the three lines here don’t help speed much (or maybe at all).”</p>

<p>The remaining configuration change that “probably has the biggest impact on performance“, according to Roger, excludes exit nodes from Denmark, Ireland, United Kindgom, the Netherlands, Belgium, Italy, China, Iran, Finland, and Norway. “Whether it improves or reduces performance [Roger] cannot say, though. Depends on a lot of complex variables around Internet topologies.”</p>

<p>The browser itself is based of Firefox 23.0, with FoxyProxy configured to use Tor only for a <a href="http://piratebrowser.com/piratebrowser_patterns.json" rel="nofollow">few specific addresses</a>, and a few extra bookmarks.</p>

<p>Later, Matt <a href="https://lists.torproject.org/pipermail/tor-talk/2013-August/029707.html" rel="nofollow">also highlighted</a> that some important extensions of the Tor Browser, namely HTTPS Everywhere, NoScript, and Torbutton were also missing from the Pirate Browser.</p>

<p>In any cases, the Pirate Browser is unlikely to explain the sudden influx of new Tor clients. grarpamp forwarded <a href="https://lists.torproject.org/pipermail/tor-talk/2013-August/029736.html" rel="nofollow">an email exchanged with the Pirate Browser admin contact</a> which shows that numbers (550 000 known direct downloads) and dates (“most downloads during the first week”) do not match.</p>

<h1>Monthly status reports for August 2013</h1>

<p>The wave of regular monthly reports from Tor project members for the month of August has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000314.html" rel="nofollow">Sherief Alaa released his report first</a>, followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000315.html" rel="nofollow">George Kadianakis</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000316.html" rel="nofollow">Lunar</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000317.html" rel="nofollow">Arturo Filastò</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000318.html" rel="nofollow">Colin C.</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000319.html" rel="nofollow">Arlo Breault</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000320.html" rel="nofollow">Philipp Winter</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000321.html" rel="nofollow">Roger Dingledine</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000322.html" rel="nofollow">Karsten Loesing</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000323.html" rel="nofollow">Isis Lovecruft</a>.  The latter also caught up with <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000324.html" rel="nofollow">June</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2013-September/000325.html" rel="nofollow">July</a>.</p>

<h1>Help Desk Roundup</h1>

<p>This week Tor help desk saw an increase in the number of users wanting to download or install Orbot. Orbot can be downloaded from the Google Play store, the Amazon App store, f-droid.org, and guardianproject.info. Guides on using Orbot can be found on the <a href="https://guardianproject.info/apps/orbot/" rel="nofollow">Guardian Project’s Orbot page</a>, or on the <a href="https://www.torproject.org/docs/android.html" rel="nofollow">Tor Project’s Android page</a>. It looks like Orbot is currently inaccessible from the Google Play store in Iran.  Please join the <a href="https://lists.torproject.org/pipermail/tor-talk/2013-August/029684.html" rel="nofollow">discussion on tor-talk</a> if you have input about the latter.</p>

<p>All versions of the Tor Browser Bundle which include tor 0.2.4.x have been reported to work in Iran. This includes the latest Pluggable Transport Bundle, the 3.0 alpha series, and the 2.4 beta series. Follow <a href="https://fa-blog.torproject.org/" rel="nofollow">our Farsi blog</a> for more Iran related news.</p>

<h1>Miscellaneous news</h1>

<p>The <a href="https://mailman.boum.org/pipermail/tails-dev/2013-August/003523.html" rel="nofollow">next Tails contributors meeting</a> will happen on IRC on September 4th at 8pm UTC (10pm CEST). “Every one interested in contributing to Tails is welcome” to join #tails-dev on the OFTC network.</p>

<p>Yawning Angel has been “designing a UDP based protocol to serve as the bulk data transport for something along the lines of ‘obfs3, but over UDP’.” They are soliciting feedback on their <a href="https://lists.torproject.org/pipermail/tor-dev/2013-August/005334.html" rel="nofollow">initial draft of the Lightweight Obfuscated Datagram Protocol (LODP)</a>.</p>

<p>Kévin Dunglas <a href="https://lists.torproject.org/pipermail/tor-dev/2013-August/005340.html" rel="nofollow">announced</a> their work on a <a href="https://github.com/dunglas/php-torcontrol/" rel="nofollow">PHP library for the Tor Control Port</a>, released under the MIT license.</p>

<p>Kathy Brade and Mark Smith have <a href="https://bugs.torproject.org/4234#comment:19" rel="nofollow">released a first patch</a> for Mozilla’s update mechanism which “successfully updated TBB on Linux, Windows, and Mac OS ‘in the lab’ using both incremental and ‘full replace’ updates.” This is meant for the 3.x series of the Tor Browser Bundle and is still a work a progress, but this is a significant milestone toward streamlined updates for TBB users.</p>

<p>Erinn Clark <a href="https://lists.torproject.org/pipermail/tor-dev/2013-August/005328.html" rel="nofollow">announced</a> that the software powering trac.torproject.org has been upgraded to version 0.12.3. Among several other improvements, this new version allowed Erinn to <a href="https://lists.torproject.org/pipermail/tor-dev/2013-September/005346.html" rel="nofollow">experiment with the often requested Git integration</a>.</p>

<p>David Goulet has <a href="https://lists.torproject.org/pipermail/tor-dev/2013-September/005359.html" rel="nofollow">released the second release candidate</a> for the 2.0 rewrite of Torsocks [43]: “Please continue to test, review and contribute it!” </p>

<p>Much to her surprise, Erinn Clark found a “<a href="https://lists.torproject.org/pipermail/tor-dev/2013-September/005348.html" rel="nofollow">fraudulent PGP key with [her] email address</a>” on the keyservers. “Do not under any circumstances trust anything that may have ever been signed or encrypted with this key” of short id 0xCEE1590D. She reminded that the Tor Project official signatures are <a href="https://www.torproject.org/docs/signing-keys.html" rel="nofollow">listed on the project’s website</a>.</p>

<p>Philipp Winter published <a href="http://www.cs.kau.se/philwint/pdf/wpes2013.pdf" rel="nofollow">the final paper version</a> of the <a href="http://www.cs.kau.se/philwint/scramblesuit/" rel="nofollow">ScrambleSuit pluggable transport</a>, dubbed “A Polymorphic Network Protocol to Circumvent Censorship”.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, dope457, mttp, malaparte, mrphs, bastik, Karsten Loesing, and Roger Dingledine.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

