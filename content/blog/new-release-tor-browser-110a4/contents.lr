title: New Release: Tor Browser 11.0a4
---
pub_date: 2021-08-17
---
author: sysrqb
---
tags:

tor browser
tbb-11.0
tbb
---
categories: applications
---
summary: Tor Browser 11.0a4 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 11.0a4 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/11.0a4/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the latest stable <a href="https://blog.torproject.org/new-release-tor-browser-1054">Windows/macOS/Linux</a> or <a href="https://blog.torproject.org/new-release-tor-browser-1053">Android</a> release instead.</p>
<p>This version updates <b>Firefox</b> to version 78.13.0esr on Windows, macOS, and Linux, and Firefox to version 91.1.0 on Android. This version includes important security updates to Firefox on <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-34/">Windows, macOS, and Linux</a>, and <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-33/">Android</a>.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser Alpha does <em><b>not</b> support <u>version 2 onion services</u></em>. Tor Browser (Stable) will <em><b>stop</b> supporting <u>version 2 onion services</u></em> <em><b>very soon</b></em>. Please see the <a href="https://support.torproject.org/onionservices/v2-deprecation/">deprecation F.A.Q.</a> entry regarding Tor version 0.4.6. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master"> Tor Browser 11.0a3:</a></p>
<ul>
<li>All Platforms</li>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.13.0esr</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40041">Bug 40041</a>: Remove V2 Deprecation banner on about:tor for desktop</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40534">Bug 40534</a>: Cannot open URLs on command line with Tor Browser 10.5</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40547">Bug 40547</a>: UX: starting in offline mode can result in difficulty to connect later</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40561">Bug 40561</a>: Refactor about:torconnect implementation</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40567">Bug 40567</a>: RFPHelper is not init until after about:torconnect bootstraps</li>
</ul>
</li>
<li>Android
<ul>
<li>Update Fenix to 91.1.0</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40186">Bug 40186</a>: Hide Credit Cards in Settings</li>
</ul>
</li>
<li>Build System
<ul>
<li>All Platforms
<ul>
<li>Update Go to 1.16.7</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292626"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292626" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 18, 2021</p>
    </div>
    <a href="#comment-292626">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292626" class="permalink" rel="bookmark">https://forums.informaction…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://forums.informaction.com/viewtopic.php?f=10&amp;t=26404" rel="nofollow">https://forums.informaction.com/viewtopic.php?f=10&amp;t=26404</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
