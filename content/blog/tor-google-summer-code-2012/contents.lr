title: Tor in Google Summer of Code 2012
---
pub_date: 2012-03-17
---
author: atagar
---
tags:

gsoc
gsoc2012
---
categories: internships
---
_html_body:

<p>Interested in coding on Tor and getting paid for it by Google? If you are a student, we have good news for you: We have been <a href="https://www.google-melange.com/gsoc/accepted_orgs/google/gsoc2012" rel="nofollow">accepted</a> as a mentoring organization for <a href="https://www.google-melange.com/gsoc/homepage/google/gsoc2012" rel="nofollow">Google Summer of Code 2012</a> together with <a href="https://www.eff.org/" rel="nofollow">The Electronic Frontier Foundation</a>! Woo!</p>

<p>Here are the facts: The summer of code gives you the opportunity to work on your own Tor-related coding project with one of the Tor developers as your mentor. You can apply for a coding project related to Tor itself or to one of its many <a href="https://www.torproject.org/getinvolved/volunteer.html.en#Projects" rel="nofollow">supplemental projects</a> (as well as for an <a href="https://trac.torproject.org/projects/tor/wiki/doc/HTTPSEverywhere/GSOC-2012" rel="nofollow">EFF-related project</a>). Your mentor will help you when you're stuck with your project and guide you in becoming part of the Tor community. Google pays you 5,000 USD for the three months of your project, so that you can focus on coding and don't have to worry about how to pay your bills.</p>

<p>Did we catch your attention? These are your next steps: Go look at the <a href="https://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2012/faqs" rel="nofollow">Google Summer of Code FAQ</a> to make sure you are eligible to participate. Have a look at our <a href="https://www.torproject.org/about/gsoc.html.en#Ideas" rel="nofollow">ideas list</a> to see if one of those projects matches your interests. If there is no project on that list that you'd want to work on, read the <a href="https://www.torproject.org/documentation.html.en" rel="nofollow">documentation on our website</a> and make up your own project idea! Come to <a href="https://www.torproject.org/about/contact.html.en#irc" rel="nofollow">#tor-dev on OFTC</a> and let us know about your project idea. Communication is essential to success in the summer of code, and we're unlikely to accept students we haven't heard from before reading their application. So really, come to the IRC channel and talk to us!</p>

<p>Finally, write down your project idea using our <a href="https://www.torproject.org/about/gsoc.html.en#Template" rel="nofollow">template</a> and <a href="https://www.google-melange.com/gsoc/document/show/gsoc_program/google/gsoc2012/faqs#student_apply" rel="nofollow">submit your application</a> to Google until <a href="https://www.google-melange.com/gsoc/events/google/gsoc2012" rel="nofollow">April 6, 2012</a>.</p>

<p>We are looking forward to discussing your project idea with you!</p>

---
_comments:

<a id="comment-14965"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14965" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 12, 2012</p>
    </div>
    <a href="#comment-14965">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14965" class="permalink" rel="bookmark">Maybe I am just paranoid,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Maybe I am just paranoid, but wouldn't working with Google be akin to sleeping with the enemy?</p>
</div>
  </div>
</article>
<!-- Comment END -->
