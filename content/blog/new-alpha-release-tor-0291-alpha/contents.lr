title: New alpha release: Tor 0.2.9.1-alpha
---
pub_date: 2016-08-08
---
author: nickm
---
tags:

tor
alpha
release
unstable
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.9.1-alpha is the first alpha release in the 0.2.9 development series. It improves our support for hardened builds and compiler warnings, deploys some critical infrastructure for improvements to hidden services, includes a new timing backend that we hope to use for better support for traffic padding, makes it easier for programmers to log unexpected events, and contains other small improvements to security, correctness, and performance.<br />
You can download the source from the usual place on the website.<br />
Packages should be available over the next several days. Remember<br />
to check the signatures!</p>

<p>Please note: This is an alpha release. You should only try this one if<br />
you are interested in tracking Tor development, testing new features,<br />
making sure that Tor still builds on unusual platforms, or generally<br />
trying to hunt down bugs. If you want a stable experience, please stick to the stable releases.</p>

<p>Below are the changes since 0.2.8.6.</p>

<h2>Changes in version 0.2.9.1-alpha - 2016-08-08</h2>

<ul>
<li>New system requirements:
<ul>
<li>Tor now requires Libevent version 2.0.10-stable or later. Older versions of Libevent have less efficient backends for several platforms, and lack the DNS code that we use for our server-side DNS support. This implements ticket <a href="https://bugs.torproject.org/19554" rel="nofollow">19554</a>.
  </li>
<li>Tor now requires zlib version 1.2 or later, for security, efficiency, and (eventually) gzip support. (Back when we started, zlib 1.1 and zlib 1.0 were still found in the wild. 1.2 was released in 2003. We recommend the latest version.)
  </li>
</ul>
</li>
<li>Major features (build, hardening):
<ul>
<li>Tor now builds with -ftrapv by default on compilers that support it. This option detects signed integer overflow (which C forbids), and turns it into a hard-failure. We do not apply this option to code that needs to run in constant time to avoid side-channels; instead, we use -fwrapv in that code. Closes ticket <a href="https://bugs.torproject.org/17983" rel="nofollow">17983</a>.
  </li>
<li>When --enable-expensive-hardening is selected, stop applying the clang/gcc sanitizers to code that needs to run in constant time. Although we are aware of no introduced side-channels, we are not able to prove that there are none. Related to ticket <a href="https://bugs.torproject.org/17983" rel="nofollow">17983</a>.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major features (compilation):
<ul>
<li>Our big list of extra GCC warnings is now enabled by default when building with GCC (or with anything like Clang that claims to be GCC-compatible). To make all warnings into fatal compilation errors, pass --enable-fatal-warnings to configure. Closes ticket <a href="https://bugs.torproject.org/19044" rel="nofollow">19044</a>.
  </li>
<li>Use the Autoconf macro AC_USE_SYSTEM_EXTENSIONS to automatically turn on C and POSIX extensions. (Previously, we attempted to do this on an ad hoc basis.) Closes ticket <a href="https://bugs.torproject.org/19139" rel="nofollow">19139</a>.
  </li>
</ul>
</li>
<li>Major features (directory authorities, hidden services):
<ul>
<li>Directory authorities can now perform the shared randomness protocol specified by proposal 250. Using this protocol, directory authorities generate a global fresh random value every day. In the future, this value will be used by hidden services to select HSDirs. This release implements the directory authority feature; the hidden service side will be implemented in the future as part of proposal 224. Resolves ticket <a href="https://bugs.torproject.org/16943" rel="nofollow">16943</a>; implements proposal 250.
  </li>
</ul>
</li>
<li>Major features (downloading, random exponential backoff):
<ul>
<li>When we fail to download an object from a directory service, wait for an (exponentially increasing) randomized amount of time before retrying, rather than a fixed interval as we did before. This prevents a group of Tor instances from becoming too synchronized, or a single Tor instance from becoming too predictable, in its download schedule. Closes ticket <a href="https://bugs.torproject.org/15942" rel="nofollow">15942</a>.
  </li>
</ul>
</li>
<li>Major bugfixes (exit policies):
<ul>
<li>Avoid disclosing exit outbound bind addresses, configured port bind addresses, and local interface addresses in relay descriptors by default under ExitPolicyRejectPrivate. Instead, only reject these (otherwise unlisted) addresses if ExitPolicyRejectLocalInterfaces is set. Fixes bug <a href="https://bugs.torproject.org/18456" rel="nofollow">18456</a>; bugfix on 0.2.7.2-alpha. Patch by teor.
  </li>
</ul>
</li>
<li>Major bugfixes (hidden service client):
<ul>
<li>Allow Tor clients with appropriate controllers to work with FetchHidServDescriptors set to 0. Previously, this option also disabled descriptor cache lookup, thus breaking hidden services entirely. Fixes bug <a href="https://bugs.torproject.org/18704" rel="nofollow">18704</a>; bugfix on 0.2.0.20-rc. Patch by "twim".
  </li>
</ul>
</li>
<li>Minor features (build, hardening):
<ul>
<li>Detect and work around a libclang_rt problem that would prevent clang from finding __mulodi4() on some 32-bit platforms, and thus keep -ftrapv from linking on those systems. Closes ticket <a href="https://bugs.torproject.org/19079" rel="nofollow">19079</a>.
  </li>
<li>When building on a system without runtime support for the runtime hardening options, try to log a useful warning at configuration time, rather than an incomprehensible warning at link time. If expensive hardening was requested, this warning becomes an error. Closes ticket <a href="https://bugs.torproject.org/18895" rel="nofollow">18895</a>.
  </li>
</ul>
</li>
<li>Minor features (code safety):
<ul>
<li>In our integer-parsing functions, ensure that maxiumum value we give is no smaller than the minimum value. Closes ticket <a href="https://bugs.torproject.org/19063" rel="nofollow">19063</a>; patch from U+039b.
  </li>
</ul>
</li>
<li>Minor features (controller):
<ul>
<li>Implement new GETINFO queries for all downloads that use download_status_t to schedule retries. This allows controllers to examine the schedule for pending downloads. Closes ticket <a href="https://bugs.torproject.org/19323" rel="nofollow">19323</a>.
  </li>
<li>Allow controllers to configure basic client authorization on hidden services when they create them with the ADD_ONION control command. Implements ticket <a href="https://bugs.torproject.org/15588" rel="nofollow">15588</a>. Patch by "special".
  </li>
<li>Fire a STATUS_SERVER controller event whenever the hibernation status changes between "awake"/"soft"/"hard". Closes ticket <a href="https://bugs.torproject.org/18685" rel="nofollow">18685</a>.
  </li>
</ul>
</li>
<li>Minor features (directory authority):
<ul>
<li>Directory authorities now only give the Guard flag to a relay if they are also giving it the Stable flag. This change allows us to simplify path selection for clients. It should have minimal effect in practice, since &gt;99% of Guards already have the Stable flag. Implements ticket <a href="https://bugs.torproject.org/18624" rel="nofollow">18624</a>.
  </li>
<li>Directory authorities now write their v3-status-votes file out to disk earlier in the consensus process, so we have a record of the votes even if we abort the consensus process. Resolves ticket <a href="https://bugs.torproject.org/19036" rel="nofollow">19036</a>.
  </li>
</ul>
</li>
<li>Minor features (hidden service):
<ul>
<li>Stop being so strict about the payload length of "rendezvous1" cells. We used to be locked in to the "TAP" handshake length, and now we can handle better handshakes like "ntor". Resolves ticket <a href="https://bugs.torproject.org/18998" rel="nofollow">18998</a>.
  </li>
</ul>
</li>
<li>Minor features (infrastructure, time):
<ul>
<li>Tor now uses the operating system's monotonic timers (where available) for internal fine-grained timing. Previously we would look at the system clock, and then attempt to compensate for the clock running backwards. Closes ticket <a href="https://bugs.torproject.org/18908" rel="nofollow">18908</a>.
  </li>
<li>Tor now includes an improved timer backend, so that we can efficiently support tens or hundreds of thousands of concurrent timers, as will be needed for some of our planned anti-traffic- analysis work. This code is based on William Ahern's "timeout.c" project, which implements a "tickless hierarchical timing wheel". Closes ticket <a href="https://bugs.torproject.org/18365" rel="nofollow">18365</a>.
  </li>
</ul>
</li>
<li>Minor features (logging):
<ul>
<li>Provide a more useful warning message when configured with an invalid Nickname. Closes ticket <a href="https://bugs.torproject.org/18300" rel="nofollow">18300</a>; patch from "icanhasaccount".
  </li>
<li>When dumping unparseable router descriptors, optionally store them in separate files, named by digest, up to a configurable size limit. You can change the size limit by setting the MaxUnparseableDescSizeToLog option, and disable this feature by setting that option to 0. Closes ticket <a href="https://bugs.torproject.org/18322" rel="nofollow">18322</a>.
  </li>
<li>Add a set of macros to check nonfatal assertions, for internal use. Migrating more of our checks to these should help us avoid needless crash bugs. Closes ticket <a href="https://bugs.torproject.org/18613" rel="nofollow">18613</a>.
  </li>
</ul>
</li>
<li>Minor features (performance):
<ul>
<li>Changer the "optimistic data" extension from "off by default" to "on by default". The default was ordinarily overridden by a consensus option, but when clients were bootstrapping for the first time, they would not have a consensus to get the option from. Changing this default When fetching a consensus for the first time, use optimistic data. This saves a round-trip during startup. Closes ticket <a href="https://bugs.torproject.org/18815" rel="nofollow">18815</a>.
  </li>
</ul>
</li>
<li>Minor features (relay, usability):
<ul>
<li>When the directory authorities refuse a bad relay's descriptor, encourage the relay operator to contact us. Many relay operators won't notice this line in their logs, but it's a win if even a few learn why we don't like what their relay was doing. Resolves ticket <a href="https://bugs.torproject.org/18760" rel="nofollow">18760</a>.
  </li>
</ul>
</li>
<li>Minor features (testing):
<ul>
<li>Let backtrace tests work correctly under AddressSanitizer. Fixes part of bug <a href="https://bugs.torproject.org/18934" rel="nofollow">18934</a>; bugfix on 0.2.5.2-alpha.
  </li>
<li>Move the test-network.sh script to chutney, and modify tor's test- network.sh to call the (newer) chutney version when available. Resolves ticket <a href="https://bugs.torproject.org/19116" rel="nofollow">19116</a>. Patch by teor.
  </li>
<li>Use the lcov convention for marking lines as unreachable, so that we don't count them when we're generating test coverage data. Update our coverage tools to understand this convention. Closes ticket <a href="https://bugs.torproject.org/16792" rel="nofollow">16792</a>.
  </li>
</ul>
</li>
<li>Minor bugfixes (bootstrap):
<ul>
<li>Remember the directory we fetched the consensus or previous certificates from, and use it to fetch future authority certificates. This change improves bootstrapping performance. Fixes bug <a href="https://bugs.torproject.org/18963" rel="nofollow">18963</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (build):
<ul>
<li>The test-stem and test-network makefile targets now depend only on the tor binary that they are testing. Previously, they depended on "make all". Fixes bug <a href="https://bugs.torproject.org/18240" rel="nofollow">18240</a>; bugfix on 0.2.8.2-alpha. Based on a patch from "cypherpunks".
  </li>
</ul>
</li>
<li>Minor bugfixes (circuits):
<ul>
<li>Make sure extend_info_from_router() is only called on servers. Fixes bug <a href="https://bugs.torproject.org/19639" rel="nofollow">19639</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>When building with Clang, use a full set of GCC warnings. (Previously, we included only a subset, because of the way we detected them.) Fixes bug <a href="https://bugs.torproject.org/19216" rel="nofollow">19216</a>; bugfix on 0.2.0.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (directory authority):
<ul>
<li>Authorities now sort the "package" lines in their votes, for ease of debugging. (They are already sorted in consensus documents.) Fixes bug <a href="https://bugs.torproject.org/18840" rel="nofollow">18840</a>; bugfix on 0.2.6.3-alpha.
  </li>
<li>When parsing a detached signature, make sure we use the length of the digest algorithm instead of an hardcoded DIGEST256_LEN in order to avoid comparing bytes out-of-bounds with a smaller digest length such as SHA1. Fixes bug <a href="https://bugs.torproject.org/19066" rel="nofollow">19066</a>; bugfix on 0.2.2.6-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (documentation):
<ul>
<li>Document the --passphrase-fd option in the tor manpage. Fixes bug <a href="https://bugs.torproject.org/19504" rel="nofollow">19504</a>; bugfix on 0.2.7.3-rc.
  </li>
<li>Fix the description of the --passphrase-fd option in the tor-gencert manpage. The option is used to pass the number of a file descriptor to read the passphrase from, not to read the file descriptor from. Fixes bug <a href="https://bugs.torproject.org/19505" rel="nofollow">19505</a>; bugfix on 0.2.0.20-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (ephemeral hidden service):
<ul>
<li>When deleting an ephemeral hidden service, close its intro points even if they are not completely open. Fixes bug <a href="https://bugs.torproject.org/18604" rel="nofollow">18604</a>; bugfix on 0.2.7.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (guard selection):
<ul>
<li>Use a single entry guard even if the NumEntryGuards consensus parameter is not provided. Fixes bug <a href="https://bugs.torproject.org/17688" rel="nofollow">17688</a>; bugfix on 0.2.5.6-alpha.
  </li>
<li>Don't mark guards as unreachable if connection_connect() fails. That function fails for local reasons, so it shouldn't reveal anything about the status of the guard. Fixes bug <a href="https://bugs.torproject.org/14334" rel="nofollow">14334</a>; bugfix on 0.2.3.10-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden service client):
<ul>
<li>Increase the minimum number of internal circuits we preemptively build from 2 to 3, so a circuit is available when a client connects to another onion service. Fixes bug <a href="https://bugs.torproject.org/13239" rel="nofollow">13239</a>; bugfix on 0.1.0.1-rc.
  </li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>When logging a directory ownership mismatch, log the owning username correctly. Fixes bug <a href="https://bugs.torproject.org/19578" rel="nofollow">19578</a>; bugfix on 0.2.2.29-beta.
  </li>
</ul>
</li>
<li>Minor bugfixes (memory leaks):
<ul>
<li>Fix a small, uncommon memory leak that could occur when reading a truncated ed25519 key file. Fixes bug <a href="https://bugs.torproject.org/18956" rel="nofollow">18956</a>; bugfix on 0.2.6.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Allow clients to retry HSDirs much faster in test networks. Fixes bug <a href="https://bugs.torproject.org/19702" rel="nofollow">19702</a>; bugfix on 0.2.7.1-alpha. Patch by teor.
  </li>
<li>Disable ASAN's detection of segmentation faults while running test_bt.sh, so that we can make sure that our own backtrace generation code works. Fixes another aspect of bug <a href="https://bugs.torproject.org/18934" rel="nofollow">18934</a>; bugfix on 0.2.5.2-alpha. Patch from "cypherpunks".
  </li>
<li>Fix the test-network-all target on out-of-tree builds by using the correct path to the test driver script. Fixes bug <a href="https://bugs.torproject.org/19421" rel="nofollow">19421</a>; bugfix on 0.2.7.3-rc.
  </li>
</ul>
</li>
<li>Minor bugfixes (time):
<ul>
<li>Improve overflow checks in tv_udiff and tv_mdiff. Fixes bug <a href="https://bugs.torproject.org/19483" rel="nofollow">19483</a>; bugfix on all released tor versions.
  </li>
<li>When computing the difference between two times in milliseconds, we now round to the nearest millisecond correctly. Previously, we could sometimes round in the wrong direction. Fixes bug <a href="https://bugs.torproject.org/19428" rel="nofollow">19428</a>; bugfix on 0.2.2.2-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (user interface):
<ul>
<li>Display a more accurate number of suppressed messages in the log rate-limiter. Previously, there was a potential integer overflow in the counter. Now, if the number of messages hits a maximum, the rate-limiter doesn't count any further. Fixes bug <a href="https://bugs.torproject.org/19435" rel="nofollow">19435</a>; bugfix on 0.2.4.11-alpha.
  </li>
<li>Fix a typo in the passphrase prompt for the ed25519 identity key. Fixes bug <a href="https://bugs.torproject.org/19503" rel="nofollow">19503</a>; bugfix on 0.2.7.2-alpha.
  </li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Remove redundant declarations of the MIN macro. Closes ticket <a href="https://bugs.torproject.org/18889" rel="nofollow">18889</a>.
  </li>
<li>Rename tor_dup_addr() to tor_addr_to_str_dup() to avoid confusion. Closes ticket <a href="https://bugs.torproject.org/18462" rel="nofollow">18462</a>; patch from "icanhasaccount".
  </li>
<li>Split the 600-line directory_handle_command_get function into separate functions for different URL types. Closes ticket <a href="https://bugs.torproject.org/16698" rel="nofollow">16698</a>.
  </li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Fix spelling of "--enable-tor2web-mode" in the manpage. Closes ticket <a href="https://bugs.torproject.org/19153" rel="nofollow">19153</a>. Patch from "U+039b".
  </li>
</ul>
</li>
<li>Removed features:
<ul>
<li>Remove support for "GET /tor/bytes.txt" DirPort request, and "GETINFO dir-usage" controller request, which were only available via a compile-time option in Tor anyway. Feature was added in 0.2.2.1-alpha. Resolves ticket <a href="https://bugs.torproject.org/19035" rel="nofollow">19035</a>.
  </li>
<li>There is no longer a compile-time option to disable support for TransPort. (If you don't want TransPort; just don't use it.) Patch from "U+039b". Closes ticket <a href="https://bugs.torproject.org/19449" rel="nofollow">19449</a>.
  </li>
</ul>
</li>
<li>Testing:
<ul>
<li>Run more workqueue tests as part of "make check". These had previously been implemented, but you needed to know special command-line options to enable them.
  </li>
<li>We now have unit tests for our code to reject zlib "compression bombs". (Fortunately, the code works fine.)
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-198014"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-198014" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 09, 2016</p>
    </div>
    <a href="#comment-198014">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-198014" class="permalink" rel="bookmark">Now this is getting</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Now this is getting exciting! Maybe a blog post to detail the traffic padding research? I know the lack of padding has always been a weak point as tor was not intended to defend against a global passive adversary. Perhaps the time has come.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-198742"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-198742" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 12, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-198014" class="permalink" rel="bookmark">Now this is getting</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-198742">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-198742" class="permalink" rel="bookmark">Padding. Now there&#039;s a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Padding. Now there's a concept whose time has come.  It's been kicked around the remailer fraternity too for many years because padding will completely befuddle those who regard - in the true sense of what "regard" means.</p>
<p>I didn't know the TOR researchers were coding padding into TOR.  While remailers have latency to confuse the global passive adversary (NSA) TOR does not and while remailers are technically challenging and therefore have users only in the thousands, TOR has users in the millions and minimal use is not at all challenging.  Therefore, padding is simply a necessity for TOR much more so than other non-crypto hardening efforts.  </p>
<p>I second the OP and request a blog post on padding research.  Please.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-202234"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-202234" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 23, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-198014" class="permalink" rel="bookmark">Now this is getting</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-202234">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-202234" class="permalink" rel="bookmark">I like to know, why TOR is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I like to know, why TOR is forcing me to go through that Ukraine 185.61.138.18 IP again and again?? even when i am using new tor circuit function :P</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-202582"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-202582" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 24, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-202234" class="permalink" rel="bookmark">I like to know, why TOR is</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-202582">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-202582" class="permalink" rel="bookmark">Do you mean the first node</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you mean the first node always stays the same while the other two get changed?</p>
<p>The first node is your Guard Node, It changes infrequently (constant for months) so that an adversary has less chance of catching you in traffic analysis using a small number of bad nodes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-198125"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-198125" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 09, 2016</p>
    </div>
    <a href="#comment-198125">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-198125" class="permalink" rel="bookmark">congrats on another</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>congrats on another excellent release!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-198254"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-198254" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 09, 2016</p>
    </div>
    <a href="#comment-198254">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-198254" class="permalink" rel="bookmark">need a really good messaging</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>need a really good messaging app like tor messenger to communicate through censored isp.Please focus on improving tor messenger.<br />
<a href="https://trac.torproject.org/projects/tor/wiki/doc/TorMessenger" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/doc/TorMessenger</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-199613"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-199613" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  dcf
  </article>
    <div class="comment-header">
      <p class="comment__submitted">dcf said:</p>
      <p class="date-time">August 16, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-198254" class="permalink" rel="bookmark">need a really good messaging</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-199613">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-199613" class="permalink" rel="bookmark">Tor Messenger can use</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Messenger can use <a href="https://www.torproject.org/docs/pluggable-transports.html.en#user" rel="nofollow">pluggable transports</a> just like Tor Browser. You might have to interrupt it while it is starting up by clicking the "Open settings" button.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-198448"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-198448" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 10, 2016</p>
    </div>
    <a href="#comment-198448">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-198448" class="permalink" rel="bookmark">Smart people! Many thanks!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Smart people! Many thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-198522"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-198522" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 11, 2016</p>
    </div>
    <a href="#comment-198522">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-198522" class="permalink" rel="bookmark">http://www.theregister.co.uk/</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://www.theregister.co.uk/2016/08/10/linux_tor_users_open_corrupted_communications/" rel="nofollow">http://www.theregister.co.uk/2016/08/10/linux_tor_users_open_corrupted_…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-198595"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-198595" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 11, 2016</p>
    </div>
    <a href="#comment-198595">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-198595" class="permalink" rel="bookmark">When can we expect an alpha</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When can we expect an alpha release for replacing Tonga?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-199612"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-199612" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  dcf
  </article>
    <div class="comment-header">
      <p class="comment__submitted">dcf said:</p>
      <p class="date-time">August 16, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-198595" class="permalink" rel="bookmark">When can we expect an alpha</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-199612">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-199612" class="permalink" rel="bookmark">You can follow the progress</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can follow the progress here:<br />
<a href="https://bugs.torproject.org/19728" rel="nofollow">#19728 Pick, and deploy, a new bridge authority</a><br />
<a href="https://lists.torproject.org/pipermail/tor-project/2016-August/000636.html" rel="nofollow">[tor-project] New Bridge Authority</a> (click "Next message" to read the other messages in the thread)<br />
In short, a replacement is already running and being tested.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-199245"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-199245" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2016</p>
    </div>
    <a href="#comment-199245">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-199245" class="permalink" rel="bookmark">Pray tell : what is the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Pray tell : what is the current "user agent" string used by the Tor browser, in order to best mix in the (tiny) Tor-using-crowd ? Please no commets on (not) using the Tor browser bundle ...</p>
<p>Thanks for your continous efforts to make Tor better, looking froward to future advances in cell "padding" with excitement !</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-202455"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-202455" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 23, 2016</p>
    </div>
    <a href="#comment-202455">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-202455" class="permalink" rel="bookmark">great!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>great!</p>
</div>
  </div>
</article>
<!-- Comment END -->
