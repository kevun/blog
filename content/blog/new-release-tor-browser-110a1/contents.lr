title: New Release: Tor Browser 11.0a1 (Android Only)
---
pub_date: 2021-07-12
---
author: sysrqb
---
tags:

tor browser
tbb-11.0
tbb
---
categories: applications
---
summary: Tor Browser 11.0a1 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 11.0a1 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/11.0a1/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-1051">latest stable release</a> instead.</p>
<p>This version updates Fenix to 90.0.0-beta.6.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser Alpha does <em><b>not</b> support <u>version 2 onion services</u></em>. Tor Browser (Stable) will <em><b>stop</b> supporting <u>version 2 onion services</u></em> later <em><b>this year</b></em>. Please see the <a href="https://support.torproject.org/onionservices/v2-deprecation/">deprecation F.A.Q.</a> entry regarding Tor version 0.4.6. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master"> Tor Browser 10.5a17:</a></p>
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40172">Bug 40172</a>: Find the Quit button</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40173">Bug 40173</a>: Rebase fenix patches to fenix v90.0.0-beta.6</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40179">Bug 40179</a>: Show Snowflake bridge option on Release</li>
</ul>
</li>
<li>Build System
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40312">Bug 40312</a>: Update components for mozilla90</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40323">Bug 40323</a>: Remove unused gomobile project</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292282"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292282" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 13, 2021</p>
    </div>
    <a href="#comment-292282">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292282" class="permalink" rel="bookmark">Good job. Anonymity and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good job. Anonymity and privacy is very necessary these days.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292353"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292353" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>. (not verified)</span> said:</p>
      <p class="date-time">July 16, 2021</p>
    </div>
    <a href="#comment-292353">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292353" class="permalink" rel="bookmark">When is the next stable…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When is the next stable release?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292354"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292354" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 16, 2021</p>
    </div>
    <a href="#comment-292354">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292354" class="permalink" rel="bookmark">Current stable version is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Current stable version is leaking Accept-Language according to browserleaks.com/IP<br />
System language is EN-US, shows as EN-US and if I change language in Tor it still says EN-US along with whatever language I've selected. Isn't this a fingerprint and identity risk?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292390"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292390" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292354" class="permalink" rel="bookmark">Current stable version is…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292390">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292390" class="permalink" rel="bookmark">Do you have enabled the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you have enabled the setting "Request English versions of web pages for enhanced privacy"? This setting is on the Languages menu at the top.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-292355"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292355" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon (not verified)</span> said:</p>
      <p class="date-time">July 16, 2021</p>
    </div>
    <a href="#comment-292355">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292355" class="permalink" rel="bookmark">Why do I keep getting…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why do I keep getting unresponsive script warnings? The app only works properly on its first use, after that its always warnings about httpseverywhere. I have to erase all data and start over every time I want to browse without being spied on. Could anyone give advice what to do?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292392"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292392" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292355" class="permalink" rel="bookmark">Why do I keep getting…</a> by <span>Anon (not verified)</span></p>
    <a href="#comment-292392">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292392" class="permalink" rel="bookmark">Can you confirm you see this…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you confirm you see this issue: <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/32361" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/323…</a>?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292393"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292393" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-292393">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292393" class="permalink" rel="bookmark">Yes it is the same as what…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes it is the same as what user "LokiAstaroth" shows a photo of except it's for the HTTPS Everywhere add-on instead of whatever Pako is. It's been a known issue for over a year? I expected I was the odd one out likely due to idiocy but clearly not. Please get around to fixing it. It doesn't render the app unusable but it is a needless extra step and I worry many will click OK and continue browsing perhaps with insufficient protection due to broken scripts.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292398"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292398" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292393" class="permalink" rel="bookmark">Yes it is the same as what…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292398">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292398" class="permalink" rel="bookmark">Can you please provide the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you please provide the exact file name when you see it again?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292401"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292401" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-292401">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292401" class="permalink" rel="bookmark">Just managed to trigger it…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just managed to trigger it. I can't copy/paste what it says as the app won't let me. This is what it says.</p>
<p>Warning: Unresponsive script<br />
A script on this page may be busy, or it may have stopped responding. You can stop the script now, or you can continue to see if the script will complete.</p>
<p>Script: moz-extention://87ab41ed-6d15-...m/https_everywhere_lib_wasm.js317</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292405"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292405" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292401" class="permalink" rel="bookmark">Just managed to trigger it…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292405">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292405" class="permalink" rel="bookmark">Thank you. &quot;https_everywhere…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you. "https_everywhere_lib_wasm.js:317" is the part I was hoping you could provide.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292407"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292407" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-292407">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292407" class="permalink" rel="bookmark">Very welcome. If possible…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very welcome. If possible please try to work a fix or the next release, it must be affecting plenty of people, I know it's open and it can all be done at home but I'm clueless. Another minor point you might want whilst here is that changing theme doesn't work, if you click the option for dark design the app flashes for a second and remains purple. It's nothing anyone would care about but might be worth mentioning Incase it links up with other codes/scripts</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div></div></div><a id="comment-292368"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292368" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Perri (not verified)</span> said:</p>
      <p class="date-time">July 16, 2021</p>
    </div>
    <a href="#comment-292368">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292368" class="permalink" rel="bookmark">Why can&#039;t we fully disable…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why can't we fully disable cookies? Tor is set to only block third parties by default and you can't change it? Host cookies can be used to identify you and we should have cookies fully disabled at default</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292384"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292384" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292368" class="permalink" rel="bookmark">Why can&#039;t we fully disable…</a> by <span>Perri (not verified)</span></p>
    <a href="#comment-292384">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292384" class="permalink" rel="bookmark">The site can only identify…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The site can only identify you when you re-visit the same domain (as the first-party) who originally set the cookie, however when Javascript is not disabled then blocking first-party cookies would not prevent re-identification in this way because a web site can use local storage for a similar purpose. If you are concerned about a web site re-identifying you, then you should use the New Identity button or restart the browser before revisiting the site.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292388"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292388" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-292388">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292388" class="permalink" rel="bookmark">So so long as JavaScript it…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So so long as JavaScript it fully disabled as it should be in "safest" mode then cookies pose zero threat? I'd appreciate confirmation as I've had cookies breach VPN previously.<br />
Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292394"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292394" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292388" class="permalink" rel="bookmark">So so long as JavaScript it…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292394">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292394" class="permalink" rel="bookmark">No, when using Safest mode a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, when using Safest mode a cookie may be stored and a web site can potentially re-identify "you" if you reload the web page, or if you navigate away from that site and then return at a later time. A web site can not re-identify you using cookies after you restart the browser or use New Identity. Hopefully this is clearer.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292396"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292396" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-292396">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292396" class="permalink" rel="bookmark">Yes that is clearer…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes that is clearer. Provided that "You" contains no real information about the end user then it's fine. I do still believe we should be given the option of fully blocking if we want to. Does the desktop version also have this restriction? If not then behind Tor + 3rd party enabled = Tor on Android. Site and service hosts will be able to tell who is on mobile and who is on desktop. I also appreciate the speed of your responses, I must have caught you with no Cheeto dust on them fangers</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292399"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292399" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292396" class="permalink" rel="bookmark">Yes that is clearer…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292399">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292399" class="permalink" rel="bookmark">On Windows/macOS/Linux …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>On Windows/macOS/Linux (Stable/Release) and Android (Alpha), you can adjust the preference in about:config: `network.cookie.cookieBehavior` to 2. <a href="https://wiki.mozilla.org/Security/Cookies" rel="nofollow">https://wiki.mozilla.org/Security/Cookies</a></p>
<p>Be aware this will allow web sites to distinguish you from Tor Browser users who do not change this setting.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-292389"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292389" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-292389">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292389" class="permalink" rel="bookmark">It&#039;s about avoiding being…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's about avoiding being identified to begin with, if you've been identified once then it's game over so re-identification just gives them what they already have. You need to explain in idiot friendly terms how to avoid identification from cookies. I can't tell if you mean we get identified straight away or we get identified by visiting a site that another previously visited site used to embed cookies, despite us not knowing when the third party cookie was attempted to be placed or which website it belongs to.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292395"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292395" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292389" class="permalink" rel="bookmark">It&#039;s about avoiding being…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292395">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292395" class="permalink" rel="bookmark">Yes, let me rephrase that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, let me rephrase that answer. Cookies (from a first-party) are used for recognizing returning visitors, not identifying/tracking individual users. There is a fine line between these two actions, but cookies only providing a mechanism linking together requests from the same user/browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292434"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292434" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Thorin (not verified)</span> said:</p>
      <p class="date-time">July 22, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-292434">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292434" class="permalink" rel="bookmark">the &quot;cookie&quot; setting…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the "cookie" setting actually controls "Site Data" and includes the ability for a site to use localStorage, sessionStorage, cookies, indexedDB, sharedWorker, and serviceWorker (and therefore service worker cache and notifications). Note some of these do not apply to Tor Browser or PB mode</p>
<p>So in reality, the cookie setting can be abused by websites in many ways to facilitate tracking. But FPI isolates all those (maybe not service workers = not used in PB mode - I think there's a ticket for that) to first party. In fact, I don't even see the issue with 3rd party cookies here TBH</p>
<p>As sysrqb says, you need to use New Identity if you want to properly sanitize all methods that can be used to linkify. That is by design - but could do with some work to educate people (like myself, even I was confused)</p>
<p><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40222" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/402…</a> is currently confidential, sysrqb feel free to unhide it</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-292376"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292376" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>johnson jo  (not verified)</span> said:</p>
      <p class="date-time">July 18, 2021</p>
    </div>
    <a href="#comment-292376">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292376" class="permalink" rel="bookmark">Just would like to inform…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just would like to inform the team that bug 40110 has yet to be fixed and wondering if this more of a persistant feature than a temporary bug. Also would like to know if there's a workaround for it. Tq.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292385"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292385" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292376" class="permalink" rel="bookmark">Just would like to inform…</a> by <span>johnson jo  (not verified)</span></p>
    <a href="#comment-292385">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292385" class="permalink" rel="bookmark">It is the side-effect of a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It is the side-effect of a feature, but it is not a feature itself. Please see this comment for more information: <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40504#note_2744778" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/405…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-292387"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292387" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292376" class="permalink" rel="bookmark">Just would like to inform…</a> by <span>johnson jo  (not verified)</span></p>
    <a href="#comment-292387">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292387" class="permalink" rel="bookmark">Both upload and download don…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Both upload and download don't work. I tap download, it prompts to confirm, I click yes, it downloads a 0kb file. Every single time</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292397"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292397" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292387" class="permalink" rel="bookmark">Both upload and download don…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292397">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292397" class="permalink" rel="bookmark">Can you provide an example…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you provide an example site where the download is failing? Is it for specific file formats?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-292386"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292386" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
    <a href="#comment-292386">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292386" class="permalink" rel="bookmark">When is new release? It&#039;s…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When is new release? It's been month from last stable built. Lots of people still worried from tracking telemetry and unfixed bugs. Please do not see Android as a lesser priority</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-292400"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292400" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 20, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-292386" class="permalink" rel="bookmark">When is new release? It&#039;s…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-292400">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292400" class="permalink" rel="bookmark">Today</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Today</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
