title: New Release: Tor Browser 10.0a4
---
pub_date: 2020-07-29
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.0
---
categories: applications
---
_html_body:

<p>Tor Browser 10.0a4 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.0a4/">distribution directory.</a></p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-953">latest stable release</a> instead.</p>
<p>This release updates Firefox to 68.11.0esr, Tor to 0.4.4.2-alpha, and NoScript 11.0.34.</p>
<p>This release also includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-31/">security updates</a> to Firefox.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.0a4">changelog</a> since Tor Browser 10.0a2 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 68.11.0esr</li>
<li>Update NoScript to 11.0.34</li>
<li>Update Tor to 0.4.4.2-alpha</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40019">Bug 40019:</a> "Onion-Location should not be processed on .onion webpages</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-288811"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288811" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2020</p>
    </div>
    <a href="#comment-288811">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288811" class="permalink" rel="bookmark">78esr when?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>78esr when?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288822"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288822" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 29, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288811" class="permalink" rel="bookmark">78esr when?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288822">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288822" class="permalink" rel="bookmark">Hopefully the first alpha…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hopefully the first alpha version based on esr78 will be available in a few weeks. Tor Browser Nightly is already based on 78esr.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288849"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288849" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 30, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-288849">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288849" class="permalink" rel="bookmark">cheers.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sounds good, cheers!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288879"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288879" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 31, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-288879">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288879" class="permalink" rel="bookmark">https://blog.torproject.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://blog.torproject.org/comment/288828#comment-288828" rel="nofollow">https://blog.torproject.org/comment/288828#comment-288828</a></p>
<p>replying here because comments are closed</p>
<p>this issue is also in latest torbrowser and alpha</p>
<p>i cant reach github about it because I need an account</p>
<p>console shows lots of warnings and this error </p>
<p>Content Security Policy: The page’s settings blocked the loading of a resource at https://[redacted].arkoselabs.com/[redacted] (“frame-src”).</p>
<p>removed revealing parts, but that site is related to captcha</p>
<p>please do something about it or if its not torbrowser can you please tell github?</p>
<p>this has been a long standing problem with multiple torbrowser versions and nothing has been done on either side yet</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-288812"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288812" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>dave jones (not verified)</span> said:</p>
      <p class="date-time">July 29, 2020</p>
    </div>
    <a href="#comment-288812">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288812" class="permalink" rel="bookmark">When will we get a new tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When will we get a new tor based on firefox 78 esr?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288824"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288824" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2020</p>
    </div>
    <a href="#comment-288824">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288824" class="permalink" rel="bookmark">When will the new redesign…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When will the new redesign of Androids Firefox which is currently available in Firefox for Android Beta, be implemented into Tor Browser for Android?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288831"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288831" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 29, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288824" class="permalink" rel="bookmark">When will the new redesign…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288831">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288831" class="permalink" rel="bookmark">At the end of September.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>At the end of September.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-288865"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288865" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 31, 2020</p>
    </div>
    <a href="#comment-288865">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288865" class="permalink" rel="bookmark">New update released and now…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>New update released and now I cannot save to bookmarks. Plz help!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288870"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288870" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 31, 2020</p>
    </div>
    <a href="#comment-288870">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288870" class="permalink" rel="bookmark">Bug 40019: &quot;Onion-Location…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40019" rel="nofollow">Bug 40019:</a> "Onion-Location should not be processed on .onion webpages</p></blockquote>
<p>But what if Onion-Location on v2 onions can help admins transition to v3?<br />
<a href="https://blog.torproject.org/comment/288593#comment-288593" rel="nofollow">https://blog.torproject.org/comment/288593#comment-288593</a><br />
<a href="https://blog.torproject.org/comment/288577#comment-288577" rel="nofollow">https://blog.torproject.org/comment/288577#comment-288577</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288877"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288877" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>disappointed (not verified)</span> said:</p>
      <p class="date-time">July 31, 2020</p>
    </div>
    <a href="#comment-288877">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288877" class="permalink" rel="bookmark">My Tor Alpha for Android…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My Tor Alpha for Android browser used to work on my Samsung cellphone. Now it boots up to 50% then closes everything down, during the last 6 months or so.  Where can I get some help to fix this?  Thankyou.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288930"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288930" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 08, 2020</p>
    </div>
    <a href="#comment-288930">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288930" class="permalink" rel="bookmark">The fix for bug 40019 in 10…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The fix for bug 40019 in 10.0a4 is needed in the main release 9.5.X right away.  <a href="https://www.dw.com/" rel="nofollow">https://www.dw.com/</a> refreshes endlessly. Probably other sites do too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288958"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288958" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>No need to know (not verified)</span> said:</p>
      <p class="date-time">August 12, 2020</p>
    </div>
    <a href="#comment-288958">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288958" class="permalink" rel="bookmark">Saya harap tor browser 10…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Saya harap tor browser 10.0a4 bisa berjalan sebagaimana mestinya. Saya harap ini juga bisa meningkatkan performa aplikasi.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289030"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289030" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 17, 2020</p>
    </div>
    <a href="#comment-289030">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289030" class="permalink" rel="bookmark">Millions of
[08-18 05:39:28]…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Millions of<br />
[08-18 05:39:28] Torbutton INFO: controlPort &gt;&gt; 650 STREAM 7674 SENTCONNECT 24 cflaresuje2rb7w2u3w43pn4luxdi6o7oatv6r2zrfb5xvsugj35d2qd.onion:443 SOCKS_USERNAME="%username%" SOCKS_PASSWORD="4a659aaaa361731434171a21850216a1" CLIENT_PROTOCOL=SOCKS5 NYM_EPOCH=0 SESSION_GROUP=-4 ISO_FIELDS=SOCKS_USERNAME,SOCKS_PASSWORD,CLIENTADDR,SESSION_GROUP,NYM_EPOCH<br />
are still f*cking the browser &amp; cpu even that all tabs are closed!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289037"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289037" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 18, 2020</p>
    </div>
    <a href="#comment-289037">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289037" class="permalink" rel="bookmark">hi guys please add the onion…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi guys please add the onion button back or at least move the current icon over to where the old onion button was.</p>
<p>I continuously keep restarting the damn browser by mistake when removing downloaded files or going to the settings menu. its too close to the other stuff.</p>
<p>thanks in advance</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289102"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289102" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">August 20, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289037" class="permalink" rel="bookmark">hi guys please add the onion…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289102">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289102" class="permalink" rel="bookmark">You aren&#039;t prompted to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You aren't prompted to confirm restarting the browser (creating a New Identity)? Did you select "Never ask me again" and it restarts without asking? You can check (and disable) by opening `about:config` and toggling `extensions.torbutton.confirm_newnym`.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
