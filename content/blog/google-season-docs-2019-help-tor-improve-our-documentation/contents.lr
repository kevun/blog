title: Google Season of Docs 2019: Help Tor Improve Our Documentation
---
pub_date: 2019-04-23
---
author: pili
---
tags: google, seasonofdocs, Google Season of Docs
---
summary:

We’re a small nonprofit organization that develops free and open source software used by millions around the world, and our community of contributors and users would greatly benefit from improved documentation.

Through Season of Docs, we want to collaborate with technical writers who can help us improve.
---
_html_body:

<p>There are a few elements that are critical for the sustainability of an open source project. One of them is good documentation.</p>
<p>Although often neglected, documentation can be crucial for open source projects to provide meaningful guides on how to start contributing and guides on the architecture of the technology being developed.</p>
<p>These resources help not only newcomers and contributors to understand the technological aspects of a project but can also expose both decision making and work processes, outlining the best way to contribute.</p>
<p>This year Google has launched Season of Docs, similar to Summer of Code. Season of Docs brings open source and technical writer communities together. The aim is to foster collaboration and give technical writers the “experience in contributing to open source projects, and to give open source projects an opportunity to engage the technical writing community.”</p>
<p>The program runs for approximately 3 months from September to November 2019 and there is additionally the option of a "long-running project" which goes on for approximately 5 months, from September 2019 to February 2020.</p>
<p>We are pleased to announce that we have been accepted as a mentoring organization for <a href="https://developers.google.com/season-of-docs/docs/participants/">Google’s Season of Docs 2019</a>.</p>
<p>We’re a small nonprofit organization that develops free and open source software used by millions around the world, and our community of contributors and users would greatly benefit from improved documentation.</p>
<p>Through Season of Docs, we want to collaborate with technical writers who can help us with these projects:</p>
<ul>
<li><strong><a href="https://2019.www.torproject.org/docs/tor-manual.html.en">Tor Manual</a>:</strong> We would like to re-think the information architecture of the Tor manual.  ​</li>
<li><strong><a href="https://gitweb.torproject.org/torspec.git/tree/">Tor Specifications</a>:</strong> We have several documents outlining how different elements in Tor work. We would like to rewrite them so they can be useful references.</li>
<li><strong><a href="https://2019.www.torproject.org/projects/torbrowser/design/">Tor Browser Design Doc</a></strong>: We would like to re-think the information architecture of this document and bring it up to date for the latest Tor Browser versions.</li>
<li><strong><a href="https://gitweb.torproject.org/user/nickm/torguts.git/tree/">Developer documentation</a>:</strong> We would like to consolidate and rewrite the developer documentation for new contributions and volunteers that are taking new roles in the community.</li>
<li><strong><a href="https://2019.www.torproject.org/docs/documentation.html.en">Tor Documentation</a>:</strong> We would like to reorganize and rewrite part of the existing documentation.</li>
</ul>
<p>You can read other possible documentation projects at Tor in our <a href="https://trac.torproject.org/projects/tor/wiki/doc/GoogleSeasonOfDocs2019">wiki</a>, and we are open to other ideas, too. We would love to read your own proposals about how we could improve any of our documentation.</p>
<p><em>Applications for Google Season of Docs open on May 29, 2019 at 18:00 UTC</em>. We welcome you to discuss your project with us before applying. You can email us with your ideas or questions at <a href="mailto:gso@torproject.org">gso@torproject.org</a>.</p>
<p>We look forward to working with you.</p>

---
_comments:

<a id="comment-281041"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281041" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 02, 2019</p>
    </div>
    <a href="#comment-281041">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281041" class="permalink" rel="bookmark">&quot;We&quot;, &quot;we&quot;, &quot;we would like…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"We", "we", "we would like to", "we want to" + [ambiguous action].  Sounds like you already have strong editorial feelings for what you want.  Comments from the (user) community sound satisfied for the most part with the language of the documentation, old and new, except when they can't find it.  What could a writing volunteer contribute but copy and paste while filling in outline drafts from micro-managed subjective orders?  Was this initiative brought on by comments from users?  Because the post cites none.  Or is it something to do to look busy as a follow up from grant requests?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-281072"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281072" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="very confusing and saddening">very confusing… (not verified)</span> said:</p>
      <p class="date-time">May 03, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-281041" class="permalink" rel="bookmark">&quot;We&quot;, &quot;we&quot;, &quot;we would like…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-281072">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281072" class="permalink" rel="bookmark">When asked we put in a lot…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When asked we put in a lot of suggestions. Where are they now?  Is T.O.R. yours or ours?  Aren't relay and exit operators (from our pockets) T.O.R. and therefore the community and us and you are one?<br />
2019.torproject.org you say is this year's site but half the links don't go to that one. They go to the usual one.<br />
You've disappeared up your own artifice.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-281042"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281042" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Fox (not verified)</span> said:</p>
      <p class="date-time">May 02, 2019</p>
    </div>
    <a href="#comment-281042">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281042" class="permalink" rel="bookmark">[...] all of the people who…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>[...] all of the people who have been involved in Tor are united by a common belief: internet users should have private access to an uncensored web.</p></blockquote>
<p>Google is probably the biggest non-government enemy of "private access to an uncensored web".<br />
I cannot fathom how white-washing them in this way can possibly advance this cause.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-281060"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281060" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Kevin (not verified)</span> said:</p>
      <p class="date-time">May 03, 2019</p>
    </div>
    <a href="#comment-281060">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281060" class="permalink" rel="bookmark">I&#039;m watching my privacy melt…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm watching my privacy melt away before my very eyes. I can't block script on this new version of Tor and am getting Google trackers and am clearly being finger printed by google; also, it would appear that I'm being tracked by the Google Geo-location. There's not a simple way to stop this. Indeed, it feels like Tor has been bought out by the Google. :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-281070"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281070" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 03, 2019</p>
    </div>
    <a href="#comment-281070">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281070" class="permalink" rel="bookmark">This is a worthy effort and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is a worthy effort and I hope that it results in documentation which helps newcomers quickly learn what they need to know right now to start using Tor Browser, while also making it easy for more experienced users to find up to date information on topics such as encryption used in Tor circuits or links to key papers on theoretical attacks on onions, plus information about real world attacks in the wild. </p>
<p>Because I am a user but neither a coder nor a tech writer I consider myself well qualified :-) to offer some general advice about writing "tech docs for the ordinary citizen".</p>
<p>One basic approach might be to write a core page for newcomers with links to other pages with more specialized/detailed information, or to pages which discuss difficult topics like Tor Project's somewhat troubling (especially for newcomers outside the US) historical origins, criminalization of privacy research (c.f. Ola Bini), or the community response to FBI's "Going Dark" hysteria.  I suspect no-one at Tor will be eager to pen a thoughtful admission that Yasha Levine makes some good points in his book Surveillance Valley, but I think acknowledging past mis-steps (such as hiring a former USIC coder to work on sensitive app code) is the best way to convince wary potential users that TP has seriously reformed its character from "US soft power coders" to "international human rights coders".</p>
<p>I was very impressed when I first read the Tails documentation because that was the first site I'd seen which followed the rule of putting the essential stuff first and then adding links to comprehensible explanations of how to use various software bundled with Tails.  Infinitely superior to off-putting injunctions to RTFM (meaning the Tor Design Document, which is not really written for users but for coders, and is in any case seriously outdated).</p>
<p>Here and there the writers can possibly insert encouragement to intermediate level users: when we say understanding privacy tech involves surmounting a "steep learning curve", we mean that initially a newcomer might find almost everything confusing or counterintuitive, but if they keep using Tor and trying to learn more about how the magic happens, at some point they will find more and more is rapidly making more and more sense to them.</p>
<p>Because things change so quickly, every page should begin with "last reviewed April 2020" etc.  Because many people will come across these documentation via a search engine, and because search engines often link to outrageously outdated and even dangerous misleading information about Tor, regular review and updating should be an essential part of documentation.</p>
<p>Last but not least, rigorous link testing, editing, and some kind of user testing before you launch should be regarded as essential.  The recent launch of the "new look" was more rocky than it should have been.  I hope that just before you announce the new documents in this blog someone will test the blog post using various security slider selections (by that time there may be four not three as you know!) and if possible using slow as well as fast internet connections.  Remember that some Tor users are not in regions with fast internet and we do not want to make life more difficult for them than it already is.</p>
<p>P.S. Please make sure Isabela knows about (and ideally responds to) a troubling opinion piece which calls for (among other things) DARPA funding of anti-GFWC software without even mentioning Tor!  Or anonymity or privacy.  See:</p>
<p>thehill.com<br />
Tear down the 'Great Firewall' to win the war with China<br />
Bradley A. Thayer and Lianchao Han<br />
29 Apr 2019</p>
<p>When you read this, make sure to note the authors's affiliations:</p>
<p>&gt; Bradley A. Thayer, Ph.D., is a professor of political science at the University of Texas, San Antonio. He is coauthor of “How China Sees the World: Han-Centrism and the Balance of Power in International Politics.”</p>
<p>UT San Antonio has close ties to NSA/TAO, which is situated on the grounds of Lackland AFB in that city.  Several companies in San Antonio are essential cybermercenary companies staffed by former US military USIC cyberwarriors.  Don't know about Thayer himself, but possible ties to NSA/TAO are concerning.</p>
<p>&gt; Lianchao Han is vice president of Citizen Power Initiatives for China and a visiting fellow at the Hudson Institute. After the Tiananmen Square Massacre in 1989, Dr. Han was one of the founders of the Independent Federation of Chinese Students and Scholars. He worked in the U.S. Senate for 12 years, as legislative counsel and policy director for three senators.</p>
<p>Hudson Institute: conservative think tank founded by thermonuclear extremist Herman Kahn.  Don't know about Han himself, but these ties are concerning.</p>
<p>Simultaneously, Pompeo appears to be trying to co-opt HRW's very important revelation of the massive dragnet surveillance/harassment system tracking Uighurs everywhere they go in China (not just in Xinjiang Province) by calling out the CN government for (apparently) incarcerating millions of heads of Uighur households in vast internment camps (called "vocation training centers", with guard towers).  </p>
<p>I am concerned that Thayer and Han may be players is a well planned political influence campaign whose goals may include</p>
<p>o delegitimizing (even criminalizing) Tor</p>
<p>o decoupling "anti-censorship" from anonymity, privacy, and even from free speech</p>
<p>o making a show of ideological combat with CN (but certainly not RU!) while quietly encouraging FBI's "Going Dark" ambitions of eliminating all on-line privacy.</p>
<p>Also, I think Isabela may be too young to remember that the Democratic candidate for US President in 2020 who is being heavily promoted by "the establishment", Joe Biden, was one of the chief proponents of NSA's "clipper chip" and other attacks on NSA-defeating public key cryptography.  He was also one of the chief architects of one of the most socially destructive laws in US history, the Crime Omnibus (mandatory sentencing, etc).  And his son turns out to have invested heavily in the very surveillance app which plays a key role in the CN Uighur-tracking/harassment dragnet.</p>
<p>I hardly need remark that Drump agrees with CN on the "need" for cyberactions (and "kinetic" actions) targeting "troublemakers" (e.g. ACLU, left leaning journalists, community organizers, union leaders) as a "national security matter" [sic], so Pompeo's criticisms will not be taken seriously by the international community.</p>
<p>Further, tech reporter Sean Gallagher, who years ago was not unfriendly to reporting on the Snowden leaks, now appears to have been thoroughly co-opted by the US military/USIC cyberwarrior mythology:</p>
<p>arstechnica.com<br />
Spot the not-Fed: A day at AvengerCon, the Army’s answer to hacker conferences<br />
Army unit's effort brings hacker con culture to the home of military cyber.<br />
Sean Gallagher<br />
2 May 2019</p>
<p>In short, Tor (and the right to privacy, and the right to freedom from cyberassault from the likes of NSA/TAO) has few friends in the US executive, legislature, judiciary, or media.</p>
<p>It follows that Tor Project must redouble its efforts to move to a user funded structure, and to relocate its base of operations to a safer country.  So not RU, not CN, not VE, probably not BR or MX, but maybe CH or SE or IS or even Chile?  Or those seem too far, what about CA?  Not really safe but safer than the US, probably.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-281575"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281575" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 09, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-281070" class="permalink" rel="bookmark">This is a worthy effort and…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-281575">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281575" class="permalink" rel="bookmark">Your P.S. is 150% longer…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Your P.S. is 150% longer than your post. Brevity, please! Microblog or real-time chat sometime. Tor Project posts on Mastodon and operates IRC channels. Your main post makes some good basic points, but they are as lost as needles in a haystack. Tech writing is supposed to avoid political subjectivity. It's supposed to be concise, direct, clear, simple -- in effect, dry.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-281106"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281106" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 04, 2019</p>
    </div>
    <a href="#comment-281106">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281106" class="permalink" rel="bookmark">Emergency alerts are a very…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Emergency alerts are a very special but very important type of documentation.  Users have begged TP for years to try to develop a faster response.  E.g. you are a small organization but not so small that someone cannot be designated as the person who will respond within minutes to a security crisis.</p>
<p>As of today (4 May 2019) tails.boum.org (?) is saying:</p>
<p>&gt; Tor Browser in Tails 3.13.1 is not safe to use without taking the manual steps listed below each time you start Tails!<br />
&gt;<br />
&gt; Due to a mistake in Mozilla's signing infrastructure all Firefox and Tor Browser add-ons are currently disabled, which disables important protections (the NoScript add-on) for Tails users.<br />
&gt;<br />
&gt; To secure your Tor Browser you must follow these steps each time you start Tails 3.13.1:<br />
&gt;<br />
&gt;    Open the address about:config in the Tor Browser address bar<br />
&gt;    Click the "I accept the risk!" button<br />
&gt;    At the top of the page, search for xpinstall.signatures.required<br />
&gt;    Set the xpinstall.signatures.required entry to false by double clicking it<br />
&gt;<br />
&gt; To the right of the address bar an icon with a blue S (and possibly a red question mark or other variations) should appear, and this indicates that your browser is safe again.</p>
<p>Is this genuine?  We want to change the value from TRUE to FALSE?  Disabling the requirement for signatures?</p>
<p>Does this affect the current version of Tor Browser offered by Tor Project as well as the version in Tails 3.13.1?</p>
<p>Does this affect user irrespective of their security slider setting?</p>
<p>Does this affect only Windows users?</p>
<p>Was the mistake made by Mozilla?</p>
<p>Is this good as bad as it sounds?  Bad guys can remotely install any add-on they please by messing with the code on some popular website?  Is that what the "signing infrastructure" mistake implies?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-281614"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-281614" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 10, 2019</p>
    </div>
    <a href="#comment-281614">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-281614" class="permalink" rel="bookmark">Good technical writing style…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good technical writing style guideline documents:</p>
<ul>
<li>Ahlstrom, V., &amp; Longo, K. (2003). <em><a href="https://hf.tc.faa.gov/hfds/" rel="nofollow">Human Factors Design Standard</a></em> (HF-STD-001) (DOT/FAA/CT-03/05). Atlantic City International Airport, NJ: Federal Aviation Administration (FAA), William J. Hughes Technical Center. <a href="http://www.tc.faa.gov/its/worldpac/techrpt/ct03-5.pdf" rel="nofollow">Alternate host</a>.
<ul>
<li>Preceded by: FAA. (2001). <em><a href="http://www.tc.faa.gov/its/worldpac/techrpt/ct01-8.pdf" rel="nofollow">Human Factors Design Guide Update (Report Number DOT/FAA/CT-96/01): A Revision to Chapter 8-Computer Human Interface Guidelines</a></em> (DOT/FAA/CT-01/08).
<ul>
<li>Preceded by: FAA. (1996). <em><a href="https://www.faa.gov/about/initiatives/maintenance_hf/library/documents/media/human_factors_maintenance/hfdg.pdf" rel="nofollow">Human Factors Design Guide</a></em> (DOT/FAA/CT-96/1).</li>
</ul>
</li>
</ul>
</li>
<li>Centers for Disease Control and Prevention (CDC). (2010). <em><a href="https://stacks.cdc.gov/view/cdc/11938" rel="nofollow">Simply put; a guide for creating easy-to-understand materials</a></em>. Atlanta, GA: Office of the Associate Director for Communication, Division of Communication Services, Strategic and Proactive Communication Branch.</li>
<li><em><a href="https://en.wikipedia.org/wiki/Wikipedia:Manual_of_Style" rel="nofollow">Wikipedia:Manual of Style</a></em></li>
<li><em><a href="https://en.wikipedia.org/wiki/Wikipedia:Basic_copyediting" rel="nofollow">Wikipedia:Basic copyediting</a></em></li>
<li><em><a href="https://en.wikipedia.org/wiki/Wikipedia:Writing_better_articles" rel="nofollow">Wikipedia:Writing better articles</a></em></li>
<li>World Wide Web Consortium (W3C). Web Accessibility Initiative (WAI).
<ul>
<li><em><a href="https://www.w3.org/WAI/standards-guidelines/wcag/" rel="nofollow">Web Content Accessibility Guidelines (WCAG) Overview</a></em>.</li>
<li><em><a href="https://www.w3.org/WAI/tutorials/" rel="nofollow">Web Accessibility Tutorials</a></em>.</li>
<li><em><a href="https://www.w3.org/WAI/standards-guidelines/uaag/" rel="nofollow">User Agent Accessibility Guidelines (UAAG) Overview</a></em>.</li>
<li><em><a href="https://www.w3.org/WAI/standards-guidelines/mobile/" rel="nofollow">Mobile Accessibility at W3C</a></em>.</li>
</ul>
</li>
<li>Cheung, I. (2016). <em><a href="https://www.ivacheung.com/2016/09/four-levels-to-accessible-communications/" rel="nofollow">Four levels to accessible communications</a></em>.</li>
<li>Spyridakis, J. H., &amp; Wenger, M. J. (1992). <em><a href="https://www.hcde.washington.edu/files/people/docs/Writing_Human_Performance.pdf" rel="nofollow">Writing for human performance: Relating reading research to document design</a></em>. Technical Communications, 39, (pp. 202-215).</li>
<li>Grissinger, M. (2014). <em><a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3989081/" rel="nofollow">Affirmative Warnings (Do This) May Be Better Understood Than Negative Warnings (Do Not Do That)</a></em>. Pharmacy &amp; Theraputics.</li>
<li>Rauterberg, G. <em><a href="http://www.idemployee.id.tue.nl/g.w.m.rauterberg/lecturenotes/UI-guide-line-collection.htm" rel="nofollow">A collection of UI guidelines</a></em>.</li>
</ul>
</div>
  </div>
</article>
<!-- Comment END -->
