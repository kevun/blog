title: New Alpha Release: Tor Browser 11.5a7 (Android)
---
pub_date: 2022-03-09
---
author: aguestuser
---
categories:

applications
releases
---
summary: Tor Browser 11.5a7 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a7 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a7/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-09/) to Firefox.

We also use the opportunity to update various components of Tor Browser, bumping NoScript to 11.3.7, while switching to the latest version of Go (1.17.8) for building our Go-related projects.

As usual, please report any issues you find in this alpha version, so we can fix them for the next upcoming major stable release (11.5).

The full changelog since [Tor Browser 11.5a5](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Android
  - Update NoScript to 11.3.7
  - [Bug tor-browser#40829](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40829): port fixes for Mozilla security advisory mfsa2022-09 to Android
- Build System
  - Android
    - Update Go to 1.17.8
