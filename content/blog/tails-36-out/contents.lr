title: Tails 3.6 is out
---
pub_date: 2018-03-13
---
author: tails
---
tags: anonymous operating system
---
_html_body:

<p>This release fixes <span class="createlink">many security issues</span> and users should upgrade as soon as possible.</p>
<h1>Changes</h1>
<h2>New features</h2>
<ul>
<li>
<p>You can now lock your screen by clicking on the <span class="button"><img alt="" class="symbolic" height="115" src="https://tails.boum.org/doc/first_steps/introduction_to_gnome_and_the_tails_desktop/system.png" width="115" /></span> button in the system menu.</p>
<ul>
<li>
<p>If you set up an <span class="createlink">administration password</span> when starting Tails, you can unlock your screen with your administration password.</p>
</li>
<li>
<p>Otherwise, you can set up a password to unlock your screen when locking your screen for the first time.</p>
<p><span class="createlink">screen-locker.png</span></p>
</li>
</ul>
</li>
<li>
<p>We improved a lot the backend of the <a href="https://tails.boum.org/doc/first_steps/persistence/configure/#additional_software"><strong>Additional Software</strong> persistence feature</a>. Your additional software is now:</p>
<ul>
<li>Installed in the background after the session starts instead of <span class="createlink">blocking the opening of the desktop</span>.</li>
<li>Always installed even if you are offline.</li>
</ul>
</li>
<li>
<p>Install <a href="https://github.com/firstlookmedia/pdf-redact-tools/blob/master/README.md">pdf-redact-tools</a>, a command line tool to clean metadata and redact PDF files by converting them to PNG images.</p>
</li>
<li>
<p>An error message indicating the name of the graphics card is now displayed when Tails fails to start GNOME.</p>
</li>
</ul>
<h2>Upgrades and changes</h2>
<ul>
<li>
<p>The <strong>Tails documentation</strong> launcher on the desktop now opens the documentation on our website if you are online.</p>
</li>
<li>
<p>Install drivers for the <a href="https://en.wikipedia.org/wiki/Video%5FAcceleration%5FAPI">Video Acceleration API</a> to improve the display of videos on many graphics cards.</p>
</li>
<li>
<p>Upgrade <em>Electrum</em> from 2.7.9 to <a href="https://github.com/spesmilo/electrum/blob/3.0.x/RELEASE-NOTES">3.0.6</a>.</p>
</li>
<li>
<p>Upgrade <em>Linux</em> to 4.15.0.</p>
</li>
<li>
<p>Upgrade <em>Tor</em> to 0.3.2.10.</p>
</li>
<li>
<p>Upgrade <em>Thunderbird</em> <a href="https://www.mozilla.org/en-US/thunderbird/52.6.0/releasenotes/">52.6.0</a>.</p>
<p>We also activated the <em>AppArmor</em> profile of <em>Thunderbird</em> from Debian to <a href="https://tails.boum.org/contribute/design/application_isolation/">confine <em>Thunderbird</em> for security</a>.</p>
</li>
<li>
<p>Rewrite some internal scripts from shell scripting to Python, thanks to <a href="https://goodcrypto-private-server.sourceforge.io/">GoodCrypto</a>.</p>
</li>
</ul>
<h2>Fixed problems</h2>
<ul>
<li>
<p>Remove the display of debug and error messages when starting.</p>
</li>
<li>
<p>Fix the ISO image selection being unavailable in <em>Tails Installer</em> in languages other than English. (<a href="https://labs.riseup.net/code/issues/15233">#15233</a>)</p>
</li>
<li>
<p>Fix <em>OpenPGP Applet</em> being unresponsive when decrypting a lot of text. (<a href="https://labs.riseup.net/code/issues/6398">#6398</a>)</p>
</li>
</ul>
<p>For more details, read our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog">changelog</a>.</p>
<p><a id="known-issues"></a></p>
<h1>Known issues</h1>
<ul>
<li>
<p><span class="application">Enigmail</span> is unable to download OpenPGP keys for new contacts if you have the <span class="guilabel">GnuPG</span> persistence feature activated. You can still download OpenPGP keys using <span class="application">Passwords and Keys</span>. (<a href="https://labs.riseup.net/code/issues/15395">#15395</a>)</p>
</li>
<li>
<p>This release is not reproducible, due to various python files in /usr/local having different timestamps. (<a href="https://labs.riseup.net/code/issues/15400">#15400</a>)</p>
</li>
</ul>
<p>See the list of <a href="https://tails.boum.org/support/known_issues/">long-standing issues</a>.</p>
<h1>Get Tails 3.6</h1>
<ul>
<li>
<p>To install, follow our <a href="https://tails.boum.org/install/">installation instructions</a>.</p>
</li>
<li>
<p>We do not provide automatic upgrades from older Tails installations for this release, as we've hit an old bug while generating the necessary files (<a href="https://labs.riseup.net/code/issues/13426">#13426</a>). You have to <a href="https://tails.boum.org/upgrade/">upgrade manually</a>.</p>
</li>
<li>
<p><a href="https://tails.boum.org/install/download/">Download Tails 3.6.</a></p>
</li>
</ul>
<h1>What's coming up?</h1>
<p>Tails 3.7 is <a href="https://tails.boum.org/contribute/calendar/">scheduled</a> for May 8.</p>
<p>Have a look at our <a href="https://tails.boum.org/contribute/roadmap">roadmap</a> to see where we are heading to.</p>
<p>We need your help and there are many ways to <a href="https://tails.boum.org/contribute/">contribute to Tails</a> (<a href="https://tails.boum.org/donate?r=3.6">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev">talk to us</a>!</p>

