title: Google Chrome Incognito Mode, Tor, and Fingerprinting
---
pub_date: 2010-09-14
---
author: mikeperry
---
tags:

torbutton
google
privacy
EFF
firefox
threat models
private browsing
panopticlick
chrome
---
categories:

applications
partners
---
_html_body:

<p>A few months back, I posted that <a href="https://blog.torproject.org/blog/firefox-private-browsing-mode-torbutton-and-fingerprinting" rel="nofollow">we have been in discussion</a> with Mozilla about improving their Private Browsing mode to resist <a href="https://wiki.mozilla.org/Fingerprinting" rel="nofollow">fingerprinting</a> and the <a href="https://wiki.mozilla.org/Security/Anonymous_Browsing" rel="nofollow">network adversary</a>. As I mentioned there, we have also been doing the same thing with Google for Google Chrome. Google has taken the same approach as Firefox for their Incognito mode, which means that it provides little to no protections against a network adversary.</p>

<p>This means that Chrome Incognito mode is not safe to use with Tor.</p>

<h1>The Story So Far</h1>

<p>In general, Incognito's major shortcomings are plugins, fingerprinting, DNS leaks,  SSL state leaks, auto-formfill and site-specific zoom. The Applied Crypto research group at Stanford recently published a <a href="http://crypto.stanford.edu/~dabo/pubs/abstracts/privatebrowsing.html" rel="nofollow">comparison of the four major browser's private browsing modes</a> against a dedicated local and remote adversary which details some of these issues. The added poison for Tor is that plugins and DNS leaks can also reveal your IP address and complete web activity to the network adversary.</p>

<p>So, starting almost a year ago, we began providing feedback on Google Chrome's extension system APIs to ensure that Google is aware of the APIs that we and others need to write defenses against a network adversary. </p>

<p>We drew up <a href="https://groups.google.com/group/chromium-extensions/browse_thread/thread/ceba26ca9e2f6a78/e83920020719a6b2" rel="nofollow">a wish list of API categories</a> that would be useful for privacy enhancing extensions. Many of the items on that first list were a bit far-reaching, and not all were strictly required to produce a Tor Mode for Google Chrome.</p>

<p>Over the past year, a rather astonishing amount of progress has been made on the Google Chrome Extension API set and also on many items of that list. In fact, work is now in progress on all the major API sets that we need to build a Tor Mode for Google Chrome, to port <a href="https://blog.torproject.org/blog/https-everywhere-firefox-addon-helps-you-encrypt-web-traffic" rel="nofollow">HTTPS Everywhere</a>, and also write a separate extension to provide defenses against fingerprinting and a network adversary, all using much simpler mechanisms that we had to use for the equivalent functionality in Firefox. It should also soon be possible for many similar privacy and security enhancing extensions to follow suit.</p>

<h1>Key Security and Privacy Enhancing APIs</h1>

<p>The three API sets that will make this possible are <a href="http://www.chromium.org/developers/design-documents/extensions/notifications-of-web-request-and-navigation" rel="nofollow">Web Request</a>, <a href="https://code.google.com/chrome/extensions/dev/content_scripts.html" rel="nofollow">Content Scripts</a>, and the <a href="http://dev.chromium.org/developers/design-documents/extensions/proxy proposal" rel="nofollow">Proxy APIs</a>. Our prolific Tor volunteer <a href="http://roberthogan.net/" rel="nofollow">Robert Hogan</a> has also been doing some work on <a href="https://code.google.com/p/chromium/issues/detail?id=30877" rel="nofollow">SSL related issues</a> that will prove crucial, as well.</p>

<p>Each of these API sets, however, has its own shortcomings, rough edges, and missing pieces that need to be taken care of before it becomes suitable for us to use. I've been spending my time over the past couple months looking over these rough spots, documenting them, and informing Google. I'm also lucky enough to have my brother on the inside: he's one of the Google Chrome engineers who is working on these very APIs.</p>

<p>With his help, I've identified three major stumbling blocks to us and anyone else who is interested in writing security and privacy enhancing extensions on their platform.</p>

<h1>Three Major Stumbling Blocks</h1>

<p>The first major stumbling block is that not only do we need to convince Google that these APIs are useful and needed, but there also has to be a way for them to implement the APIs easily, cleanly, and with very little overhead. Google Chrome is a very fine-tuned high performance browser, and keeping it this way is priority #1 for Google. At the present time, all of the extension APIs are asynchronous, and delivery is optimized so that increasing the number of installed extensions listening for events has negligible to absolutely zero effect on page load time.</p>

<p>However, security and privacy enhancing extensions will need the ability to actually block and modify requests with certainty. This means that fully asynchronous notification and response is not a viable option for some events.</p>

<p>Therefore, last month, I wrote up a <a href="https://groups.google.com/a/chromium.org/group/chromium-extensions/browse_thread/thread/17ea6efa15bfea0a" rel="nofollow">review of the Web Request APIs</a>, describing a method that could allow for blocking versions of three of the APIs to function with minimal performance impact, which is the key factor in Google's decision to provide them.</p>

<p>The second major stumbling block is the fact that none of the Google Extension APIs have yet been used to provide any real added security against any threat model, let alone one as <a href="https://www.torproject.org/torbutton/design/#adversary" rel="nofollow">advanced as the one Torbutton employs</a>. Extensions such as <a href="https://chrome.google.com/extensions/detail/flcpelgcagfhfoegekianiofphddckof?hl=en" rel="nofollow">KB SSL Enforcer</a> and <a href="http://www.chromeextensions.org/appearance-functioning/adblock/" rel="nofollow">Adblock Plus for Chrome</a> do not actually have any security guarantees beyond that which they can get through asynchronous DOM manipulation. There are also magical "special" urls and types of requests that currently are exempt from extension notification or control.</p>

<p>This is a real potential danger for us, and one that we only narrowly dodged with Firefox due only to the significantly more flexibility we have on that platform to reimplement arbitrary browser components, and to use different APIs and observers at different levels of notification. We have learned the hard way over the years that any URLs that go through special codepaths can leave the browser vulnerable to plugin injection, fingerprinting, unmasking, and exploitation.</p>

<p>Because of this, I took the time to write up <a href="https://groups.google.com/a/chromium.org/group/chromium-extensions/browse_thread/thread/f5a73572eb040bea" rel="nofollow">what extensions using the Chrome APIs for security will expect</a>. This includes things like uniform applicability, no functional surprises, and no unilateral exemptions.</p>

<p>The third stumbling block is how Google initially approached Incognito mode and privacy, and extension development in general. The entire extension system is built upon least privilege to ensure security rather than code reviews and audits. Extensions must request permissions to perform classes of actions, and Google is wary of creating permissions that are attractive to (semi-)malicious extensions that want control over everything. </p>

<p>Therefore, it was first believed that no extension should be allowed in Incognito mode, because no assurances could be made about its willingness or ability not to record usage information. This June, my brother finally <a href="http://blog.chromium.org/2010/06/extensions-in-incognito.html" rel="nofollow">flipped the switch</a> to allow extensions to be run in Incognito mode, but only with with manual and explicit user approval, after installation.</p>

<p>As such, the behavior of an Incognito-specific extension still has not received a lot of attention at Google. This caused me to write a third post to describe <a href="https://groups.google.com/a/chromium.org/group/chromium-extensions/browse_thread/thread/8a46d570807c9a72" rel="nofollow">how Incognito-specific extensions might function</a>. My hope is that this will help provide another perspective towards extension developers in terms of adding privacy and security to Incognito mode, and to encourage discussions about how to best allow it.</p>

<h1>The Road Ahead</h1>

<p>We're still many more months away from being able to write a full and working version of Tor Mode, HTTPS Everywhere, or the fingerprint resistance addon, but we're at least much closer than before, and everything seems to be heading in the right direction, so far.</p>

<p>For those who want to track our progress, as well as Google's, you can see the <a href="https://trac.torproject.org/projects/tor/ticket/1925" rel="nofollow">entire list of remaining/important issues</a> on our bugtracker.</p>

---
_comments:

<a id="comment-7683"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7683" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 14, 2010</p>
    </div>
    <a href="#comment-7683">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7683" class="permalink" rel="bookmark">It sure would be nice to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It sure would be nice to have a Tor mode in Chromium. :)<br />
I hope the process of fixing the remaining issues will be faster with Chromium than with Firefox: Firefox has bugs that are still open after nearly SIX years (i.e. the infamous bug #280661)... :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-7691"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7691" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 15, 2010</p>
    </div>
    <a href="#comment-7691">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7691" class="permalink" rel="bookmark">I use the Mac internet</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I use the Mac internet browser Safari designed for PC.  Basically, I run Mac's Safari in Windows 7.  I would enjoy running TOR in Safari rather than FireFox.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7692"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7692" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 15, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-7691" class="permalink" rel="bookmark">I use the Mac internet</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-7692">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7692" class="permalink" rel="bookmark">The problem with Safari is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The problem with Safari is that it isn't open source.  If we can't see what happens inside, we can't protect your privacy/anonymity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-7921"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7921" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 01, 2010</p>
    </div>
    <a href="#comment-7921">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7921" class="permalink" rel="bookmark">Im currently running Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Im currently running Tor with Chrome + Switchy.  So far using the test page on the Tor site with this combo seems to be working. Of course im not using Chrome in incongnito mode. </p>
<p>The interesting discovery is that once Chrome + Switchy are up and running with Tor one can open up Safari and Firefox and surf "anonymously" with these browsers.  I say  "anonymously" because this is only based on the browsers passing the Tor test from the Tor site. The IP address of all three are the same and different from my original IP address. However this is not an in-depth test of the all the factors that go into making a browser anonymous. </p>
<p>I would like some feedback from the experts on the Tor team regarding the viability of this combination for use to surf anonymously. Are some of the limitations inherent in the current version of Chrome in incognito mode also present in the Chrome browser when not using this extension? What are these limitations?</p>
<p>Im also interested in testing and trying to fix some of these limitations.<br />
I would appreciate a list of the limitations the Tor team thinks need to be overcome for Chrome to be a working browser with Tor.  A reply on this post would be appreciated. </p>
<p>All the best on a terrific organization!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-7957"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7957" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 07, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-7921" class="permalink" rel="bookmark">Im currently running Tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-7957">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7957" class="permalink" rel="bookmark">Using chrome, safari, or</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Using chrome, safari, or anything but Firefox and torbutton is not safe.  You are not anonymous and probably leaking data all over the place.</p>
<p>Switchy sets the global proxy config in os x, not just for chrome.  Look in the network preferences and notice what it does.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-9915"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9915" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 23, 2011</p>
    </div>
    <a href="#comment-9915">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9915" class="permalink" rel="bookmark">I&#039;m sorry but I wanted to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm sorry but I wanted to confirm.  After manually enabling the ProxySwitchy! with Tor for Chrome Incognito mode, we are safe?  Or should we stick with non-Incognito mode for now?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-10163"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10163" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 25, 2011</p>
    </div>
    <a href="#comment-10163">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10163" class="permalink" rel="bookmark">thanks for this mike.  i</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks for this mike.  i really like to see this progress towards a fingerprint resistant plugin for chrome</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11261"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11261" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 29, 2011</p>
    </div>
    <a href="#comment-11261">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11261" class="permalink" rel="bookmark">I&#039;ve created a simple script</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've created a simple script to run Chromium browser in incognito mode with tor:<br />
<a href="https://github.com/ViliusN/secure-chromium" rel="nofollow">https://github.com/ViliusN/secure-chromium</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-11941"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11941" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 30, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11261" class="permalink" rel="bookmark">I&#039;ve created a simple script</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-11941">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11941" class="permalink" rel="bookmark">Hi
That script seems too</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi</p>
<p>That script seems too simple.  You are missing a lot of proxy settings:<br />
OPT_PROXY="127.0.0.1:8118;<br />
https=127.0.0.1:8118;<br />
socks=127.0.0.1:8118;<br />
sock4=127.0.0.1:8118;<br />
sock5=127.0.0.1:8118,<br />
ftp=127.0.0.1:8118"</p>
<p>Also you are missing a lot of chrome setting which would help leaking data...I don't use this script but it it much more complete and has most of the settings I use:<br />
<a href="http://blog.zx2c4.com/440" rel="nofollow">http://blog.zx2c4.com/440</a></p>
<p>TIp: If you are using Google to search or Gmail via TOR, you can probably forget about being anonymous.<br />
tip Search: DuckDuckGo <a href="http://3g2upl4pq6kufc4m.onion/" rel="nofollow">http://3g2upl4pq6kufc4m.onion/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11942"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11942" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 30, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11261" class="permalink" rel="bookmark">I&#039;ve created a simple script</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-11942">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11942" class="permalink" rel="bookmark">Hi
That script seems too</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi</p>
<p>That script seems too simple.  You are missing a lot of proxy settings:<br />
OPT_PROXY="127.0.0.1:8118;<br />
https=127.0.0.1:8118;<br />
socks=127.0.0.1:8118;<br />
sock4=127.0.0.1:8118;<br />
sock5=127.0.0.1:8118,<br />
ftp=127.0.0.1:8118"</p>
<p>Also you are missing a lot of chrome setting which would help leaking data...I don't use this script but it it much more complete and has most of the settings I use:<br />
<a href="http://blog.zx2c4.com/440" rel="nofollow">http://blog.zx2c4.com/440</a></p>
<p>TIp: If you are using Google to search or Gmail via TOR, you can probably forget about being anonymous.<br />
tip Search: DuckDuckGo <a href="http://3g2upl4pq6kufc4m.onion/" rel="nofollow">http://3g2upl4pq6kufc4m.onion/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-14585"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14585" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 14, 2012</p>
    </div>
    <a href="#comment-14585">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14585" class="permalink" rel="bookmark">Before I always run Chrome</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Before I always run Chrome with Tor (on Mac OS), and no nececessary using change proxy, Chrome was working normally. But when Chrome update, I can't configure it to continue using with Tor. I try to use every ways tutor but cannot using with Chrome.<br />
Otherwise, in Windows platform, Chrome is working normally, of course no need change proxy.</p>
</div>
  </div>
</article>
<!-- Comment END -->
