title: Tor 0.2.9.10 is released
---
pub_date: 2017-03-01
---
author: nickm
---
tags:

tor
stable
release
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.9.10 backports a security fix for users who build Tor with the --enable-expensive-hardening option. It also includes fixes for some major issues affecting directory authorities, LibreSSL compatibility, and IPv6 correctness.<br />
The Tor 0.2.9.x release series is now marked as a long-term-support series.  We intend to backport security fixes to 0.2.9.x until at least January of 2020.<br />
You can download the source code from <a href="https://dist.torproject.org/" rel="nofollow">https://dist.torproject.org/</a> but most users should wait for next week's upcoming Tor Browser release, or for their upcoming system package updates.</p>

<h2>Changes in version 0.2.9.10 - 2017-03-01</h2>

<ul>
<li>Major bugfixes (directory authority, 0.3.0.3-alpha):
<ul>
<li>During voting, when marking a relay as a probable sybil, do not clear its BadExit flag: sybils can still be bad in other ways too. (We still clear the other flags.) Fixes bug <a href="https://bugs.torproject.org/21108" rel="nofollow">21108</a>; bugfix on 0.2.0.13-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (IPv6 Exits, backport from 0.3.0.3-alpha):
<ul>
<li>Stop rejecting all IPv6 traffic on Exits whose exit policy rejects any IPv6 addresses. Instead, only reject a port over IPv6 if the exit policy rejects that port on more than an IPv6 /16 of addresses. This bug was made worse by 17027 in 0.2.8.1-alpha, which rejected a relay's own IPv6 address by default. Fixes bug <a href="https://bugs.torproject.org/21357" rel="nofollow">21357</a>; bugfix on commit 004f3f4e53 in 0.2.4.7-alpha.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major bugfixes (parsing, also in 0.3.0.4-rc):
<ul>
<li>Fix an integer underflow bug when comparing malformed Tor versions. This bug could crash Tor when built with --enable-expensive-hardening, or on Tor 0.2.9.1-alpha through Tor 0.2.9.8, which were built with -ftrapv by default. In other cases it was harmless. Part of TROVE-2017-001. Fixes bug <a href="https://bugs.torproject.org/21278" rel="nofollow">21278</a>; bugfix on 0.0.8pre1. Found by OSS-Fuzz.
  </li>
</ul>
</li>
<li>Minor features (directory authorities, also in 0.3.0.4-rc):
<ul>
<li>Directory authorities now reject descriptors that claim to be malformed versions of Tor. Helps prevent exploitation of bug <a href="https://bugs.torproject.org/21278" rel="nofollow">21278</a>.
  </li>
<li>Reject version numbers with components that exceed INT32_MAX. Otherwise 32-bit and 64-bit platforms would behave inconsistently. Fixes bug <a href="https://bugs.torproject.org/21450" rel="nofollow">21450</a>; bugfix on 0.0.8pre1.
  </li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the February 8 2017 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor features (portability, compilation, backport from 0.3.0.3-alpha):
<ul>
<li>Autoconf now checks to determine if OpenSSL structures are opaque, instead of explicitly checking for OpenSSL version numbers. Part of ticket <a href="https://bugs.torproject.org/21359" rel="nofollow">21359</a>.
  </li>
<li>Support building with recent LibreSSL code that uses opaque structures. Closes ticket <a href="https://bugs.torproject.org/21359" rel="nofollow">21359</a>.
  </li>
</ul>
</li>
<li>Minor bugfixes (code correctness, also in 0.3.0.4-rc):
<ul>
<li>Repair a couple of (unreachable or harmless) cases of the risky comparison-by-subtraction pattern that caused bug <a href="https://bugs.torproject.org/21278" rel="nofollow">21278</a>.
  </li>
</ul>
</li>
<li>Minor bugfixes (tor-resolve, backport from 0.3.0.3-alpha):
<ul>
<li>The tor-resolve command line tool now rejects hostnames over 255 characters in length. Previously, it would silently truncate them, which could lead to bugs. Fixes bug <a href="https://bugs.torproject.org/21280" rel="nofollow">21280</a>; bugfix on 0.0.9pre5. Patch by "junglefowl".
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-238725"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-238725" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2017</p>
    </div>
    <a href="#comment-238725">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-238725" class="permalink" rel="bookmark">Could you answer to the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Could you answer to the question about ControlPort:<br />
<a href="https://blog.torproject.org/comment/reply/1297/232924" rel="nofollow">https://blog.torproject.org/comment/reply/1297/232924</a><br />
and question about exonerator:<br />
<a href="https://blog.torproject.org/comment/reply/1306/234709" rel="nofollow">https://blog.torproject.org/comment/reply/1306/234709</a><br />
?<br />
Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-238852"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-238852" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 03, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-238725" class="permalink" rel="bookmark">Could you answer to the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-238852">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-238852" class="permalink" rel="bookmark">I replied with my two cents</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I replied with my two cents on the first one. I would be interested in hearing what the Tor people have to say.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-239939"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-239939" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 04, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-238852" class="permalink" rel="bookmark">I replied with my two cents</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-239939">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-239939" class="permalink" rel="bookmark">I cannot see it yet. If</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I cannot see it yet. If anybody cares. these are the comments/questions from these threads, consequently:<br />
<a href="https://blog.torproject.org/blog/tor-0299-released" rel="nofollow">https://blog.torproject.org/blog/tor-0299-released</a><br />
<a href="https://blog.torproject.org/blog/tor-0303-alpha-released" rel="nofollow">https://blog.torproject.org/blog/tor-0303-alpha-released</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-242010"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-242010" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-239939" class="permalink" rel="bookmark">I cannot see it yet. If</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-242010">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-242010" class="permalink" rel="bookmark">I cannot see it yet.
Already</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><i>I cannot see it yet.</i></p>
<p>Already got it, thanks, and replied ( on the thread <a href="https://blog.torproject.org/blog/tor-0299-released" rel="nofollow">https://blog.torproject.org/blog/tor-0299-released</a> ). That would be nice if arma lets us know his opinion about ControlPort discussion.</p>
<p>Then, concerning the second question, I think karsten gave good reply <a href="https://blog.torproject.org/comment/reply/1311/241421" rel="nofollow">https://blog.torproject.org/comment/reply/1311/241421</a> in the thread about Atlas <a href="https://blog.torproject.org/blog/atlas-recent-improvements#comments" rel="nofollow">https://blog.torproject.org/blog/atlas-recent-improvements#comments</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-239940"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-239940" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 05, 2017</p>
    </div>
    <a href="#comment-239940">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-239940" class="permalink" rel="bookmark">I wonder why my Debian</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I wonder why my Debian jessie doesn't show an update, it's still version 0.2.9.9. I use official onion tor repository for apt-get. Am I alone who has this problem?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-240387"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-240387" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 05, 2017</p>
    </div>
    <a href="#comment-240387">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-240387" class="permalink" rel="bookmark">most users should wait for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><i>most users should wait for next week's upcoming Tor Browser release, or for their upcoming system package updates.</i><br />
They have just arrived to apt-get of Debian, Tor repo! Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-240401"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-240401" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 05, 2017</p>
    </div>
    <a href="#comment-240401">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-240401" class="permalink" rel="bookmark">I see that around middle of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I see that around middle of September 2016 about 1 thousand bridges suddenly disappeared, then reappeared, and then disappeared again and started <b>to grow monotonically and linearly</b> (it looks even more artificial if you observe it from the perspective of the graph for last years):<br />
<a href="https://metrics.torproject.org/networksize.html?start=2016-08-06&amp;end=2017-03-06" rel="nofollow">https://metrics.torproject.org/networksize.html?start=2016-08-06&amp;end=20…</a><br />
What is this? Do they all belong to the same entity?  It is also strange that during 2015 the amount of bridges was only decreasing.</p>
<p>Moreover, I see that during 2013 amount of Tor relays was continuously growing, in total the amount was increased 1.5 times. The same is true for 2014. However, during last 2 years the amount of Tor relays is almost the same. How it can happen in natural circumstances? In addition, during the half of 2015 the amount of relays was only decreasing:<br />
<a href="https://metrics.torproject.org/networksize.html?start=2011-08-06&amp;end=2017-03-06" rel="nofollow">https://metrics.torproject.org/networksize.html?start=2011-08-06&amp;end=20…</a></p>
<p>I think people knowing statistics theory would say it is quite improbable behavior. Natural dependence should be close to logarithm, but what we see is very far from that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-241241"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-241241" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 07, 2017</p>
    </div>
    <a href="#comment-241241">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-241241" class="permalink" rel="bookmark">Nice work, only 6 days until</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nice work, only 6 days until the win32 build. Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-243536"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-243536" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 12, 2017</p>
    </div>
    <a href="#comment-243536">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-243536" class="permalink" rel="bookmark">i always wondered if the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i always wondered if the dark web was real is this it</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-260835"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-260835" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 17, 2017</p>
    </div>
    <a href="#comment-260835">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-260835" class="permalink" rel="bookmark">Thanks</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><strong>Thanks</strong></p>
</div>
  </div>
</article>
<!-- Comment END -->
