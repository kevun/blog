title: Advisory: remote DoS when using Tor with recent OpenSSL versions built with the "no-ssl3" option
---
pub_date: 2014-10-21
---
author: phoul
---
tags:

tor
ssl
tls
little-t-tor
sslv3
poodle
---
categories: network
---
_html_body:

<p><cite>This is a copy of the message Nick Mathewson sent to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-talk" rel="nofollow">tor-talk</a> &amp; <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays" rel="nofollow">tor-relays</a> mailing lists.</cite></p>

<p><strong>Hello, relay operators!</strong></p>

<p>There's one important bugfix in the 0.2.5.9-rc release that relay operators should know about. If you have a version of OpenSSL that came out last week (like 1.0.1j, 1.0.0, ) and if your version of OpenSSL is built with the "no-ssl3" flag, then it's possible to crash your Tor relay remotely if you don't upgrade to 0.2.5.9-rc or to 0.2.4.25 (when that's out).</p>

<p>This appears to be an OpenSSL bug.  The Tor releases in question contain a workaround for it.</p>

<p>To tell if your version of OpenSSL was built with 'no-ssl3': run:</p>

<blockquote><p>
<div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">openssl s_client <span style="color: #339933;">-</span>ssl3 <span style="color: #339933;">-</span>connect <span style="color: #339933;">&lt;</span>a href<span style="color: #339933;">=</span><span style="color: #0000ff;">&quot;http://www.torproject.org:443[/geshifilter-code&quot;</span> rel<span style="color: #339933;">=</span><span style="color: #0000ff;">&quot;nofollow&quot;</span><span style="color: #339933;">&gt;</span>www<span style="color: #339933;">.</span>torproject<span style="color: #339933;">.</span>org<span style="color: #339933;">:</span><span style="color: #cc66cc;">443</span><span style="color: #009900;">&#91;</span><span style="color: #339933;">/</span>geshifilter<span style="color: #339933;">-</span>code<span style="color: #339933;">&lt;/</span>a<span style="color: #339933;">&gt;</span><span style="color: #009900;">&#93;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;/</span>p<span style="color: #339933;">&gt;&lt;/</span>blockquote<span style="color: #339933;">&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>p<span style="color: #339933;">&gt;</span><span style="color: #b1b100;">If</span> it gives you output beginning with something like<span style="color: #339933;">:&lt;/</span>p<span style="color: #339933;">&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #339933;">&lt;</span>blockquote<span style="color: #339933;">&gt;&lt;</span>p<span style="color: #339933;">&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #009900;">&#91;</span>geshifilter<span style="color: #339933;">-</span>code<span style="color: #009900;">&#93;</span>CONNECTED<span style="color: #009900;">&#40;</span><span style="color: #208080;">00000003</span><span style="color: #009900;">&#41;</span><span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #cc66cc;">140632971298688</span><span style="color: #339933;">:</span>error<span style="color: #339933;">:</span><span style="color: #cc66cc;">14094410</span><span style="color: #339933;">:</span>SSL routines<span style="color: #339933;">:</span>SSL3_READ_BYTES<span style="color: #339933;">:</span>sslv3<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">alert handshake failure<span style="color: #339933;">:</span>s3_pkt<span style="color: #339933;">.</span>c<span style="color: #339933;">:</span><span style="color: #cc66cc;">1257</span><span style="color: #339933;">:</span>SSL alert number <span style="color: #cc66cc;">40</span><span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #cc66cc;">140632971298688</span><span style="color: #339933;">:</span>error<span style="color: #339933;">:</span>1409E0E5<span style="color: #339933;">:</span>SSL routines<span style="color: #339933;">:</span>SSL3_WRITE_BYTES<span style="color: #339933;">:</span>ssl<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">handshake failure<span style="color: #339933;">:</span>s3_pkt<span style="color: #339933;">.</span>c<span style="color: #339933;">:</span><span style="color: #cc66cc;">596</span><span style="color: #339933;">:&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li></ol></pre></div>
</p></blockquote>

<p>then you're fine and you don't need to upgrade Tor on your relay.  But if it says something that starts with:</p>

<blockquote><p>
<div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">unknown option <span style="color: #339933;">-</span>ssl3<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">usage<span style="color: #339933;">:</span> s_client args<span style="color: #339933;">&lt;</span>br <span style="color: #339933;">/&gt;</span></div></li></ol></pre></div>
</p></blockquote>

<p>then you need to upgrade Tor.</p>

<p><strong>Some questions and answers:</strong></p>

<p>Q: Does this affect clients?<br />
A: No.  Only relays.</p>

<p>Q: Does this affect me if I'm running a version of OpenSSL other than 1.0.1j, 1.0.0o, or 0.9.8zc?<br />
A: No. Only those versions.</p>

<p>Q: Does this affect me if I'm running a version of OpenSSL configured without the "no-ssl3" option?<br />
A: No. Only versions that were built with the "no-ssl3" option are affected.</p>

<p>Q: Does the OpenSSL team know?<br />
A: Yes. Have a look at this <a href="http://marc.info/?l=openssl-dev&amp;m=141357408522028&amp;w=2" rel="nofollow">thread</a>. Also, before I saw that thread, I informed them the other day.</p>

<p>Q: Does this affect Tor packages?<br />
A: I don't think that we shipped any packages where we used the "no-ssl3" flag to diable ssl3.  So only if you're using OpenSSL from another source (say, your operating system) will you be affected.</p>

<p>Q: What can I do to remediate this problem?<br />
A: You can upgrade to the most recent Tor, or you can use a version of OpenSSL built without the "no-ssl3" flag.  Downgrading your OpenSSL is not recommended.</p>

<p>Q: What is the potential impact of this bug?<br />
A: If a relay is affected by this bug, anybody can make the relay crash remotely. It does not enable any data leaks or remote code execution. Still, the ability to selectively disable relays might enable a sophisticated attacker to do some kinds of traffic analysis more efficiently.  So, fix your relay if it's affected.</p>

<p>Q: Should we run in circles and freak out?<br />
A: Not this time. We should just make sure we fix affected relays.</p>

<p>Q: Hey, Nick, you didn't explain this properly!<br />
A: Please send a follow-up message that explains it better. :)</p>

