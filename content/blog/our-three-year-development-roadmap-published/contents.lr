title: Our three-year development roadmap is published
---
pub_date: 2008-12-19
---
author: phobos
---
tags:

funding
censorship circumvention
press release
development
roadmap
---
categories:

circumvention
devops
fundraising
---
_html_body:

<p>We've published our <a href="https://svn.torproject.org/svn/tor/trunk/doc/roadmaps/2008-12-19-roadmap-full.pdf" rel="nofollow">three-year development roadmap</a>.  There are two main goals in publishing this document: first to be more transparent in what we're doing; and second to ask for help in improving everything related to Tor.  </p>

<p>While we don't expect everyone will read through the roadmap start to finish, the Table of Contents provides a quick overview of the high-level goals.  Each high-level goal is fairly independent of another, so you can simply read about the goals that interest you.</p>

<p>And for the first time ever, we've created a <a href="https://www.torproject.org/press/2008-12-19-roadmap-press-release.html.en" rel="nofollow">press release</a>.  This is our attempt to become more press friendly and provide a way for the media to start a conversation with us.  </p>

<p>We welcome your comments and feedback!</p>

---
_comments:

<a id="comment-445"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-445" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://moblog.wiredwings.com/">Moritz Bartl (not verified)</a> said:</p>
      <p class="date-time">December 19, 2008</p>
    </div>
    <a href="#comment-445">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-445" class="permalink" rel="bookmark">Nice work!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very good and compact information that was previously scattered in various places. Thank you! I definitely recommend this to anyone interested in onion networking.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-448"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-448" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2008</p>
    </div>
    <a href="#comment-448">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-448" class="permalink" rel="bookmark">i heard that you use</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i heard that you use unencrypted request of the Tor Node Directory, might some evil ISP shut me down because I used Tor and they know it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-449"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-449" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 22, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-448" class="permalink" rel="bookmark">i heard that you use</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-449">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-449" class="permalink" rel="bookmark">re: i heard that you use</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor (as of 0.2.0.x, which is the current stable version) uses encryption when<br />
doing directory lookups. In Tor 0.1.2.x and earlier, the lookups were in fact<br />
unencrypted (though they were still signed, so you couldn't be tricked into<br />
believing something you shouldn't believe).</p>
<p>That said, even an encrypted directory lookup will probably connect to an IP<br />
address that your ISP can watch for: either the six Tor directory authorities,<br />
or to some relay that's listed in the network status list.</p>
<p>If you want to stop that possibility, then you'll want to use a "bridge relay" to<br />
connect to the Tor network:<br />
<a href="https://www.torproject.org/bridges" rel="nofollow">https://www.torproject.org/bridges</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-450"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-450" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2008</p>
    </div>
    <a href="#comment-450">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-450" class="permalink" rel="bookmark">&quot;pop-up&quot; knew where I lived</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just got Tor to play with and while I was randomly surfing usung the Firefox browser I got a "pop-up" ad. from thoses spammers "Adult Friend Finder" offering me contacts in my town. Surely if this site knew where I lived then Tor must be compromised. Someone please explain.</p>
<p>Thanks, Mike</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-455"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-455" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">December 24, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-450" class="permalink" rel="bookmark">&quot;pop-up&quot; knew where I lived</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-455">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-455" class="permalink" rel="bookmark">test your browser config</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>visit <a href="http://decloak.net" rel="nofollow">http://decloak.net</a> and see what if finds.  Something isn't setup correctly.  Or, your exit node was in your town.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-463"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-463" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>WeAreAnonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-450" class="permalink" rel="bookmark">&quot;pop-up&quot; knew where I lived</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-463">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-463" class="permalink" rel="bookmark">Mike yea, javascript and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Mike yea, javascript and flash and pretty much any client side executable content is a big nono unless you are absolutely sure you can trust it, its code running on your machine and can completely bypass tor, well outside of some certain paranoid configurations</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-451"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-451" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2008</p>
    </div>
    <a href="#comment-451">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-451" class="permalink" rel="bookmark">Mike: are you using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Mike: are you using TorButton?  If not, there are many ways you could be identified based on browser cookies, persisted Flash applet data, etc.</p>
<p>For example, you visit site.com without Tor and unique identifier is written to your hard drive with Flash.  Then you run Tor and visit the same site from an exit node.  You are still sending the same unique identifier due to insecure use of Flash.  There are many more examples like this; you must use Tor properly to protect yourself!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-452"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-452" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 23, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-451" class="permalink" rel="bookmark">Mike: are you using</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-452">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-452" class="permalink" rel="bookmark">YES I did you Tor button and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>YES I did you Tor button and it was enabled and I disabled all plug-ins in Firefox. Maybe an old cookie not properly deleted?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-456"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-456" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 25, 2008</p>
    </div>
    <a href="#comment-456">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-456" class="permalink" rel="bookmark">Country to Ip</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>geoip.vidalia-project.net.  dosnt seem to be connecting, does it affect the way TOR operates and can it be used to control which nodes you can connect to?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-459"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-459" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">December 27, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-456" class="permalink" rel="bookmark">Country to Ip</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-459">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-459" class="permalink" rel="bookmark">vidalia&#039;s server is down</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>no, it doesn't affect which nodes you can connect to.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-483"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-483" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 01, 2009</p>
    </div>
    <a href="#comment-483">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-483" class="permalink" rel="bookmark">You should supply a tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You should supply a tor distribution for embedded appliances if you want to increase the number of nodes. I don´t want to run a server constantly on my desktop for security, practical and power reasons.</p>
<p>For an example of this look at how m0n0wall supplies finished ready-to-run compact flash images for the PC Engines embedded PC.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-486"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-486" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 02, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-483" class="permalink" rel="bookmark">You should supply a tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-486">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-486" class="permalink" rel="bookmark">great idea</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>we'd love to do this, could you start the process?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
