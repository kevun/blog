title: New Release: Tor Browser 12.0.1
---
author: ma1
---
pub_date: 2022-12-15
---
categories:
applications
releases
---
summary: Tor Browser 12.0.1 is now available from the Tor Browser download page and also from our distribution directory.
---
body:

Tor Browser 12.0.1 is now available from the Tor Browser [download page](https://www.torproject.org/download/) and also 
from our [distribution directory](https://dist.torproject.org/torbrowser/12.0.1/). 

This release updates Firefox to 102.6, including bug fixes, stability improvements 
and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-52/).

We've fixed a regression in our drag and drop protection, whose hardening disrupted some interface interactions, and most notably rearranging bookmarks by dragging them around. 

We'd like to thank poncho for fixing another regression causing the `TOR_SOCKS_IPC_PATH` environment variable to be ignored.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 12.0](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/blob/tbb-12.0.1-build1/projects/browser/Bundle-Data/Docs/ChangeLog.txt) is:

- Windows + macOS + Linux
  - Updated Firefox to 102.6esr
  - [Bug tor-browser#41519](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41519): TOR_SOCKS_IPC_PATH environment variable not honored
  - [Bug tor-browser#41520](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41520): Regression: rearranging  bookmarks / place items by drag & drop doesn't work anymore
- Android
  - Updated GeckoView to 102.6esr
- Build System
  - All Platforms
    - Updated Go to 1.19.4
    - [Bug tor-browser-build#40653](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40653): Build compiler-rt with runtimes instead of the main LLVM build
    - [Bug tor-browser-build#40681](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40681): Run apt-get clean, after installing packages in projects/container-image/config
    - [Bug tor-browser-build#40683](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40683): Install more packages in the default containers to reduce the number of custom containers
    - [Bug tor-browser-build#40693](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40693): Can't build container-image in main
    - [Bug tor-browser-build#40705](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40705): Add empty commit mentioning that gitolite repo is no longer updated
  - Windows
    - [Bug tor-browser-build#40708](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40708): tor.exe in tor-expert-bundle not writing stdout even when run from cmd.exe
  - Linux
    - [Bug tor-browser-build#40689](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40689): Update Ubuntu version from projects/mmdebstrap-image/config to 22.04.1
